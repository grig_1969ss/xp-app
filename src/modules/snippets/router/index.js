import SnippetsList from '@/modules/snippets/pages/SnippetListPage'
import SnippetsForm from '@/modules/snippets/pages/SnippetFormPage'

export default [
  {
    name: 'Snippets',
    path: '/content/snippet/index',
    component: SnippetsList,
    meta: {
      auth: ['viewProjectItem'],
      title: 'Snippets List'
    }
  },
  {
    name: 'SnippetsCreate',
    path: '/content/snippet/create',
    component: SnippetsForm,
    meta: {
      auth: ['createProjectItem'],
      title: 'Snippets Create'
    }
  },
  {
    name: 'SnippetsUpdate',
    path: '/content/snippet/update',
    component: SnippetsForm,
    meta: {
      auth: ['updateProjectItem'],
      title: 'Snippets Update'
    }
  },
  {
    name: 'SnippetsView',
    path: '/content/snippet/view',
    component: SnippetsForm,
    props: { readonly: true },
    meta: {
      auth: ['viewProjectItem'],
      title: 'Snippets View'
    }
  },
  {
    name: 'SnippetsCopy',
    path: '/content/snippet/copy',
    component: SnippetsForm,
    props: { copy: true },
    meta: {
      auth: ['viewProjectItem'],
      title: 'Snippets Copy'
    }
  }
]
