import NotificationListPage from '@/modules/notifications-log/pages/NotificationListPage'
import NotificationDetailsPage from '@/modules/notifications-log/pages/NotificationDetailsPage'

export default [
  {
    name: 'NotificationList',
    path: '/push/action/index',
    component: NotificationListPage,
    meta: {
      auth: ['viewProjectItem'],
      title: 'NotificationLog_ListTitle'
    }
  },
  {
    name: 'NotificationDetails',
    path: '/push/action/view',
    component: NotificationDetailsPage,
    meta: {
      auth: ['Message::Read'],
      title: 'NotificationLog_DetailsTitle'
    }
  }
]
