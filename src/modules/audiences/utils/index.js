const validateIdentifier = (identifier, features) => {
  switch (identifier) {
    case 'user_id':
    case 'profile_id':
      return features.profileEnabled === true

    case 'external_id':
      return !features.profileEnabled || !features.profileIdfybyExtid

    default:
      return true
  }
}

export { validateIdentifier }
