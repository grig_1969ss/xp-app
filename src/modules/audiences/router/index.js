import Audiences from '@/modules/audiences/pages/Audiences'
import UserLists from '@/modules/audiences/pages/UserLists'
import UserListItems from '@/modules/audiences/pages/UserListItems'
import SuppressionLists from '@/modules/audiences/pages/SuppressionLists'
import SuppressionListItems from '@/modules/audiences/pages/SuppressionListItems'
import SegmentList from '@/modules/audiences/pages/SegmentList'
import SegmentPage from '@/modules/audiences/pages/SegmentPage'
import SegmentPageUpdate from '@/modules/audiences/pages/SegmentPageUpdate'
import SegmentPageCreate from '@/modules/audiences/pages/SegmentPageCreate'
import SegmentTabEvaluations from '@/modules/audiences/pages/SegmentTabEvaluations'
import SegmentTabOverview from '@/modules/audiences/pages/SegmentTabOverview'

export default [
  {
    name: 'Audiences',
    path: '/audiences',
    component: Audiences,
    meta: {
      auth: ['viewProjectItem'],
      title: 'Audiences_Title'
    },
    children: [
      {
        name: 'AudienceSegmentList',
        path: 'segments',
        component: SegmentList,
        meta: {
          auth: ['viewProjectItem']
        }
      },
      {
        name: 'AudienceUserList',
        path: 'user-lists',
        component: UserLists,
        project: true,
        meta: {
          auth: ['viewProjectItem']
        }
      },
      {
        name: 'AudienceSuppressionList',
        path: 'suppression-lists',
        component: SuppressionLists,
        project: true,
        meta: {
          auth: ['viewProjectItem']
        }
      }
    ]
  },
  {
    name: 'AudienceUserListItems',
    path: '/audiences/user-list/items',
    component: UserListItems,
    project: true,
    meta: {
      auth: ['viewProjectItem']
    }
  },
  {
    name: 'AudienceSuppressionListItems',
    path: '/audiences/suppression-list/items',
    component: SuppressionListItems,
    project: true,
    meta: {
      auth: ['viewProjectItem']
    }
  },
  {
    name: 'AudienceSegment',
    path: '/audiences/segment/overview',
    component: SegmentPage,
    meta: {
      auth: ['viewProjectItem'],
      title: 'Audiences_Title'
    },
    children: [
      {
        name: 'AudienceSegmentOverview',
        path: '/audiences/segment/overview',
        component: SegmentTabOverview,
        meta: {
          auth: ['viewProjectItem']
        }
      },
      {
        name: 'AudienceSegmentEvaluations',
        path: '/audiences/segment/evaluations',
        component: SegmentTabEvaluations,
        project: true,
        meta: {
          auth: ['viewProjectItem']
        }
      }
    ]
  },
  {
    name: 'AudienceSegmentCreate',
    path: '/audiences/segment/create',
    component: SegmentPageCreate,
    meta: {
      auth: ['createProjectItem'],
      title: 'Audiences_Title'
    }
  },
  {
    name: 'AudienceSegmentUpdate',
    path: '/audiences/segment/update',
    component: SegmentPageUpdate,
    meta: {
      auth: ['updateProjectItem'],
      title: 'Audiences_Title'
    }
  }
]
