import HistoryListPage from '@/modules/tasks/pages/TaskListPage'
import HistoryGroupPage from '@/modules/tasks/pages/TaskGroupPage'
import HistoryViewPage from '@/modules/tasks/pages/TaskViewPage'

export default [
  {
    name: 'TaskHistory',
    path: '/task/history/index',
    component: HistoryListPage,
    meta: {
      auth: ['viewProjectItem'],
      title: 'TaskHistoryList_Title'
    }
  },
  {
    name: 'TaskHistoryGroup',
    path: '/task/history/group',
    component: HistoryGroupPage,
    meta: {
      auth: ['viewProjectItem'],
      title: 'TaskHistoryGroup_Title'
    }
  },
  {
    name: 'TaskHistoryView',
    path: '/task/history/view',
    component: HistoryViewPage,
    meta: {
      auth: ['viewProjectItem'],
      title: 'TaskHistoryView_Title'
    }
  }
]
