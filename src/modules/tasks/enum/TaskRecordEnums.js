const taskIdentifierLabel = Object.freeze({
  segment_export_devices: 'TaskHistoryList_ExportSegmentDevices',
  segment_export_profiles: 'TaskHistoryList_ExportSegmentProfiles',
  segment_count_recipients: 'TaskHistoryList_SegmentRecipientsCount',
  segment_count_channels: 'TaskHistoryList_SegmentChannelsCount',
  segment_preview_recipients: 'TaskHistoryList_SegmentRecipientsPreview',
  campaign_count_recipients: 'TaskHistoryList_CampaignRecipientsCount',
  campaign_preview_recipients: 'TaskHistoryList_CampaignPreviewRecipients',
  message_export_campaign: 'TaskHistoryList_ExportCampaignMessages',
  message_export_campaign_failed:
    'TaskHistoryList_ExportCampaignMessagesFailed',
  message_export_send: 'TaskHistoryList_ExportSendMessages',
  list_import: 'TaskHistoryList_UserListImport',
  list_export: 'TaskHistoryList_UserListExport',
  profile_attribute_create_index: 'TaskHistoryList_ProfileAttributeCreateIndex',
  profile_attribute_remove: 'TaskHistoryList_ProfileAttributeRemove',
  profile_import: 'TaskHistoryList_ProfileImport',
  profile_export_data: 'TaskHistoryList_ExportProfileData',
  profile_delete_personal_data: 'TaskHistoryList_DeletePersonalData',
  device_delete_personal_data: 'TaskHistoryList_DeleteDevicePersonalData',
  active_directory_profiles_sync: 'TaskHistoryList_ActiveDirectoryProfilesSync',
  active_directory_lists_sync: 'TaskHistoryList_ActiveDirectoryListsSync',
  kochava_sync_installs: 'TaskHistoryList_KochavaSyncInstalls',
  sbtech_promotions_sync: 'TaskHistoryList_SbtechPromotionsSync',
  cleanup_users: 'TaskHistoryList_CleanupUsers',
  fs_import_automation: 'TaskHistoryList_ImportData'
})
const evaluateTaskIdentifierLabel = Object.freeze({
  segment_export_devices: 'TaskHistoryList_ExportSegmentDevices',
  segment_export_profiles: 'TaskHistoryList_ExportSegmentProfiles',
  segment_count_recipients: 'TaskHistoryList_SegmentRecipientsCount',
  segment_count_channels: 'TaskHistoryList_SegmentChannelsCount',
  segment_preview_recipients: 'TaskHistoryList_SegmentRecipientsPreview'
})

const taskStatusEnum = Object.freeze({
  NEW: 0,
  EXECUTING: 1,
  DONE: 2,
  ERROR: 3,
  FATAL: 4
})

const taskStatusLabel = Object.freeze({
  0: 'TaskHistoryList_Status_New',
  1: 'TaskHistoryList_Status_Executing',
  2: 'TaskHistoryList_Status_Complete',
  3: 'TaskHistoryList_Status_Error',
  4: 'TaskHistoryList_Status_Fatal'
})

export {
  taskStatusEnum,
  taskIdentifierLabel,
  taskStatusLabel,
  evaluateTaskIdentifierLabel
}
