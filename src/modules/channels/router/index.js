import ChannelFormPage from '@/modules/channels/pages/ChannelFormPage'
import ChannelListPage from '@/modules/channels/pages/ChannelListPage'

export default [
  {
    name: 'ChannelList',
    path: '/channel/config/index',
    component: ChannelListPage,
    meta: {
      auth: ['admin', 'admin-support', 'admin-viewer'],
      title: 'ChannelConfig_List_Title'
    }
  },
  {
    name: 'ChannelCreate',
    path: '/channel/config/create',
    component: ChannelFormPage,
    meta: {
      auth: ['admin'],
      title: 'ChannelConfig_AddTitle'
    }
  },
  {
    name: 'ChannelUpdate',
    path: '/channel/config/update',
    component: ChannelFormPage,
    meta: {
      auth: ['admin'],
      title: 'ChannelConfig_UpdateTitle'
    }
  }
]
