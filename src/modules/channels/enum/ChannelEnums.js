const channelOptionLabel = Object.freeze({
  dummy_sms: 'ChannelConfig_Identifier_DummySms',
  twilio: 'ChannelConfig_Identifier_Twilio',
  phonovation: 'ChannelConfig_Identifier_Phonovation',
  mobivate: 'ChannelConfig_Identifier_Mobivate',
  ssc: 'ChannelConfig_Identifier_Ssc',
  messagebird: 'ChannelConfig_Identifier_Messagebird',
  messagebird_whatsapp: 'ChannelConfig_Identifier_MessagebirdWhatsapp',
  facebook: 'ChannelConfig_Identifier_Facebook',
  mitto: 'ChannelConfig_Identifier_Mitto',
  vonage: 'ChannelConfig_Identifier_Vonage'
})

const channelOptionComponentName = Object.freeze({
  dummy_sms: 'DummySms',
  twilio: 'Twilio',
  phonovation: 'Phonovation',
  mobivate: 'Mobivate',
  ssc: 'Ssc',
  messagebird: 'MessageBird',
  messagebird_whatsapp: 'MessageBirdWhatsapp',
  facebook: 'Facebook',
  mitto: 'Mitto',
  vonage: 'Vonage'
})

export { channelOptionLabel, channelOptionComponentName }
