import ApplicationListPage from '@/modules/account/pages/ApplicationListPage'
import ApplicationFormPage from '@/modules/account/pages/ApplicationFormPage'
import ApplicationSDKIntegrationPage from '@/modules/account/pages/ApplicationSDKIntegrationPage'

export default [
  {
    name: 'Applications',
    path: '/account/application/index',
    component: ApplicationListPage,
    meta: {
      title: 'ApplicationList_Title'
    }
  },
  {
    name: 'ApplicationCreate',
    path: '/account/application/create',
    component: ApplicationFormPage,
    meta: {
      title: 'ApplicationForm_CreateTitle',
      auth: ['createProjectItem']
    }
  },
  {
    name: 'ApplicationUpdate',
    path: '/account/application/update',
    component: ApplicationFormPage,
    meta: {
      title: 'ApplicationForm_UpdateTitle',
      auth: ['updateProjectItem']
    }
  },
  {
    name: 'ApplicationView',
    path: '/account/application/view',
    component: ApplicationFormPage,
    props: { readonly: true },
    meta: {
      title: 'ApplicationForm_ViewTitle'
    }
  },
  {
    name: 'ApplicationSDKIntegration',
    path: '/account/application/sdk-integration',
    component: ApplicationSDKIntegrationPage,
    meta: {
      title: 'ApplicationSDKIntegration_Title'
    }
  }
]
