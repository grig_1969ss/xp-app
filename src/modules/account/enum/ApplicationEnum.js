const ApplicationTypes = Object.freeze({
  ios: 'ios',
  android: 'android',
  web: 'web'
})

export { ApplicationTypes }
