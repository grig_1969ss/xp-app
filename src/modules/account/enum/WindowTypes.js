const WindowTypes = Object.freeze({
  0: 'window_mode_none',
  1: 'window_mode_all',
  2: 'window_mode_http'
})

export { WindowTypes }
