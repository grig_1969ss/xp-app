import IntegrationsPage from '@/modules/integrations/pages/IntegrationsPage'
import IntegrationsList from '@/modules/integrations/pages/IntegrationsList'
import IntegrationsMarketplace from '@/modules/integrations/pages/IntegrationsMarketplace'
import IntegrationCreatePage from '@/modules/integrations/pages/IntegrationCreatePage'
import IntegrationPage from '@/modules/integrations/pages/IntegrationPage'
import IntegrationOverview from '@/modules/integrations/pages/IntegrationOverview'
import IntegrationSettings from '@/modules/integrations/pages/IntegrationSettings'

export default [
  {
    name: 'Integrations',
    path: '/integrations',
    component: IntegrationsPage,
    meta: { auth: ['viewProjectTechnical'] },
    children: [
      {
        name: 'IntegrationsList',
        path: 'index',
        component: IntegrationsList,
        meta: { auth: ['viewProjectTechnical'] }
      },
      {
        name: 'IntegrationsMarketplace',
        path: 'marketplace',
        component: IntegrationsMarketplace,
        meta: { auth: ['viewProjectTechnical'] }
      }
    ]
  },

  {
    name: 'IntegrationCreate',
    path: '/integration/create',
    component: IntegrationCreatePage,
    meta: { auth: ['manageProjectTechnical'] }
  },

  {
    name: 'Integration',
    path: '/integration',
    component: IntegrationPage,
    meta: { auth: ['viewProjectTechnical'] },
    children: [
      {
        name: 'IntegrationOverview',
        path: 'overview',
        component: IntegrationOverview,
        meta: { auth: ['viewProjectTechnical'] }
      },
      {
        name: 'IntegrationSettings',
        path: 'settings',
        component: IntegrationSettings,
        meta: { auth: ['viewProjectTechnical'] }
      }
    ]
  }
]
