const PlatformTypes = Object.freeze({
  ios: 'ios',
  android: 'android'
})

export { PlatformTypes }
