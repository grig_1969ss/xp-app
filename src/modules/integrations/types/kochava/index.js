import KochavaOverview from '@/modules/integrations/types/kochava/KochavaOverview'
import KochavaSettings from '@/modules/integrations/types/kochava/KochavaSettings'
import KochavaSyncDialog from '@/modules/integrations/types/kochava/KochavaSyncDialog'

export default Object.freeze({
  type: 'kochava',
  title: 'Integrations_Kochava',
  category: 'Integrations_Kochava_Category',
  description: 'Integrations_Kochava_Description',
  image: require('@/img/integrations/kochava.png'),
  requiresAdmin: true,
  components: {
    overview: KochavaOverview,
    settings: KochavaSettings
  },
  actions: [
    {
      icon: 'el-icon-refresh',
      label: 'Integrations_Kochava_Action_SyncInstalls',
      dialog: KochavaSyncDialog
    }
  ],
  urls: {
    guide: 'https://docs.xtremepush.com/docs/kochava'
  }
})
