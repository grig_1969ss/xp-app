import segment from './segment'
import crowdtwist from './crowdtwist'
import optimove from './optimove'
import activedirectory from './activedirectory'
import kochava from './kochava'
import sftp from './sftp'
import sbtech from './sbtech'

export default {
  segment,
  crowdtwist,
  optimove,
  activedirectory,
  kochava,
  sftp,
  sbtech
}
