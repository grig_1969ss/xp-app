import SftpOverview from '@/modules/integrations/types/sftp/SftpOverview'
import SftpSettings from '@/modules/integrations/types/sftp/SftpSettings'

export default Object.freeze({
  type: 'sftp',
  title: 'Integrations_Sftp',
  category: 'Integrations_Sftp_Category',
  description: 'Integrations_Sftp_Description',
  image: require('@/img/integrations/sftp.png'),
  requiresAdmin: false,
  components: {
    overview: SftpOverview,
    settings: SftpSettings
  },
  urls: {
    guide: 'https://docs.xtremepush.com/docs/third-party-integrations'
  }
})
