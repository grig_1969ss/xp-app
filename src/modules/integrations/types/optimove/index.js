import OptimoveOverview from './OptimoveOverview'
import OptimoveSettings from './OptimoveSettings'

export default Object.freeze({
  type: 'optimove',
  title: 'Integrations_Optimove',
  category: 'Integrations_Optimove_Category',
  description: 'Integrations_Optimove_Description',
  image: require('@/img/integrations/optimove.png'),
  requiresAdmin: false,
  components: {
    overview: OptimoveOverview,
    settings: OptimoveSettings
  },
  urls: {
    guide: 'https://docs.xtremepush.com/docs/optimove'
  }
})
