const OptimoveChannelLabel = Object.freeze({
  402: 'Integrations_Optimove_Channel_402',
  461: 'Integrations_Optimove_Channel_461',
  207: 'Integrations_Optimove_Channel_207'
})

export { OptimoveChannelLabel }
