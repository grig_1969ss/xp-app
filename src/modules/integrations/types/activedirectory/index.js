import ActiveDirectoryOverview from './ActiveDirectoryOverview'
import ActiveDirectorySettings from './ActiveDirectorySettings'
import ActiveDirectorySyncDialog from '@/modules/integrations/types/activedirectory/ActiveDirectorySyncDialog'
import ActiveDirectorySyncListsDialog from '@/modules/integrations/types/activedirectory/ActiveDirectorySyncListsDialog'
import ActiveDirectorySyncUsersDialog from '@/modules/integrations/types/activedirectory/ActiveDirectorySyncUsersDialog'

export default Object.freeze({
  type: 'activedirectory',
  title: 'Integrations_ActiveDirectory',
  category: 'Integrations_ActiveDirectory_Category',
  description: 'Integrations_ActiveDirectory_Description',
  image: require('@/img/integrations/activedirectory.png'),
  requiresAdmin: false,
  components: {
    overview: ActiveDirectoryOverview,
    settings: ActiveDirectorySettings
  },
  actions: [
    {
      icon: 'el-icon-refresh',
      label: 'Integrations_ActiveDirectory_Action_Sync',
      dialog: ActiveDirectorySyncDialog
    },
    {
      icon: 'el-icon-refresh',
      label: 'Integrations_ActiveDirectory_Action_SyncUsers',
      dialog: ActiveDirectorySyncUsersDialog
    },
    {
      icon: 'el-icon-refresh',
      label: 'Integrations_ActiveDirectory_Action_SyncLists',
      dialog: ActiveDirectorySyncListsDialog
    }
  ],
  urls: {
    guide: 'https://docs.xtremepush.com/docs/azure-active-directory'
  }
})
