import CrowdtwistOverview from './CrowdtwistOverview'
import CrowdtwistSettings from './CrowdtwistSettings'

export default Object.freeze({
  type: 'crowdtwist',
  title: 'Integrations_Crowdtwist',
  category: 'Integrations_Crowdtwist_Category',
  description: 'Integrations_Crowdtwist_Description',
  image: require('@/img/integrations/crowdtwist.png'),
  requiresAdmin: false,
  components: {
    overview: CrowdtwistOverview,
    settings: CrowdtwistSettings
  },
  urls: {
    guide: 'https://docs.xtremepush.com/docs/crowdtwist'
  }
})
