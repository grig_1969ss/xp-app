import SegmentOverview from './SegmentOverview'
import SegmentSettings from './SegmentSettings'

export default Object.freeze({
  type: 'segment',
  title: 'Integrations_Segment',
  category: 'Integrations_Segment_Category',
  description: 'Integrations_Segment_Description',
  image: require('@/img/integrations/segment.png'),
  requiresAdmin: false,
  components: {
    overview: SegmentOverview,
    settings: SegmentSettings
  },
  urls: {
    guide: 'https://docs.xtremepush.com/docs/segment'
  }
})
