import SbTechOverview from '@/modules/integrations/types/sbtech/SbTechOverview'
import SbTechSettings from '@/modules/integrations/types/sbtech/SbTechSettings'
import SbTechSyncDialog from '@/modules/integrations/types/sbtech/SbTechSyncDialog'

export default Object.freeze({
  type: 'sbtech',
  title: 'Integrations_SbTech',
  category: 'Integrations_SbTech_Category',
  description: 'Integrations_SbTech_Description',
  image: require('@/img/integrations/sbtech.png'),
  requiresAdmin: false,
  components: {
    overview: SbTechOverview,
    settings: SbTechSettings
  },
  actions: [
    {
      icon: 'el-icon-refresh',
      label: 'Integrations_SbTech_Action_Sync',
      dialog: SbTechSyncDialog
    }
  ],
  urls: {
    guide: 'https://docs.xtremepush.com/docs'
  }
})
