const DataManagerLocationsTypesLabels = Object.freeze({
  0: 'DataManager_Locations_Field_GeoFence',
  2: 'DataManager_Locations_Field_iBeacon'
})

export { DataManagerLocationsTypesLabels }
