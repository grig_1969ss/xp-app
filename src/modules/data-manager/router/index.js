import DataManagerEventsPage from '@/modules/data-manager/pages/DataManagerEventsPage'
import DataManagerAttributesTagsPage from '@/modules/data-manager/pages/DataManagerAttributesTagsPage'
import DataManagerMetricsPage from '@/modules/data-manager/pages/DataManagerMetricsPage'
import DataManagerLocationsPage from '@/modules/data-manager/pages/DataManagerLocationsPage'
import DataManagerLocationPage from '@/modules/data-manager/pages/DataManagerLocationPage'
import PassThrough from '@/common/components/PassThrough'

export default [
  {
    name: 'DataManager',
    path: '/data-manager',
    component: PassThrough,
    meta: { auth: ['viewProjectTechnical'] },
    children: [
      {
        name: 'DataManagerEvents',
        path: 'events',
        component: DataManagerEventsPage,
        meta: { auth: ['viewProjectTechnical'] }
      },
      {
        name: 'DataManagerAttributesTags',
        path: 'tags',
        component: DataManagerAttributesTagsPage,
        meta: { auth: ['viewProjectTechnical'] }
      },
      {
        name: 'DataManagerMetrics',
        path: 'metrics',
        component: DataManagerMetricsPage,
        meta: { auth: ['viewProjectTechnical'] }
      },
      {
        name: 'DataManagerLocations',
        path: 'locations',
        component: DataManagerLocationsPage,
        meta: { auth: ['viewProjectTechnical'] }
      }
    ]
  },
  {
    name: 'DataManagerLocationsCreate',
    path: '/data-manager/locations/create',
    component: DataManagerLocationPage,
    meta: { auth: ['manageProjectTechnical'] }
  },
  {
    name: 'DataManagerLocationsEdit',
    path: '/data-manager/locations/edit',
    component: DataManagerLocationPage,
    meta: { auth: ['manageProjectTechnical'] }
  },
  {
    name: 'DataManagerLocationsView',
    path: '/data-manager/locations/view',
    component: DataManagerLocationPage,
    props: { readonly: true },
    meta: { auth: ['viewProjectTechnical'] }
  }
]
