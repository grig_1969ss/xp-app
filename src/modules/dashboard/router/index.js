import ProjectDashboard from '@/modules/dashboard/pages/ProjectDashboard'
import CompanyDashboard from '@/modules/dashboard/pages/CompanyDashboard'

export default [
  {
    name: 'Home',
    path: '/',
    component: CompanyDashboard,
    meta: { project: false }
  },
  {
    name: 'ProjectOverview',
    path: '/analytics/analytics/index',
    component: ProjectDashboard
  }
]
