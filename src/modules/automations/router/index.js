import AutomationsPage from '@/modules/automations/pages/AutomationsPage'
import ExportsTab from '@/modules/automations/pages/ExportsTab'
import ImportsTab from '@/modules/automations/pages/ImportsTab'
import ExportFormPage from '@/modules/automations/pages/ExportFormPage'
import ImportFormPage from '@/modules/automations/pages/ImportFormPage'

export default [
  {
    name: 'Automations',
    path: '/automations',
    component: AutomationsPage,
    meta: {
      auth: ['viewProjectItem'],
      title: 'Automations_Title'
    },
    children: [
      {
        name: 'AutomationImports',
        path: 'imports',
        component: ImportsTab,
        meta: {
          auth: ['viewProjectItem']
        }
      },
      {
        name: 'AutomationExports',
        path: 'exports',
        component: ExportsTab,
        meta: {
          auth: ['viewProjectItem']
        }
      }
    ]
  },
  {
    name: 'AutomationExportView',
    path: '/automations/export/view',
    component: ExportFormPage,
    props: { readonly: true },
    project: true,
    meta: {
      auth: ['manageProjectTechnical']
    }
  },
  {
    name: 'AutomationExportCreate',
    path: '/automations/export/create',
    component: ExportFormPage,
    props: { readonly: false },
    project: true,
    meta: {
      auth: ['manageProjectTechnical']
    }
  },
  {
    name: 'AutomationExportUpdate',
    path: '/automations/export/update',
    component: ExportFormPage,
    props: { readonly: false },
    project: true,
    meta: {
      auth: ['manageProjectTechnical']
    }
  },
  {
    name: 'AutomationImportView',
    path: '/automations/import/view',
    component: ImportFormPage,
    props: { readonly: true },
    project: true,
    meta: {
      auth: ['manageProjectTechnical']
    }
  },
  {
    name: 'AutomationImportCreate',
    path: '/automations/import/create',
    component: ImportFormPage,
    props: { readonly: false },
    project: true,
    meta: {
      auth: ['manageProjectTechnical']
    }
  },
  {
    name: 'AutomationImportUpdate',
    path: '/automations/import/update',
    component: ImportFormPage,
    props: { readonly: false },
    project: true,
    meta: {
      auth: ['manageProjectTechnical']
    }
  }
]
