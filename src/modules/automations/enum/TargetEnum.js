const TargetLabel = Object.freeze({
  email: 'Target_Field_Type_Email',
  sftp: 'Target_Field_Type_Sftp'
})

export { TargetLabel }
