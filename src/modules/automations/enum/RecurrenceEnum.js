const FrequencyTypeLabel = Object.freeze({
  daily: 'RecurrenceSettings_Field_Frequency_Daily',
  weekly: 'RecurrenceSettings_Field_Frequency_Weekly',
  monthly: 'RecurrenceSettings_Field_Frequency_Monthly'
})

const FrequencyDayLabel = Object.freeze({
  first: 'RecurrenceSettings_Field_Day_First',
  last: 'RecurrenceSettings_Field_Day_Last',
  '': 'RecurrenceSettings_Field_Day_Number'
})

const DayOfWeekLabel = Object.freeze({
  1: 'RecurrenceSettings_DayOfWeek_Mon',
  2: 'RecurrenceSettings_DayOfWeek_Tue',
  3: 'RecurrenceSettings_DayOfWeek_Wed',
  4: 'RecurrenceSettings_DayOfWeek_Thu',
  5: 'RecurrenceSettings_DayOfWeek_Fri',
  6: 'RecurrenceSettings_DayOfWeek_Sat',
  7: 'RecurrenceSettings_DayOfWeek_Sun'
})

export { FrequencyTypeLabel, FrequencyDayLabel, DayOfWeekLabel }
