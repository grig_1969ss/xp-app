const AutomationStatusLabel = Object.freeze({
  0: 'Automations_Field_StatusInactive',
  1: 'Automations_Field_StatusActive'
})

export { AutomationStatusLabel }
