import DeviceListPage from '@/modules/devices/pages/DeviceListPage'
import DevicePage from '@/modules/devices/pages/DevicePage'

export default [
  {
    name: 'Devices',
    path: '/devices',
    component: DeviceListPage,
    meta: {
      title: 'Devices_DeviceListPage_Title',
      auth: ['Device::Read']
    }
  },

  {
    name: 'Device',
    path: '/device',
    alias: ['/client/device/view'],
    component: DevicePage,
    meta: { auth: ['Device::Read'] }
  }
]
