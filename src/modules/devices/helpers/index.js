const isMobile = device => {
  return device.type === 'ios' || device.type === 'android'
}

const deviceDescription = device => {
  if (isMobile(device)) {
    return device.device_full_name || 'Unknown Device'
  } else {
    if (device.browser_name) {
      if (device.device_full_name) {
        return device.browser_name + ' for ' + device.device_full_name
      } else if (device.os_name) {
        return device.browser_name + ' for ' + device.os_name
      } else {
        return device.browser_name
      }
    } else if (device.device_full_name) {
      return device.device_full_name
    } else if (device.os_name) {
      return device.os_name
    } else {
      return 'Unknown Device'
    }
  }
}

export { isMobile, deviceDescription }
