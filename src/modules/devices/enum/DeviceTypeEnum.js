const DeviceTypeLabel = Object.freeze({
  ios: 'DeviceType_Ios',
  android: 'DeviceType_Android',
  web: 'DeviceType_Web'
})

export { DeviceTypeLabel }
