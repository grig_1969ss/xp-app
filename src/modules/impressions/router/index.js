import ImpressionsListPage from '@/modules/impressions/pages/ImpressionsListPage'

export default [
  {
    name: 'Impressions',
    path: '/tag/impression/index',
    component: ImpressionsListPage,
    meta: {
      auth: ['viewProjectTechnical'],
      title: 'AppImpressions_ImpressionList_Title'
    }
  }
]
