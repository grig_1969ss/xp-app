const channelLabels = Object.freeze({
  email: 'CampaignChannelAnalytics_Email',
  sms: 'CampaignChannelAnalytics_Sms',
  mobile_push: 'CampaignChannelAnalytics_MobilePush',
  web_push: 'CampaignChannelAnalytics_WebPush',
  inbox: 'CampaignChannelAnalytics_Inbox',
  inapp: 'CampaignChannelAnalytics_InApp',
  onsite: 'CampaignChannelAnalytics_OnSite',
  facebook: 'CampaignChannelAnalytics_Facebook',
  whatsapp: 'CampaignChannelAnalytics_WhatsApp'
})

const platformChannelLabels = Object.freeze({
  mobile_push_android: 'CampaignChannelAnalytics_MobilePushAndroid',
  mobile_push_ios: 'CampaignChannelAnalytics_MobilePushIos',
  inapp_android: 'CampaignChannelAnalytics_InAppAndroid',
  inapp_ios: 'CampaignChannelAnalytics_InAppIos'
})

export { channelLabels, platformChannelLabels }
