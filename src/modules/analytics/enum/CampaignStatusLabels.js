const campaignStatusLabels = Object.freeze({
  '1': 'CampaignsList_Tag_Running',
  '2': 'CampaignsList_Tag_Closed',
  '3': 'CampaignsList_Tag_Paused'
})

export { campaignStatusLabels }
