const campaignTypeLabels = Object.freeze({
  api: 'AnalyticsCampaignsPage_Table_Type_Api',
  event: 'AnalyticsCampaignsPage_Table_Type_Event',
  location: 'AnalyticsCampaignsPage_Table_Type_Location',
  optimove: 'AnalyticsCampaignsPage_Table_Type_Optimove',
  repeating: 'AnalyticsCampaignsPage_Table_Type_Repeating',
  scheduled: 'AnalyticsCampaignsPage_Table_Type_Scheduled'
})

export { campaignTypeLabels }
