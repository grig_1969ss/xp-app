import AnalyticsCampaignsPage from '@/modules/analytics/pages/AnalyticsCampaignsPage'
import CampaignOverviewPage from '@/modules/analytics/pages/CampaignOverviewPage'
import CampaignChannelAnalyticsPage from '@/modules/analytics/pages/CampaignChannelAnalyticsPage'
import StatisticsTab from '@/modules/analytics/components/StatisticsTab'
import ClickedLinks from '@/modules/analytics/components/ClickedLinks'
import ClickMap from '@/modules/analytics/components/ClickMap'
import Domains from '@/modules/analytics/components/Domains'
import Bounces from '@/modules/analytics/components/Bounces'
import Actions from '@/modules/analytics/components/Actions'

export default [
  {
    name: 'CampaignChannelAnalytics',
    path: '/analytics/campaign/channel',
    component: CampaignChannelAnalyticsPage,
    meta: {
      auth: ['viewProjectItem'],
      title: 'CampaignChannelAnalytics_PageTitle'
    },
    children: [
      {
        name: 'StatisticsTab',
        path: 'statistics',
        component: StatisticsTab,
        project: true,
        meta: {
          auth: ['viewProjectItem']
        }
      },
      {
        name: 'ClickedLinks',
        path: 'clicked-links',
        component: ClickedLinks,
        project: true,
        meta: {
          auth: ['viewProjectItem']
        }
      },
      {
        name: 'ClickMap',
        path: 'click-map',
        component: ClickMap,
        project: true,
        meta: {
          auth: ['viewProjectItem']
        }
      },
      {
        name: 'Domains',
        path: 'domains',
        component: Domains,
        project: true,
        meta: {
          auth: ['viewProjectItem']
        }
      },
      {
        name: 'Bounces',
        path: 'bounces',
        component: Bounces,
        project: true,
        meta: {
          auth: ['viewProjectItem']
        }
      },
      {
        name: 'Actions',
        path: 'actions',
        component: Actions,
        project: true,
        meta: {
          auth: ['viewProjectItem']
        }
      }
    ]
  },
  {
    name: 'AnalyticsCampaigns',
    path: '/analytics/analytics/campaigns',
    component: AnalyticsCampaignsPage,
    meta: {
      auth: ['viewProjectItem'],
      title: 'LeftNavigation_CampaignAnalytics'
    }
  },
  {
    name: 'CampaignOverview',
    path: '/analytics/analytics/campaign',
    component: CampaignOverviewPage,
    meta: {
      auth: ['viewProjectItem'],
      title: 'LeftNavigation_CampaignAnalytics'
    }
  }
]
