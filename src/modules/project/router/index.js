import ProjectSettingsPage from '@/modules/project/pages/ProjectSettingsPage'

export default [
  {
    name: 'ProjectCreate',
    path: '/account/project/create',
    component: ProjectSettingsPage,
    meta: {
      auth: ['createProject'],
      title: 'ProjectSettings_CreateTitle',
      project: false
    }
  },
  {
    name: 'ProjectSettings',
    path: '/account/project/settings',
    component: ProjectSettingsPage,
    meta: {
      auth: ['updateProject', 'viewProject'],
      title: 'ProjectSettings_Title'
    }
  }
]
