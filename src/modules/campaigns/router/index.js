import LiveCampaignsPage from '@/modules/campaigns/pages/LiveCampaignsPage'
import PastCampaignsPage from '@/modules/campaigns/pages/PastCampaignsPage'
import CampaignMessagesPage from '@/modules/campaigns/pages/CampaignMessagesPage'

export default [
  {
    name: 'LiveCampaigns',
    path: '/push/campaign/active',
    component: LiveCampaignsPage,
    meta: {
      auth: ['viewProjectItem'],
      title: 'LeftNavigation_LiveCampaigns'
    }
  },
  {
    name: 'PastCampaigns',
    path: '/push/campaign/past',
    component: PastCampaignsPage,
    meta: {
      auth: ['viewProjectItem'],
      title: 'LeftNavigation_PastCampaigns'
    }
  },
  {
    name: 'CampaignInfo',
    path: '/push/campaign/info',
    component: CampaignMessagesPage,
    meta: {
      auth: ['viewProjectItem']
    }
  }
]
