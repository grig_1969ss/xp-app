// Campaign schedule types
const campaignSendTypeEnum = Object.freeze({
  SEND_TYPE_NOW: 0,
  SEND_TYPE_FUTURE: 1,
  SEND_TYPE_SCHEDULE: 2,
  SEND_TYPE_EXECUTE: 3
})

export { campaignSendTypeEnum }
