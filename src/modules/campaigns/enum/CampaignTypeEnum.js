const campaignTypeEnum = Object.freeze({
  TYPE_NOTIFICATION: 1,
  TYPE_INAPP: 2,
  TYPE_WORKFLOW: 3
})

export { campaignTypeEnum }
