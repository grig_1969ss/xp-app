import AttributionIos from '@/modules/attribution/pages/AttributionIos'
import AttributionAndroid from '@/modules/attribution/pages/AttributionAndroid'

export default [
  {
    name: 'KochavaSettingsIos',
    path: '/attribution/settings/kochava-ios',
    component: AttributionIos,
    meta: {
      auth: ['admin', 'provider-admin', 'admin-support'],
      title: 'Attribution_Ios_Title'
    }
  },
  {
    name: 'KochavaSettingsAndroid',
    path: '/attribution/settings/kochava-android',
    component: AttributionAndroid,
    meta: {
      auth: ['admin', 'provider-admin', 'admin-support'],
      title: 'Attribution_Android_Title'
    }
  }
]
