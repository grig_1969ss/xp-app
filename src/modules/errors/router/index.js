import NotFoundPage from '@/modules/errors/pages/NotFoundPage'
import ForbiddenPage from '@/modules/errors/pages/ForbiddenPage'
import RestErrorPage from '@/modules/errors/pages/RestErrorPage'

export default [
  {
    path: '*',
    name: 'NotFound',
    component: NotFoundPage,
    meta: { project: false }
  },
  {
    path: '*',
    name: 'Forbidden',
    component: ForbiddenPage,
    meta: { project: false }
  },
  {
    path: '*',
    name: 'RestError',
    component: RestErrorPage,
    meta: { project: false }
  }
]
