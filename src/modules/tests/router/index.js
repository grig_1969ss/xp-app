import VueTestsPage from '@/modules/tests/pages/VueTestsPage'
import pageComponentsTestRoutes from '@/common/tests/page/routes'

export default [
  {
    name: 'VueTests',
    path: '/vue-tests',
    component: VueTestsPage,
    meta: {
      auth: ['admin'],
      title: 'VueTestsPage_Title',
      project: false
    }
  },

  ...pageComponentsTestRoutes
]
