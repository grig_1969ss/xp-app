import EventListPage from '@/modules/events/pages/EventListPage'
import EventFormPage from '@/modules/events/pages/EventFormPage'

export default [
  {
    name: 'EventList',
    path: '/event/event/index',
    component: EventListPage,
    meta: {
      auth: ['viewProjectTechnical'],
      title: 'Events_EventList_Title'
    }
  },
  {
    name: 'EventView',
    path: '/event/event/view',
    component: EventFormPage,
    props: { readonly: true },
    meta: {
      auth: ['viewProjectTechnical'],
      title: 'Events_EventForm_ViewTitle'
    }
  },
  {
    name: 'EventCreate',
    path: '/event/event/create',
    component: EventFormPage,
    meta: {
      auth: ['manageProjectTechnical'],
      title: 'Events_EventForm_CreateTitle'
    }
  },
  {
    name: 'EventUpdate',
    path: '/event/event/update',
    component: EventFormPage,
    meta: {
      auth: ['manageProjectTechnical'],
      title: 'Events_EventForm_UpdateTitle'
    }
  }
]
