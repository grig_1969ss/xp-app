import ImportFormPage from '@/modules/imports/pages/ImportFormPage'

export default [
  {
    name: 'ImportCreate',
    path: '/import/create',
    component: ImportFormPage,
    props: { readonly: false },
    project: true,
    meta: {
      auth: ['manageProjectTechnical']
    }
  }
]
