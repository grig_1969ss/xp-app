const sourceTypesLabels = Object.freeze({
  fs: 'Imports_SourceType_Label_FileStorage',
  upload: 'Imports_SourceType_Label_Upload'
})

const selectionRulesLabels = Object.freeze({
  all: 'Imports_FileSelection_Label_All'
})

export { sourceTypesLabels, selectionRulesLabels }
