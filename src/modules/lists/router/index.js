import ListItemsPage from '@/modules/lists/pages/ListItemsPage'
import ListsPageSuppression from '@/modules/lists/pages/ListsPageSuppression'
import ListsPageUser from '@/modules/lists/pages/ListsPageUser'

export default [
  {
    name: 'SuppressionLists',
    path: '/lists/suppression-list/index',
    component: ListsPageSuppression,
    meta: {
      auth: ['viewProjectItem'],
      title: 'Lists_SupressionLists_Title'
    }
  },
  {
    name: 'SuppressionListItems',
    path: '/lists/suppression-list/items/index',
    component: ListItemsPage,
    props: { suppression: true },
    meta: {
      auth: ['viewProjectItem'],
      title: 'Lists_ListItemsList_Title'
    }
  },
  {
    name: 'UserLists',
    path: '/lists/user-list/index',
    component: ListsPageUser,
    meta: {
      auth: ['viewProjectItem'],
      title: 'Lists_UserLists_Title'
    }
  },
  {
    name: 'UserListItems',
    path: '/lists/user-list/items/index',
    component: ListItemsPage,
    props: { suppression: false },
    meta: {
      auth: ['viewProjectItem'],
      title: 'Lists_ListItemsList_Title'
    }
  }
]
