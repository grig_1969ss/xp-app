const listIdentifierLabel = Object.freeze({
  user_id: 'List_Identifier_UserId',
  external_id: 'List_Identifier_ExternalId',
  email: 'List_Identifier_Email',
  mobile_number: 'List_Identifier_MobileNumber',
  whatsapp_number: 'List_Identifier_WhatsappNumber',
  profile_id: 'List_Identifier_ProfileId',
  device_id: 'List_Identifier_DeviceId',
  device_idfv: 'List_Identifier_DeviceIDFV',
  device_idfa: 'List_Identifier_DeviceIDFA',
  device_anid: 'List_Identifier_DeviceANID',
  device_adid: 'List_Identifier_DeviceADID',
  token: 'List_Identifier_Token'
})

export { listIdentifierLabel }
