import TagFormPage from '@/modules/tags/pages/TagFormPage'
import TagListPage from '@/modules/tags/pages/TagListPage'

export default [
  {
    name: 'TagList',
    path: '/tag/tag/index',
    component: TagListPage,
    meta: {
      auth: ['viewProjectTechnical'],
      title: 'Tags_TagList_Title'
    }
  },
  {
    name: 'TagView',
    path: '/tag/tag/view',
    component: TagFormPage,
    props: { readonly: true },
    meta: {
      auth: ['viewProjectTechnical'],
      title: 'Tags_TagForm_ViewTitle'
    }
  },
  {
    name: 'TagCreate',
    path: '/tag/tag/create',
    component: TagFormPage,
    meta: {
      auth: ['manageProjectTechnical'],
      title: 'Tags_TagForm_CreateTitle'
    }
  },
  {
    name: 'TagUpdate',
    path: '/tag/tag/update',
    component: TagFormPage,
    meta: {
      auth: ['manageProjectTechnical'],
      title: 'Tags_TagForm_UpdateTitle'
    }
  }
]
