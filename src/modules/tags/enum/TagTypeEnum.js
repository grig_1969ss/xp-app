const TagTypes = Object.freeze({
  STRING: 0,
  NUMBER: 1,
  DATE: 2,
  ARRAY: 3
})

const TagTypeLabels = Object.freeze({
  0: 'Tags_Field_Type_String',
  1: 'Tags_Field_Type_Number',
  2: 'Tags_Field_Type_Date',
  3: 'Tags_Field_Type_Array'
})

export { TagTypes, TagTypeLabels }
