import MetricListPage from '@/modules/metrics/pages/MetricListPage'
import MetricFormPage from '@/modules/metrics/pages/MetricFormPage'

export default [
  {
    name: 'Metrics',
    path: '/tag/metric/index',
    component: MetricListPage,
    meta: {
      auth: ['viewProjectTechnical'],
      title: 'Metrics_MetricList_Title'
    }
  },
  {
    name: 'MetricCreate',
    path: '/tag/metric/create',
    component: MetricFormPage,
    meta: {
      auth: ['manageProjectTechnical'],
      title: 'Metrics_MetricForm_CreateTitle'
    }
  },
  {
    name: 'MetricUpdate',
    path: '/tag/metric/update',
    component: MetricFormPage,
    meta: {
      auth: ['manageProjectTechnical'],
      title: 'Metrics_MetricForm_UpdateTitle'
    }
  },
  {
    name: 'MetricView',
    path: '/tag/metric/view',
    component: MetricFormPage,
    props: { readonly: true },
    meta: {
      auth: ['viewProjectTechnical'],
      title: 'Metrics_MetricForm_ViewTitle'
    }
  }
]
