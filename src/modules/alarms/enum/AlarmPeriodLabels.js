const AlarmPeriodLabels = Object.freeze({
  86400: 'Alarms_Field_Period_Day',
  604800: 'Alarms_Field_Period_Week',
  2592000: 'Alarms_Field_Period_Month'
})

export { AlarmPeriodLabels }
