import AlarmFormPage from '@/modules/alarms/pages/AlarmFormPage'
import AlarmListPage from '@/modules/alarms/pages/AlarmListPage'

export default [
  {
    name: 'AlarmList',
    path: '/alarm/alarm/index',
    component: AlarmListPage,
    meta: {
      auth: ['viewProjectTechnical'],
      title: 'Alarms_AlarmList_Title'
    }
  },
  {
    name: 'AlarmView',
    path: '/alarm/alarm/view',
    component: AlarmFormPage,
    props: { readonly: true },
    meta: {
      auth: ['viewProjectTechnical'],
      title: 'Alarms_AlarmForm_ViewTitle'
    }
  },
  {
    name: 'AlarmCreate',
    path: '/alarm/alarm/create',
    component: AlarmFormPage,
    meta: {
      auth: ['manageProjectTechnical'],
      title: 'Alarms_AlarmForm_CreateTitle'
    }
  },
  {
    name: 'AlarmUpdate',
    path: '/alarm/alarm/update',
    component: AlarmFormPage,
    meta: {
      auth: ['manageProjectTechnical'],
      title: 'Alarms_AlarmForm_UpdateTitle'
    }
  }
]
