const subscriptionStatus = Object.freeze({
  0: 'SubscriptionStatus_Unsubscribed',
  1: 'SubscriptionStatus_Subscribed',
  2: 'SubscriptionStatus_Undefined'
})

export { subscriptionStatus }
