const profileIdentifiers = Object.freeze({
  NONE: 0,
  USER_ID: 1,
  EMAIL: 2
})

const profileIdentifiersLabels = Object.freeze({
  0: 'UserProfiles_Identifier_None',
  1: 'UserProfiles_Identifier_UserId',
  2: 'UserProfiles_Identifier_Email'
})

const ProfileLocationsEnterExitLabels = Object.freeze({
  0: 'Profiles_Locations_Label_Exit',
  1: 'Profiles_Locations_Label_Enter'
})

const ProfileLocationsTypesLabels = Object.freeze({
  0: 'Profiles_Locations_Label_Type_Geofence',
  2: 'Profiles_Locations_Label_Type_IBeacon'
})

export {
  profileIdentifiers,
  profileIdentifiersLabels,
  ProfileLocationsEnterExitLabels,
  ProfileLocationsTypesLabels
}
