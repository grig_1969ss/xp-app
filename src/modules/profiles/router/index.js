import ProfileList from '@/modules/profiles/pages/ProfileListPage'
import ProfilePage from '@/modules/profiles/pages/ProfilePage'
import ProfileChannelsPage from '@/modules/profiles/pages/ProfileChannelsPage'
import ProfileMessagesPage from '@/modules/profiles/pages/ProfileMessagesPage'
import ProfileDataPage from '@/modules/profiles/pages/ProfileDataPage'
import ProfileDevicesPage from '@/modules/profiles/pages/ProfileDevicesPage'
import EventsListPage from '@/modules/profiles/pages/EventsListPage'
import TagsListPage from '@/modules/profiles/pages/TagsListPage'
import SessionsListPage from '@/modules/profiles/pages/SessionsListPage'
import ProfileLocationsPage from '@/modules/profiles/pages/ProfileLocationsPage'
import ProfileCreatePage from '@/modules/profiles/pages/ProfileCreatePage'
import ProfileOverviewPage from '@/modules/profiles/pages/ProfileOverviewPage'

export default [
  {
    name: 'Profiles',
    path: '/profiles',
    component: ProfileList,
    meta: {
      auth: ['Profile::Read'],
      title: 'UserProfiles_ProfileList_Title'
    }
  },

  {
    name: 'ProfileCreate',
    path: '/profile/create',
    component: ProfileCreatePage,
    meta: {
      auth: ['Profile::Create'],
      title: 'Profiles_Title_CreateProfile'
    }
  },

  {
    name: 'Profile',
    path: '/profile',
    component: ProfilePage,
    meta: { auth: ['Profile::Read'] },
    children: [
      {
        name: 'ProfileOverview',
        path: 'overview',
        component: ProfileOverviewPage,
        meta: { auth: ['Profile::Read'] }
      },
      {
        name: 'ProfileData',
        path: 'data',
        component: ProfileDataPage,
        meta: { auth: ['Profile::Read'] }
      },
      {
        name: 'ProfileChannels',
        path: 'channels',
        component: ProfileChannelsPage,
        meta: { auth: ['Profile::Read'] }
      },
      {
        name: 'ProfileDevices',
        path: 'devices',
        component: ProfileDevicesPage,
        meta: { auth: ['Device::Read'] }
      },
      {
        name: 'ProfileMessages',
        path: 'messages',
        component: ProfileMessagesPage,
        meta: { auth: ['Message::Read'] }
      },
      {
        name: 'ProfileTags',
        path: 'tags',
        component: TagsListPage,
        meta: { auth: ['TagHit::Read'] }
      },
      {
        name: 'ProfileSessions',
        path: 'sessions',
        component: SessionsListPage,
        meta: { auth: ['Session::Read'] }
      },
      {
        name: 'ProfileEvents',
        path: 'events',
        component: EventsListPage,
        meta: { auth: ['EventHit::Read'] }
      },
      {
        name: 'ProfileLocations',
        path: 'locations',
        component: ProfileLocationsPage,
        meta: { auth: ['Profile::Read'] }
      }
    ]
  }
]
