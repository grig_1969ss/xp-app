import ApiIntegrationPage from '@/modules/settings/pages/ApiIntegrationPage'
import ChannelSettingsPage from '@/modules/settings/pages/ChannelSettingsPage'
import DataRetentionPage from '@/modules/settings/pages/DataRetentionPage'
import EnableChannelsTab from '@/modules/settings/components/EnableChannelsTab'
import EngagementRulesAndCategoriesTab from '@/modules/settings/components/EngagementRulesAndCategoriesTab'
import SmsFromNameTab from '@/modules/settings/components/SmsFromNameTab'
import UrlTrackingTab from '@/modules/settings/components/UrlTrackingTab'
import EmailSettingsTab from '@/modules/settings/components/EmailSettingsTab'
import PushSettingsTab from '@/modules/settings/components/PushSettingsTab'
import InboxSettingsTab from '@/modules/settings/components/InboxSettingsTab'
import GaSettingsPage from '@/modules/settings/pages/GaSettingsPage'
import SegmentationSettingsPage from '@/modules/settings/pages/SegmentationSettingsPage'
import SubscriptionPreferencesPage from '@/modules/settings/pages/SubscriptionPreferencesPage'

export default [
  {
    name: 'ApiIntegration',
    path: '/account/project/api-integration',
    component: ApiIntegrationPage,
    meta: {
      auth: ['manageProjectTechnical'],
      title: 'ApiIntegration_Title'
    }
  },
  {
    name: 'ChannelSettings',
    path: '/account/project/channels-settings',
    component: ChannelSettingsPage,
    meta: {
      auth: ['viewProjectTechnical', 'manageProjectTechnical'],
      title: 'ProjectSettings_ChannelSettings_Title'
    },
    children: [
      {
        name: 'EnableChannelsTab',
        path: 'enable-channels',
        component: EnableChannelsTab,
        project: true,
        meta: {
          auth: ['viewProjectTechnical', 'manageProjectTechnical']
        }
      },
      {
        name: 'EngagementRulesAndCategoriesTab',
        path: 'engagement-rules-categories',
        component: EngagementRulesAndCategoriesTab,
        project: true,
        meta: {
          auth: ['viewProjectTechnical', 'manageProjectTechnical']
        }
      },
      {
        name: 'SmsFromNameTab',
        path: 'sms-from-name',
        component: SmsFromNameTab,
        project: true,
        meta: {
          auth: ['viewProjectTechnical', 'manageProjectTechnical']
        }
      },
      {
        name: 'UrlTrackingTab',
        path: 'link-tracking',
        component: UrlTrackingTab,
        project: true,
        meta: {
          auth: ['viewProjectTechnical', 'manageProjectTechnical']
        }
      },
      {
        name: 'EmailSettingsTab',
        path: 'email-settings',
        component: EmailSettingsTab,
        project: true,
        meta: {
          auth: ['viewProjectTechnical', 'manageProjectTechnical']
        }
      },
      {
        name: 'PushSettingsTab',
        path: 'push-settings',
        component: PushSettingsTab,
        project: true,
        meta: {
          auth: ['viewProjectTechnical', 'manageProjectTechnical']
        }
      },
      {
        name: 'InboxSettingsTab',
        path: 'inbox-settings',
        component: InboxSettingsTab,
        project: true,
        meta: {
          auth: ['viewProjectTechnical', 'manageProjectTechnical']
        }
      }
    ]
  },
  {
    name: 'SubscriptionPreferences',
    path: '/account/project/subscription-preferences',
    component: SubscriptionPreferencesPage,
    meta: {
      auth: ['manageProjectTechnical'],
      title: 'SubscriptionPreferences_Title'
    }
  },
  {
    name: 'GoogleAnalyticsSettings',
    path: '/account/project/ga-settings',
    component: GaSettingsPage,
    meta: {
      auth: ['viewProjectTechnical', 'manageProjectTechnical'],
      title: 'GoogleAnalyticsSettings_Title'
    }
  },
  {
    name: 'DataRetention',
    path: '/account/project/data-retention',
    component: DataRetentionPage,
    meta: {
      auth: ['admin', 'admin-support', 'admin-viewer'],
      title: 'DataRetention_Title'
    }
  },
  {
    name: 'SegmentationSettings',
    path: '/account/project/segmentation-settings',
    component: SegmentationSettingsPage,
    meta: {
      auth: ['admin', 'admin-support', 'admin-viewer'],
      title: 'SegmentationSettings_Title'
    }
  }
]
