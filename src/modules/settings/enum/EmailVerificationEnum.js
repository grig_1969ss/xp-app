const emailVerificationEnum = Object.freeze({
  NOT_VERIFIED: 0,
  SENT: 1,
  VERIFIED: 2
})

export { emailVerificationEnum }
