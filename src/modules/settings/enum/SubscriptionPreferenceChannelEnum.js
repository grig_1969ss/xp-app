const SubscriptionPreferenceChannelLabels = Object.freeze({
  push: 'SubscriptionPreferenceChannel_Push',
  email: 'SubscriptionPreferenceChannel_Email',
  sms: 'SubscriptionPreferenceChannel_Sms'
})

export { SubscriptionPreferenceChannelLabels }
