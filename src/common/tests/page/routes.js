import TabbedPage from './TabbedPage'
import TabbedPageTabOne from './TabbedPageTabOne'
import TabbedPageTabTwo from './TabbedPageTabTwo'
import SteppedPage from './SteppedPage'

export default [
  {
    name: 'VueTestsTabbedPage',
    path: '/vue-tests/tabbed-page',
    component: TabbedPage,
    meta: {
      auth: ['admin'],
      project: false
    },
    children: [
      {
        name: 'VueTestsTabbedPageTabOne',
        path: 'one',
        component: TabbedPageTabOne,
        meta: {
          auth: ['admin'],
          project: false
        }
      },
      {
        name: 'VueTestsTabbedPageTabTwo',
        path: 'two',
        component: TabbedPageTabTwo,
        meta: {
          auth: ['admin'],
          project: false
        }
      }
    ]
  },
  {
    name: 'VueTestsSteppedPage',
    path: '/vue-tests/stepped-page',
    component: SteppedPage,
    meta: {
      auth: ['admin'],
      project: false
    }
  }
]
