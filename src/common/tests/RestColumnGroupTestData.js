export default [
  {
    fields: {
      id: 3001,
      title: 'XP-744 Email & Push',
      active: 1,
      category: 'marketing',
      type: 'Event Bla',
      channels: 'Email',
      app_opens_direct: 10
    },
    metricGroups: [
      {
        name: 'send_date',
        metrics: [
          {
            name: 'first_send_date',
            value: '2020-02-11 10:00'
          },
          {
            name: 'last_send_date',
            value: '2020-03-19 17:00'
          }
        ]
      },
      {
        name: 'mobile_push',
        metrics: [
          {
            name: 'mobile_push_sent',
            value: null
          },
          {
            name: 'mobile_push_opened',
            value: null
          },
          {
            name: 'mobile_push_open_rate',
            value: null,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'web_push',
        metrics: [
          {
            name: 'web_push_sent',
            value: '29'
          },
          {
            name: 'web_push_delivered',
            value: '24'
          },
          {
            name: 'web_push_opened',
            value: '1'
          },
          {
            name: 'web_push_open_rate',
            value: 4,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'inapp',
        metrics: [
          {
            name: 'inapp_sent',
            value: null
          },
          {
            name: 'inapp_delivered',
            value: null
          },
          {
            name: 'inapp_opened',
            value: null
          },
          {
            name: 'inapp_open_rate',
            value: null,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'onsite',
        metrics: [
          {
            name: 'inweb_sent',
            value: null
          },
          {
            name: 'inweb_opened',
            value: null
          },
          {
            name: 'inweb_open_rate',
            value: null,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'inbox',
        metrics: [
          {
            name: 'inbox_sent',
            value: null
          },
          {
            name: 'inbox_delivered',
            value: null
          },
          {
            name: 'inbox_opened',
            value: null
          },
          {
            name: 'inbox_open_rate',
            value: null,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'email',
        metrics: [
          {
            name: 'email_sent',
            value: '24'
          },
          {
            name: 'email_failed',
            value: 0
          },
          {
            name: 'email_delivered',
            value: 24
          },
          {
            name: 'email_unsubscribed',
            value: 0
          },
          {
            name: 'email_complained',
            value: 0
          },
          {
            name: 'email_opened',
            value: '24'
          },
          {
            name: 'email_clicked',
            value: 0
          },
          {
            name: 'email_open_rate',
            value: 100,
            type: 'percentage'
          },
          {
            name: 'email_click_rate',
            value: 0,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'sms',
        metrics: [
          {
            name: 'sms_sent',
            value: null
          },
          {
            name: 'sms_delivered',
            value: null
          },
          {
            name: 'sms_opened',
            value: null
          },
          {
            name: 'sms_open_rate',
            value: null,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'conversion',
        metrics: [
          {
            name: 'actions_sent_unique',
            value: null
          },
          {
            name: 'actions_converted_unique',
            value: null
          },
          {
            name: 'actions_conversion',
            value: null,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'goal',
        metrics: [
          {
            name: 'goals',
            value: null
          },
          {
            name: 'goals_value',
            value: null
          }
        ]
      }
    ]
  },
  {
    fields: {
      id: 3001,
      title: 'XP-745 Email & Push',
      active: 1,
      category: null,
      type: 'Event Triggered',
      channels: 'Web Push, Email',
      app_opens_direct: null
    },
    metricGroups: [
      {
        name: 'send_date',
        metrics: [
          {
            name: 'first_send_date',
            value: '2018-02-11 10:00'
          },
          {
            name: 'last_send_date',
            value: '2020-03-19 17:00'
          }
        ]
      },
      {
        name: 'mobile_push',
        metrics: [
          {
            name: 'mobile_push_sent',
            value: null
          },
          {
            name: 'mobile_push_opened',
            value: null
          },
          {
            name: 'mobile_push_open_rate',
            value: null,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'web_push',
        metrics: [
          {
            name: 'web_push_sent',
            value: '29'
          },
          {
            name: 'web_push_delivered',
            value: '25'
          },
          {
            name: 'web_push_opened',
            value: '1'
          },
          {
            name: 'web_push_open_rate',
            value: 4,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'inapp',
        metrics: [
          {
            name: 'inapp_sent',
            value: null
          },
          {
            name: 'inapp_delivered',
            value: null
          },
          {
            name: 'inapp_opened',
            value: null
          },
          {
            name: 'inapp_open_rate',
            value: null,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'onsite',
        metrics: [
          {
            name: 'inweb_sent',
            value: null
          },
          {
            name: 'inweb_opened',
            value: null
          },
          {
            name: 'inweb_open_rate',
            value: null,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'inbox',
        metrics: [
          {
            name: 'inbox_sent',
            value: null
          },
          {
            name: 'inbox_delivered',
            value: null
          },
          {
            name: 'inbox_opened',
            value: null
          },
          {
            name: 'inbox_open_rate',
            value: null,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'email',
        metrics: [
          {
            name: 'email_sent',
            value: '24'
          },
          {
            name: 'email_failed',
            value: 0
          },
          {
            name: 'email_delivered',
            value: 24
          },
          {
            name: 'email_unsubscribed',
            value: 0
          },
          {
            name: 'email_complained',
            value: 0
          },
          {
            name: 'email_opened',
            value: '24'
          },
          {
            name: 'email_clicked',
            value: 0
          },
          {
            name: 'email_open_rate',
            value: 100,
            type: 'percentage'
          },
          {
            name: 'email_click_rate',
            value: 0,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'sms',
        metrics: [
          {
            name: 'sms_sent',
            value: null
          },
          {
            name: 'sms_delivered',
            value: null
          },
          {
            name: 'sms_opened',
            value: null
          },
          {
            name: 'sms_open_rate',
            value: null,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'conversion',
        metrics: [
          {
            name: 'actions_sent_unique',
            value: null
          },
          {
            name: 'actions_converted_unique',
            value: null
          },
          {
            name: 'actions_conversion',
            value: null,
            type: 'percentage'
          }
        ]
      },
      {
        name: 'goal',
        metrics: [
          {
            name: 'goals',
            value: null
          },
          {
            name: 'goals_value',
            value: null
          }
        ]
      }
    ]
  }
]
