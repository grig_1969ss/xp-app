export default {
  getToken() {
    return new Promise((resolve, reject) => {
      fetch('/email/bee-auth/token?project_id=' + window.xp.project.id, {
        credentials: 'include'
      })
        .then(response => {
          return response.json()
        })
        .then(json => {
          resolve(json)
        })
        .catch(reject)
    })
  }
}
