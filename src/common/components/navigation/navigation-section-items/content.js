import { $can } from '@/core/utils/VueAcl'

export default {
  sectionName: 'LeftNavigation_ContentMenu',
  icon: 'navigation-content',
  index: '4',
  subItems: [
    {
      sectionName: 'LeftNavigation_CampaignTemplates',
      index: '4-1',
      isAllowed: () => $can('CampaignTemplate::Read'),
      routeName: 'CampaignTemplates'
    },
    {
      sectionName: 'LeftNavigation_Snippets',
      index: '4-2',
      routeName: 'Snippets',
      subRouteName: [
        'SnippetsCreate',
        'SnippetsUpdate',
        'SnippetsView',
        'SnippetsCopy'
      ]
    },
    {
      sectionName: 'LeftNavigation_PushCategories',
      index: '4-3',
      isAllowed: () => $can('viewProjectItem'),
      routeName: 'PushCategories'
    }
  ]
}
