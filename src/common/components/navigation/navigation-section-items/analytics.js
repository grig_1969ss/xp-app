import { $can, $hasFeature } from '@/core/utils/VueAcl'

export default {
  sectionName: 'LeftNavigation_AnalyticsMenu',
  icon: 'navigation-analytics',
  index: '2',
  isAllowed: () => $can('viewProjectItem'),
  subItems: [
    {
      sectionName: 'LeftNavigation_Analytics_Campaigns',
      index: '2-1',
      subItems: [
        {
          sectionName: 'LeftNavigation_Analytics_CampaignsStatistics',
          index: '2-1-1',
          routeName: 'AnalyticsCampaigns',
          subRouteName: ['CampaignOverview']
        },
        {
          sectionName: 'LeftNavigation_Analytics_PushMessages',
          index: '2-1-2',
          isAllowed: () => $hasFeature('mobile') || $hasFeature('web'),
          routeName: 'AnalyticsPushMessages'
        },
        {
          sectionName: 'LeftNavigation_Analytics_EmailMessages',
          index: '2-1-3',
          isAllowed: () => $hasFeature('email'),
          routeName: 'AnalyticsEmailMessages'
        },
        {
          sectionName: 'LeftNavigation_Analytics_Attributions',
          index: '2-1-4',
          isAllowed: () => $hasFeature('mobile') && $hasFeature('attributions'),
          routeName: 'AnalyticsAttributions'
        }
      ]
    },
    {
      sectionName: 'LeftNavigation_Analytics_Users',
      index: '2-2',
      isAllowed: () => $hasFeature('mobile'),
      subItems: [
        {
          sectionName: 'LeftNavigation_Analytics_NewReturningUsers',
          index: '2-2-1',
          routeName: 'AnalyticsNewReturningUsers'
        },
        {
          sectionName: 'LeftNavigation_Analytics_UsersEngagement',
          index: '2-2-2',
          routeName: 'AnalyticsUsersEngagement'
        }
      ]
    },
    {
      sectionName: 'LeftNavigation_Analytics_Behaviour',
      index: '2-3',
      subItems: [
        {
          sectionName: 'LeftNavigation_Analytics_TagsImpressions',
          index: '2-3-1',
          routeName: 'AnalyticsTagsImpressions'
        },
        {
          sectionName: 'LeftNavigation_Analytics_Sessions',
          index: '2-3-2',
          isAllowed: () => $hasFeature('mobile'),
          routeName: 'AnalyticsSessions'
        },
        {
          sectionName: 'LeftNavigation_Analytics_Metrics',
          index: '2-3-3',
          routeName: 'AnalyticsMetrics'
        },
        {
          sectionName: 'LeftNavigation_Analytics_Funnels',
          index: '2-3-4',
          routeName: 'AnalyticsFunnels'
        }
      ]
    },
    {
      sectionName: 'LeftNavigation_Analytics_Locations',
      index: '2-4',
      subItems: [
        {
          sectionName: 'LeftNavigation_Analytics_GeoIBeacons',
          index: '2-4-1',
          routeName: 'AnalyticsGeoIbeacons'
        },
        {
          sectionName: 'LeftNavigation_Analytics_Countries',
          index: '2-4-2',
          routeName: 'AnalyticsCountries'
        },
        {
          sectionName: 'LeftNavigation_Analytics_Languages',
          index: '2-4-3',
          routeName: 'AnalyticsLanguages'
        }
      ]
    },
    {
      sectionName: 'LeftNavigation_Analytics_Devices',
      index: '2-5',
      isAllowed: () => $hasFeature('mobile') || $hasFeature('web'),
      subItems: [
        {
          sectionName: 'LeftNavigation_Analytics_Browsers',
          index: '2-5-1',
          routeName: 'AnalyticsBrowsers'
        },
        {
          sectionName: 'LeftNavigation_Analytics_DeviceInfo',
          index: '2-5-2',
          routeName: 'AnalyticsDeviceInfo'
        },
        {
          sectionName: 'LeftNavigation_Analytics_Carriers',
          index: '2-5-3',
          routeName: 'AnalyticsCarriers'
        },
        {
          sectionName: 'LeftNavigation_Analytics_AppVersions',
          index: '2-5-4',
          routeName: 'AnalyticsAppVersions'
        }
      ]
    }
  ]
}
