import { $can, $hasFeature } from '@/core/utils/VueAcl'

export default {
  sectionName: 'LeftNavigation_CampaignsMenu',
  icon: 'navigation-campaigns',
  index: '3',
  isAllowed: () => $can('viewProjectItem'),
  subItems: [
    {
      sectionName: 'LeftNavigation_CreateCampaign',
      index: '3-1',
      isAllowed: () => $can('createProjectItem'),
      routeName: 'CreateCampaign'
    },
    {
      sectionName: 'LeftNavigation_ApproveCampaign',
      index: '3-2',
      isAllowed: () => !$can('createProjectItem') && $can('sendPushCampaign'),
      routeName: 'ApproveCampaign'
    },
    {
      sectionName: 'LeftNavigation_ExistingCampaigns',
      index: '3-3',
      subItems: [
        {
          sectionName: 'LeftNavigation_LiveCampaigns',
          index: '3-3-1',
          routeName: 'LiveCampaigns'
        },
        {
          sectionName: 'LeftNavigation_PastCampaigns',
          index: '3-3-2',
          routeName: 'PastCampaigns'
        },
        {
          sectionName: 'LeftNavigation_InboxFeed',
          index: '3-3-3',
          isAllowed: () => $hasFeature('inbox'),
          routeName: 'InboxFeed'
        },
        {
          sectionName: 'LeftNavigation_AdCampaigns',
          index: '3-3-4',
          isAllowed: () => $hasFeature('attributions'),
          routeName: 'AdCampaigns'
        },
        {
          sectionName: 'LeftNavigation_RedemptionCampaigns',
          index: '3-3-5',
          isAllowed: () => $hasFeature('redemptions'),
          routeName: 'RedemptionCampaigns'
        }
      ]
    },
    {
      sectionName: 'LeftNavigation_NotificationsLog',
      index: '3-5',
      routeName: 'NotificationList',
      subRouteName: ['NotificationDetails']
    }
  ]
}
