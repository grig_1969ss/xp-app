import { $can, $hasFeature } from '@/core/utils/VueAcl'

export default {
  sectionName: 'LeftNavigation_DataMenu',
  icon: 'navigation-data',
  index: '5',
  isAllowed: () => $can('viewProjectItem'),
  subItems: [
    {
      sectionName: 'LeftNavigation_Users',
      index: '5-1',
      routeName: 'Profiles',
      subRouteName: [
        'ProfileCreate',
        'Profile',
        'ProfileOverview',
        'ProfileData',
        'ProfileChannels',
        'ProfileMessages',
        'ProfileDevices',
        'ProfileTags',
        'ProfileSessions',
        'ProfileEvents',
        'ProfileLocations'
      ],
      isAllowed: () => $hasFeature('profileEnabled') && $can('Profile::Read')
    },
    {
      sectionName: 'LeftNavigation_UserDevices',
      index: '5-2',
      routeName: 'Devices',
      subRouteName: ['Device'],
      isAllowed: () => $can('Device::Read')
    },
    {
      sectionName: 'LeftNavigation_Audiences',
      index: '5-3',
      routeName: 'AudienceSegmentList',
      subRouteName: [
        'AudienceUserList',
        'AudienceUserListItems',
        'AudienceSuppressionList',
        'AudienceSuppressionListItems',
        'AudienceSegmentCreate',
        'AudienceSegmentUpdate',
        'AudienceSegmentOverview',
        'AudienceSegmentEvaluations'
      ]
    },
    {
      sectionName: 'LeftNavigation_DataManager',
      index: '5-4',
      isAllowed: () =>
        $can('manageProjectTechnical') || $can('viewProjectTechnical'),
      routeName: 'DataManagerAttributesTags',
      subRouteName: [
        'DataManagerEvents',
        'DataManagerMetrics',
        'DataManagerLocations',
        'DataManagerLocationsCreate',
        'DataManagerLocationsEdit',
        'DataManagerLocationsView'
      ]
    },
    {
      sectionName: 'LeftNavigation_Automations',
      index: '5-5',
      routeName: 'AutomationImports',
      subRouteName: [
        'AutomationImports',
        'AutomationExports',
        'AutomationExportView',
        'AutomationExportCreate',
        'AutomationImportCreate',
        'AutomationImportUpdate'
      ]
    },
    {
      sectionName: 'LeftNavigation_TaskHistory',
      index: '5-6',
      routeName: 'TaskHistory',
      subRouteName: ['TaskHistoryGroup', 'TaskHistoryView']
    }
  ]
}
