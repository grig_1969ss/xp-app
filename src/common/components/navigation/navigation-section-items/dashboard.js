import { $can } from '@/core/utils/VueAcl'

export default {
  sectionName: 'LeftNavigation_Dashboard',
  icon: 'navigation-dashboard',
  index: '1',
  isAllowed: () => $can('viewProjectItem'),
  routeName: 'ProjectOverview'
}
