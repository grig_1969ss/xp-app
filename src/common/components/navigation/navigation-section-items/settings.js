import { $can, $isAdmin, $isSuperAdmin, $hasFeature } from '@/core/utils/VueAcl'

export default {
  sectionName: 'LeftNavigation_SettingsMenu',
  icon: 'navigation-settings',
  index: '6',
  isAllowed: () =>
    $can('manageProjectTechnical') || $can('viewProjectTechnical'),
  subItems: [
    {
      sectionName: 'LeftNavigation_ProjectSettings',
      index: '6-1',
      routeName: 'ProjectSettings',
      subRouteName: ['ProjectCreate']
    },
    {
      sectionName: 'LeftNavigation_Applications',
      index: '6-2',
      routeName: 'Applications',
      subRouteName: [
        'ApplicationCreate',
        'ApplicationUpdate',
        'ApplicationView',
        'ApplicationSDKIntegration'
      ]
    },
    {
      sectionName: 'LeftNavigation_ChannelSettingsPage',
      index: '6-6',
      routeName: 'EnableChannelsTab',
      subRouteName: [
        'EnableChannelsTab',
        'EngagementRulesAndCategoriesTab',
        'UrlTrackingTab',
        'EmailSettingsTab',
        'SmsFromNameTab',
        'PushSettingsTab',
        'InboxSettingsTab'
      ]
    },
    {
      sectionName: 'LeftNavigation_SubscriptionPreferences',
      index: '6-7',
      routeName: 'SubscriptionPreferences',
      isAllowed: () => $hasFeature('subscriptionPreferences')
    },
    {
      sectionName: 'LeftNavigation_Integrations',
      index: '6-3',
      subItems: [
        {
          sectionName: 'LeftNavigation_ApiIntegration',
          index: '6-3-1',
          isAllowed: () => $can('manageProjectTechnical'),
          routeName: 'ApiIntegration'
        },
        {
          sectionName: 'LeftNavigation_GoogleAnalyticsSettings',
          index: '6-3-2',
          routeName: 'GoogleAnalyticsSettings'
        },
        {
          sectionName: 'LeftNavigation_ChannelsIntegrations',
          index: '6-3-3',
          isAllowed: () => $isAdmin(),
          routeName: 'ChannelList',
          subRouteName: ['ChannelCreate', 'ChannelUpdate']
        },
        {
          sectionName: 'LeftNavigation_IntegrationsMarketplace',
          index: '6-3-4',
          routeName: 'IntegrationsMarketplace'
        }
      ]
    },
    {
      sectionName: 'LeftNavigation_Advanced',
      index: '6-4',
      subItems: [
        {
          sectionName: 'LeftNavigation_Translations',
          index: '6-4-1',
          routeName: 'Translations'
        },
        {
          sectionName: 'LeftNavigation_Alarms',
          index: '6-4-2',
          routeName: 'AlarmList',
          subRouteName: ['AlarmUpdate', 'AlarmCreate', 'AlarmView']
        },
        {
          sectionName: 'LeftNavigation_DataRetention',
          index: '6-4-3',
          isAllowed: () => $isSuperAdmin(),
          routeName: 'DataRetention'
        },
        {
          sectionName: 'LeftNavigation_SegmentationSettings',
          index: '6-4-4',
          isAllowed: () => $isSuperAdmin(),
          routeName: 'SegmentationSettings'
        }
      ]
    }
  ]
}
