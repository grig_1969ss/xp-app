const metricLabels = Object.freeze({
  users_total: 'CompanyDashboard_Metric_TotalUsers',
  addressable_users_total: 'CompanyDashboard_Metric_TotalAddressableUsers',
  sent: 'Dashboard_Sent',
  delivered: 'Dashboard_Delivered',
  opened: 'Dashboard_Opened',
  open_rate: 'Dashboard_OpenRate',
  clicked: 'Dashboard_Clicked',
  bounced: 'Dashboard_Bounced',
  click_rate: 'Dashboard_ClickRate',
  failed: 'Dashboard_Failed',
  mobile_users_new: 'Dashboard_MobileUsersNew',
  mobile_users_active: 'Dashboard_MobileUsersActive',
  mobile_users_returning: 'Dashboard_MobileUsersReturning',
  new_users: 'Dashboard_NewUsers',
  active_users: 'Dashboard_ActiveUsers',
  total_sessions: 'Dashboard_TotalSessions',
  opt_in: 'Dashboard_OptIn',
  opt_out: 'Dashboard_OptOut',
  engagement_rate: 'CampaignOverview_Metric_EngagementRate',
  sent_total: 'CampaignOverview_Metric_SentTotal',
  goals: 'CampaignOverview_Metric_Goals',
  goals_value: 'CampaignOverview_Metric_GoalsValue',
  actions_conversion: 'CampaignOverview_Metric_ActionsConversion',
  actions_converted_unique: 'CampaignOverview_Metric_ConvertedUnique',
  actions_sent_unique: 'CampaignOverview_Metric_SentUnique'
})

const channelLabels = Object.freeze({
  mobile_push: 'Dashboard_MobilePush',
  mobile_push_android: 'Dashboard_MobilePushAndroid',
  mobile_push_ios: 'Dashboard_MobilePushIos',
  web_push: 'Dashboard_WebPush',
  email: 'Dashboard_Email',
  sms: 'Dashboard_Sms',
  inapp: 'Dashboard_Inapp',
  inapp_ios: 'Dashboard_InappIos',
  inapp_android: 'Dashboard_InappAndroid',
  inbox: 'Dashboard_Inbox',
  onsite: 'Dashboard_Onsite',
  facebook: 'Dashboard_Facebook',
  whatsapp: 'Dashboard_Whatsapp'
})

const keyMetricLabels = Object.freeze({
  mobile: 'Dashboard_AppMetrics'
})

const eventMetricLabels = Object.freeze({
  mobile: 'Dashboard_EventMobile',
  web: 'Dashboard_EventWeb'
})

const dateFilterPeriods = Object.freeze({
  today: { label: 'DateFilter_Today', value: 0 },
  last7days: { label: 'DateFilter_7_Days', value: 7 },
  last30days: { label: 'DateFilter_30_Days', value: 30 },
  custom: { label: 'DateFilter_Custom', value: null }
})

const dateFilterMonths = Object.freeze({
  0: 'DateFilter_Jan',
  1: 'DateFilter_Feb',
  2: 'DateFilter_Mar',
  3: 'DateFilter_Apr',
  4: 'DateFilter_May',
  5: 'DateFilter_Jun',
  6: 'DateFilter_Jul',
  7: 'DateFilter_Aug',
  8: 'DateFilter_Sep',
  9: 'DateFilter_Oct',
  10: 'DateFilter_Nov',
  11: 'DateFilter_Dec'
})

export {
  metricLabels,
  channelLabels,
  keyMetricLabels,
  eventMetricLabels,
  dateFilterPeriods,
  dateFilterMonths
}
