export default {
  props: {
    startDate: {
      type: String,
      required: true
    },
    endDate: {
      type: String,
      required: true
    },
    projectId: {
      required: true
    }
  },
  computed: {
    settings() {
      return {
        project_id: this.projectId,
        startDate: this.startDate,
        endDate: this.endDate
      }
    }
  }
}
