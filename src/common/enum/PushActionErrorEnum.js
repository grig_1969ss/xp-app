const PushActionErrorLabel = Object.freeze({
  auth: 'PushAction_Error_GcmAuth',
  gcm: 'PushAction_Error_GcmRetryLimit'
})

export { PushActionErrorLabel }
