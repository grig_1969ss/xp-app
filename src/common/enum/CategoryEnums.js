const categoryCooldownLabels = Object.freeze({
  minutes: 'Engagement_Category_Minutes',
  hours: 'Engagement_Category_Hours',
  days: 'Engagement_Category_Days',
  weeks: 'Engagement_Category_Weeks',
  months: 'Engagement_Category_Months'
})

export { categoryCooldownLabels }
