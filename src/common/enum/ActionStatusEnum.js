const ActionStatusEnum = Object.freeze({
  PROCESSING: 0,
  SENT: 1,
  FEEDBACK: 2,
  CANCELLED: 3,
  PREPARING: 4,
  SCHEDULED: 5
})

const ActionStatusLabel = Object.freeze({
  0: 'Action_Status_Processing',
  1: 'Action_Status_Sent',
  2: 'Action_Status_Feedback',
  3: 'Action_Status_Cancelled',
  4: 'Action_Status_Preparing',
  5: 'Action_Status_Scheduled'
})

export { ActionStatusEnum, ActionStatusLabel }
