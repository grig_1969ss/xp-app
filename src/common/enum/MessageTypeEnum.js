const MessageTypeLabel = Object.freeze({
  1: 'MessageType_PushIos',
  2: 'MessageType_PushAndroid',
  3: 'MessageType_PushWeb',
  4: 'MessageType_InApp',
  5: 'MessageType_OnSite',
  6: 'MessageType_Inbox',
  7: 'MessageType_Email',
  8: 'MessageType_Webhook',
  9: 'MessageType_Sms',
  10: 'MessageType_Facebook',
  11: 'MessageType_Whatsapp',
  12: 'MessageType_Workflow'
})

const MessageTypeEnum = Object.freeze({
  PUSH_IOS: 1,
  PUSH_ANDROID: 2,
  PUSH_WEB: 3,
  IN_APP: 4,
  ON_SITE: 5,
  INBOX: 6,
  EMAIL: 7,
  WEBHOOK: 8,
  SMS: 9,
  FACEBOOK: 10,
  WHATSAPP: 11,
  WORKFLOW: 12
})

export { MessageTypeLabel, MessageTypeEnum }
