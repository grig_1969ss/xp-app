const PushEnvironmentLabel = Object.freeze({
  production: 'PushEnvironment_Production',
  sandbox: 'PushEnvironment_Sandbox',
  legacy: 'PushEnvironment_Legacy',
  safari: 'PushEnvironment_Safari',
  chrome: 'PushEnvironment_Chrome',
  firefox: 'PushEnvironment_Firefox'
})

export { PushEnvironmentLabel }
