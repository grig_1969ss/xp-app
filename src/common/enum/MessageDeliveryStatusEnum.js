const MessageDeliveryStatusLabel = Object.freeze({
  failed: 'MessageDeliveryStatus_Failed',
  sent: 'MessageDeliveryStatus_Sent',
  delivered: 'MessageDeliveryStatus_Delivered',
  bounced: 'MessageDeliveryStatus_Bounced',
  control_group: 'MessageDeliveryStatus_ControlGroup'
})

const MessageDeliveryStatusEnum = Object.freeze({
  FAILED: 'failed',
  SENT: 'sent',
  DELIVERED: 'delivered',
  BOUNCED: 'bounced',
  CONTROL_GROUP: 'control_group'
})

export { MessageDeliveryStatusLabel, MessageDeliveryStatusEnum }
