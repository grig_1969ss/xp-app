import '@/core/bootstrap'

import 'vendor/highcharts/highcharts.js'
import 'vendor/highcharts/highcharts-more.js'
import 'vendor/highcharts/modules/exporting.js'

import Raven from 'raven-js'
import AnalyticsPage from '@/jquery/analytics/analyticsPage'
import AttributePage from '@/jquery/analytics/attributePage'
import AppVersionsPage from '@/jquery/analytics/appVersionsPage'
import AttributionsPage from '@/jquery/analytics/attributionsPage'
import AttributionsNetworkPage from '@/jquery/analytics/attributionsNetworkPage'
import AttributionsCampaignPage from '@/jquery/analytics/attributionsCampaignPage'
import AttributionsAdGroupPage from '@/jquery/analytics/attributionsAdGroupPage'
import CountriesPage from '@/jquery/analytics/countriesPage'
import DeviceInfoPage from '@/jquery/analytics/deviceInfoPage'
import FunnelPage from '@/jquery/analytics/funnelPage'
import FunnelsPage from '@/jquery/analytics/funnelsPage'
import FunnelFormPage from '@/jquery/analytics/funnelFormPage'
import LocationsPage from '@/jquery/analytics/locationsPage'
import MetricsPage from '@/jquery/analytics/metricsPage'
import PushesPage from '@/jquery/analytics/pushesPage'
import EmailPage from '@/jquery/analytics/emailPage'
import TagsPage from '@/jquery/analytics/tagsPage'
import TagValuesPage from '@/jquery/analytics/tagValuesPage'
import SessionsPage from '@/jquery/analytics/sessionsPage'
import UsersPage from '@/jquery/analytics/usersPage'
import UsersEngagementPage from '@/jquery/analytics/usersEngagementPage'

switch (window.YII_CONTROLLER) {
  case 'analytics':
    switch (window.YII_ACTION) {
      case 'index':
      case 'index-web':
        window.analyticPage = new AnalyticsPage()
        break
      case 'attribute':
      case 'browsers':
      case 'languages':
      case 'carriers':
        window.analyticPage = new AttributePage()
        break
      case 'app-versions':
        window.analyticPage = new AppVersionsPage()
        break
      case 'attributions':
        window.analyticPage = new AttributionsPage()
        break
      case 'attributions-network':
        window.analyticPage = new AttributionsNetworkPage()
        break
      case 'attributions-campaign':
        window.analyticPage = new AttributionsCampaignPage()
        break
      case 'attributions-ad-group':
        window.analyticPage = new AttributionsAdGroupPage()
        break
      case 'countries':
        window.analyticPage = new CountriesPage()
        break
      case 'device-info':
        window.analyticPage = new DeviceInfoPage()
        break
      case 'funnel':
        window.analyticPage = new FunnelPage()
        break
      case 'funnels':
        window.analyticPage = new FunnelsPage()
        break
      case 'locations':
        window.analyticPage = new LocationsPage()
        break
      case 'metrics':
        window.analyticPage = new MetricsPage()
        break
      case 'pushes':
        window.analyticPage = new PushesPage()
        break
      case 'email':
        window.analyticPage = new EmailPage()
        break
      case 'tags':
        window.analyticPage = new TagsPage()
        break
      case 'tag-values':
        window.analyticPage = new TagValuesPage()
        break
      case 'sessions':
        window.analyticPage = new SessionsPage()
        break
      case 'users':
        window.analyticPage = new UsersPage()
        break
      case 'users-engagement':
        window.analyticPage = new UsersEngagementPage()
        break
    }
    break
  case 'funnel':
    window.analyticPage = new FunnelFormPage()
    break
}

if (window.analyticPage) {
  Raven.context(function() {
    window.addEventListener('vue-ready', function() {
      window.analyticPage.init()
    })
  })
}
