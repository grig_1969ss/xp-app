import '@/core/bootstrap'

import Vue from 'vue'
import Raven from 'raven-js'
import InboxMessagePreview from '@/common/components/inbox/InboxMessagePreview'
import ProjectPage from '@/jquery/general/projectPage'
import ProjectInboxSettingsPage from '@/jquery/general/projectInboxSettingsPage'
import ProjectCertificatesPage from '@/jquery/general/projectCertificatesPage'
import PushCategoryPage from '@/jquery/general/pushCategoryPage'
import SnippetPage from '@/jquery/general/snippetPage'
import ProjectCategoriesPage from '@/jquery/general/projectCategoriesPage'
import ProjectIntegrationsPage from '@/jquery/general/projectIntegrationsPage'
import TrackingPage from '@/jquery/general/trackingPage'
import LocationPage from '@/jquery/general/locationsPage'
import ModelIndexPage from '@/jquery/general/modelIndexPage'
import RedemptionPage from '@/jquery/general/redemptionPage'
import SegmentsPage from '@/jquery/general/segmentsPage'
import InboxFeedPage from '@/jquery/general/inboxFeedPage'
import UsagePage from '@/jquery/general/usagePage'
import AdCampaignPage from '@/jquery/general/adCampaignPage'
import ApplicationPage from '@/jquery/general/applicationPage'
import ProjectEmailPage from '@/jquery/general/projectEmailPage'
import TaskRecordPage from '@/jquery/general/taskRecordPage'
import CkEditor from 'vendor/ckeditor/editor'

Vue.component(InboxMessagePreview.name, InboxMessagePreview)
Vue.component(CkEditor.name, CkEditor)

switch (window.YII_CONTROLLER + '/' + window.YII_ACTION) {
  case 'location/manage-geofences':
  case 'location/manage-ibeacons':
    window.page = new LocationPage()
    break

  case 'segment/create':
  case 'segment/update':
  case 'segment/view':
    window.page = new SegmentsPage()
    break

  case 'device/index':
  case 'profile/index':
    window.page = new ModelIndexPage()
    break

  case 'inbox/index':
    window.page = new InboxFeedPage()
    break

  case 'ad-campaign/create':
  case 'ad-campaign/update':
  case 'ad-campaign/view':
    window.page = new AdCampaignPage()
    break

  case 'redemption-campaign/create':
  case 'redemption-campaign/update':
  case 'redemption-campaign/view':
    window.page = new RedemptionPage()
    break

  case 'account/usage':
    window.page = new UsagePage()
    break

  case 'project/update':
    window.page = new ProjectPage()
    break

  case 'project/inbox-settings':
    window.page = new ProjectInboxSettingsPage()
    break

  case 'project/email-settings':
    window.page = new ProjectEmailPage()
    break

  case 'project/keys':
  case 'project/ga-settings':
    window.page = new ProjectCertificatesPage()
    break

  case 'url-tracking/manage':
    window.page = new TrackingPage()
    break

  case 'category/manage':
    window.page = new ProjectCategoriesPage()
    break

  case 'project-integration/index':
    window.page = new ProjectIntegrationsPage()
    break

  case 'push-category/create':
  case 'push-category/update':
  case 'push-category/view':
    window.page = new PushCategoryPage()
    break

  case 'snippet/create':
  case 'snippet/update':
  case 'snippet/view':
    window.page = new SnippetPage()
    break

  case 'application/create':
  case 'application/update':
  case 'application/view':
    window.page = new ApplicationPage()
    break

  case 'history/view':
    window.page = new TaskRecordPage()
    break
}

if (window.page) {
  Raven.context(function() {
    window.addEventListener('vue-ready', function() {
      window.page.init()
    })
  })
}
