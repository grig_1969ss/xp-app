import '@/core/app'

import '@/font/OpenSans/stylesheet.css'

import '@/css/global.css'
import '@/css/layout.css'
import '@/css/deprecated/global.less'
import '@/css/deprecated/style.less'
import '@/css/deprecated/button.less'
import '@/css/deprecated/error.less'
import '@/css/deprecated/login.less'
import '@/css/deprecated/registration.less'
import '@/css/deprecated/email-profile.less'

import '@/css/main.scss'
