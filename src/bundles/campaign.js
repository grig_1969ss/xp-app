import '@/core/bootstrap'

import 'vendor/emoji/css/nanoscroller.css'
import 'vendor/emoji/css/emoji.css'

import Vue from 'vue'
import Raven from 'raven-js'
import PushPreviewSection from '@/common/components/push/PushPreviewSection'
import WebpushPreviewSection from '@/common/components/webpush/WebpushPreviewSection.vue'
import SmsPreviewSection from '@/common/components/sms/SmsPreviewSection.vue'
import InboxPreviewSection from '@/common/components/inbox/InboxPreviewSection'
import OnsitePreviewSection from '@/common/components/onsite/OnsitePreviewSection'
import OnsitePopupPreviewSection from '@/common/components/onsite/OnsitePopupPreviewSection'
import FacebookPreviewSection from '@/common/components/facebook/FacebookPreviewSection'
import WhatsappPreviewSection from '@/common/components/whatsapp/WhatsappPreviewSection'
import WebhookPreviewSection from '@/common/components/webhook/WebhookPreviewSection'
import EmailBuilder from '@/common/components/email/EmailBuilder'
import EmailHtmlEditor from '@/common/components/email/EmailHtmlEditor'
import SegmentCategorySelector from '@/modules/audiences/components/SegmentCategorySelector'
import CampaignPage from '@/jquery/campaign/campaignPage'
import CampaignTemplatePage from '@/jquery/campaign/campaignTemplatePage'
import WorkflowBuilder from '@/common/components/workflow/WorkflowBuilder'

Vue.component(PushPreviewSection.name, PushPreviewSection)
Vue.component(WebpushPreviewSection.name, WebpushPreviewSection)
Vue.component(SmsPreviewSection.name, SmsPreviewSection)
Vue.component(InboxPreviewSection.name, InboxPreviewSection)
Vue.component(OnsitePreviewSection.name, OnsitePreviewSection)
Vue.component(OnsitePopupPreviewSection.name, OnsitePopupPreviewSection)
Vue.component(FacebookPreviewSection.name, FacebookPreviewSection)
Vue.component(WhatsappPreviewSection.name, WhatsappPreviewSection)
Vue.component(WebhookPreviewSection.name, WebhookPreviewSection)
Vue.component(EmailBuilder.name, EmailBuilder)
Vue.component(WorkflowBuilder.name, WorkflowBuilder)
Vue.component(EmailHtmlEditor.name, EmailHtmlEditor)
Vue.component(SegmentCategorySelector.name, SegmentCategorySelector)

switch (window.YII_CONTROLLER) {
  case 'campaign':
    window.campaignPage = new CampaignPage()
    break

  case 'campaign-template':
    window.campaignPage = new CampaignTemplatePage()
    break
}

if (window.campaignPage) {
  Raven.context(function() {
    window.addEventListener('vue-ready', function() {
      window.campaignPage.init()
    })
  })
}
