import store from '@/core/store'
import i18n from '@/core/utils/I18n'
import { dateFilterPeriods } from '@/common/components/dashboard/Labels'
import moment from 'moment'
import 'moment-timezone'

const getEnumTranslations = list => {
  let translations = {}
  Object.keys(list).forEach(key => {
    translations[key] = i18n.t(list[key])
  })
  return translations
}

const getDateFilterRange = () => {
  const dateFilter = store.getters.userConfig.dateFilter
  let range = JSON.parse(
    window.sessionStorage.getItem(`config.${store.getters.userUid}.dateRange`)
  )
  if (!range) {
    let startDayDiff = dateFilterPeriods[dateFilter.period]
      ? dateFilterPeriods[dateFilter.period].value
      : 0
    let endDayDiff = startDayDiff > 0 ? 1 : 0
    range = {
      start: moment().subtract(startDayDiff, 'days'),
      end: moment().subtract(endDayDiff, 'days')
    }
  }

  return {
    start: moment(range.start).toDate(),
    end: moment(range.end).toDate()
  }
}

const projectRoute = (route, projectId) => {
  if (!projectId) {
    projectId = store.getters.projectId
  }

  if (!route.query) {
    route.query = {}
  }

  route.query.project_id = projectId
  return route
}

const projectEndpoint = (endpoint, projectId) => {
  endpoint.url = projectUrl(endpoint.url, projectId)

  return endpoint
}

const projectUrl = (url, projectId) => {
  if (!projectId) {
    projectId = store.getters.projectId
  }
  if (url !== '') {
    url = `projects/${projectId}/${url}`
  } else {
    url = `projects/${projectId}`
  }

  return url
}

const baseUrl = url => {
  if (url == null) {
    url = ''
  }
  return `${window.BASE_URL}/${url}`
}

const projectTimezoneDate = time => {
  let date = moment.unix(time)
  if (store.state.app.project) {
    return date.tz(store.state.app.project.timezone)
  } else {
    return date
  }
}

const formatNumber = number => {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

const toBase64 = file =>
  new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () =>
      resolve(addParamToBase64DataUrl(reader.result, 'name', file.name))
    reader.onerror = error => reject(error)
  })

const addParamToBase64DataUrl = (url, key, value) => {
  return url.replace(
    ';base64',
    ';' + key + '=' + value.replace(/[^a-zA-Z0-9-_.]/g, '-') + ';base64'
  )
}
const uuid = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    // eslint-disable-next-line one-var
    var r = (Math.random() * 16) | 0,
      v = c === 'x' ? r : (r & 0x3) | 0x8
    return v.toString(16)
  })
}

const isValidUrl = str => {
  let pattern = new RegExp(
    '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$',
    'i'
  ) // fragment locator
  return !!pattern.test(str)
}

export {
  getEnumTranslations,
  getDateFilterRange,
  projectRoute,
  projectEndpoint,
  projectUrl,
  projectTimezoneDate,
  formatNumber,
  toBase64,
  uuid,
  baseUrl,
  isValidUrl
}
