import RestApi from '@/core/services/RestApi'
import { RestApiPaginator } from '@/core/utils/RestApiPaginator'
import memoize from 'memoizee'

const fetchPages = memoize(
  (uid, start, limit, paginator) =>
    paginator.fetchPages({ start: start, limit: limit }),
  { promise: true, length: 3, maxAge: 10000 }
)

export default {
  namespace: true,

  state: {
    projects: [],
    project: null,
    user: null,
    features: null,
    provider: null,
    domains: null,
    pages: null,
    company: null,
    region: null,
    dateFilter: null,
    show_channel_platforms: null,
    channels: null,
    applications: null,
    dashboardCampaign: null,
    tutorialCards: null,
    home: null
  },

  getters: {
    permissions(state) {
      if (state.user && state.user.permissions) {
        return state.user.permissions
      }

      return []
    },
    userUid(state) {
      return state.user ? state.user.uid : undefined
    },

    projectId(state) {
      return state.project ? state.project.id : undefined
    },

    mxaDomain(state) {
      return state.domains ? state.domains.mxa : ''
    },

    userConfig(state) {
      return {
        dateFilter: state.dateFilter,
        channels: state.channels,
        show_channel_platforms: state.show_channel_platforms
      }
    },
    tutorialCards(state) {
      return state.tutorialCards
    }
  },

  mutations: {
    setProjects(state, projects) {
      state.projects = projects
    },

    setConfig(state, config) {
      state.user = config.user
      state.company = config.company
      state.domains = config.domains
      state.pages = config.pages
      state.provider = config.provider
      state.region = config.region
      state.home = config.home
    },

    setProjectConfig(state, config) {
      state.project = config.project
      state.features = config.features
      state.applications = config.applications
    },

    setUserPreferences(state, config) {
      state.dateFilter = config.dateFilter
      state.channels = config.channels
      state.show_channel_platforms = config.show_channel_platforms
      // To be implemented when needed w/ other componenets (will need to create getter function)
      // state.applications = config.applications
      // state.dashboardCampaign = preferences.dashboardCampaign
    },
    setTutorialCards(state, config) {
      state.tutorialCards = config
    }
  },

  actions: {
    fetchProjects: (function() {
      // This allows us to keep the paginator in scope between requests
      let paginator

      return async function({ commit, dispatch, state }, filter) {
        if (paginator) {
          // If the filter changes we want to cancel previous fetch requests
          paginator.cancel()
        }

        let payload = {
          method: 'get',
          url: 'projects',
          params: { name: filter, sort: 'title' }
        }

        paginator = new RestApiPaginator(payload)
        let uid = filter ? 'projects-' + filter : 'projects'

        try {
          let data = await fetchPages(uid, 1, 2, paginator)
          commit('setProjects', data)
        } catch (e) {
          if (!paginator.isCancel(e)) {
            throw e
          }
        }
      }
    })(),

    fetchConfig: async ({ commit }) => {
      await RestApi.get(`config`).then(({ data }) => {
        commit('setConfig', data)
      })
    },

    fetchProjectConfig: ({ commit }, projectId) => {
      return new Promise((resolve, reject) => {
        RestApi.get(`projects/${projectId}/config`)
          .then(({ data }) => {
            commit('setProjectConfig', data)
            resolve()
          })
          .catch(error => {
            commit('setProjectConfig', {})
            reject(error)
          })
      })
    },

    fetchUserPreferences: ({ commit }, projectId) => {
      return new Promise((resolve, reject) => {
        RestApi.get(`projects/${projectId}/user/config`)
          .then(({ data }) => {
            commit('setUserPreferences', data)
            resolve()
          })
          .catch(error => {
            commit('setUserPreferences', {})
            reject(error)
          })
      })
    },

    reloadProjectConfig: ({ getters, dispatch }) => {
      return dispatch('fetchProjectConfig', getters.projectId)
    },

    saveUserPreferences: ({ commit, getters }, payload) => {
      return new Promise((resolve, reject) => {
        RestApi.patch(`projects/${getters.projectId}/user/config`, payload)
          .then(({ data }) => {
            // update store value
            commit('setUserPreferences', data)
            resolve()
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    fetchTutorialCards: ({ commit, getters }) => {
      let tutorialCardsStr = localStorage.getItem(`tutorialCards`)
      let tutorialCards = {}
      try {
        tutorialCards = JSON.parse(tutorialCardsStr)
      } catch (e) {}
      commit('setTutorialCards', tutorialCards)
    },
    saveTutorialCard: ({ commit, getters }, payload) => {
      let tutorialCardsStr = localStorage.getItem(`tutorialCards`)
      let tutorialCards = {}
      if (tutorialCardsStr) {
        try {
          tutorialCards = JSON.parse(tutorialCardsStr)
        } catch (e) {}
      }

      tutorialCards = { ...tutorialCards, ...payload }
      localStorage.setItem(`tutorialCards`, JSON.stringify(tutorialCards))
      commit('setTutorialCards', tutorialCards)
    }
  }
}
