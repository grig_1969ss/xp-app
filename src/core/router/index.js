import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/core/store'
import i18n from '@/core/utils/I18n'
import routes from '@/core/router/routes'
import { isEqual } from 'lodash'
Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: routes
})

router.beforeEach(async function(to, from, next) {
  if (from.name) {
    // Check above is to avoid redirecting on initial page load
    if (from.meta.vue === false || to.meta.vue === false) {
      if (!to.hash || from.path !== to.path || !isEqual(from.query, to.query)) {
        window.location.href = to.fullPath
      }
      return
    }
  }

  if (to.meta.project !== false) {
    if (!to.query.project_id) {
      return next(notFoundPage(to))
    }

    if (Number(to.query.project_id) !== Number(store.getters.projectId)) {
      try {
        await store.dispatch('fetchProjectConfig', to.query.project_id)
        await store.dispatch('fetchUserPreferences', to.query.project_id)
      } catch (error) {
        return next(restErrorPage(to, error))
      }
    }
  } else {
    store.commit('setProjectConfig', {})
    store.commit('setUserPreferences', {})
  }

  if (to.meta.auth) {
    if (!checkPermissions(to)) {
      next(forbiddenPage(to))
    }
  }

  if (to.meta.title) {
    document.title = i18n.t(to.meta.title)
  }

  next()
})

const checkPermissions = route => {
  return route.meta.auth.find(
    role =>
      store.getters.permissions.includes(role) ||
      store.state.app.user.role === role
  )
}

const notFoundPage = route => {
  return {
    name: 'NotFound',
    params: [route.path],
    query: route.query,
    replace: true
  }
}

const forbiddenPage = route => {
  return {
    name: 'Forbidden',
    params: [route.path],
    query: route.query,
    replace: true
  }
}

const restErrorPage = (route, error) => {
  return {
    name: 'RestError',
    params: [route.path, error],
    query: route.query,
    replace: true
  }
}

export default router
