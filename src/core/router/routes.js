import analyticsRoutes from '@/modules/analytics/router'
import dashboardRoutes from '@/modules/dashboard/router'
import campaignsRoutes from '@/modules/campaigns/router'
import profilesRoutes from '@/modules/profiles/router'
import yiiRoutes from '@/core/router/yii-routes'
import tagsRoutes from '@/modules/tags/router'
import eventsRoutes from '@/modules/events/router'
import metricsRoutes from '@/modules/metrics/router'
import projectRoutes from '@/modules/project/router'
import settingsRoutes from '@/modules/settings/router'
import alarmsRoutes from '@/modules/alarms/router'
import attributionRoutes from '@/modules/attribution/router'
import tasksRoutes from '@/modules/tasks/router'
import testsRouter from '@/modules/tests/router'
import impressionsRoutes from '@/modules/impressions/router'
import channelsRoutes from '@/modules/channels/router'
import snippetsRoutes from '@/modules/snippets/router'
import notificationsLogRoutes from '@/modules/notifications-log/router'
import listsRoutes from '@/modules/lists/router'
import audienceRoutes from '@/modules/audiences/router'
import integrationsRoutes from '@/modules/integrations/router'
import devicesRoutes from '@/modules/devices/router'
import dataManagerRoutes from '@/modules/data-manager/router'
import errorsRoutes from '@/modules/errors/router'
import accountRoutes from '@/modules/account/router'
import automationsRoutes from '@/modules/automations/router'
import importRoutes from '@/modules/imports/router'

export default [
  ...analyticsRoutes,
  ...dashboardRoutes,
  ...campaignsRoutes,
  ...accountRoutes,
  ...profilesRoutes,
  ...yiiRoutes,
  ...tagsRoutes,
  ...eventsRoutes,
  ...metricsRoutes,
  ...projectRoutes,
  ...settingsRoutes,
  ...alarmsRoutes,
  ...attributionRoutes,
  ...tasksRoutes,
  ...testsRouter,
  ...impressionsRoutes,
  ...channelsRoutes,
  ...tasksRoutes,
  ...notificationsLogRoutes,
  ...listsRoutes,
  ...audienceRoutes,
  ...integrationsRoutes,
  ...devicesRoutes,
  ...snippetsRoutes,
  ...dataManagerRoutes,
  ...automationsRoutes,
  ...importRoutes,
  ...errorsRoutes
]
