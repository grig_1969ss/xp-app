export default [
  {
    name: 'SignIn',
    path: '/signin',
    meta: { project: false, vue: false }
  },
  {
    name: 'AnalyticsNewReturningUsers',
    path: '/analytics/analytics/users',
    meta: { vue: false }
  },
  {
    name: 'ProjectOverview',
    path: '/analytics/analytics/index-old',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsUsersEngagement',
    path: '/analytics/analytics/users-engagement',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsCampaigns',
    path: '/analytics/analytics/campaign',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsAttributions',
    path: '/analytics/analytics/attributions',
    alias: [
      '/analytics/analytics/attributions-network',
      '/analytics/analytics/attributions-campaign',
      '/analytics/analytics/attributions-ad-group'
    ],
    meta: { vue: false }
  },
  {
    name: 'AnalyticsPushMessages',
    path: '/analytics/analytics/pushes',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsEmailMessages',
    path: '/analytics/analytics/email',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsTagsImpressions',
    path: '/analytics/analytics/tags',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsSessions',
    path: '/analytics/analytics/sessions',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsFunnels',
    path: '/analytics/analytics/funnels',
    alias: ['/analytics/analytics/funnel'],
    meta: { vue: false }
  },
  {
    name: 'AnalyticsMetrics',
    path: '/analytics/analytics/metrics',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsCountries',
    path: '/analytics/analytics/countries',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsLanguages',
    path: '/analytics/analytics/languages',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsGeoIbeacons',
    path: '/analytics/analytics/locations',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsBrowsers',
    path: '/analytics/analytics/browsers',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsCarriers',
    path: '/analytics/analytics/carriers',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsDeviceInfo',
    path: '/analytics/analytics/device-info',
    meta: { vue: false }
  },
  {
    name: 'AnalyticsAppVersions',
    path: '/analytics/analytics/app-versions',
    meta: { vue: false }
  },

  {
    name: 'Campaigns',
    path: '/push/campaign/index',
    alias: [
      '/push/campaign/copy',
      '/push/campaign/update',
      '/push/campaign/view',
      '/push/export/campaign'
    ],
    meta: { vue: false }
  },
  {
    name: 'ViewCampaign',
    path: '/push/campaign/view',
    meta: { vue: false }
  },
  {
    name: 'CreateCampaign',
    path: '/push/campaign/create',
    meta: { vue: false }
  },
  {
    name: 'DeviceMessages',
    path: '/push/action-to-device/index',
    meta: { vue: false }
  },
  {
    name: 'ApproveCampaign',
    path: '/push/campaign/approve',
    meta: { vue: false }
  },
  {
    name: 'CampaignView',
    path: '/push/campaign/view',
    meta: { vue: false }
  },
  {
    name: 'InboxFeed',
    path: '/push/inbox/index',
    meta: { vue: false }
  },
  {
    name: 'CampaignTemplates',
    path: '/push/campaign-template/index',
    alias: [
      '/push/campaign-template/create',
      '/push/campaign-template/view',
      '/push/campaign-template/update',
      '/push/campaign-template/copy',
      '/push/campaign-template/import'
    ],
    meta: { vue: false }
  },
  {
    name: 'PushCategories',
    path: '/push/push-category/index',
    alias: [
      '/push/push-category/create',
      '/push/push-category/view',
      '/push/push-category/update'
    ],
    meta: { vue: false }
  },
  {
    name: 'RedemptionCampaigns',
    path: '/redemption/redemption-campaign/index',
    alias: [
      '/redemption/redemption-campaign/create',
      '/redemption/redemption-campaign/update',
      '/redemption/redemption-campaign/view'
    ],
    meta: { vue: false }
  },
  {
    name: 'AdCampaigns',
    path: '/attribution/ad-campaign/index',
    alias: [
      '/attribution/ad-campaign/create',
      '/attribution/ad-campaign/update',
      '/attribution/ad-campaign/view'
    ],
    meta: { vue: false }
  },
  {
    name: 'UserDevices',
    path: '/client/device/index',
    alias: ['/client/device/create', '/client/device/update'],
    meta: { vue: false }
  },
  {
    name: 'TagAttributesClient',
    path: '/tag/tag-attribute-client/index',
    alias: [
      '/tag/tag-attribute-client/create',
      '/tag/tag-attribute-client/view',
      '/tag/tag-attribute-client/update'
    ],
    meta: { vue: false }
  },
  {
    name: 'TagAttributesExternal',
    path: '/tag/tag-attribute-external/index',
    alias: [
      '/tag/tag-attribute-external/create',
      '/tag/tag-attribute-external/view',
      '/tag/tag-attribute-external/update'
    ],
    meta: { vue: false }
  },
  {
    name: 'TagHits',
    path: '/tag/tag-hit/index',
    meta: { vue: false }
  },
  {
    name: 'EventHits',
    path: '/event/event-hit/index',
    meta: { vue: false }
  },
  {
    name: 'LocationHits',
    path: '/location/location-hit/index',
    meta: { vue: false }
  },
  {
    name: 'Segments',
    path: '/segment/segment/index',
    alias: [
      '/segment/segment/create',
      '/segment/segment/update',
      '/segment/segment/view'
    ],
    meta: { vue: false }
  },
  {
    name: 'GeoFences',
    path: '/location/location/manage-geofences',
    meta: { vue: false }
  },
  {
    name: 'IBeacons',
    path: '/location/location/manage-ibeacons',
    meta: { vue: false }
  },
  {
    name: 'IBeaconsUUIDs',
    path: '/location/ibeacon-uuid/index',
    alias: [
      '/location/ibeacon-uuid/create',
      '/location/ibeacon-uuid/update',
      '/location/ibeacon-uuid/view'
    ],
    meta: { vue: false }
  },
  {
    name: 'EmailSettings',
    path: '/account/project/email-settings',
    meta: { vue: false }
  },
  {
    name: 'InboxSettings',
    path: '/account/project/inbox-settings',
    meta: { vue: false }
  },
  {
    name: 'AttributionEvents',
    path: '/attribution/attribution-event/index',
    meta: { vue: false }
  },
  {
    name: 'Translations',
    path: '/translation/project-translation/index',
    alias: [
      '/translation/project-translation/create',
      '/translation/project-translation/update',
      '/translation/project-translation/view'
    ],
    meta: { vue: false }
  }
]
