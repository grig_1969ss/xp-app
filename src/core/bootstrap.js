import 'vendor/moment/moment-with-langs.js'

import 'vendor/jquery/jquery.js'
import 'vendor/jquery/jquery.form.js'
import 'vendor/jquery/jquery.numeric.js'
import 'vendor/jquery/jquery.validate.js'
import 'vendor/jquery/jquery.bpopup.min.js'
import 'vendor/jquery/jquery.cookie.js'
import 'vendor/jquery/jquery.ui/jquery-ui-1.9.2.custom.js'
import 'vendor/jquery/jquery.ui/jquery-ui-timepicker-addon.js'
import 'vendor/jquery/jquery.daterangepicker/jquery.daterangepicker.js'
import 'vendor/jquery/jquery.colorbox/jquery.colorbox.js'
import 'vendor/jquery/jquery.ui/custom-theme/jquery-ui-1.9.2.custom.css'
import 'vendor/jquery/jquery.daterangepicker/daterangepicker.css'
import 'vendor/jquery/jquery.serializejson.min.js'

import 'vendor/selectize/selectize.js'
import 'vendor/selectize/selectize.css'
import 'vendor/colorpicker/colorpicker.js'
import 'vendor/colorpicker/colorpicker.css'
import 'vue-tour/dist/vue-tour.css'

import 'vendor/redactor2/redactor.js'
import 'vendor/redactor2/plugins/table.js'
import 'vendor/redactor2/plugins/imagemanager.js'
import 'vendor/redactor2/plugins/source.js'
import 'vendor/redactor2/custom-plugins/alignment.js'
import 'vendor/redactor2/custom-plugins/fontsize.js'
import 'vendor/redactor2/custom-plugins/fontfamily.js'
import 'vendor/redactor2/custom-plugins/fontcolor.js'
import 'vendor/redactor2/custom-plugins/cleanformat.js'
import 'vendor/redactor2/custom-plugins/lineheight.js'
import 'vendor/redactor2/custom-plugins/personalization.js'
import 'vendor/redactor2/custom-plugins/snippet.js'
import 'vendor/redactor2/custom-plugins/optimove.js'
import 'vendor/redactor2/custom-plugins/emojis.js'
import 'vendor/redactor2/redactor.css'
import 'vendor/redactor2/custom-plugins/redactor.css'

import '@/font/OpenSans/stylesheet.css'
import '@/font/Fontello/fontello.css'
import '@/font/Platform/platform.css'
import '@/font/Mobile/mobile.css'
import '@/font/Xtremepush/style.css'

import '@/css/global.css'
import '@/css/layout.css'
import '@/css/deprecated.less'
import '@/css/margin-padding.sass'
import '@/css/main.scss'
import '@/jquery/vue'
import '@/jquery/translation.js'
import '@/jquery/layout.js'
import '@/jquery/widgets/richAutocomplete'
import '@/jquery/widgets/general'

import '@/core/app'

import Raven from 'raven-js'

// eslint-disable-next-line no-undef
if (process.env.RAVEN_DSN) {
  // eslint-disable-next-line no-undef
  Raven.config(process.env.RAVEN_DSN).install()
}

let hasTheme =
  document.getElementsByTagName('BODY')[0].className.split(' ')[1] !== ''
if (hasTheme) {
  let themeName = document
    .getElementsByTagName('BODY')[0]
    .className.split(' ')[1]

  import(`@/css/themes/${themeName}.scss`)
}
