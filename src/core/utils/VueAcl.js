import store from '@/core/store/index'

export function $can(permission) {
  return store.getters.permissions.includes(permission)
}

export function $hasFeature(feature) {
  return store.state.app.features ? store.state.app.features[feature] : false
}

export function $isAdmin() {
  if (!store.state.app.user) {
    return false
  }

  return [
    'admin',
    'admin-support',
    'admin-viewer',
    'provider-admin',
    'provider-admin-viewer'
  ].includes(store.state.app.user.role)
}

export function $isSuperAdmin() {
  if (!store.state.app.user) {
    return false
  }

  return ['admin', 'admin-support', 'admin-viewer'].includes(
    store.state.app.user.role
  )
}

export default {
  install(Vue) {
    Vue.mixin({
      methods: {
        $can,
        $isRole(role) {
          if (!store.state.app.user) {
            return false
          }
          return store.state.app.user.role === role
        }
      }
    })
  }
}
