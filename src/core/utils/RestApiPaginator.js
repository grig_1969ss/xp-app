import RestApi from '@/core/services/RestApi'
import axios from 'axios'

export class RestApiPaginator {
  constructor(payload) {
    this.payload = { ...payload }
  }

  setPayload(payload) {
    this.payload = { ...payload }
  }

  setPage(payload, page) {
    let newPayload = JSON.parse(JSON.stringify(payload))
    newPayload.params.page = page
    return newPayload
  }

  async fetchAllPages() {
    return this.fetchPages({ start: 1, limit: null })
  }

  async fetchPage(page) {
    let payload = this.setPage(this.payload, page)
    this.cancelSource = axios.cancelToken
    return RestApi(payload)
  }

  async fetchPages({ start, limit = 1 }) {
    let page = start
    let fetchData = []
    let pageCount = 0

    do {
      let payload = this.setPage(this.payload, page)
      this.cancelSource = axios.cancelToken

      let response = await RestApi(payload)

      pageCount = response.headers['x-pagination-page-count'] || 1
      limit = limit || pageCount

      fetchData = fetchData.concat(response.data)
      page++
    } while (page <= pageCount && page <= limit)

    this.cancelSource = null

    return fetchData
  }

  cancel() {
    if (this.cancelSource) {
      this.cancelSource.cancel()
    }
  }

  isCancel(e) {
    return RestApi.isCancel(e)
  }
}
