import Vue from 'vue'
import VueI18n from 'vue-i18n'
import RestApi from '@/core/services/RestApi'
import memoizee from 'memoizee'
import axios from 'axios/index'

Vue.use(VueI18n)

const i18n = new VueI18n({
  silentTranslationWarn: true
})

export async function bootstrapTranslations(language) {
  i18n.locale = language

  const translations = await fetchTranslations(language)
  i18n.setLocaleMessage(language, translations)

  i18n._missing = memoizee(
    (language, message) =>
      RestApi.post('/translations', {
        category: 'vue',
        message: message,
        language: language
      }),
    { promise: true, maxAge: 5000 }
  )
}

async function fetchTranslations(language) {
  let messages = {}
  await axios.get(`/translations/vue.${language}.json`).then(data => {
    messages = data.data
  })

  return messages
}

export default i18n
