import Vue from 'vue'
import vClickOutside from 'v-click-outside'
import ElementUI from 'element-ui'
import VCalendar from 'v-calendar'
import locale from 'element-ui/lib/locale/lang/en'
import Gravatar from '@/common/components/Gravatar'
import Icon from '@/common/components/Icon'
import VueAcl from '@/core/utils/VueAcl'
import i18n from '@/core/utils/I18n'
import router from '@/core/router'
import store from '@/core/store'
import TopBar from '@/common/components/navigation/TopToolbar'
import Layout from '@/common/components/Layout'
import LeftNav from '@/common/components/navigation/LeftNavigation'
import ElSwitch from '@/common/components/element/ElSwitch'
import ElRow from '@/common/components/element/ElRow'
import ElTable from '@/common/components/element/ElTable'
import ElForm from '@/common/components/element/ElForm'
import VueTour from 'vue-tour'
import FlagIcon from 'vue-flag-icon'
import Zendesk from '@/core/utils/Zendesk'

import appBootstrap from '@/core/app-bootstrap'
Vue.use(VueTour)
Vue.use(vClickOutside)
Vue.use(ElementUI, { locale })
Vue.use(VueAcl)
Vue.use(VCalendar, {
  componentPrefix: 'vc'
})
Vue.use(FlagIcon)

Vue.use(Zendesk, {
  key: '44230901-c116-4a57-9634-93375d7c329d',
  disabled: false,
  hideOnLoad: true,
  settings: {
    webWidget: {}
  }
})

Vue.component('gravatar', Gravatar)
Vue.component('icon', Icon)
Vue.component('top-bar', TopBar)
Vue.component('layout', Layout)
Vue.component('left-nav', LeftNav)

Vue.component('el-table', ElTable)
Vue.component('el-switch', ElSwitch)
Vue.component('el-row', ElRow)
Vue.component('el-form', ElForm)

Vue.config.productionTip = false

appBootstrap().then(() => {
  window.vue = new Vue({
    // eslint-disable-line no-new
    el: '#app',
    template: '#app-template',
    router,
    i18n,
    store,
    mounted() {
      // We emit an event to which non vue components can hook in to if needed
      window.dispatchEvent(new Event('vue-ready'))
    }
  })
})
