import { bootstrapTranslations } from '@/core/utils/I18n'
import { $can } from '@/core/utils/VueAcl'
import store from '@/core/store'

export default async function appBootstrap() {
  await store.dispatch('fetchConfig')

  if (location.pathname === '/' && !$can('viewDashboard')) {
    location.href = store.state.app.home
    return new Promise()
  }

  if (store.state.app.user && $can('viewProject')) {
    await store.dispatch('fetchProjects')
  }

  let language = 'en'
  if (store.state.app.user && store.state.app.user.language) {
    language = store.state.app.user.language
  } else if (store.state.app.provider.language) {
    language = store.state.app.provider.language
  }

  await bootstrapTranslations(language)
}
