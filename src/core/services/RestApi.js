import axios from 'axios'
import i18n from '@/core/utils/I18n'

let RestApi = axios.create({
  baseURL: xp.restUrl, // eslint-disable-line
  headers: {
    Accept: 'application/json',
    Authorization: `Bearer ${xp.accessToken || ''}` // eslint-disable-line
  }
})

RestApi.interceptors.response.use(
  response => {
    return response
  },
  error => {
    error = addErrorMessage(error)
    return Promise.reject(error)
  }
)

const addErrorMessage = error => {
  return {
    ...error,
    ...{ message: errorMessage(error) }
  }
}

const errorMessage = error => {
  if (error.response && error.response.data.message) {
    return error.response.data.message
  } else if (error.response) {
    switch (error.response.status) {
      case 500:
        return i18n.t('RestApi_Error_Internal')
      case 422:
        if (error.response.data.errors) {
          return error.response.data.errors[0].message
        } else {
          return i18n.t('RestApi_Error_InvalidRequest')
        }
      case 404:
        return i18n.t('RestApi_Error_NotFound')
      case 403:
        return i18n.t('RestApi_Error_Forbidden')
      case 401:
        return i18n.t('RestApi_Error_Unauthorized')
    }
  } else if (error.request) {
    return i18n.t('RestApi_Error_NoResponse')
  } else {
    return i18n.t('RestApi_Error_Unknown')
  }
}

export default RestApi
