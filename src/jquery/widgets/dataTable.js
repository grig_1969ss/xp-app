import { ajaxLoaderShow, ajaxLoaderHide } from './ajaxLoader'
import { randomString, escapeHTML, addCommas } from '@/jquery/utils'
import { twig } from 'vendor/twig/twig'

export default function (settings, inherit) {

    var that = this;

    this.table = null;

    this.data = settings.data;
    this.data_source = settings.data_source;
    this.method = settings.method;
    this.options = settings.options;

    this.row_sort = settings.row_sort;
    this.row_sort_type_backend = settings.row_sort_type_backend;
    this.row_sort_default_column = settings.row_sort_default_column;
    this.row_sort_default_order = settings.row_sort_default_order;
    this.row_sort_force = settings.row_sort_force;

    this.row_search = settings.row_search;
    this.row_search_type_backend = settings.row_search_type_backend;
    this.row_search_type_custom = settings.row_search_type_custom;
    this.row_search_save = settings.row_search_save;

    this.pagination = settings.pagination;
    this.pagination_pages = settings.pagination_pages;
    this.pagination_show_more = settings.pagination_show_more;
    this.pagination_per_page = settings.pagination_per_page || 25;

    this.columns = settings.columns;
    this.columns_groups = settings.columns_groups;
    this.columns_headers = settings.columns_headers || true;
    this.row_template_path = settings.row_template_path;
    this.row_template_value = settings.row_template_value;
    this.row_template_params = settings.row_template_params || {};

    this.row_selection = settings.row_selection;
    this.row_selection_on_select_rows = settings.row_selection_on_select_rows;
    this.row_selection_on_deselect_rows = settings.row_selection_on_deselect_rows;
    this.row_selection_selected_ids = settings.row_selection_selected_ids || [];

    this.row_click = settings.row_click;
    this.empty_label = settings.empty_label;
    this.on_build_table = settings.on_build_table;
    this.auto_layout = settings.auto_layout;

    this.settings = settings;


    this.init = function() {
        this.table = this.settings.element;

        this.initTable();

        this.initTemplates();

        this.initSearch();

        this.initSort();

        this.initSelection();

        this.initEmptyLabel();

        this.initPagination();
    };


    this.load = function() {
        if (this.data_source) {
            ajaxLoaderShow(function(){
                that.dataRequest(
                    function(data)
                    {
                        that.data = data;
                        that.buildTable();
                        ajaxLoaderHide();
                    }
                );
            });
        } else if (this.data) {
            that.buildTable();
        }
    };

    this.buildTable = function() {
        if(this.data) {
            this.processData();
            this.renderTable();

            if (this.row_search && !this.row_sort_type_backend && !this.row_search_save) {
                this.clearSearch();
            }

            if (typeof this.on_build_table == 'function') {
                this.on_build_table();
            }
        }
    };

    this.processData = function () {
        if (!(this.data.data === undefined)) {
            this.data = this.data.data;
        }

        if (typeof this.data == 'object') {
            var array_data = [];
            var index_key = 0;
            for (var index in this.data) {
                var item = this.data[index];
                if (item !== null) {
                    item.index = index;
                    item.index_key = index_key++;
                    array_data.push(item);
                }
            }
            this.data = array_data;
        }

        if (this.pagination_limit) {
            this.pagination_show_more_visible = false;
            if (this.data.length > this.pagination_limit) {
                this.pagination_show_more_visible = true;
                delete this.data[this.data.length - 1];
            }
        }

        if (this.row_search) {
            if (!this.row_search_type_backend) {
                if (this.row_search_save) {
                    this.frontendSearch();
                }
            }
        }

        if (this.row_sort) {
            if (!this.row_sort_type_backend && (this.row_sort_default_column != this.row_sort_column || this.row_sort_default_order != this.row_sort_order || this.row_sort_force)) {
                this.frontendSort();
            }
        }
    };

    this.renderTable = function() {
        var tbody = $("<tbody></tbody>");
        for (var row_index in this.data) {
            if (this.data[row_index].visible === undefined || this.data[row_index].visible) {
                tbody.append(this.renderRow(this.data[row_index]));
            }
        }
        this.table.find("tbody").replaceWith(tbody);
        this.renderEmptyLabel();
        this.renderShowMore();
    };

    this.renderRow = function(item) {
        if (this.row_template_path) {
            var params = this.row_template_params;
            params.item = item;
            return this.row_template.render(params);
        }

        else if (that.row_template_value) {
            return item[this.row_template_value];
        }

        else {
            var row = $("<tr></tr>");
            row.attr('data-id', this.getRowId(item));
            for (var col_index in this.columns) {
                row.append(this.renderCell(item, col_index));
            }
            if (this.row_click) {
                row.addClass('clickable').click(function(){
                    var item = that.getRowById($(this).data('id').toString());
                    that.row_click(item);
                });
            }
            return row;
        }
    };

    this.renderCell = function(item, col_index) {
        var cell = $("<td></td>");
        if (this.columns[col_index].checkbox) {
            var id = randomString(10);
            var checked = ($.inArray(this.getRowId(item), this.row_selection_selected_ids) !== -1) ? 'checked="checked"' : '';
            cell.html('<input type="checkbox" id="'+id+'" class="small" '+checked+'/> <label class="checkbox small" for="'+id+'"></label>');
            cell.addClass("checkboxColumn");
        } else {
            cell.html(this.getCellValue(item, col_index, 'display'));
        }
        if (this.columns[col_index].class) {
            cell.addClass(this.columns[col_index].class);
        }
        if (this.columns[col_index].hint) {
            cell.attr('title', this.columns[col_index].hint);
        }
        return cell;
    };

    this.getRowId = function(item) {
        if (item.id === undefined) {
            return item.index.toString();
        } else {
            return item.id.toString();
        }
    };

    this.getCellValue = function(item, col_index, value_type) {
        if (value_type == 'sort') {
            if (this.columns[col_index].sort_value) {
                if (typeof this.columns[col_index].sort_value == 'function') {
                    return this.columns[col_index].sort_value(item);
                } else {
                    return item[this.columns[col_index].sort_value];
                }
            } else if (this.columns[col_index].type == 'number' || this.columns[col_index].type == 'percent') {
                return parseFloat(item[this.columns[col_index].value].toString().replace(',',''));
            }
        }

        else if (value_type == 'search') {
            if (this.columns[col_index].search_value) {
                if (typeof this.columns[col_index].search_value == 'function') {
                    return this.columns[col_index].search_value(item);
                } else {
                    return item[this.columns[col_index].search_value];
                }
            }
        }

        if (typeof this.columns[col_index].value == 'function') {
            return this.columns[col_index].value(item);
        }

        if (item[this.columns[col_index].value] === null) {
            return t('all', 'Common__Na');
        }

        if (this.columns[col_index].type == 'number') {
            return escapeHTML(addCommas(item[this.columns[col_index].value]));
        }

        if (this.columns[col_index].type == 'percent') {
            return parseFloat(item[this.columns[col_index].value]).toFixed(2)+'%';
        }

        return escapeHTML(item[this.columns[col_index].value]);
    };

    this.initTable = function() {
        var table = $("<table></table>");
        var thead = $("<thead></thead>");
        var tbody = $("<tbody></tbody>");
        var tfoot = $("<tfoot></tfoot>");

        if (this.auto_layout) {
            table.css('table-layout', 'auto');
        }

        table.append(thead).append(tbody).append(tfoot);
        this.table.append(table);
        this.renderTableHeaders();
    };

    this.renderTableHeaders = function() {
        if (this.columns_headers) {
            if (this.columns) {
                this.table.find('thead .tableHeaders').remove();

                var tr = $("<tr class='tableHeaders'></tr>");

                for (var col_index in this.columns) {
                    if (!this.columns_groups || this.columns[col_index].group) {
                        var th = this.renderTableHeaderCell(col_index);
                        tr.append(th);
                    }
                }
                this.table.find('thead').prepend(tr);

                if (this.columns_groups) {
                    var tr = $("<tr class='tableHeaders'></tr>");
                    for (var col_group_index in this.columns_groups) {
                        var th = this.renderTableHeaderGroupCell(col_group_index);
                        tr.append(th);
                    }
                    this.table.find('thead').prepend(tr);
                }
            }
        }
    };

    this.renderTableHeaderGroupCell = function(col_group_index) {
        if (!this.columns_groups[col_group_index].name && this.columns_groups[col_group_index].columns.length == 1) {
            var th = this.renderTableHeaderCell(this.columns_groups[col_group_index].columns[0]);
            th.attr('rowspan', 2);
            return th;
        } else {
            var th = $("<th></th>");
            th.attr('colspan', this.columns_groups[col_group_index].columns.length);
            th.css('text-align', 'center');
            if (this.columns_groups[col_group_index].name) {
                th.html(this.columns_groups[col_group_index].name);
            }
            if (this.columns_groups[col_group_index].width) {
                th.css('width', this.columns_groups[col_group_index].width);
                if (this.auto_layout) {
                    th.css('min-width', this.columns_groups[col_group_index].width);
                }
            }
            if (this.columns_groups[col_group_index].min_width) {
                th.css('min-width', this.columns_groups[col_group_index].min_width);
            }
            return th;
        }
    };

    this.renderTableHeaderCell = function(col_index) {
        var th = $("<th></th>");
        if (this.columns[col_index].checkbox) {
            th.html('<div class="checkCheck checkAll"><div class="cbox cbox1"></div><div class="cbox cbox2"></div></div><div class="checkCheck checkNone"><div class="cbox cbox1"></div><div class="cbox cbox2"></div></div>');
            th.addClass("checkboxHeader");
        }
        if (this.columns[col_index].name) {
            th.html(this.columns[col_index].name);
        }
        if (this.columns[col_index].width) {
            th.css('width', this.columns[col_index].width);
            if (this.auto_layout) {
                th.css('min-width', this.columns[col_index].width);
            }
        }
        if (this.columns[col_index].min_width) {
            th.css('min-width', this.columns[col_index].min_width);
        }
        if (this.columns[col_index].class) {
            th.addClass(this.columns[col_index].class);
        }
        if (this.columns[col_index].hint) {
            th.attr('title', this.columns[col_index].hint);
        }
        th.addClass('sortHeader');
        th.attr('data-index', col_index);
        return th;
    };


    this.reload = function() {
        this.load();
    };

    this.reloadByOptions = function(options) {
        this.options = options;
        this.reload();
    };


    this.requestLink = function() {
        var requestLink = API_URL + this.data_source;
        var requestData = this.requestData();

        var first = true;
        if (requestLink.indexOf('?')>=0) {
            first = false;
        }
        for (var i in requestData) {
            if (first) requestLink += '?'; else requestLink += '&'; first = false;
            requestLink += i + '=' + encodeURI(requestData[i]);
        }

        return requestLink;
    };

    this.requestData = function() {
        var requestData = {};

        if (this.options) {
            $.each(this.options, function(index,value) {
                if (value != null) {
                    requestData[index] = value;
                }
            });
        }

        var sort = this.backendSortColumn();
        if (sort) {
            requestData['order'] = sort;
        }

        var search = this.backendSearchKeyword();
        if (search) {
            requestData['search'] = search;
        }

        var limit = this.backendLimit();
        if (limit) {
            requestData['limit'] = limit;
        }

        return requestData;
    };

    this.dataRequest = function(doneCallback, errorCallback) {
        var requestLink = this.requestLink();
        console.log(requestLink);

        $.ajax(requestLink).success(function(data){
            console.log(data);
            if (typeof doneCallback == 'function') {
                doneCallback(data);
            }
        }).error(function(){
            if (typeof errorCallback == 'function') {
                errorCallback();
            }
            ajaxLoaderHide();
        });
    };


    this.initTemplates = function() {
        if (this.row_template_path) {
            twig({
                href: this.row_template_path,
                load: function(template) {
                    that.row_template = template;
                },
                async: false
            });
        }
    };

    this.initEmptyLabel = function() {
        if (this.empty_label && !this.table.find('.emptyLabel').length) {
            if (this.empty_label === true) {
                this.empty_label = t("Common__There_is_no_items");
            }
            var colspan = this.table.find('thead th').length;
            this.table.find('tfoot').append("<tr class='emptyLabel' style='display: none'><td colspan='"+colspan+"'>"+this.empty_label+"</td></tr>");
        }
    };

    this.renderEmptyLabel = function() {
        if (this.empty_label) {
            if (!Object.keys(this.data).length) {
                this.table.find('.emptyLabel').show();
            } else {
                this.table.find('.emptyLabel').hide();
            }
        }
    };


    this.initSearch = function() {
        if (this.row_search) {
            var colspan = Object.keys(this.columns).length;
            this.table.find('thead .search_row').remove();
            this.table.find('thead').append("<tr class='search_row'><td colspan='"+colspan+"' class='searchColumn'><input placeholder='"+t("Analytics__Type_to_search")+"'><i class='icon-search'></i></td></tr>");
            this.table.find('thead .searchColumn input').on('change, keyup', function(e){
                $(this).parent().removeClass('searchColumnActive');
                var keyword = $(this).val();
                if (that.row_search_type_backend) {
                    if (that.row_search_timer) {
                        clearTimeout(that.row_search_timer);
                    }
                    that.row_search_timer = setTimeout(function() {
                        that.search(keyword);
                    }, 800);
                } else {
                    that.search(keyword);
                }
            });
            this.table.find('thead .searchColumn input').on('focus, click', function() {
                $(this).select();
            });
        }
    };

    this.search = function(keyword) {
        this.row_search_keyword = keyword.replace('#','').toLowerCase();
        if (this.row_search_type_backend) {
            this.reload();
        } else {
            this.frontendSearch();
            this.renderTable();
        }
    };

    this.frontendSearch = function() {
        for (var row_index in this.data) {
            if (this.row_search_keyword) {
                this.data[row_index].visible = false;
                for (var col_index in this.columns) {
                    if (this.columns[col_index].search) {
                        if (this.getCellValue(this.data[row_index], col_index, 'search').toLowerCase().indexOf(this.row_search_keyword) >= 0) {
                            this.data[row_index].visible = true;
                            break;
                        }
                    }
                }
            } else {
                this.data[row_index].visible = true;
            }
        }
    };

    this.clearSearch = function() {
        this.row_search_keyword = "";
        this.table.find('.searchColumn input').val("");
    };

    this.backendSearchKeyword = function() {
        if (this.row_search && this.row_search_type_backend) {
            return this.row_search_keyword;
        } else {
            return null;
        }
    };


    this.initSort = function() {
        if (this.row_sort) {
            for (var col_index in this.columns) {
                if (this.columns[col_index].sort) {
                    this.table.find('.sortHeader[data-index="'+col_index+'"]').addClass('sortable').click(function(){
                        var column = $(this).data('index');
                        var order;
                        if (column != that.row_sort_column) {
                            order = (that.columns[column].sort_default === undefined) ? 'asc' : that.columns[column].sort_default;
                        } else {
                            order = (that.row_sort_order == 'asc') ? 'desc' : 'asc';
                        }
                        that.sort(column, order);
                    });
                }
            }
        }

        if (!(this.row_sort_default_column===undefined)) {
            this.row_sort_column = this.row_sort_default_column;
        }
        if (!(this.row_sort_default_order===undefined)) {
            this.row_sort_order = this.row_sort_default_order;
        }
        this.updateSortControls();
    };

    this.sort = function(column, order) {
        this.row_sort_column = column;
        this.row_sort_order = order;
        this.updateSortControls();

        if (this.row_sort_type_backend) {
            this.reload();
        } else {
            this.frontendSort();
            this.renderTable();
        }
    };

    this.frontendSort = function() {
        this.data.sort(function(a, b) {
            var val1 = that.getCellValue(a, that.row_sort_column, 'sort');
            var val2 = that.getCellValue(b, that.row_sort_column, 'sort');

            if (that.row_sort_order == 'desc') {
                val2 = [val1, val1 = val2][0];
            }

            if (that.columns[that.row_sort_column].type == 'string') {
                return val1.localeCompare(val2);
            }
            else if (that.columns[that.row_sort_column].type == 'number' || that.columns[that.row_sort_column].type == 'percent') {
                val1 = parseFloat(val1);
                val2 = parseFloat(val2);
                if (val1 > val2) return 1;
                else if (val1 < val2) return -1;
                else return 0;
            }
            else {
                return 0;
            }
        });
    };

    this.updateSortControls = function() {
        var order_title = (this.row_sort_order == 'asc') ? 'up' : 'down';
        this.table.find('.sortHeader .sortIcon').remove();
        this.table.find('.sortHeader[data-index="'+this.row_sort_column+'"]').append("<div class='sortIcon'><div class='icon-"+order_title+"-dir'></div></div>");
    };

    this.backendSortColumn = function() {
        if (this.row_sort && this.row_sort_type_backend) {
            var column = this.columns[this.row_sort_column].sort_value ?
                this.columns[this.row_sort_column].sort_value :
                this.columns[this.row_sort_column].value;
            return column + ' ' + this.row_sort_order;
        } else {
            return null;
        }
    };


    this.initPagination = function() {
        if (this.pagination) {
            if (this.pagination_show_more) {
                this.pagination_limit = this.pagination_per_page;
                this.initShowMore();
            }
        }
    };

    this.initShowMore = function() {
        var colspan = Object.keys(this.columns).length;
        this.table.find('table tfoot').append("<tr class='showMore' style='display: none'><td colspan='"+colspan+"'><i class='icon-down'></i>Show More</td></tr>");
        this.table.find('tfoot .showMore').click($.proxy(function(){
            this.showMore();
        }, this));
    };

    this.renderShowMore = function() {
        if (this.pagination_show_more_visible) {
            this.table.find('tfoot .showMore').show();
        } else {
            this.table.find('tfoot .showMore').hide();
        }
    };

    this.showMore = function() {
        this.pagination_limit += this.pagination_per_page;
        this.reload();
    };

    this.backendLimit = function() {
        if (this.pagination) {
            if (this.pagination_show_more) {
                if (this.pagination_limit) {
                    return this.pagination_limit + 1;
                }
            }
        }
        return null;
    };


    this.initSelection = function() {
        if (this.row_selection) {
            $(document).on('click', this.table.find('.checkAll').selector, function() {
                that.selectAllVisibleRows();
            });

            $(document).on('click', this.table.find('.checkNone').selector, function() {
                that.deselectAllRows();
            });

            $(document).on('change', this.table.find('.checkboxColumn input').selector, function(){
                var id = $(this).closest('tr').attr('data-id').toString();
                if ($(this).is(':checked')) {
                    if (that.selectRow(id)) {
                        that.notifySelectRows([id]);
                    }
                } else {
                    if (that.deselectRow(id)) {
                        that.notifyDeselectRows([id]);
                    }
                }
            });
        }
    };

    this.selectRow = function(id) {
        if ($.inArray(id, this.row_selection_selected_ids) === -1) {
            this.row_selection_selected_ids.push(id);
            return true;
        } else {
            return false;
        }
    };
    
    this.deselectRow = function(id) {
        var n = $.inArray(id, this.row_selection_selected_ids);
        if (n !== -1) {
            this.row_selection_selected_ids.splice(n, 1);
            return true;
        } else {
            return false;
        }
    };

    this.selectAllVisibleRows = function() {
        var ids = [];
        this.table.find('.checkboxColumn input').prop('checked', true).each(function(){
            var id = $(this).closest('tr').attr('data-id').toString();
            if (that.selectRow(id)) {
                ids.push(id);
            }
        });
        this.notifySelectRows(ids);
    };

    this.deselectAllRows = function() {
        this.table.find('.checkboxColumn input').prop('checked', false);
        this.row_selection_selected_ids = [];
        this.notifyDeselectRows(null);
    };

    this.notifySelectRows = function(ids) {
        if (this.row_selection_on_select_rows && typeof this.row_selection_on_select_rows === 'function') {
            this.row_selection_on_select_rows(ids);
        }
    };

    this.notifyDeselectRows = function(ids) {
        if (this.row_selection_on_deselect_rows && typeof this.row_selection_on_deselect_rows === 'function') {
            this.row_selection_on_deselect_rows(ids);
        }
    };

    this.getRowById = function(id) {
        for (var row_index in this.data) {
            if (this.getRowId(this.data[row_index]).toString() == id.toString()) {
                return this.data[row_index];
            }
        }
        return null;
    };


    if (!inherit) {
        this.init();
    }

    return this;
};
