export function dropdownToggler (dropdown) {
    var that = this;

    this.element = $(dropdown);

    this.key = this.element.data('key');
    this.disable_key = this.element.data('disable-key');
    this.disable_inputs = this.element.data('disable-inputs');

    $(this.element).change(function(){
        if (that.key) {
            $('.'+that.key).hide();
            $('.'+that.key+' input').prop('disabled', true);
            $('.'+that.key+' select').prop('disabled', true);
        }
        if (that.disable_key) {
            $('.'+that.disable_key).prop('disabled', true);
        }

        var val = $(this).val();
        if (!val) val = "undefined";

        if (that.key) {
            var elk = $('.'+that.key+val);
            if (!elk.length) elk = $('.'+that.key+'placeholder');
            elk.show();
            elk.find('input, input.visible').prop('disabled', false);
            elk.find('select, select.visible').prop('disabled', false);
            elk.find('.dropdownToggler, .dropdownToggler.visible').trigger('change');
        }
        if (that.disable_key) {
            var eldk = $('.'+that.disable_key+val);
            if (!eldk.length) eldk = $('.'+that.disable_key+'placeholder');
            eldk.prop('disabled', false);
        }
    });
}

export function stdPlashes (element) {
    var that = this;

    this.element = $(element);

    this.init = function() {
        this.element.data('stdPlashes', this);

        this.element.find('.stdPlashesColumn.stdPlashesChooseOnClick, .stdPlashesColumn.stdPlashesCheckOnClick').click(function(e){
            if (!$(e.target).parents('.stdPlashesColumnCheckbox').length) {
                var disabled = $(this).find('.stdPlashesColumnCheckbox input').prop('disabled');
                var checked = $(this).find('.stdPlashesColumnCheckbox input').prop('checked');
                if (!disabled && !checked) {
                    that.selectPlash(this);
                }
            }
        });

        this.element.find('.stdPlashesColumn.stdPlashesToggleOnClick').click(function(e){
            if (!$(e.target).parents('.stdPlashesColumnCheckbox').length) {
                var disabled = $(this).find('.stdPlashesColumnCheckbox input').prop('disabled');
                var checked = $(this).find('.stdPlashesColumnCheckbox input').prop('checked');
                if (!disabled) {
                    if (checked) {
                        that.deselectPlash(this);
                    } else {
                        that.selectPlash(this);
                    }
                }
            }
        });

        this.element.find('.stdPlashesColumn.stdPlashesChooseOnClick .stdPlashesColumnCheckbox input').change(function(){
            if ($(this).prop('checked')) {
                that.selectPlash($(this).parents('.stdPlashesColumn').eq(0));
            }
        });

        this.element.find('.stdPlashesColumn.stdPlashesToggleOnClick .stdPlashesColumnCheckbox input, .stdPlashesColumn.stdPlashesCheckOnClick .stdPlashesColumnCheckbox input').change(function(){
            if ($(this).prop('checked')) {
                that.selectPlash($(this).parents('.stdPlashesColumn').eq(0));
            } else {
                that.deselectPlash($(this).parents('.stdPlashesColumn').eq(0));
            }
        });

        this.element.find('.stdPlashesChooseStatusOn').hide();

        var selectedPlashes = this.element.find('.stdPlashesColumn.stdPlashesChooseOnClick .stdPlashesColumnCheckbox input:checked, .stdPlashesColumn.stdPlashesToggleOnClick .stdPlashesColumnCheckbox input:checked, .stdPlashesColumn.stdPlashesCheckOnClick .stdPlashesColumnCheckbox input:checked');
        selectedPlashes.each(function(){
            that.selectPlash($(this).parents('.stdPlashesColumn').eq(0));
        });
    };

    this.selectPlash = function(plash) {
        this.element.find('.stdPlashesColumn.stdPlashesChooseOnClick').each(function(index, eachPlash){
            if ($(eachPlash)[0] != $(plash)[0] && $(eachPlash).find('.stdPlashesColumnCheckbox input').prop('checked')) {
                that.deselectPlash(eachPlash);
            }
        });

        $(plash).find('.stdPlashesColumnCheckbox input').prop('checked', true).trigger('select');
        $(plash).find('.stdPlashesChooseStatusOff').hide();
        $(plash).find('.stdPlashesChooseStatusOn').show();
        $(plash).addClass('stdPlashesChoosed');
    };

    this.deselectPlash = function(plash) {
        $(plash).find('.stdPlashesColumnCheckbox input').prop('checked', false).trigger('deselect');
        $(plash).find('.stdPlashesChooseStatusOn').hide();
        $(plash).find('.stdPlashesChooseStatusOff').show();
        $(plash).removeClass('stdPlashesChoosed');
    };

    this.init();
    return this;
}


export function stdBlock(element) {
    var that = this;

    this.element = $(element);
    this.opened = null;

    this.init = function() {
        if (this.element.hasClass('stdBlockCollapsable'))
        {
            this.opened = !this.element.hasClass('stdBlockHiddenByDefault');

            this.element.find('.stdBlockTitle').click(function(){
                if (that.opened) {
                    that.close();
                } else {
                    that.open();
                }
            });

            if (this.opened) {
                this.open();
            } else {
                this.close();
            }
        }
    };

    this.open = function() {
        this.element.find('.stdBlockTitle i').removeClass('icon-right-dir').addClass('icon-down-dir');
        this.element.find('.stdBlockContent').slideDown();
        this.opened = true;
    };

    this.close = function() {
        this.element.find('.stdBlockTitle i').removeClass('icon-down-dir').addClass('icon-right-dir');
        this.element.find('.stdBlockContent').slideUp();
        this.opened = false;
    };

    this.init();
    return this;
}

export function inputAffix (element) {
    var input = document.createElement('input');
    input.className = 'stdAffix';
    input.readOnly = true;
    input.type = 'text';
    input.style.backgroundColor = '#E0E0E0';
    input.style.width = '35%';
    element.style.width = '65%';
    return input;
}

export function inputSuffix (element) {
    var suffix = inputAffix(element);
    suffix.value = element.getAttribute('data-suffix');
    element.parentNode.insertBefore(suffix, element.nextSibling);
}

export function inputPreffix(element) {
    var preffix = inputAffix(element);
    preffix.value = element.getAttribute('data-preffix');
    element.parentNode.insertBefore(document.createElement('br'), element);
    element.parentNode.insertBefore(preffix, element);
}


$(window).on('vue-ready', function() {
    $('.dropdownToggler').each(function(){
        new dropdownToggler(this);
    });
    $('.dropdownToggler[data-auto-init="1"]').trigger("change");


    $('.stdPlashes').each(function(){
        new stdPlashes(this);
    });

    $('.stdBlock').each(function(){
        new stdBlock(this);
    });

    $('.defaultDatepicker').datepicker({
        dateFormat: "d MM, yy",
        altFormat: "d MM, yy"
    });

    $('input[data-suffix]').each(function(){
        new inputSuffix(this);
    });

    $('input[data-preffix]').each(function(){
        new inputPreffix(this);
    });
});


$(window).on('vue-ready', function() {
    $('.passwordValidator').each(function() {

        var that = this;

        var tip = $('<div class="password-tip"><ul><li>'+t('all', 'User__Password_minimum_8_characters')+'</li><li>'+t('all', 'User__Password_must_contain_uppercase_letters')+'</li><li>'+t('all', 'User__Password_must_contain_lowercase_letters')+'</li><li>'+t('all', 'User__Password_must_contain_numbers')+'</li><li>'+t('all', 'User__Password_must_contain_symbols')+'</li></ul></div>');
        tip.insertAfter($(this));

        // Minimum 8 characters in length
        // Must contain uppercase letters

        $(this).keyup(function() {
            var text = $(this).val();
            tip.find('li').removeClass('active');

            if (text.length >= 8) {
                tip.find('li:eq(0)').addClass('active');
            }

            if (/[A-Z]/.test(text)) {
                tip.find('li:eq(1)').addClass('active');
            }

            if (/[a-z]/.test(text)) {
                tip.find('li:eq(2)').addClass('active');
            }

            if (/[0-9]/.test(text)) {
                tip.find('li:eq(3)').addClass('active');
            }

            if (/[^A-Za-z0-9]/.test(text)) {
                tip.find('li:eq(4)').addClass('active');
            }
        });

        $(this).blur(function() {
            $(that).trigger('keyup');
        });

        $(this).focus(function() {
            $(that).trigger('keyup');
        });
    });
});

$(document).on('change', $('.fake-input-file input[type="file"]').selector, function(event){
    var $inputFile = $(this);
    var $inputfileBox = $inputFile.parent('.fake-input-file');
    var filename = $inputFile.val().split(/(\\|\/)/g).pop();
    $inputfileBox.find('> a').text(filename || 'Click to upload');
});

$('.slider').find('input[type="range"]').on('change',function(){
    var $rangePointer = $(this);
    var $slider = $rangePointer.parent('.slider');
    $slider.find('.rangevalue').val($rangePointer.val());
});
