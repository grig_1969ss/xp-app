import { isEqual } from 'lodash'
export default function($form) {
  this.submitting = false
  this.initialChange = false
  this.init = function() {
    $form.on('submit', () => {
      this.submitting = true
    })
    let initialStateArray = $form.serializeArray().sort(compare)
    $(window).on('beforeunload', () => {
      let isChanged =
        this.initialChange ||
        !isEqual(initialStateArray, $form.serializeArray().sort(compare))
      if (!this.submitting && isChanged) {
        return true
      }
    })
  }
  this.init()
}
function compare(a, b) {
  if (a.name < b.name) {
    return -1
  }
  if (a.name > b.name) {
    return 1
  }
  return 0
}
