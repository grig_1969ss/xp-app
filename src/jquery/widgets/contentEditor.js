import emojis from './emojis'

export default function ($container, options) {
    var that = this;

    var $toolbar,
        $emojisIcon,
        $emojisPopup,
        $emojisMenu,
        $emojisCategory,
        $emojisCategoryTitle,
        $emojisCategoryList,
        $personalizationIcon,
        $personalizationPopup,
        $snippetIcon,
        $snippetPopup,
        $optimoveIcon,
        $optimovePopup
            = null;

    this.emojisCategory = null;

    this.init = function() {
        if (!$container.length) return;

        $toolbar = $('<div class="contentEditorToolbar"></div>');

        if (options.emojis) {
            $container.addClass('emojisEditor');
            this.initEmojisIcon();
            this.initEmojisPopup();
        }

        if (options.personalization) {
            $container.addClass('personalizationEditor');
            this.initPersonalizationIcon();
            this.initPersonalizationPopup();
        }

        if (options.snippets) {
            var html;
            if(options.wysiwyg){
                html = 1;
            }
            $container.addClass('snippetEditor');
            this.initSnippetIcon();
            this.initSnippetPopup(html);
        }

        if (options.optimove) {
            $container.addClass('optimoveEditor');
            this.initOptimoveIcon();
            this.initOptimovePopup();
        }

        this.initWysiwyg();

        $container.data('contentEditor', this);
        $container.addClass('contentEditor');
       if (options.emojis && !options.emojisButton
               || options.personalization && !options.personalizationButton
               || options.snippets && !options.snippetButton) {
            $container.append($toolbar);
       }
    };


    this.initEmojisIcon = function() {
        if (options.emojisButton) {
            $emojisIcon = options.emojisButton;
        } else {
            $emojisIcon = $('<span>☺</span>');
            $toolbar.append($emojisIcon);
        }
        $emojisIcon.mousedown(function(){
            if (options.wysiwyg === 'redactor') {
                $container.find('textarea').eq(0).redactor('selection.save');
            }
        });
        $emojisIcon.click(function(e){
            that.toggleEmojisPopup();
            e.stopPropagation();
            return false;
        });
    };

    this.initEmojisPopup = function() {
        $emojisPopup = $('<div class="emojisPopup"></div>');

        $emojisMenu = $('<div class="emojisMenu"></div>');
        for (var i = 0; i < emojis.length; i++) {
            var $emojisItem = $('<span title="'+emojis[i].title+'" data-category="'+i+'">'+emojis[i].icon+'</span>');
            $emojisItem.addClass('emojisItem');
            $emojisMenu.append($emojisItem);
        }
        $emojisPopup.append($emojisMenu);

        $emojisCategory = $('<div class="emojisCategory"></div>');
        $emojisCategoryTitle = $('<div class="emojisTitle"></div>');
        $emojisCategoryList = $('<div class="emojisList"></div>');
        $emojisCategory.append($emojisCategoryTitle);
        $emojisCategory.append($emojisCategoryList);
        $emojisPopup.append($emojisCategory);

        $emojisMenu.on('click', 'span', function(){
            that.renderEmojisCategory($(this).data('category'));
        });

        $emojisCategoryList.on('click', 'span', function(){
            that.insertTag($(this).text());
            that.closeEmojisPopup();
        });

        $emojisPopup.mousedown(function(e) {
            e.stopPropagation();
        });

        $container.append($emojisPopup);
    };

    this.toggleEmojisPopup = function() {
        if ($emojisPopup.is(':visible')) {
            this.closeEmojisPopup();
        } else {
            this.openEmojisPopup();
        }
    };

    this.openEmojisPopup = function() {
        this.positionPopup($emojisPopup, $emojisIcon);
        $emojisPopup.fadeIn(200);
        $("body").bind('mousedown', this.closeEmojisPopup);
        if (this.emojisCategory === null) {
            this.renderEmojisCategory(0);
        }
    };

    this.closeEmojisPopup = function() {
        $("body").unbind('mousedown', this.closeEmojisPopup);
        $emojisPopup.fadeOut(200);
    };

    this.renderEmojisCategory = function(category) {
        this.emojisCategory = category;
        $emojisCategoryTitle.text(emojis[category].title);
        $emojisCategoryList.html('');
        for (var i = 0; i < emojis[category].icons.length; i++) {
            var $emojisItem = $('<span title="'+emojis[category].icons[i][1]+'">'+emojis[category].icons[i][0]+'</span>');
            $emojisCategoryList.append($emojisItem);
            if ($emojisItem.width() && $emojisItem.width() < 30) {
                $emojisItem.addClass('emojisItem');
            } else {
                $emojisItem.remove();
            }
        }
    };



    this.initPersonalizationIcon = function() {
        if (options.personalizationButton) {
            $personalizationIcon = options.personalizationButton;
        } else {
            $personalizationIcon = $('<i class="icon iconp-user-1"></i>');
            $toolbar.append($personalizationIcon);
        }
        $personalizationIcon.mousedown(function(){
            if (options.wysiwyg === 'redactor') {
                $container.find('textarea').eq(0).redactor('selection.save');
            }
        });
        $personalizationIcon.click(function(e){
            that.togglePersonalizationPopup();
            e.stopPropagation();
            return false;
        });
    };

    this.initPersonalizationPopup = function() {
        $personalizationPopup = $(
            '<div class="personalizationPopup">'+
                '<label class="label1">'+t('all', 'Campaign__Push_select_attribute')+'</label>'+
                '<i class="jsRemoveButton personalizationPopupRemoveButton icon icon-cancel"></i>'+
                '<input type="text" class="jsAutocomplete" />'+
            '</div>');

        $personalizationPopup.find('.jsAutocomplete').tagAutocomplete({
            source: '/api/tag/tag/autocomplete?project_id=' + this.getProjectId() + '&attribute=1',
            searchOnFocus: {
                allowEmptyValue: true,
                allowFilledValue: true
            },
            select: function( event, ui ) {
              let check = ui.item.value.match("^[A-z0-9_]+$");
              if (window.vue.$store.state.app.features.twig) {
                if (check) {
                  that.insertTag('{{ ' + ui.item.value + ' }}');
                } else {
                  that.insertTag('{{ user.attributes["' + ui.item.value + '"] }}');
                }
              }else{
                that.insertTag('{{ ' + ui.item.value + ' }}');
              }
              return false;
            }
        });

        $personalizationPopup.find('.jsRemoveButton').click(function() {
            that.closePersonalizationPopup();
        });

        $personalizationPopup.mousedown(function(e) {
            e.stopPropagation();
        });

        $container.append($personalizationPopup);
    };

    this.togglePersonalizationPopup = function() {
        if ($personalizationPopup.is(':visible')) {
            this.closePersonalizationPopup();
        } else {
            this.openPersonalizationPopup();
        }
    };

    this.openPersonalizationPopup = function() {
        this.positionPopup($personalizationPopup, $personalizationIcon);
        $personalizationPopup.fadeIn(200);
        $personalizationPopup.find('.jsAutocomplete').focus();
        $("body").bind('mousedown', this.closePersonalizationPopup);
    };

    this.closePersonalizationPopup = function() {
        $("body").unbind('mousedown', this.closePersonalizationPopup);
        $personalizationPopup.fadeOut(200);
        setTimeout(function() {
            $personalizationPopup.find('.jsAutocomplete').val('');
        }, 0);
    };

    this.initSnippetIcon = function() {
        if (options.snippetButton) {
            $snippetIcon = options.snippetButton;
        } else {
            $snippetIcon = $('<i class="icon icon-code"></i>');
            $toolbar.append($snippetIcon);
        }
        $snippetIcon.mousedown(function(){
            if (options.wysiwyg === 'redactor') {
                $container.find('textarea').eq(0).redactor('selection.save');
            }
        });
        $snippetIcon.click(function(e){
            that.toggleSnippetPopup();
            e.stopPropagation();
            return false;
        });
    };

    this.initSnippetPopup = function(html) {
        $snippetPopup = $(
            '<div class="snippetPopup">'+
            '<label class="label1">'+t('all', 'Campaign__Push_select_snippet')+'</label>'+
            '<i class="jsRemoveButton snippetPopupRemoveButton icon icon-cancel"></i>'+
            '<input type="text" class="jsAutocomplete" />'+
            '</div>');

        let source = html
            ? '/api/content/snippet/autocomplete?project_id=' + this.getProjectId()
            : '/api/content/snippet/autocomplete?project_id=' + this.getProjectId() + '&type=1';

        $snippetPopup.find('.jsAutocomplete').tagAutocomplete({
            source: source,
            searchOnFocus: {
                allowEmptyValue: true,
                allowFilledValue: true
            },
            select: function( event, ui ) {
                that.insertTag('{{ snippet("'+ ui.item.value +'") }}');
                return false;
            }
        });

        $snippetPopup.find('.jsRemoveButton').click(function() {
            that.closeSnippetPopup();
        });

        $snippetPopup.mousedown(function(e) {
            e.stopPropagation();
        });

        $container.append($snippetPopup);
    };

    this.toggleSnippetPopup = function() {
        if ($snippetPopup.is(':visible')) {
            this.closeSnippetPopup();
        } else {
            this.openSnippetPopup();
        }
    };

    this.openSnippetPopup = function() {
        this.positionPopup($snippetPopup, $snippetIcon);
        $snippetPopup.fadeIn(200);
        $snippetPopup.find('.jsAutocomplete').focus();
        $("body").bind('mousedown', this.closeSnippetPopup);
    };

    this.closeSnippetPopup = function() {
        $("body").unbind('mousedown', this.closeSnippetPopup);
        $snippetPopup.fadeOut(200);
        setTimeout(function() {
            $snippetPopup.find('.jsAutocomplete').val('');
        }, 0);
    };

    this.initOptimoveIcon = function() {
        if (options.optimoveButton) {
            $optimoveIcon = options.optimoveButton;
        } else {
            $optimoveIcon = $('<i class="icon iconp-Optimove-Head"></i>');
            $toolbar.append($optimoveIcon);
        }
        $optimoveIcon.mousedown(function(){
            if (options.wysiwyg === 'redactor') {
                $container.find('textarea').eq(0).redactor('selection.save');
            }
        });
        $optimoveIcon.click(function(e){
            that.toggleOptimovePopup();
            e.stopPropagation();
            return false;
        });
    };

    this.initOptimovePopup = function() {
        $optimovePopup = $(
            '<div class="personalizationPopup">'+
            '<label class="label1">'+t('all', 'Campaign__Optimove_select_attribute')+'</label>'+
            '<i class="jsRemoveButton optimovePopupRemoveButton icon icon-cancel"></i>'+
            '<input type="text" class="jsAutocomplete" />'+
            '</div>');

        $optimovePopup.find('.jsAutocomplete').tagAutocomplete({
            source: '/api/integration/optimove-attribute/autocomplete?project_id=' + this.getProjectId(),
            searchOnFocus: {
                allowEmptyValue: true,
                allowFilledValue: true
            },
            select: function( event, ui ) {
                that.insertTag('{{ optimove.' + ui.item.value + ' }}');
                return false;
            }
        });

        $optimovePopup.find('.jsRemoveButton').click(function() {
            that.closeOptimovePopup();
        });

        $optimovePopup.mousedown(function(e) {
            e.stopPropagation();
        });

        $container.append($optimovePopup);
    };

    this.toggleOptimovePopup = function() {
        if ($optimovePopup.is(':visible')) {
            this.closeOptimovePopup();
        } else {
            this.openOptimovePopup();
        }
    };

    this.openOptimovePopup = function() {
        this.positionPopup($optimovePopup, $optimoveIcon);
        $optimovePopup.fadeIn(200);
        $optimovePopup.find('.jsAutocomplete').focus();
        $("body").bind('mousedown', this.closeOptimovePopup);
    };

    this.closeOptimovePopup = function() {
        $("body").unbind('mousedown', this.closeOptimovePopup);
        $optimovePopup.fadeOut(200);
        setTimeout(function() {
            $optimovePopup.find('.jsAutocomplete').val('');
        }, 0);
    };

    this.positionPopup = function ($popup, $icon) {
        let left = $icon.offset().left - $container.offset().left - $popup.width() / 2;
        if (left + $popup.width() > $container.width()) {
            $popup.css({
                left: 'auto',
                right: '0'
            });
        } else {
            $popup.css({
                left: left + 'px',
                right: 'auto'
            });
        }
    };


    this.insertTag = function(tag) {
        var input = $container.find('input');
        if (input.length) {
            that.insertAtCursor(tag, input[0]);
            input.trigger('change');
        }
        if (options.wysiwyg === 'ckeditor') {
          options.ckeditor.insertHtml(tag);
        } else {
          var textarea = $container.find('textarea').eq(0);
          if (textarea.length) {
            if (options.wysiwyg === 'redactor') {
              textarea.redactor('selection.restore');
              textarea.redactor('insert.text', tag);
            } else {
              that.insertAtCursor(tag, textarea[0]);
              textarea.trigger('change');
            }
          }
        }

    };

    this.insertAtCursor = function(text, el) {
        var val = el.value, endIndex, startIndex, range;
        if (typeof el.selectionStart != 'undefined'
            && typeof el.selectionEnd != 'undefined') {
            startIndex = el.selectionStart;
            endIndex = el.selectionEnd;
            val = val.substring(0, startIndex) + text
                + val.substring(el.selectionEnd);
            el.value = val;
            el.selectionStart = el.selectionEnd = startIndex + text.length;
        } else if (typeof document.selection != 'undefined'
            && typeof document.selection.createRange != 'undefined') {
            el.focus();
            range = document.selection.createRange();
            range.text = text;
            range.select();
        }
    };


    this.initWysiwyg = function() {
        this.updateWysiwyg();
    };

    this.setWysiwyg = function(enabled) {
        options.wysiwyg = enabled;
        this.updateWysiwyg();
    };

    this.updateWysiwyg = function() {
        if (options.wysiwyg) {
            $container.addClass('contentEditorWysiwyg');
        } else {
            $container.removeClass('contentEditorWysiwyg');
        }
    };

    this.getProjectId = function() {
      return window.xp.isSpa ? window.vue.$store.state.app.project.id : window.xp.project.id
    }

    return this.init();
};
