var $ajaxLoaderLayout = $('#ajaxLoaderLayout');
var $ajaxLoader = $('#ajaxLoader');
var ajaxLoaderNum = 0;

export function ajaxLoaderShow (callback) {
  if (!ajaxLoaderNum) {
    $ajaxLoaderLayout.show();
    $ajaxLoader.show();
  }
  ajaxLoaderNum += 1;
  if (callback && typeof(callback) == "function") {
    callback();
  }
}

export function ajaxLoaderHide (callback) {
  ajaxLoaderNum--;
  if (!ajaxLoaderNum) {
    $ajaxLoaderLayout.hide();
    $ajaxLoader.hide();
  }
  if(callback && typeof(callback) == "function") {
    callback();
  }
}
