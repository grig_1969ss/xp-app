import { buildURL } from '@/jquery/utils'

$.widget("custom.richAutocomplete", $.ui.autocomplete, {
    options: {
        params: undefined,
        processItem: undefined,
        relatedChange: undefined,
        combobox: false,
        searchOnFocus: {
            allowEmptyValue: false,
            allowFilledValue: true
        },
        allItem: false,
        noneItem: false
    },
    _valueElement: null,
    _create: function() {
        var that = this;
        if (that.options.params) {
            that.options.source = buildURL(that.options.source, that.options.params);
        }
        that._super();
        if (that.options.searchOnFocus) {
            if (that.options.searchOnFocus.allowEmptyValue) {
                that.options.minLength = 0;
            }
            this.element.focus(function(){
                if (($(this).val() && that.options.searchOnFocus.allowFilledValue) || (!$(this).val() && that.options.searchOnFocus.allowEmptyValue)) {
                    that.search();
                }
            });
        }
        if (that.options.combobox) {
            this._valueElement = $("<input>").attr("type", "hidden").attr("name", this.element.attr("name")).val(this.element.val());
            this._valueElement.insertAfter(this.element);

            this.element.change(function() {
                if (that.element.val() != that.element.data('selected-value')) {
                    that._valueElement.val("");
                    that.element.val("");
                }
            });

            if (this.element.val()) {
                this.element.val(t('all', 'Common__loading...')).prop("readonly", true).addClass("ui-autocomplete-loading");
                $.ajax({
                    type: "POST",
                    url: buildURL(that.options.source, {id: this._valueElement.val()}),
                    success: function(data) {
                        if (data && data.length == 1) {
                            that.element.val(data[0].value);
                        } else {
                            that.element.val(that._valueElement.val());
                        }
                        that.element.data('selected-value', that.element.val());
                        that.element.prop("readonly", false).removeClass("ui-autocomplete-loading");

                        if (data && typeof that.options.relatedChange == 'function') {
                            that.options.relatedChange(data[0].id, that.element);
                        }
                    },
                    error: function() {
                        that.element.val(that._valueElement.val());
                        that.element.data('selected-value', that.element.val());
                        that.element.prop("readonly", false).removeClass("ui-autocomplete-loading");
                    }
                });
            }
        }
    },
    _renderItem: function (ul, item) {
        var $el = $("<li>");
        var $label = $("<div>").text(item.label);
        if (item.tags) {
            var $tags = $("<span>").addClass('tags').text(item.tags);
            $label.append($tags);
        }
        if (item.description) {
            var $description = $("<div>").addClass('description').text(item.description);
            $label.append($description);
        }

        if (item.disabled) {
            $el.addClass("ui-state-disabled");
            $el.append($label);
        } else {
            var $link = $("<a>");
            $link.html($label.html());
            $el.append($link);
        }
        return $el.appendTo(ul);
    },
    _normalize: function(content) {
        if (typeof this.options.processItem == 'function') {
            var newContent = [];
            for (var i=0; i<content.length; i++) {
                var item = this.options.processItem(content[i]);
                if (item) {
                    newContent.push(item);
                }
            }
            content = newContent;
        }
        if (this.options.noneItem && !content.length) {
            content.push({
                label: t('all', 'Autocomplete__No_items_found'),
                disabled: true
            });
        }
        if (this.options.allItem && content.length > 1) {
            content.unshift({
                label: t('all', 'Autocomplete__Add_all'),
                description: t('all', 'Autocomplete__Add_all_{count}_items', {'count': content.length}),
                items: content
            });
        }
        return this._super(content);
    },
    _trigger: function(type, event, ui) {
        if (type == 'select' && this.options.combobox) {
            this._valueElement.val(ui.item.id);
            this.element.data('selected-value', ui.item.value);
        }
        if (type == 'select' && typeof this.options.relatedChange == 'function') {
            this.options.relatedChange(ui.item.id, this.element, true);
        }
        if (type == 'change' && typeof this.options.relatedChange == 'function') {
            this.options.relatedChange(ui.item ? ui.item.id : 0, this.element, true);
        }
        return this._super(type, event, ui);
    }
});

$.widget("custom.comboboxAutocomplete", $.custom.richAutocomplete, {
    options: {
        combobox: true,
        searchOnFocus: {
            allowEmptyValue: true,
            allowFilledValue: true
        }
    }
});

$.widget("custom.tagAutocomplete", $.custom.richAutocomplete, {
    options: {
        params: {
            show_aliases: 0
        },
        searchOnFocus: {
            allowEmptyValue: false,
            allowFilledValue: true
        },
        processItem: function(item) {
            if (item.mode == 'regex') {
                item.disabled = true;
            }
            return item;
        }
    }
});

$.widget("custom.affectedAutocomplete", $.custom.richAutocomplete, {
    options: {
        combobox: true,
        searchOnFocus: {
            allowEmptyValue: true,
            allowFilledValue: true
        },
        relatedChange: function(id, element, clean) {
            let nextElements = $(element).nextAll('.jsEntityRelatedValue');
            let attribute = $(element).data('affect-attribute');
            if (!attribute || !nextElements) {
                return;
            }

            nextElements.each(function(index) {
                if ($(this).hasClass('jsAutocomplete')) {
                    let source = $(this).data('source');
                    let attributeRegex = new RegExp("(" + attribute + "=).+(&?)");
                    if (source.match(attributeRegex)) {
                        source = source.replace(attributeRegex, '$1' + id + '$2');
                    } else {
                        source = buildURL(source, {[attribute]: id});
                    }
                    $(this).data('source', source);
                    $.fn[$(this).data('plugin')].apply($(this), [{source: $(this).data('source')}]);
                }

                if (clean) {
                    $(this).val('').data('selected-value', '');
                }

                if(+id > 0 && (index === 0 || this.value)) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        }
    }
});
