export default function ($field, onChange) {
  let that = this;
  let $input = $field.find('input');
  let $preview = $field.find('span');

  this.init = function () {
    $input.ColorPicker({
      onSubmit: function (hsb, hex) {
        $input.val('#'+hex);
        $input.ColorPickerHide();
        that.updateColor();
      },

      onBeforeShow: function () {
        $(this).ColorPickerSetColor(this.value);
      },

      onChange: function (hsb, hex, rgb, el) {
        $input.val('#'+hex);
        that.updateColor();
      }
    });

    $input.change(function () {
      if (!$input.val().match(/^#[0-9a-f]{6}$/)) {
        $input.val('');
      }

      that.updateColor();
    });

    $preview.click(function () {
      $input.click();
    });

    that.updateColor(true);
  };

  this.updateColor = function (init) {
    let color = $input.val();
    $preview.css('background-color', color);

    if (!init) {
      onChange(color);
    }
  };

  this.init();
}