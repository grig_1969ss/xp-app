export default function (textarea, maxMessages) {
  let that = this;

  this.gsm7bitChars = '@£$¥èéùìòÇ\\nØø\\rÅåΔ_ΦΓΛΩΠΨΣΘΞÆæßÉ !\\"#¤%&\'()*+,-./0123456789:;<=>?¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§¿abcdefghijklmnopqrstuvwxyzäöñüà';

  this.gsm7bitExChar = '\\^{}\\\\\\[~\\]|€';

  this.gsm7bitRegExp = RegExp('^[' + this.gsm7bitChars + ']*$');

  this.gsm7bitExRegExp = RegExp('^[' + this.gsm7bitChars + this.gsm7bitExChar + ']*$');

  this.gsm7bitExOnlyRegExp = RegExp('^[\\' + this.gsm7bitExChar + ']*$');

  this.GSM_7BIT = 'GSM_7BIT';

  this.GSM_7BIT_EX = 'GSM_7BIT_EX';

  this.UTF16 = 'UTF16';

  this.maxMessages = maxMessages === undefined ? 9 : maxMessages;

  this.messageLength = {
    GSM_7BIT: 160,
    GSM_7BIT_EX: 160,
    UTF16: 70
  };

  this.multiMessageLength = {
    GSM_7BIT: 153,
    GSM_7BIT_EX: 153,
    UTF16: 67
  };

  this.count = function (text) {
    let encoding, length, messages, per_message, remaining;
    if (text === undefined) text = textarea.val();
    encoding = this.detectEncoding(text);
    length = text.length;
    if (encoding === this.GSM_7BIT_EX) {
      length += this.countGsm7bitEx(text);
    }
    per_message = this.messageLength[encoding];
    if (length > per_message) {
      per_message = this.multiMessageLength[encoding];
    }
    messages = Math.ceil(length / per_message);
    remaining = (per_message * messages) - length;
    if (remaining === 0 && messages === 0) {
      remaining = per_message;
    }

    let maxCount = that.maxMessages * per_message;
    if (length > maxCount) {
      let smsText = textarea.val().substring(0, maxCount);
      textarea.val(smsText);
    }

    if (messages > 1) {
      return {
        encoding: encoding,
        length: length,
        per_message: per_message,
        per_messages: per_message * messages,
        remaining: remaining,
        messages: messages
      };
    } else {
      return {
        encoding: encoding,
        length: length,
        per_message: per_message,
        per_messages: per_message,
        remaining: remaining,
        messages: messages
      };
    }
  };

  this.detectEncoding = function (text) {
    switch (false) {
      case text.match(this.gsm7bitRegExp) == null:
        return this.GSM_7BIT;
      case text.match(this.gsm7bitExRegExp) == null:
        return this.GSM_7BIT_EX;
      default:
        return this.UTF16;
    }
  };

  this.countGsm7bitEx = function (text) {
    let char2, chars;
    chars = (function () {
      let _i, _len, _results;
      _results = [];
      for (_i = 0, _len = text.length; _i < _len; _i++) {
        char2 = text[_i];
        if (char2.match(that.gsm7bitExOnlyRegExp) != null) {
          _results.push(char2);
        }
      }
      return _results;
    }).call(this);
    return chars.length;
  };

  this.countSms = function (target) {
    let count_sms;
    target = $(target);
    count_sms = function () {
      let count, k, v, _results;
      count = that.count(textarea.val());
      _results = [];
      for (k in count) {
        if (count.hasOwnProperty(k)) {
          v = count[k];
          _results.push(target.find('.' + k).text(v));
        }
      }
      return _results;
    };
    return count_sms();
  };

  return this;
}
