import { getHashParam, setHashParam } from '@/jquery/utils'

const TabContent = function (settings) {
    var that = this;
    var $container = settings.element;
    var $tabsContainer = settings.tabs_element || $container.find('.tabContentTabs');
    var $contentsContainer = settings.contents_element || $container.find('.tabContents');

    this.settings = settings;
    this.tab = settings.default_tab;        // Default selected tab (if there is no saved state tab in url hash)
    this.save_state = settings.save_state;  // Bool - whether to save selected tab in url hash (to use it after page reload)
    this.on_change = settings.on_change;    // Function - on change tab handler
    this.hash_param = settings.hash_param || 'tab';

    /**
     * Add handlers on tab buttons and select default tab
     */
    this.init = function() {
        $container.data('tabContent', this);

        $tabsContainer.on('click', 'input', function(){
            var key = $(this).data('key');
            if (key) {
                that.selectTab(key);
            }
        });

        var default_tab = this.tab;
        var hash_tab = getHashParam(this.hash_param);

        if (this.save_state && hash_tab) {
            if ($tabsContainer.find('input[data-key="'+hash_tab+'"]').length) {
                default_tab = hash_tab;
            }
        }

        if (default_tab) {
            this.selectTab(default_tab);
        }
    };

    /**
     * Select tab by key
     */
    this.selectTab = function(tab) {
        this.tab = tab;

        $tabsContainer.find('input').removeClass('blue').addClass('light-grey').removeClass('tabContentActive');

        $tabsContainer.find('input[data-key="'+this.tab+'"]').removeClass('light-grey').addClass('blue').addClass('tabContentActive');

        if (this.save_state) {
            setHashParam(this.hash_param, tab);
        }

        $contentsContainer.children('.tabContent').addClass('tabContentHidden').removeClass('tabContentActive');

        $contentsContainer.children('.tabContent[data-key="'+this.tab+'"]').removeClass('tabContentHidden').addClass('tabContentActive');

        if (typeof this.on_change == 'function') {
            this.on_change(this.tab);
        }
    };

    this.addTab = function(button, tab) {
        $tabsContainer.append(button);
        $contentsContainer.append(tab);
    };

    this.disableTab = function (tab) {
        $tabsContainer.find('input[data-key="'+tab+'"]').prop('disabled', true);
    };

    this.enableTab = function (tab) {
        $tabsContainer.find('input[data-key="'+tab+'"]').prop('disabled', false);
    };

    this.init();
    return this;
};

(function ($) {
    $.fn.tabContent = function (settings) {
        this.each(function () {
            settings.element = $(this);
            new TabContent(settings);
        });
    };
}(jQuery));

export default TabContent