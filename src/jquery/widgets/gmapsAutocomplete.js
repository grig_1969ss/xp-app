var geocoder;

export function geocode_lookup(value, update, callback) {
    update = typeof update !== 'undefined' ? update : false;
    request = {};
    request['address'] = value;
    geocoder.geocode(request, function(results, status) {
        console.log(results);
        if ((status == google.maps.GeocoderStatus.OK) && results[0] && update) {

            var country = null;
            var country_name = "";
            for (var i in results[0].address_components) {
                if (results[0].address_components[i].types[0] == 'country') {
                    country = results[0].address_components[i].short_name;
                    country_name = results[0].address_components[i].long_name;
                }
            }

            if (typeof callback == 'function') {
                callback(results[0].geometry, country, country_name);
            }
        }
    });
}

export function gmaps_autocomplete_init(selector, callback) {
    geocoder = new google.maps.Geocoder();
    $(selector).autocomplete({
        source: function(request, response) {
            geocoder.geocode({'address': request.term}, function(results, status) {
                response($.map(results, function(item) {
                    return {
                        label: item.formatted_address, // appears in dropdown box
                        value: item.formatted_address, // inserted into input element when selected
                        geocode: item                  // all geocode data: used in select callback event
                    }
                }));
            })
        },
        select: function(event, ui) {
            geocode_lookup(ui.item.value, true, callback);
        }
    });
}


