import RestApi from "@/core/services/RestApi"

export default function(variant) {
  let that = this

  this.init = function () {
    that.dialog = $(variant).find('.jsEmailSendTestDialog').dialog({
      appendTo: '#popupsContainer',
      dialogClass: 'send-test-dialog',
      draggable: false,
      modal:true,
      resizable: false,
      width: 600,
      zIndex: 5000000,
      autoOpen: false
    })

    let userEmail
    if (window.xp.user && (userEmail = window.xp.user.email)) {
      that.dialog.find('input[name=recipient_email]').val(userEmail)
    }

    that.dialog.find('input[name=from_address]').comboboxAutocomplete({
      source: '/email/email-address/autocomplete?project_id=' + window.xp.project.id,
      select: function (event, ui) {
        if (ui.item.name) {
          that.dialog.find('input[name=from_name]').val(ui.item.name)
        }
      }
    })

    that.dialog.find('input[name=render_profile_email]').comboboxAutocomplete({
      source: function(request, response) {
        if (!request.term || request.term.length < 3) {
          return response([])
        }
        $.ajax({
          type: 'get',
          url: '/profile/profile/autocomplete?project_id='+window.xp.project.id,
          data: {
            Profile: {
              email: request.term
            }
          },
          success: (results) => {
            response(results.map((item) => ({
              label: item.email,
              value: item.email,
              id: item.id
            })))
          }
        })
      },
      select: function(event, ui) {
        that.dialog.find('input[name=render_profile_id]').val(ui.item.id)
      }
    })
  }

  this.show = function (emailDetails) {
    this.renderDefaultDetails(emailDetails)

    const closeSend = t('all', 'Campaign__Email_Send_test_close_send')
    const closeCancel = t('all', 'Campaign__Email_Send_test_close_cancel')
    that.dialog.dialog({
      buttons: {
        [closeSend]: function () {
          that.sendTest({
            ...emailDetails,
            subject: that.dialog.find('input[name=subject]').val(),
            from_address: that.dialog.find('input[name=from_address]').val(),
            from_name: that.dialog.find('input[name=from_name]').val(),
            recipient_email: that.dialog.find('input[name=recipient_email]').val(),
            render_profile_id: that.dialog.find('input[name=render_profile_id]').val() || null
          })
        },
        [closeCancel]: () => that.dialog.dialog('close')
      }
    })
    that.dialog.dialog('open')
  }

  this.renderDefaultDetails = function (emailDetails) {
    if (emailDetails.subject) {
      that.dialog.find('input[name=subject]').val(emailDetails.subject)
    }
    if (emailDetails.from_address) {
      that.dialog.find('input[name=from_address]').val(emailDetails.from_address)
    }
    if (emailDetails.from_name) {
      that.dialog.find('input[name=from_name]').val(emailDetails.from_name)
    }
    if (emailDetails.recipient_email) {
      that.dialog.find('input[name=recipient_email]').val(emailDetails.recipient_email)
    }
    if (emailDetails.render_profile_id) {
      that.dialog.find('input[name=render_profile_id]').val(emailDetails.render_profile_id)
    }
  }

  this.sendTest = async function (data) {
    const projectId = window.xp.project.id
    this.hideFieldErrors()

    try {
      that.toggleDisabled(true)
      await RestApi.post(`projects/${projectId}/email-test-send`, data)
      that.dialog.dialog('close')

    } catch (e) {
      that.onError(e)

    } finally {
      that.toggleDisabled(false)
    }
  }

  this.toggleDisabled = function (disabled) {
    that.dialog.dialog('option', 'disabled', disabled)
    const dialogTop = that.dialog.parent('.ui-dialog')
    dialogTop.find('button').button('option', 'disabled', disabled)
  }

  this.onError = function (e) {
    if (e.response && e.response.data && e.response.data.errors && e.response.data.errors.length) {
      let errors = e.response.data.errors

      errors.forEach((error) => {
        if (!that.showFieldError(error)) {
          that.showAlertError(error)
        }
      })

    } else {
      that.showAlertError()
    }
  }

  this.hideFieldErrors = function () {
    that.dialog.find('.inputError p').text('')
  }

  this.showFieldError = function (error) {
    let fieldError
    if (error.field &&
      (fieldError = that.dialog.find(`input[name=${error.field}]`).parent().find('.inputError p')).length
    ) {
      fieldError.text(error.message)
      return true
    }
    return false
  }

  this.showAlertError = function (error) {
    if (error) {
      alert(t('all', 'Campaign__Email_Send_test_error') + ': ' + error.message)
    } else {
      alert(t('all', 'Campaign__Email_Send_test_uknown_error'))
    }
  }

  this.init()
}
