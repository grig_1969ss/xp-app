import { twig } from 'vendor/twig/twig'
import moment from 'vendor/moment/moment-with-langs.js'
import LoadGoogleMaps from 'vendor/gmaps/loader'

export default function () {
    var $conditionsWidget  = null;
    var $conditionsContainer = null;
    var conditionsModelName = null;
    var conditionsModelAttribute = null;
    var conditionsSubHeaderCondition = null;
    var templateHeader = null;
    var templateRow = null;
    var geocoder = null;
    var gmap = null;
    var gmapCircle = null;

    var conditionEntity = {
        operators: [],
        values: [],
        attributes: [],
		    params: [],
        menu: [],
        attributeList: function(attr) {
            var result = [];
            var i = 0;
            for(let atributeIndex in this.attributes) {
                var atribute = this.attributes[atributeIndex];
                for(let index in atribute) {
                    result[i] = {};
                    result[i]['key'] = index;
                    result[i]['name'] = atribute[index];
                }
                i++;
            }
            return result;
        },
        operatorList: function(attr){
            return this.operators[attr];
        },
        valueList: function(attr){
            return this.values[attr];
        },
        paramList: function(attr){
            return this.params[attr];
        }
    };

    this.conditionEntity = conditionEntity

    this.init = function (element, url) {
        $conditionsContainer       = $(element);
        $conditionsWidget          = $conditionsContainer.find('.segmentConditions');
        conditionsModelName        = $conditionsContainer.data('model-name');
        conditionsModelAttribute   = $conditionsContainer.data('model-attribute');
        conditionsSubHeaderCondition = $conditionsContainer.data('sub-header-condition');

        initRowEvents();
        loadAttributeListData(url);
        loadTemplates();
        initEvents();
        initConditions();
        initChoicesContainer();
        updateRelations();
    };

    this.initDOM = function() {
        $conditionsContainer.find('.jsInitable').trigger('change').addClass('jsInitialised');
        $conditionsContainer.find('.conditionRow').filter(':not(.jsInitialised)').each(function(){
            initConditionRow($(this))
            $(this).addClass('jsInitialised');
        });
    };

    var loadAttributeListData = function(url) {
        let searchParams = new URLSearchParams(window.location.search)
        let project_id = searchParams.get('project_id')
        $.ajax(API_URL + url, {
            async: false,
            data: {
                id: $conditionsContainer.data('id'),
                project_id: project_id ? project_id: window.xp.project.id
            }
        }).done(function(responseData) {
            for(let key in responseData.items) {
                var attr = {};
                attr[key] = responseData.items[key]['name'];
                conditionEntity.params[key]    = responseData.items[key]['params'];
                conditionEntity.operators[key] = responseData.items[key]['operators'];
                conditionEntity.values[key]    = responseData.items[key]['values'];
                conditionEntity.attributes.push(attr);
            }
            conditionEntity.menu = responseData.menu;
        });
    };

  var isTemplateLoaded = function(id) {
    return twig({
      ref: id
    })
  }

  var loadTemplates = function() {
    if (
      !isTemplateLoaded(
        `templateConditionHeader_${conditionsModelName}_${conditionsModelAttribute}`
      )
    ) {
      twig({
        id: `templateConditionHeader_${conditionsModelName}_${conditionsModelAttribute}`,
        href:
          VIEWS_DIR + '/segment/views/segment/templates/condition-header.twig',
        async: false,
        load: function(template) {
          templateHeader = template
        }
      })
    }
    let loaded_template = isTemplateLoaded(
      `tempateConditionRow_${conditionsModelName}_${conditionsModelAttribute}`
    )
    if (!loaded_template) {
      twig({
        id: `tempateConditionRow_${conditionsModelName}_${conditionsModelAttribute}`,
        href:
          VIEWS_DIR + '/segment/views/segment/templates/condition-item.twig',
        async: false,
        load: function(template) {
          templateRow = template
        }
      })
    } else {
      templateRow = loaded_template
    }
  }

    var initEvents = function() {
        $(document).on('change', $conditionsContainer.find('.sectionHeader input').selector, function() {
            $conditionsContainer.find('.segmentConditions').removeClass('relations_and').removeClass('relations_or');
            if ($conditionsContainer.find('.sectionHeader input:checked').val() === 'AND') {
                $conditionsContainer.find('.segmentConditions').addClass('relations_and');
            } else {
                $conditionsContainer.find('.segmentConditions').addClass('relations_or');
            }
        });

        $(document).on('change', $conditionsContainer.find('.jsConditionOperator').selector, function() {
            var $conditionBlock = $(this).closest('.conditionWrapper');
            $conditionBlock.removeClass('relations_and').removeClass('relations_or');
            if ($conditionBlock.find('.jsConditionOperator:checked').val() === 'AND') {
                $conditionBlock.addClass('relations_and');
            } else {
                $conditionBlock.addClass('relations_or');
            }
        });

        $(document).on('click', $conditionsContainer.find('.jsAddConditionBlock').selector, function() {
            if($conditionsWidget.find('.conditionWrapper').length < 10) {
                addConditionBlock();
            }
            if ($conditionsWidget.find('.conditionWrapper').length >= 10) {
                $conditionsContainer.find('.jsAddConditionBlock').attr('disabled', true);
            }
        });

        $(document).on('click', $conditionsContainer.find('.jsRemoveConditionBlock').selector, function() {
            var $conditionBlock = $(this).closest('.conditionWrapper');
            removeConditionBlock($conditionBlock);
            $conditionsContainer.find('.jsAddConditionBlock').attr('disabled', false);
        });

        $(document).on('click', $conditionsContainer.find('.jsAddCondition').selector, function() {
            var $conditionBlock = $(this).closest('.conditionWrapper');
            if ($conditionBlock.find('.conditionRow').length < 10) {
                addConditionRow($conditionBlock);
            }
            if ($conditionBlock.find('.conditionRow').length >= 10) {
                $conditionBlock.find('.jsAddCondition').attr('disabled', true);
            }
        });

        $(document).on("click", $conditionsContainer.find('.jsDeleteConditionRow').selector, function() {
            var removeBtn = $(this)
              .closest('.condition-row-wrapper')
              .find('.remove-row')
            if (removeBtn.length > 0){
              removeBtn.click()
            }else {
              var $conditionBlock = $(this).closest('.conditionWrapper')
              var $conditionRow = $(this).closest('.conditionRow')
              removeConditionRow($conditionRow)
              $conditionBlock.find('.jsAddCondition').attr('disabled', false)
            }
        });

        $(document).on('change', $conditionsContainer.find('.jsEntityName').selector, function() {
            var $conditionRow   = $(this).closest('.conditionRow');
            var $conditionBlock = $conditionRow.closest('.conditionWrapper');
            var attribute       = $conditionRow.find('.jsEntityName').val();

            renderConditionRow($conditionBlock, $conditionRow, attribute);
        });

        $(document).on('change', $conditionsContainer.find('.jsEntityCompare').selector, function() {
            var $conditionRow   = $(this).closest('.conditionRow');
            var $entityValue = $conditionRow.find('.jsEntityValue');
            var $entityRelated = $conditionRow.find('.jsEntityRelatedValue');
            if ($(this).val() == 'not set' || $(this).val() == 'is set' || $(this).val() == 'unattributed') {
                $entityValue.hide();
                $entityRelated.hide();
            } else {
                $entityValue.show();
                $entityRelated.show();
            }
        });

        $(document).on('change', $conditionsContainer.find('.jsEntityAdditionalOperatorCompare').selector, function() {
            let $conditionRow   = $(this).closest('.conditionRow');
            let $entityValue = $conditionRow.find('.jsEntityAdditionalOperatorValue');
            let $betweenEntityValues = $conditionRow.find('.jsEntityAdditionalOperatorValueBetween');
            let $entityHidden = $entityValue.data('hidden');
            let $additional2 = $conditionRow.find('.jsEntityAdditionalTwoWrapper');

            $entityHidden = $.merge(
                ['not set', 'is set', 'date is', 'contains item'],
                $.isArray($entityHidden) ? $entityHidden : [],
            );
            if ($(this).val() === 'between' || $(this).val() === 'not between'){
                $betweenEntityValues.show().find('input').prop('disabled',false);
                $entityValue.hide().prop('disabled',true);
            }else {
                $betweenEntityValues.hide().find('input').prop('disabled',true);
                if ($.inArray($(this).val(), $entityHidden) !== -1) {
                    $entityValue.hide().prop('disabled',true);
                } else {
                    $entityValue.show().prop('disabled',false);
                }
            }

            if ($(this).val() === 'contains item with property' || $(this).val() === 'contains item') {
                $additional2.show().find('input, select').prop('disabled', false);
                $additional2.find('.jsEntityAdditionalTwoOperatorCompare').trigger('change');
            } else {
                $additional2.hide().find('input, select').prop('disabled', true);
                $additional2.find('.jsEntityAdditionalTwoOperatorCompare').trigger('change');
            }
        });

        $(document).on('change', $conditionsContainer.find('.jsEntityAdditionalTwoOperatorCompare').selector, function() {
            var $conditionRow   = $(this).closest('.conditionRow');
            let $betweenEntityValues = $conditionRow.find('.jsEntityAdditionalTwoOperatorValueBetween');
            var $entityValue = $conditionRow.find('.jsEntityAdditionalTwoOperatorValue');
            if ($(this).is(":visible")) {
                if (
                    $(this).val() === 'not set' ||
                    $(this).val() === 'is set' ||
                    $(this).val() === 'date is' ||
                    $(this).val() === 'between' ||
                    $(this).val() === 'not between'
                ) {
                    $entityValue.hide().prop('disabled',true)
                } else {
                    $entityValue.show().prop('disabled',false)
                }

                if ($(this).val() === 'between' || $(this).val() === 'not between'){
                    $betweenEntityValues.show().prop('disabled',false)
                }else {
                    $betweenEntityValues.hide().prop('disabled',true)
                }
            }

            if ($(this).find('option[value="date is"]').length) {
                if ($(this).val() === 'date is' && !$(this).prop('disabled')) {
                    $conditionRow.find('.jsEntityDateWrapper').show().find('input, select, .input').prop('disabled', false);
                    $conditionRow.find('.jsEntityDateCompare').trigger('change');
                } else {
                    $conditionRow.find('.jsEntityDateWrapper').hide().find('input, select, .input').prop('disabled', true);
                }
            }
        });
    };

    var initRowEvents = function() {
        $(document).on('change', $conditionsContainer.find('.jsAdjustableWidth').selector, function() {
            $(this).width($(this).find("option[value='"+$(this).val()+"']").text().length * 6 + 24);
        });

        $(document).on('change', $conditionsContainer.find('.jsEntityHitCompare').selector, function() {
            var $conditionRow = $(this).closest('.conditionRow');
            if ($(this).val() == '>0' || $(this).val() == '=0') {
                $conditionRow.find('.jsEntityHitValue').hide();
            } else {
                $conditionRow.find('.jsEntityHitValue').show();
            }
        });

        $(document).on('change', $conditionsContainer.find('.jsEntityDateCompare').selector, function() {
            var $conditionRow = $(this).closest('.conditionRow');
            if($(this).val() == 'between') {
                $conditionRow.find('.period').hide().find('input, select').prop('disabled', true);
                $conditionRow.find('.datePicker').show().find('.input').prop('disabled', false);
            } else if($(this).val() == 'within'
                || $(this).val() == 'in_the_last'
                || $(this).val() == 'in_the_next'
                || $(this).val() == 'no_later_than'
                || $(this).val() == 'prior_to'
                || $(this).val() == 'exactly'
                || $(this).val() == 'anniversary_exactly') {
                $conditionRow.find('.period').show().find('input, select').prop('disabled', false);
                $conditionRow.find('.datePicker').hide().find('.input').prop('disabled', true);
                if ($(this).val() == 'prior_to') {
                    $conditionRow.find('.jsEntityPeriodDimensionNormal').prop('disabled', true).hide();
                    $conditionRow.find('.jsEntityPeriodDimensionCalendar').prop('disabled', true).hide();
                    $conditionRow.find('.jsEntityPeriodDimensionUserCalendar').prop('disabled', true).hide();
                    $conditionRow.find('.jsEntityPeriodDimensionAgo').prop('disabled', false).show();
                } else if ($(this).val() == 'exactly') {
                    $conditionRow.find('.jsEntityPeriodDimensionAgo').prop('disabled', true).hide();
                    $conditionRow.find('.jsEntityPeriodDimensionNormal').prop('disabled', true).hide();
                    $conditionRow.find('.jsEntityPeriodDimensionUserCalendar').prop('disabled', true).hide();
                    $conditionRow.find('.jsEntityPeriodDimensionCalendar').prop('disabled', false).show();
                } else if ($(this).val() == 'anniversary_exactly') {
                    $conditionRow.find('.jsEntityPeriodDimensionAgo').prop('disabled', true).hide();
                    $conditionRow.find('.jsEntityPeriodDimensionNormal').prop('disabled', true).hide();
                    $conditionRow.find('.jsEntityPeriodDimensionCalendar').prop('disabled', true).hide();
                    $conditionRow.find('.jsEntityPeriodDimensionUserCalendar').prop('disabled', false).show();
                } else {
                    $conditionRow.find('.jsEntityPeriodDimensionAgo').prop('disabled', true).hide();
                    $conditionRow.find('.jsEntityPeriodDimensionCalendar').prop('disabled', true).hide();
                    $conditionRow.find('.jsEntityPeriodDimensionUserCalendar').prop('disabled', true).hide();
                    $conditionRow.find('.jsEntityPeriodDimensionNormal').prop('disabled', false).show();
                }
            } else {
                $conditionRow.find('.period').hide().find('input, select').prop('disabled', true);
                $conditionRow.find('.datePicker').hide().find('.input').prop('disabled', true);
            }
            $conditionRow.find('.jsEntityPeriodDimension:not(:disabled)').trigger('change');
        });

        $(document).on('change', $conditionsContainer.find('.jsEntityPeriodDimension').selector + ':not(:disabled)', function() {
            var $conditionRow = $(this).closest('.conditionRow');
            if($(this).val() == 'user_calendar_today' || $(this).val() == 'calendar_today') {
                $conditionRow.find('.jsEntityPeriodValue').hide().val('');
            } else {
                $conditionRow.find('.jsEntityPeriodValue').show();
            }
        });

        $conditionsContainer.find('.jsInitable').filter(':not(.jsInitialised)').trigger('change').addClass('jsInitialised');
    };

    var initConditions = function() {
        $conditionsContainer.find('.sectionHeader input').trigger('change');
        $conditionsContainer.find('.conditionWrapper').each(function(){
            initConditionBlock($(this));
        });
    };

    var initConditionBlock = function ($conditionBlock) {
        $conditionBlock.find('.jsConditionOperator').trigger('change');
        $conditionBlock.find('.conditionRow').each(function(){
            initConditionRow($(this));
        });
    };

    var initConditionRow = function ($conditionRow) {
        $conditionRow.find('.jsEntityHitValue').each(function() {
            $(this).numeric({negative: false, decimal: false});
        });

        $conditionRow.find('.jsDatePicker').each(function() {
            if ($(this).closest('.datePicker').find('.jsEntityDate').prop('disabled')) {
                return;
            }

            $(this).dateRangePicker({
                format: 'YYYY-MM-DD',
                separator: ' to ',
                getValue: function() {
                    var $detepickerBox = $(this).closest('.datePicker');
                    return $detepickerBox.find('input.jsEntityDate').val();
                },
                setValue: function(s) {
                    var $detepickerBox = $(this).closest('.datePicker');
                    var dateStart = s.split('to')[0];
                    var dateEnd = s.split('to')[1];
                    dateStart = moment(dateStart).format('DD MMM YYYY');
                    dateEnd = moment(dateEnd).format('DD MMM YYYY');
                    $detepickerBox.find('span.jsDatePicker').text(dateStart + ' to '+ dateEnd);
                    $detepickerBox.find('input.jsEntityDate').val(s);
                }
            });
        });

        $conditionRow.find('.jsAutocomplete').each(function(){
            $.fn[$(this).data('plugin')].apply($(this), [{
                source: $(this).data('source')
            }]);
        });

        $conditionRow.find('.jsCoordsPicker').each(function() {
            var self = this;
            $(self).click(function(event) {
                self.blur();
                initLocationCoordinates(event, self);
                initGmapSearch();
            });
        });

        $conditionRow.find('.jsInitable').filter(':not(.jsInitialised)').trigger('change').addClass('jsInitialised');

        $conditionRow.find('.jsFileDownload').click(function () {
            location.href = '/segment/segment/download-file?project_id=' + window.xp.project.id + '&file=' + $(this).closest('.conditionRow').find('.jsFilePath').val()
        })

        $conditionRow.find('.jsFile').change(function() {
            var fd = new FormData();
            var files = this.files[0];
            fd.append('file', files);

            $.ajax({
                url: API_URL + '/segment/segment/upload-file?project_id=' + window.xp.project.id,
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                async: false,
                success: response => {
                    $(this).closest('.conditionRow').find('.jsFilePath').val(response.data.file);
                },
                error: () => {
                    $(this).val('')
                }
            });
        });
    };


    var addConditionBlock = function () {
        var conditionBlockNumber = $conditionsWidget.find('.conditionWrapper').length;
        var $conditionOuterBlock = $('<div class="conditionOuterWrapper"><div class="top-relation relation">&nbsp;</div><div class="bottom-relation relation">&nbsp;</div></div>');
        var $conditionBlock = $('<div class="conditionWrapper" data-number="'+conditionBlockNumber+'"></div>').data('number', conditionBlockNumber);

        var $conditionBlockHeader = templateHeader.render({
            modelName: conditionsModelName,
            modelAttribute: conditionsModelAttribute,
            subHeaderCondition: conditionsSubHeaderCondition,
            gLoop: conditionBlockNumber,
            subcondition: "AND"
        });
        $conditionBlock.append($conditionBlockHeader);
        $conditionOuterBlock.append($conditionBlock);

        $conditionsWidget.append($conditionOuterBlock);
        $conditionsWidget.append('<div class="conditionRelation"><span class="conditionAnd">' + t("all", "Common__And") + '</span><span class="conditionOr">' + t("all", "Common__Or") + '</span></div>');

        initConditionBlock($conditionBlock);
        addConditionRow($conditionBlock);
    };

    var removeConditionBlock = function ($conditionBlock) {
        $conditionBlock = $conditionBlock.parent();
        $conditionBlock.next('.conditionRelation').remove();
        $conditionBlock.remove();
        reorganizeConditions();
        updateRelations();
    };

    var addConditionRow = function ($conditionBlock) {
        renderConditionRow($conditionBlock, null, '');
        updateRelations();
    };

    var removeConditionRow = function ($conditionRow) {
        var $conditionBlock = $conditionRow.closest('.conditionWrapper');
        $conditionRow.next('.conditionRelation').remove();
        $conditionRow.remove();
        reorganizeConditionBlock($conditionBlock);
        updateRelations();
    };

    var renderConditionRow = function ($conditionBlock, $conditionRow, attribute) {
        var conditionRowNumber = $conditionBlock.find('.conditionRow').length;
        if ($conditionRow) conditionRowNumber = $conditionRow.data('number');

        var $newConditionRow = $(templateRow.render({
            gLoop: $conditionBlock.data('number'),
            iLoop: conditionRowNumber,
            model: conditionEntity,
            modelName: conditionsModelName,
            modelAttribute: conditionsModelAttribute,
            subHeaderCondition: conditionsSubHeaderCondition,
            subcondition: [attribute],
            showAllTime: $conditionsWidget.data('all-time'),
            js: true
        }));

        if($conditionRow === null) {
            $conditionBlock.append($newConditionRow);
        } else {
            $conditionRow.next('.conditionRelation').remove();
            $conditionRow.replaceWith($newConditionRow);
        }

        initConditionRow($newConditionRow);
    };


    var reorganizeConditions = function () {
        $conditionsWidget.find('.conditionWrapper').each(function(index, value) {
            var $conditionBlock = $(value);
            $conditionBlock.data('number', index).attr('data-number', index);

            $conditionBlock.find('.jsConditionOperator').each(function(){
                $(this).prop('name', conditionsModelName+'['+conditionsModelAttribute+']['+index+'][operator]');
                $(this).prop('id', 'conditions_'+conditionsModelName+'_'+conditionsModelAttribute+'_'+index+'_operator_'+$(this).data('operator'));
            });

            $conditionBlock.find('.jsConditionOperatorLabel').each(function(){
                $(this).prop('for', 'conditions_'+conditionsModelName+'_'+conditionsModelAttribute+'_'+index+'_operator_'+$(this).data('operator'));
            });

            reorganizeConditionBlock($conditionBlock);
        });
    };

    var reorganizeConditionBlock = function ($conditionBlock) {
        $conditionBlock.find('.conditionRow').each(function(index, value) {
            var $conditionRow = $(value);
            $conditionRow.data('number', index).attr('data-number', index);
			$conditionRow.find('select, input').each(function(){
                var matches = $(this).prop('name').match(/(\d+)/g);
				if (matches) {
					matches[0] = $conditionBlock.data('number');
					matches[1] = index;
                    $(this).prop('name', conditionsModelName+'['+conditionsModelAttribute+']['+matches.join('][')+']');
				}
			});
        });
    };

    var initChoicesContainer = function () {
        $(document).on('mousedown keydown', $conditionsContainer.find('.jsEntityName').selector, function (e) {
            let new_segment = false
            if ($(this).closest('.condition-row-wrapper').length > 0) {
              new_segment = true
            }
            if (e.keyCode !== 9 && e.keyCode !== 16){
              e.preventDefault();
            }
            if (e.type === 'mousedown' ||(e.type === 'keydown' &&
              (e.keyCode === 13 || e.keyCode === 40))) {
                toggleChoicesContainer($(this),new_segment);
            }
        });

        $(document).on('mousedown', function(e) {
            if (!$(e.target).hasClass('jsEntityName') &&
              $(e.target).parents('.choices-container').length === 0) {
              $conditionsContainer.find('.choices-container').fadeOut(200);
            }
        });

        var $choicesContainer = $conditionsContainer.find('.choices-container');

        $choicesContainer.find('.jsRemoveButton').mousedown(function() {
            $choicesContainer.fadeOut(200);
        });

        $choicesContainer.mousedown(function(event) {
            event.stopPropagation();
        });

        $choicesContainer.find('a').click(function(e) {
            if ($(this).hasClass('choice-final')) {
                $choicesContainer.data('entity').val($(this).data('value')).trigger('change');
                $choicesContainer.fadeOut(200);
            } else {
                $choicesContainer.find('.main-choices').hide();
                $choicesContainer.find('.' + $(this).data('target')).show();
            }
        });
    };

    var toggleChoicesContainer = function ($entity,is_new_segment) {
        var $choicesContainer = $conditionsContainer.find('.choices-container');
        if ($choicesContainer.is(':visible')) {
            $choicesContainer.data('entity', null);
            $choicesContainer.fadeOut(200);
        } else {
          if (is_new_segment){
            var $choicesContainer = $entity.closest('.condition-row-wrapper').find('.choices-container');
            var top = $entity.closest('.conditionRow').position().top + $entity.closest('.conditionRow').height() + 3;
             $choicesContainer.data('entity', $entity);
             $choicesContainer.find('.stdColumns').hide();
            $entity.closest('.main-choices').show();
             $choicesContainer.css('top', top + 'px').css('left', '25px');
             $choicesContainer.fadeIn(200);
             $choicesContainer.focus()
          }else {
            var top = $entity.closest('.conditionOuterWrapper').position().top + $entity.closest('.conditionRow').position().top + $entity.closest('.conditionRow').height() + 3;
            $choicesContainer.data('entity', $entity);
            $choicesContainer.find('.stdColumns').hide();
            $choicesContainer.find('.main-choices').show();
            $choicesContainer.css('top', top + 'px').css('left', '25px');
            $choicesContainer.fadeIn(200);
          }
        }
    };

    var updateRelations = function () {
        var delta = 10;
        var $conditionBlocks = $(document).find('.segmentsWrapper .levels2 .conditionOuterWrapper');

        if ($conditionBlocks.length > 1) {
            $conditionBlocks.each(function(i, elem) {
                var blockHeight = $(elem).height();
                var topRelation = $(elem).find('.top-relation');
                var bottomRelation = $(elem).find('.bottom-relation');
                topRelation.css('height', Math.ceil(blockHeight / 2) + "px")
                    .css('bottom', Math.ceil((blockHeight + delta) / 2) + "px")
                    .show();
                bottomRelation.css('height', Math.ceil(blockHeight / 2) + "px")
                    .css('top', Math.ceil((blockHeight + delta) / 2) + "px")
                    .show();

                if (i == 0) {
                    topRelation.hide();
                } else if (i == $conditionBlocks.length - 1) {
                    bottomRelation.hide();
                }
            });
        } else {
            $conditionBlocks.find('.relation').hide();
        }
    };

    var initLocationCoordinates = function(event, element) {
        event.preventDefault();
        var fieldVal = $(element).val();
        // assert that coordinates have proper format, clear field otherwise.
        if (!fieldVal.match(/^[-+]?[0-9]*\.?[0-9]+;[-+]?[0-9]*\.?[0-9]+;[0-9]+$/)) {
            $(element).val('');
            fieldVal = '';
        }

        var parsed = false;
        if (fieldVal.length) {
            parsed = true;
            var arr = fieldVal.split(';');
            var lat = parseFloat(arr[0]);
            var lng = parseFloat(arr[1]);
            var radius = parseInt(arr[2]);
            var zoom = Math.round(Math.log2((40 * 156543.03392 * Math.cos(lat * Math.PI / 180)) / radius));
        }

        var $modal = $('#popupsContainer').find('#pushGmapsCircleModal');
        if (!$modal.length) {
           $modal = $('#pushGmapsCircleModal');
        }

        $modal.bPopup({
            appendTo: '#popupsContainer',
            opacity: 0.9,
            onOpen: function() {
                $modal.find('.modalContent').html(
                    '<div id="gmaps-circle" style="margin: 10px; width: 800px; height: 500px;"></div>'
                );

                setTimeout(function () {
                    LoadGoogleMaps(function (google) {
                        gmap = new google.maps.Map(document.getElementById("gmaps-circle"), {
                            zoom: parsed ? zoom : 1,
                            center: parsed ? { lat: lat, lng: lng } : { lat: 0, lng: 0 }
                        });

                        gmapCircle = new google.maps.Circle({
                            map: parsed ? gmap : null,
                            radius: parsed ? radius : null,
                            center: parsed ? { lat: lat, lng: lng } : null,
                            fillOpacity: 0.35,
                            fillColor: '#FF0000',
                            strokeColor: '#FF0000',
                            strokeOpacity: 0.5,
                            strokeWeight: 1,
                            editable: true,
                            draggable: true
                        });

                        var gmapClickTimeout;

                        gmap.addListener('click', function (e) {
                            gmapClickTimeout = setTimeout(function() {
                                setGmapCircleCenter(e.latLng.lat(), e.latLng.lng());
                            }, 300);
                        });

                        gmap.addListener('dblclick', function (e) {
                            clearTimeout(gmapClickTimeout);
                        });

                        gmapCircle.addListener('bounds_changed', function () {
                            $(element).val(gmapCircle.getCenter().lat().toFixed(5) + ';' + gmapCircle.getCenter().lng().toFixed(5) + ';' + parseInt(gmapCircle.getRadius()));
                        });
                    });
                }, 0);
            }
        });
    };

    var initGmapSearch = function() {
        LoadGoogleMaps(function (google) {
            geocoder = new google.maps.Geocoder();

            $('#gmaps-search').autocomplete({
                source: function (request, response) {
                    geocoder.geocode({ 'address': request.term }, function (results) {
                        response($.map(results, function (item) {
                            return {
                                label: item.formatted_address,
                                value: item.formatted_address,
                                geocode: item
                            }
                        }));
                    })
                },
                select: function (event, ui) {
                    geocodeLookup({ 'address': ui.item.value }, function (g, address, country, country_name) {
                        gmap.setCenter({ lat: g.location.lat(), lng: g.location.lng() });
                        gmap.setZoom(10);
                        gmapCircle.setCenter({ lat: g.location.lat(), lng: g.location.lng() });
                        gmapCircle.setRadius(10000);
                        gmapCircle.setMap(gmap);
                    });
                }
            });
        }, { libraries: ['geometry'] });
    };

    var geocodeLookup = function(request, callback) {
        geocoder.geocode(request, function(results, status) {
            if (results[0]) {
                var country = null;
                var country_name = "";
                for (var i in results[0].address_components) {
                    if (results[0].address_components[i].types[0] == 'country') {
                        country = results[0].address_components[i].short_name;
                        country_name = results[0].address_components[i].long_name;
                    }
                }

                if (typeof callback == 'function') {
                    callback(results[0].geometry, results[0].formatted_address, country, country_name);
                }
            } else {
                if (typeof callback == 'function') {
                    callback(request.latLng, "", "", "");
                }
            }
        });
    };

    var setGmapCircleCenter = function (lat, lng) {
        var bounds = gmap.getBounds();

        var center = bounds.getCenter();
        var ne = bounds.getNorthEast();

        var r = 6371.0 * 1000 / 4;

        var lat1 = center.lat() / 57.2958;
        var lon1 = center.lng() / 57.2958;
        var lat2 = ne.lat() / 57.2958;
        var lon2 = ne.lng() / 57.2958;

        var dis = r * Math.acos(Math.sin(lat1) * Math.sin(lat2) +
            Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));

        gmapCircle.setCenter({ lat: lat, lng: lng });
        gmapCircle.setRadius(dis);
        gmapCircle.setMap(gmap);
    }
};
