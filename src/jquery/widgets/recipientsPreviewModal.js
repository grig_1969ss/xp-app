import TabContent from '@/jquery/widgets/tabContent';

export default function () {
  let that = this

  let $previewModal;

  this.init = function (content, onlyShow) {
    $previewModal = $('#pushRecipientsModal')

    if (onlyShow) {
      this.initPreviewModal()
    } else {
      this.initElements(content)
    }
  };

  this.initElements = function (content) {
    $previewModal.find('.modalContent').html(content)
    this.initPreviewTabs()
    this.initToggleColumns($previewModal.find('.modalContent .tabContents .tabContent'))
    this.initPreviewAttributes()
    this.initPreviewModal()
  };

  this.initPreviewModal = function () {
    $previewModal.show()
    $previewModal.bPopup({
      appendTo: '#popupsContainer',
      opacity: 0.9,
      modalClose: false,
      onOpen: function () {
        setTimeout(function () { $(window).trigger('resize') }, 300);
      }
    })
  }

  this.initPreviewTabs = function () {
    new TabContent({
      element: $('#CampaignPreviewList_TabContent'),
      default_tab: $previewModal.find('.tabContentTabs.buttonSet td:first-child > input').data('key')
    })
    $previewModal.find('.jsTogglePreviewTable').on('click', function () {
      $(window).trigger('resize')
    })
  };

  this.initPreviewAttributes = function () {
    $previewModal.find('.checkbox-list-title').each(function () {
      $(this).nextAll().hide()
    })
    $previewModal.find('.jsPreviewAttributesToggle').on('click', function (event) {
      event.preventDefault()
      $(this).closest('.checkbox-list-title').nextAll().toggle()
    })
  };

  this.initToggleColumns = function (tabs) {
    $(tabs).each(function () {
      let table = $(this).find('.preview-table')
      let checkboxesContainer = $(this).find('.checkboxes-container')
      let configIcon = table.find('th i.select-columns')
      that.initToggleElements(checkboxesContainer, configIcon, table)
    })

    $('.tabContent').on('pjax:success', function () {
      let table = $(this).find('.preview-table')
      let checkboxesContainer = $(this).find('.checkboxes-container')
      let configIcon = table.find('th i.select-columns')

      that.initToggleElements(checkboxesContainer, configIcon, table)
    })
  };

  this.initToggleElements = function (checkboxesContainer, configIcon, table) {
    $('body').click(function () {
      checkboxesContainer.hide()
    })

    configIcon.click(function (e) {
      that.toggleColumnsListBlock(e.clientY, checkboxesContainer);
      e.stopPropagation()
    })

    checkboxesContainer.click(function (e) {
      e.stopPropagation()
    })
    checkboxesContainer.find('label.checkbox').click(function () {
      setTimeout(function () {
        that.updateTableColumns(true, checkboxesContainer, configIcon, table)
      }, 50)
    })
    this.updateTableColumns(false, checkboxesContainer, configIcon, table)
  };

  this.toggleColumnsListBlock = function (top, checkboxesContainer) {
    if (checkboxesContainer.is(':visible')) {
      checkboxesContainer.hide()
    } else {
      let height = window.innerHeight - top
      checkboxesContainer.show()
      checkboxesContainer.css('top', top + 'px').css('right', '20px').css('max-height', height + 'px')
    }
  };

  this.updateTableColumns = function (cookie, checkboxesContainer, configIcon, table) {
    let $checkboxes = checkboxesContainer.find('input[type=checkbox]');
    let enabledColumns = []
    $checkboxes.each(function (i, v) {
      let checkboxId = $(v).attr('id').split('_')[0]
      let columnSelector = '.' + checkboxId
      if ($(v).is(':checked')) {
        table.find(columnSelector).css('display', '')
        enabledColumns.push(checkboxId)
      } else {
        table.find(columnSelector).css('display', 'none')
      }
    })
    configIcon.parents('th').css('width', '40px')
    table.show()

    $(window).trigger('resize')

    if (cookie) {
      this.setEnabledColumnsCookie(enabledColumns, checkboxesContainer)
    }
  };

  this.setEnabledColumnsCookie = function (enabledColumns, checkboxesContainer) {
    let date = new Date()
    date.setTime(date.getTime() + (24 * 60 * 60 * 1000))
    let expires = '; expires=' + date.toGMTString()
    document.cookie = encodeURIComponent(checkboxesContainer.data('key')) + '='
      + encodeURIComponent(JSON.stringify(enabledColumns))
      + expires + ';path=/;secure'
  };
};
