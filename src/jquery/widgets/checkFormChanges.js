export default function ($form) {
  this.submitting = false
  this.initialChange = false

  this.init = function () {
    $form.on('submit', () => {
      this.submitting = true
    })

    let initialState = $form.serialize()
    $(window).on('beforeunload', () => {
      let isChanged = (this.initialChange || initialState !== $form.serialize())
      if (!this.submitting && isChanged) {
        return true
      }
    })
  }

  this.init()
}
