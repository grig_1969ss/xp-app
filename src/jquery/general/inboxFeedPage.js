import TabContent from '@/jquery/widgets/tabContent'

export default function () {
  this.init = function () {
    new TabContent({
      element: $('#PushInboxIndex_TabContent'),
      default_tab: 'active',
      save_state: true
    });

    $('.jsInboxMessagePreview').vue();

    $('#w0').on('pjax:complete', function () {
      $('.jsInboxMessagePreview').vue();
    });

    $('#w1').on('pjax:complete', function () {
      $('.jsInboxMessagePreview').vue();
    });

    return this;
  };
};
