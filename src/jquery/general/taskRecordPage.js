import RecipientsPreviewModal from '@/jquery/widgets/recipientsPreviewModal';

export default function () {

  let $taskRecordContainer;

  this.init = function () {
    $taskRecordContainer = $('#jsTaskRecordPage')

    $taskRecordContainer.on('click', '.jsPreviewListLink', function (event) {
      event.preventDefault()
      let taskId = $taskRecordContainer.data('id')
      $.ajax({
        url: API_URL + '/push/campaign-preview/view?project_id=' + window.xp.project.id + '&task_id=' + taskId,
        success: function (result) {
          let previewRecipientsModal = new RecipientsPreviewModal()
          previewRecipientsModal.init(result)
        }
      })
    })
  };
};