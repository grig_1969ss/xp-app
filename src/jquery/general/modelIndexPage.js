export default function () {

    var self = this;
    var $table,
        $checkboxesContainer,
        $configIcon
            = null;

    this.init = function () {
        $table = $("#model-table");
        $checkboxesContainer = $("#checkboxes-container");
        $configIcon = $table.find('th i.select-columns');

        this.initElements();
        $("body").on("pjax:success", function(){
            $table = $("#model-table");
            $checkboxesContainer = $("#checkboxes-container");
            $configIcon = $table.find('th i.select-columns');

            self.initElements();
        });
    };

    this.initElements = function() {
        $('body').click(function() {
            $checkboxesContainer.hide();
        });

        $configIcon.click(function(e) {
            self.toggleColumnsListBlock(e.clientY);
            e.stopPropagation();
        });

        $checkboxesContainer.click(function(e) {
            e.stopPropagation();
        });
        $checkboxesContainer.find('label.checkbox').click(function() {
            setTimeout(function() {
                self.updateTableColumns(true);
            }, 50);
        });
        this.updateTableColumns(false);
    };

    this.toggleColumnsListBlock = function(top) {
        if ($checkboxesContainer.is(':visible')) {
            $checkboxesContainer.hide();
        } else {
            $checkboxesContainer.show();
            $checkboxesContainer.css('top', top+'px').css('right', '20px');
        }
    };

    this.updateTableColumns = function(cookie) {
        var $checkboxes = $checkboxesContainer.find('input[type=checkbox]');
        var enabledColumns = [];
        $checkboxes.each(function (i, v) {
            var columnSelector = '.' + $(v).attr('id');
            if ($(v).is(':checked')) {
                $table.find(columnSelector).css('display', '');
                enabledColumns.push($(v).attr('id'));
            } else {
                $table.find(columnSelector).css('display', 'none');
            }
        });
        $configIcon.parents('th').css('width', '40px');
        $table.show();

        $(window).trigger('resize');

        if (cookie) {
            this.setEnabledColumnsCookie(enabledColumns);
        }
    };

    this.setEnabledColumnsCookie = function(enabledColumns) {
        var date = new Date();
        date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
        document.cookie = encodeURIComponent($checkboxesContainer.data('key')) + '='
            + encodeURIComponent(JSON.stringify(enabledColumns))
            + expires + ';secure';
    };
};