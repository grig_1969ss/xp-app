export default function () {

    var $appSettingsContainer = null;

    this.init = function () {
        $appSettingsContainer = $('#appSettingsPage');
        this.initElements();
    };

    this.initElements = function() {
        $appSettingsContainer.find('#Project_ga_certificate_key').change(function(){
            var password = prompt(t("AccountProject__Certificate_password"), "notasecret");
            if (!password) password = "";
            $appSettingsContainer.find('#Project_ga_certificate_password').val(password);
        });
    };
};