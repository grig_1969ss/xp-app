import Vue from 'vue'
import ContentEditor from '@/jquery/widgets/contentEditor'

export default function () {
    const that = this
    const TYPE_TEXT = 1
    const TYPE_HTML = 2

    let $container, $contentInput

    this.init = function () {
        $container = $("#createSnippetContentBlock");
        $contentInput = $container.find('.jsContentInput')

        that.initEditor();

        if(!$container.data('manage')) {
            $container.find('input, select, textarea').prop('disabled', true).addClass('viewable')
        }
    };

    this.initEditor = function () {
        const type = parseInt($container.find('.jsType').val())
        switch (type) {
            case TYPE_TEXT:
                that.initTextEditor()
                break
            case TYPE_HTML:
                that.initHtmlEditor()
                break
        }
    }

    this.initTextEditor = function () {
        new ContentEditor($container.find('.jsContentTextContainer'), {
            personalization: true,
            emojis: true,
            snippets: true
        })
    }

    this.initHtmlEditor = function () {
        const Connector = Vue.extend(Vue.component('ck-editor'))
        that.editor = new Connector({
            el: $container.find('.jsContentEditor')[0],
            propsData: {
                content: $contentInput.val(),
                fullPage: false,
                onChange: () => $contentInput.val(that.editor.updatedContent())
            }
        })
    }
};
