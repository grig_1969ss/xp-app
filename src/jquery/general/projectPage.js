export default function () {
  var $appSettingsContainer = null

  this.init = function () {
    $appSettingsContainer = $('#appSettingsPage')
    this.initElements()
  }

  this.initElements = function () {
    $appSettingsContainer.find('.jsTogglePassword input').click(function () {
      let container = $(this).closest('.jsTogglePassword')
      if (container.find('input').attr('type') === 'password') {
        container.find('span').addClass('icon-lock').removeClass('icon-eye')
        container.find('input').attr('type', 'text')
      }
    })

    $appSettingsContainer.find('.jsTogglePassword span').click(function () {
      let container = $(this).closest('.jsTogglePassword')
      if (container.find('input').attr('type') === 'password') {
        container.find('span').addClass('icon-lock').removeClass('icon-eye')
        container.find('input').attr('type', 'text')
      } else {
        container.find('span').addClass('icon-eye').removeClass('icon-lock')
        container.find('input').attr('type', 'password')
      }
    })
  }
}