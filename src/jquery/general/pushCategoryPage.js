import { twig } from 'vendor/twig/twig'

export default function () {
    var that = this;
    var $container,
        twPayloadItem = null;

    var datetimepickerOptions = {
        dateFormat: "yy-mm-dd",
        altFormat: "yy-mm-dd",
        timeFormat: 'hh:mm'
    };

    this.init = function () {
        $container = $("#createPushCategoryContentBlock");

        that.initPayloads();
        that.initButtons();
        that.initMultilanguage();

        if(!$container.data('manage')) {
            $container.find('input, select, textarea').prop('disabled', true).addClass('viewable');
        }
    };

    this.initPayloads = function() {
        if (!twPayloadItem) {
            twPayloadItem = twig({
                id: "payloadListItem",
                href: VIEWS_DIR+"/push/views/push-category/templates/content-payloads-item.twig",
                async: false
            });
        }

        $container.on('click', '.jsAddPayload', function() {
            var index = $container.find('.jsPayload').length;
            var template = twPayloadItem.render({index: index});
            $container.find('.jsPayloads').show().append(template);
        });

        $container.on('click', '.jsDeletePayload', function() {
            var list = $(this).parents('ul');
            $(this).parents('li').eq(0).remove();
            if ($(list).find('li').length == 0) {
                $(list).hide();
            }
            that.updatePayloadsIndexes();
        });
    };

    this.updatePayloadsIndexes = function() {
        var counter = 0;
        $container.find('.jsPayload').each(function() {
            var index = counter++;
            $(this).find('input').each(function() {
                var fieldName = $(this).attr('name');
                fieldName = fieldName.replace(/\[payloads\]\[[0-9]{0,}\]/, '[payloads]['+index+']');
                $(this).attr('name', fieldName);
            });
        });
    };

    this.initButtons = function() {
        $container.find('.jsButtons .jsButton').each(function () {
            that.initButton($(this));
        });

        $container.find('.jsAddButton').click(function() {
            that.addButton();
        });

        $container.on('click', '.jsRemoveButton', function() {
            var $button = $(this).closest('.jsButton');
            that.removeButton($button);
        });

        $container.find('.jsButtons').sortable({
            update: function() {
                that.updateButtonsIndexes();
            }
        });

        that.checkButtonsLimit();
    };

    this.addButton = function () {
        var $baseButtonElement = $container.find('.jsButtonTemplate .jsButton');
        var $newButtonElement = $baseButtonElement.clone();
        $container.find('.jsButtons').append($newButtonElement);

        that.initButton($newButtonElement);
        that.updateButtonsIndexes();
        that.checkButtonsLimit();
    };

    this.removeButton = function ($button) {
        $button.remove();
        that.updateButtonsIndexes();
        that.checkButtonsLimit();
    };

    this.initButton = function($button) {
        $button.find('input, select, textarea').removeAttr("disabled");
    };

    this.updateButtonsIndexes = function() {
        var counter = 0;
        $container.find('.jsButtons .jsButton').each(function() {
            var index = counter++;
            $(this).find('.jsButtonNumber').text(index + 1);
            $(this).find('input, select, textarea').each(function() {
                var fieldName = $(this).attr('name');
                var idName = $(this).attr('id');
                fieldName = fieldName.replace(/\[buttons\]\[[0-9]{0,}\]/, '[buttons]['+index+']');
                idName = idName.replace(/button_[0-9]{0,}/, 'button_'+index);
                $(this).attr('name', fieldName);
                $(this).attr('id', idName);
            });
        });
    };

    this.checkButtonsLimit = function () {
        var limit = ($container.find('.jsType').val() == 'web') ? 2 : 4;
        if ($container.find('.jsButtons .jsButton').length >= limit) {
            $container.find('.jsAddButton').hide();
        } else {
            $container.find('.jsAddButton').show();
        }
    };





    this.initMultilanguage = function() {
        $container.find('.jsMultilanguage').on('select', function(){
            that.enableMultilanguage();
        }).on('deselect', function(){
            that.disableMultilanguage();
        });

        $container.find('.jsAddLanguage').click(function(){
            that.addLanguage();
        });

        $container.find('.jsLanguages .jsLanguage').each(function(){
            that.initLanguageInfo($(this));
        });
    };

    this.enableMultilanguage = function() {
        $container.find('.jsLanguagesContainer').show();

        $container.find('.jsTranslationsContainer').each(function(){
            $(this).find('.jsTranslationDefault').hide();
            $(this).find('.jsTranslations').show();
        });

        this.updateTranslationLanguageDefault();
    };

    this.disableMultilanguage = function() {
        $container.find('.jsLanguagesContainer').hide();

        $container.find('.jsTranslationsContainer').each(function(){
            $(this).find('.jsTranslationDefault').show();
            $(this).find('.jsTranslations').hide();
        });

        this.updateTranslationLanguageDefault();
    };

    this.addLanguage = function() {
        var tmpLanguageInfo = $container.find('.jsLanguageTemplate .jsLanguage');
        var newLanguageInfo = tmpLanguageInfo.clone();
        $container.find('.jsLanguages').append(newLanguageInfo);
        this.initLanguageInfo(newLanguageInfo);
        this.normalizeLanguages();
    };

    this.removeLanguage = function(languageInfo) {
        var code = languageInfo.data('code');
        languageInfo.remove();
        this.normalizeLanguages();
        if (code != '-') this.removeTranslation(code);
    };

    this.updateLanguageCode = function(languageInfo, newCode) {
        var oldCode = languageInfo.data('code');
        if (oldCode == newCode) return;

        languageInfo.attr('data-code', newCode).data('code', newCode);
        languageInfo.find('.jsLanguageCode').text(newCode);

        languageInfo.find('[id^=PushCategoryLanguage_]').each(function(){
            $(this).attr('id', $(this).attr('id').replace('_'+oldCode+'_', '_'+newCode+'_'));
        });
        languageInfo.find('[for^=PushCategoryLanguage_]').each(function(){
            $(this).attr('for', $(this).attr('for').replace('_'+oldCode+'_', '_'+newCode+'_'));
        });

        languageInfo.find('.jsLanguageDefault').val(newCode);

        var newTitle = languageInfo.find('.jsLanguageSelect option[value="'+newCode+'"]').text();
        if (oldCode != '-') this.removeTranslation(oldCode);
        if (newCode != '-') this.addTranslation(newCode, newTitle);
    };

    this.initLanguageInfo = function(languageInfo) {
        languageInfo.find('.jsRemoveLanguage').click(function(){
            that.removeLanguage($(this).parents('.jsLanguage').eq(0))
        });

        languageInfo.find('.jsLanguageSelect').change(function(){
            that.updateLanguageCode(languageInfo, $(this).val());
            that.normalizeLanguages();
        });

        languageInfo.find('.jsLanguageDefault').change(function(){
            that.updateTranslationLanguageDefault();
        });
    };

    this.normalizeLanguages = function() {
        var languages = [];
        $container.find('.jsLanguages .jsLanguage').each(function(){
            languages.push($(this).data('code'));
        });

        $container.find('.jsLanguages .jsLanguage').each(function(){
            var language = $(this).find('.jsLanguageSelect').val();
            $(this).find('.jsLanguageSelect option').each(function(){
                var val = $(this).val();
                if (val != language && val != '-' && $.inArray(val, languages) >= 0) {
                    $(this).prop('disabled', true);
                } else {
                    $(this).prop('disabled', false);
                }
            });

            if (language == '-') {
                $(this).find('.jsLanguageDefault').prop('disabled', true).prop('checked', false);
            } else {
                $(this).find('.jsLanguageDefault').prop('disabled', false);
            }
        });

        if (!$container.find('.jsLanguages .jsLanguageDefault:checked').length) {
            $container.find('.jsLanguages .jsLanguage[data-code!="-"]:first .jsLanguageDefault').prop('checked', true);
        }

        this.updateTranslationLanguageDefault();
    };

    this.addTranslation = function(language, languageTitle) {
        $container.find('.jsTranslationsContainer').each(function(){
            var tmpTranslation = $(this).find('.jsTranslationTemplate .jsTranslation');
            var newTranslation = tmpTranslation.clone();
            that.updateTranslationLanguage(newTranslation, language, languageTitle);
            $(this).find('.jsTranslations').append(newTranslation);
            newTranslation.trigger('init');
        });
    };

    this.removeTranslation = function(language) {
        $container.find('.jsTranslationsContainer').each(function(){
            $(this).find('.jsTranslation[data-language="'+language+'"]').remove();
        });
    };

    this.updateTranslationLanguage = function(translation, newLanguage, newLanguageTitle) {
        var oldLanguage = translation.data('language');
        if (oldLanguage == newLanguage) return;

        translation.attr('data-language', newLanguage).data('language', newLanguage);
        translation.find('.jsLanguageTitle').text(newLanguageTitle);
        translation.find('input, textarea, select').each(function(){
            if ($(this).attr('name')) {
                $(this).attr('name', $(this).attr('name').replace('['+oldLanguage+']', '['+newLanguage+']'));
                $(this).attr('name', $(this).attr('name').replace('['+oldLanguage+'_', '['+newLanguage+'_'));
            }
        });
        translation.find('[id^=PushCategory_]').each(function(){
            $(this).attr('id', $(this).attr('id').replace('_'+oldLanguage+'_', '_'+newLanguage+'_'));
        });
        translation.find('[for^=PushCategory_]').each(function(){
            $(this).attr('for', $(this).attr('for').replace('_'+oldLanguage+'_', '_'+newLanguage+'_'));
        });
    };

    this.updateTranslationLanguageDefault = function() {
        var multiLanguage =  $container.find('.jsMultilanguage').is(':checked');
        var defaultCode = $container.find('.jsLanguages .jsLanguageDefault:checked').val();

        $container.find('.jsTranslationsContainer').each(function(){
            $(this).find('.jsTranslation').removeClass('jsTranslationLanguageDefault');
            $(this).find('.jsTranslationLanguageTitleDefault').hide();

            var translation;
            if (!multiLanguage) {
                translation = $(this).find('.jsTranslationDefault .jsTranslation');
            } else if (defaultCode) {
                translation = $(this).find('.jsTranslations .jsTranslation[data-language="'+defaultCode+'"]');
            }
            if (translation) {
                translation.addClass('jsTranslationLanguageDefault');
                translation.find('.jsTranslationLanguageTitleDefault').show();
            }

            $(this).trigger('update');
        });
    };
};
