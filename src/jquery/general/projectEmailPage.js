export default function () {

    let $emailPage = null;
    let self = this;

    this.init = function () {
        $emailPage = $('#emailPage');
        this.initColorPickers();
    };

    this.initColorPickers = function () {
        let $caller;
        $emailPage.find('span.colorpicker-field').click(function () {
            $(this).siblings('input.colorpicker-field').click();
        });
        $emailPage.find('input.colorpicker-field').ColorPicker({
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).val('#' + hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
                $caller = $(this);
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(200);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(200);
                return false;
            },
            onChange: function (hsb, hex) {
                self.updateColorPickerPreview($caller, hex);
            }
        }).bind('keyup', function () {
            $(this).ColorPickerSetColor(this.value);
            self.updateColorPickerPreview(this);
        }).bind('blur', function () {
            if (!$(this).val().match(/^#[0-9a-f]{6}$/)) {
                $(this).val('');
                self.updateColorPickerPreview(this);
            }
        }).each(function (i, elem) {
            self.updateColorPickerPreview(elem);
        });
    };

    this.updateColorPickerPreview = function ($caller, color) {
        $caller = $($caller);
        if (color === undefined) {
            color = $caller.val();
        }
        if (color.indexOf('#') !== 0 && color.length > 0) {
            color = '#' + color;
        }
        $caller.val(color);
        $caller.next().css('background', color);
    };
};