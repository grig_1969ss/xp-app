export default function () {

    var $appSettingsContainer,
        $currentSection,
        $allSectionContainer,
        $appSettingsLeftMenu,
        $appSettingsForm,
        appSettingsFormAction
            = null;

    this.init = function () {
        var $categoriesPage = $("#appSettingsCategoriesPrioritySection");
        $(document).on('click', $categoriesPage.find('.jsAddCategory').selector, function() {
            var baseCategoryElement = $categoriesPage.find('.jsCategoryTemplate .jsCategory');
            var newCategoryElement = baseCategoryElement.clone();
            $categoriesPage.find('.jsCategories').append(newCategoryElement);
            updateCategoriesIndexes();
            initCategory(newCategoryElement);
        });

        $(document).on('click', $categoriesPage.find('.jsRemoveCategory').selector, function() {
            $(this).closest('.jsCategory').remove();
            updateCategoriesIndexes();
        });

        $(document).on('change', $categoriesPage.find('.jsOptional').selector, function() {
            if ($(this).data('last')) {
                alert(t('PushCategory__Last_category_cant_be_optional'));
                $(this).prop('checked', false);
            }
        });

        updateCategoriesIndexes();

        $categoriesPage.find('.jsCategories .jsCategory').each(function(){
            initCategory($(this));
        });

        $categoriesPage.find('.jsCategories').sortable({
            update: function() {
                updateCategoriesIndexes();
            }
        });

        if(!$categoriesPage.data('manage')) {
            $categoriesPage.find('input, select, textarea').prop('disabled', true).addClass('viewable');
        }
    };

    var updateCategoriesIndexes = function() {
        var $categoriesPage = $("#appSettingsCategoriesPrioritySection");
        $categoriesPage.find('.jsCategories .jsCategory').each(function(){
            var index = $(this).index();
            $(this).find('input,select').each(function(){
                var name = $(this).attr('name');
                if (name) {
                    $(this).attr('name', name.replace(/Category\[[0-9]{0,}\]/, "Category["+index+"]"));
                }
                var id = $(this).attr('id');
                if (id) {
                    $(this).attr('id', id.replace(/category_[0-9]{0,}/, "category_"+index));
                }
            });
            $(this).find('label').each(function(){
                var forid = $(this).attr('for');
                if (forid) {
                    $(this).attr('for', forid.replace(/category_[0-9]{0,}/, "category_"+index));
                }
            });
            $(this).find('.jsCategoryIndex').text(index + 1);
            $(this).find('.priority').val(index + 1);
        });
    };

    var initCategory = function($category) {
        $category.find('input,select').removeAttr('disabled');

        $category.find('.jsAutocomplete').comboboxAutocomplete({
            source: '/api/segment/segment/autocomplete?project_id=' + window.xp.project.id,
        });

        $category.on('change', '.ignore-subscription-checkbox', function() {
            if ($(this).prop('checked')) {
                $category.find('.ignore-subscription').val('1');
            } else {
                $category.find('.ignore-subscription').val('0');
            }
        });

        $category.on('change', '.no-collapse', function() {
            if ($(this).prop('checked')) {
                $category.find('.collapse').val('0');
            } else {
                $category.find('.collapse').val('1');
            }
        });

        $category.on('change', '.no-limits', function() {
            if ($(this).prop('checked')) {
                $category.find('.limit-details').fadeOut(200, function() {
                    $category.find('.max-limit,.cooldown-amount').val('');
                });
            } else {
                $category.find('.limit-details').fadeIn(200);
            }
        });

        $category.on('change', '.max-limit,.cooldown-amount', function() {
            if ($.trim($(this).val()) != "") {
                $category.find('.no-limits').removeAttr('checked').trigger('change');
            }
        });

        $category.find('.max-limit,.cooldown-amount').each(function(i, elem) {
            if ($.trim($(elem).val()) != "") {
                $category.find('.no-limits').removeAttr('checked').trigger('change');
            }
        });

        $category.on('change', '.cooldown-measure', function() {
            if ($.trim($(this).val()) == "no-cooldown") {
                $category.find('.cooldown-amount').val('').attr('disabled', 'disabled');
            } else {
                $category.find('.cooldown-amount').removeAttr('disabled');
            }
        });
        $category.find('.cooldown-measure').trigger('change');
    };
};