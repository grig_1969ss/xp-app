export default function () {

  this.init = function () {
    let $trackingPage = $('#trackingSettingsPage');
    $(document).on('click', $trackingPage.find('.jsAddTrackingParam').selector, function () {
      let baseTrackingParamElement = $trackingPage.find('.jsTrackingParamTemplate .jsTrackingParam');
      let newTrackingParamElement = baseTrackingParamElement.clone();
      $trackingPage.find('.jsTrackingParams').append(newTrackingParamElement);
      updateTrackingParamsIndexes();
      newTrackingParamElement.find('input').removeAttr('disabled');
    });

    $(document).on('click', $trackingPage.find('.jsRemoveTrackingParam').selector, function () {
      $(this).closest('.jsTrackingParam').remove();
      updateTrackingParamsIndexes();
    });

    updateTrackingParamsIndexes();

    $trackingPage.find('.jsTrackingParams .jsTrackingParam').each(function () {
      $(this).find('input').removeAttr('disabled');
    });

    if (!$trackingPage.data('manage')) {
      $trackingPage.find('input, select, textarea').prop('disabled', true).addClass('viewable');
      $trackingPage.find('.jsRemoveTrackingParam').css('pointer-events', 'none');
    }
  };

  let updateTrackingParamsIndexes = function () {
    let $trackingPage = $('#trackingSettingsPage');
    $trackingPage.find('.jsTrackingParams .jsTrackingParam').each(function () {
      let index = $(this).index();
    console.log(index);
      $(this).find('input').each(function () {
        let name = $(this).attr('name');
        if (name) {
          $(this).attr('name', name.replace(/UtlTrackingParam\[[0-9]*\]\[key\]/, 'UrlTrackingParam[' + index + '][key]')
            .replace(/UrlTrackingParam\[[0-9]*\]\[value\]/, 'UrlTrackingParam[' + index + '][value]'));
        }
        let id = $(this).attr('id');
        if (id) {
          $(this).attr('id', id.replace(/urltrackingparam[0-9]*-key/, 'urltrackingparam-' + index + '-key')
            .replace(/urltrackingparam[0-9]*-value/, 'urltrackingparam-' + index + '-value'));
        }
      });
    });
  };
};