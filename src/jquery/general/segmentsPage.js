import ConditionsEditor from '@/jquery/widgets/conditionsEditor'
import { addCommas } from '@/jquery/utils'
import CheckFormChanges from '@/jquery/widgets/checkFormChanges'

export default function () {

  var that = this

  var $segmentsContainer,
    $activeButtonSubmit,
    $conditionsEditor,
    $segmentForm
      = null

  this.init = function () {
    $segmentsContainer = $('#segmentsContainer')
    $activeButtonSubmit = $segmentsContainer.find('.activeButton[type="submit"]')
    $segmentForm = $segmentsContainer.find('form#segmentForm')

    $activeButtonSubmit.click(function (event) {
      if ($segmentsContainer.find('#PushCriteria_title').val().trim() != '') {
        event.preventDefault()
        if ($segmentForm.hasClass('editable')) {
          if (confirm('If you change this segment it will be changed in every live campaign. Change it?')) {
            $segmentForm.valid()
            $segmentForm.submit()
          }
        } else {
          $segmentForm.valid()
          $segmentForm.submit()
        }
      } else {
        $segmentsContainer.find('#PushCriteria_title').focus()
        return false
      }
    })

    $conditionsEditor = new ConditionsEditor()
    $conditionsEditor.init(this.segmentContainer(), '/segment/segment/attribute-list-data?type=mobile')

    if (!$segmentsContainer.data('manage')) {
      $segmentsContainer.find('input, select, textarea').prop('disabled', true).addClass('viewable')
    }
    this.checkFormChanges = new CheckFormChanges($segmentForm)
    this.initDeviceCount()
  }

  this.initDeviceCount = function () {
    $(document).on('click', '#addressableDevicesNewCampaign .icon.reload.ready-to-run', function (event) {
      event.preventDefault()
      that.segmentMarkStatusInProgress()

      $segmentForm.ajaxSubmit({
        url: API_URL + '/segment/segment/recipients-count?project_id=' + window.xp.project.id + '&id=' + $segmentsContainer.data('id'),
        success: function (result) {
          if (result.task_id && !result.error) {
            that.segmentCountCheck(result.task_id)
          } else {
            that.segmentMarkStatusError(result.error)
          }
        }, error: function () {
          that.segmentMarkStatusError('Failed to calculate the segment size.<br>Please try to refresh a page.')
        }
      })
    })
  }

  this.segmentCountCheck = function (taskId, tryNum = 0) {
    $segmentForm.ajaxSubmit({
      url: API_URL + '/task/record/result?id=' + taskId,
      success: function (response) {
        switch (response.status) {
          case 0: // TASK_STATUS_NEW
          case 1: // TASK_STATUS_IN_PROGRESS
            if (tryNum >= 5) {
              that.segmentMarkStatusProgressInfo('Calculating segment size...<br>It could take a little longer to complete<br>Please wait or check the status: <a target="_blank" href="/task/history/view?project_id=' + window.xp.project.id + '&id=' + taskId + '">here</a>')
            }
            setTimeout(function () {
              that.segmentCountCheck(taskId, ++tryNum)
            }, 1000)
            break

          case 2: // TASK_STATUS_DONE
            that.segmentCountDisplay(response.result.count)
            that.segmentMarkStatusDone()
            break

          case 3: // TASK_STATUS_ERROR
            that.segmentMarkStatusError('Some of the segment conditions are invalid.')
            break

          default: // TASK_STATUS_FATAL
            that.segmentMarkStatusError('Failed to calculate the segment size.<br>Please contact account manager.')
            break
        }
      },
      error: function () {
        that.segmentMarkStatusError('Failed to calculate the segment size.<br>Please try to refresh a page.')
      }
    })
  }

  this.segmentCountDisplay = function (count) {
    $segmentsContainer.find('#addressableDevicesNewCampaign b').text('0')

    for (var key in count) {
      $segmentsContainer.find('#addressableDevicesNewCampaign [data-key="' + key + '"]').text(addCommas(parseInt(count[key])))
    }

    if (count.sampling_level) {
      $segmentsContainer.find('#addressableDevicesNewCampaign b').each(function () {
        $(this).text('~' + $(this).text())
      })
    }
  }

  this.segmentMarkStatusInProgress = function () {
    $segmentsContainer.find('#addressableDevicesNewCampaign .icon.reload').removeClass('ready-to-run')
    $segmentsContainer.find('#addressableDevicesNewCampaign .icon.reload').addClass('reload-active')
    $segmentsContainer.find('#addressableDevicesNewCampaign b').text('?')
    $segmentsContainer.find('.jsDeviceCountList').show()
    $segmentsContainer.find('.jsDeviceCountInfo').hide()
    $segmentsContainer.find('.jsDeviceCountInfo span').html('')
  }

  this.segmentMarkStatusProgressInfo = function (info) {
    $segmentsContainer.find('#addressableDevicesNewCampaign .icon.reload').removeClass('ready-to-run')
    $segmentsContainer.find('#addressableDevicesNewCampaign .icon.reload').addClass('reload-active')
    $segmentsContainer.find('.jsDeviceCountList').hide()
    $segmentsContainer.find('.jsDeviceCountInfo').show()
    $segmentsContainer.find('.jsDeviceCountInfo span').html(info)
  }

  this.segmentMarkStatusError = function (error) {
    $segmentsContainer.find('#addressableDevicesNewCampaign .icon.reload').addClass('ready-to-run')
    $segmentsContainer.find('#addressableDevicesNewCampaign .icon.reload').removeClass('reload-active')
    $segmentsContainer.find('.jsDeviceCountList').hide()
    $segmentsContainer.find('.jsDeviceCountInfo').show()
    $segmentsContainer.find('.jsDeviceCountInfo span').html(error)
  }

  this.segmentMarkStatusDone = function () {
    $segmentsContainer.find('#addressableDevicesNewCampaign .icon.reload').addClass('ready-to-run')
    $segmentsContainer.find('#addressableDevicesNewCampaign .icon.reload').removeClass('reload-active')
    $segmentsContainer.find('.jsDeviceCountList').show()
    $segmentsContainer.find('.jsDeviceCountInfo').hide()
    $segmentsContainer.find('.jsDeviceCountInfo span').html('')
  }

  this.segmentContainer = function () {
    return $segmentsContainer
  }
};
