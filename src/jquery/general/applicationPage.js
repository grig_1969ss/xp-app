export default function () {

    var $applicationContainer = null;

    this.init = function () {
        $applicationContainer = $('#applicationPage');
        this.initElements();
    };

    this.initElements = function () {
        $applicationContainer.find('#Application_ios_production_key').change(function () {
            var password = prompt(t("AccountApplication__Certificate_password"));
            if (!password) password = "";
            $applicationContainer.find('#Application_ios_production_key_password').val(password);
        });

        $applicationContainer.find('#Application_ios_sandbox_key').change(function () {
            var password = prompt(t("AccountApplication__Certificate_password"));
            if (!password) password = "";
            $applicationContainer.find('#Application_ios_sandbox_key_password').val(password);
        });

        $applicationContainer.find('#Application_safari_key').change(function () {
            var password = prompt(t("AccountApplication__Certificate_password"));
            if (!password) password = "";
            $applicationContainer.find('#Application_safari_key_password').val(password);
        });
    };
};