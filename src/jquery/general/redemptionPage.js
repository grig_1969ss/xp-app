import ContentEmailColorPicker from '@/jquery/campaign/contentEmailColorPicker'
import { twig } from 'vendor/twig/twig'

export default function () {

    var that = this;
    var TYPE_EDITOR = '3';
    var typeSelectorValue,
        modeSelectorValue,
        $section,
        $editorContainer,
        $previewContainer,
        previewModalContent,
        $previewModalWindow,
        $form = null;

    var colorpickers = [ ];

    this.init = function() {
        $section = $("#redemptionCampaignSection");
        $editorContainer = $section.find('.redemptionEditor');
        $previewContainer = $section.find('.redemptionPreviewContainer');
        $previewModalWindow = $("#pushPreviewModal");
        $form = $('form#redemptionCampaignForm.stdForm');
        previewModalContent = twig({
            id: "previewModalContent",
            href: VIEWS_DIR + "/push/views/campaign/templates/preview.twig",
            async: false
        });

        typeSelectorValue = $editorContainer.find('.redemptionTextSelectorContainer .button.active')
            .data('value');
        modeSelectorValue = $editorContainer.find('.redemptionModeSelectorContainer .button.active')
            .data('value');
        $editorContainer.find('.selectorContainer .button').click(function () {
            var self = $(this);
            var $parent = self.closest('.selectorContainer');
            if ($parent.hasClass('redemptionTextSelectorContainer')) {
                typeSelectorValue = self.data('value');
            } else if ($parent.hasClass('redemptionModeSelectorContainer')) {
                modeSelectorValue = self.data('value');
            }
            self.closest('.selectorContainer').find('.button').removeClass('active');
            self.addClass('active');

            self.closest('.selectorTabContainer').find('.innerTab').hide();
            if (modeSelectorValue == 'style') {
                self.closest('.selectorTabContainer')
                    .find('.innerTab.style').show();
            } else if (modeSelectorValue == 'text') {
                self.closest('.selectorTabContainer')
                    .find('.innerTab.text.' + typeSelectorValue).show();
            }

            $previewContainer.find('.redemptionContainer').hide();
            $previewContainer.find('.redemptionContainer.' + typeSelectorValue).show();

            that.updatePreview();
        });

        $previewContainer.find('.screenSizeSelector').change(function () {
            var original_width = parseInt($(this).val().replace(/x.+$/, ''));
            var original_height = parseInt($(this).val().replace(/^\d+x/, ''));
            var height = 530;
            var width = Math.round(height / original_height * original_width);

            var screen = $previewContainer.find('.preview .screen');
            screen.css('width', width + "px").css('height', height + "px");
            if ($(this).find('option:selected').text().indexOf('iPad') > -1) {
                screen.parents('table').removeClass('iphone').addClass('ipad');
            } else {
                screen.parents('table').removeClass('ipad').addClass('iphone');
            }
            that.updatePreview();
        }).trigger('change');

        $editorContainer.find('.sortableTitle textarea').each(function (i, element) {
            var type = $(element).closest('.innerTab').data('type');
            var $container = $previewContainer.find('.redemptionContainer.' + type);
            $(element).redactor({
                tabKey: false,
                enterKey: false,
                maxHeight: 50,
                buttons: ['bold', 'italic', 'underline', 'alignment'],
                initCallback: function () {
                    var code = this.code.get();
                    if (code != "") {
                        $container.find('.title').html(code);
                    } else {
                        this.core.getEditor().find('p').attr('style', 'font-family: Arial !important; font-size: 18px; text-align: center');
                    }
                },
                changeCallback: function () {
                    $container.find('.title').html(this.code.get());
                },
                focusCallback: function () {
                    $container.find('.title').html(this.code.get());
                },
                plugins: ['fontsize', 'fontfamily']
            });
        });

        $editorContainer.find('.sortableText textarea').each(function (i, element) {
            var type = $(element).closest('.innerTab').data('type');
            var $container = $previewContainer.find('.redemptionContainer.' + type);
            $(element).redactor({
                minHeight: 55,
                maxHeight: 700,
                buttons: ['bold', 'italic', 'underline', 'orderedlist', 'unorderedlist', 'alignment'],
                initCallback: function () {
                    var code = this.code.get();
                    if (code != "") {
                        $container.find('.text').html(code);
                    } else {
                        this.core.getEditor().find('p').attr('style', 'font-family: Arial !important; font-size: 14px; text-align: center');
                    }
                },
                changeCallback: function () {
                    $container.find('.text').html(this.code.get());
                },
                focusCallback: function () {
                    $container.find('.text').html(this.code.get());
                },
                plugins: ['fontsize', 'fontfamily']
            });
        });

        $editorContainer.find('.sortableInput input').on('keyup change', function() {
            that.updateInput();
        });

        $editorContainer.find('.sortableButton input').on('keyup change', function() {
            that.updateButton();
        });

        $editorContainer.find('.innerTab.text').sortable({
            handle: '.sortButton',
            update: function () {
                that.updatePreview();
            }
        });

        $editorContainer.find('input.colorpickerField').each(function(i, element) {
            var type = $(element).closest('.innerTab').data('type');
            var target = $(element).data('target');
            if (target == 'container') {
                var $target = $previewContainer.find('.outerContainer.portrait .container');
            } else if (target == 'this') {
                var $target = $previewContainer.find('.redemptionContainer');
            } else {
                var $target = $previewContainer.find('.redemptionContainer' + ' ' + target);
            }
            var property = $(element).data('property');

            // borders for two types of elements
            if ($(element).data('target') == 'this' || $(element).data('target') == '.button0') {
                var colorpicker = new ContentEmailColorPicker($(element), $target, property, '1px solid');
            } else {
                var colorpicker = new ContentEmailColorPicker($(element), $target, property);
            }
            colorpickers.push(colorpicker);
        });

        $('#redemptionCampaignSubmitButton').click(function(e){
            e.preventDefault();
            $previewModalWindow.find('#sendPushModal').val($(this).val());
            that.preview();
        });

        $previewModalWindow.find('#editCampaignModal').click(function(event){
            event.preventDefault();
            $previewModalWindow.bPopup({
              appendTo: '#popupsContainer'
            }).close();
            return false;
        });

        $previewModalWindow.find('#sendPushModal').click(function(event){
            return that.submitForm(event, this);
        });

        that.updatePreview();

        return that;
    };

    this.updateRedemptionColor = function(elem) {
        return false;
    };

    this.updateInput = function() {
        var $sourceInput = $editorContainer.find('.sortableInput .inputTextFormElement');
        var $targetInput = $previewContainer.find('.redemptionContainer.' + typeSelectorValue + ' .input input');
        $targetInput.attr('placeholder', $sourceInput.val());
    };

    this.updateButton = function() {
        var $sourceButton = $editorContainer.find('.sortableButton .buttonTextFormElement');
        var $targetButton = $previewContainer.find('.redemptionContainer.' + typeSelectorValue + ' .button');

        if ($sourceButton.val()) {
            $targetButton.text($sourceButton.val());
        } else {
            $targetButton.html('&nbsp;');
        }
    };

    this.updatePreview = function() {
        var params = that.getScreenParams('portrait');
        var orientation = params.or;
        var screen_width = params.width;
        var screen_height = params.height;
        if (orientation == "" || orientation == undefined) {
            return false;
        }
        var $container = $previewContainer.find('.redemptionPreview .portrait .container');
        var $preview = $container.find('.redemptionContainer.' + typeSelectorValue);
        var $img = $preview.find('img');

        $editorContainer.find('.innerTab input.colorpickerField').each(function(i, elem) {
            that.updateRedemptionColor(elem);
        });

        $container.css("width", screen_width + "px").css("height", screen_height + "px")
            .css("top", "0px").css("left", "0px").css("right", "auto").css("bottom", "auto");
        $container.find('.redemptionContainer').css('width', '300px').css('max-width', (screen_width - 20) + "px")
            .css('max-height', (screen_height - 60) + "px");

        var elements = {
            title  : $preview.find('.title'),
            text   : $preview.find('.text'),
            input : $preview.find('.input'),
            button : $preview.find('.buttons')
        };
        $editorContainer.find('.innerTab.' + typeSelectorValue + ' .sortable').each(function(i, field) {
            var sequence = $(field).data('sequence');
            $preview.append(elements[sequence]);
        });

        this.updateInput();
        this.updateButton();

        $.each(colorpickers, function(i, colorpicker) {
            var $element = colorpicker.getElement();
            if ($element.closest('.innerTab').data('type') == typeSelectorValue) {
                colorpicker.render();
            }
        });
    };

    this.getScreenParams = function(orientation) {
        if (orientation == undefined) {
            orientation = $section.find(".orientationValue").val().replace("orientation", "").toLowerCase();
        }
        var screen_size = $section.find(".screenSizeSelector").val();
        var original_width = parseInt(screen_size.replace(/x.+$/, ''));
        var original_height = parseInt(screen_size.replace(/^\d+x/, ''));
        var height = 530;
        var width = Math.round(height / original_height * original_width);
        if (orientation == 'landscape') {
            var t = height; height = width; width = t;
            t = original_height; original_height = original_width; original_width = t;
        }
        return {
            or: orientation,
            width: width,
            height: height,
            original_width: original_width,
            original_height: original_height
        };
    };

    this.submitForm = function(event, element) {
        event.preventDefault();
        $(element).prop('disabled', true);
        $form.submit();
        return false;
    };

    this.preview = function() {
        var options = {
            url:        API_URL + '/redemption/redemption-campaign/validate?project_id=' + window.xp.project.id,
            success:    function(data) {
                var template = previewModalContent.render({
                    model: data.model
                });

                if (Object.keys(data.model.errors).length > 0) {
                    $previewModalWindow.find('#sendPushModal').hide();
                } else {
                    $previewModalWindow.find('#sendPushModal').show();
                }

                $previewModalWindow.find('.modalContent').html(template);
                $previewModalWindow.bPopup({
                  appendTo: '#popupsContainer',
                  opacity: 0.9
                });
            }
        };

        $form.ajaxSubmit(options);
    };
};
