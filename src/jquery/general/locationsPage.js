import LocationsEditChart from '@/jquery/analytics/charts/locationsEditChart'
import LocationsEditTable from '@/jquery/analytics/charts/locationsEditTable'
import LoadGoogleMaps from 'vendor/gmaps/loader'
import { twig } from 'vendor/twig/twig'

export default function () {
    var that = this;
    var $locationPage, $locationsTable;

    this.chartSettings = {
        project_id  : null
    };

    this.chartOptions = {};
    this.tableOptions = {};

    this.chart = null;
    this.table = null;

    this.itemTemplate = null;
    this.geometry = null;
    this.geocoder = null;

    this.tagsSelector = null;
    this.tags = [];

    this.twPayloadItem = null;

    this.init = function() {
        $locationPage = $('#locationPage');
        $locationsTable = $locationPage.find('.locationsTable');

        this.chartSettings.project_id  = $locationPage.attr('data-project-id');
        this.tableOptions.project_id  = $locationPage.attr('data-project-id');

        this.chartOptions['location_type'] = $locationPage.data('type');
        this.tableOptions['Location[type]'] = $locationPage.data('type');

        this.chart = new LocationsEditChart({
            element: $locationPage.find('.locationsChart'),
            chartSettings: {
                onDrag: function(marker_id, pos) {
                    that.markerMoved(marker_id, pos);
                },
                onRightClick: function(pos) {
                    that.markerOnClick(pos);
                }
            }
        });

        this.table = new LocationsEditTable({
            element: $locationsTable,
            manage: $locationPage.data('manage'),
            on_build_table: function() {
                that.initLocationTable();
                that.initPayloads();
            }
        });

        this.initTags();
        this.initCreateLocation();
        this.initTableActions();

        this.redraw();
    };

    this.reloadChart = function() {
        this.chart.reloadByParam(this.chartSettings, this.chartOptions);
    };

    this.reloadTable = function() {
        this.table.reloadByOptions(this.tableOptions);
    };

    this.redraw = function() {
        this.reloadTable();
        this.reloadChart();
    };


    this.initCreateLocation = function() {
        this.initGmapSearch();

        $locationPage.find("#PushLocation_radius .bar").change(function() {
            var commaVal = $(this).val().toString().replace(/\B(?=(\d{3})+(?!\d))/g, "");
            $locationPage.find("#PushLocation_radius .crangevalue input").val(commaVal);
            that.updateTmpMarker();
        });

        $locationPage.find("#PushLocation_radius .crangevalue input").change(function() {
            var val = parseInt($(this).val());
            if (isNaN(val)) {
                val = 0;
            }
            if (val < parseInt($(this).attr("min"))) {
                val = $(this).attr("min");
            }
            if (val > parseInt($(this).attr("max"))) {
                val = $(this).attr("max");
            }
            $locationPage.find("#PushLocation_radius .bar").val(val);
            $(this).val(val);
            that.updateTmpMarker();
        });

        $locationPage.find('#createLocationForm').submit(function(){
            $(this).find('input[type=submit]').prop('disabled', true);
            if (!that.createLocation()) {
                $(this).find('input[type=submit]').prop('disabled', false);
            }
            return false;
        });

        this.tagsSelector = this.initTagsSelector($locationPage.find('#tagString'));

        this.initIBeaconUUIDSelector($locationPage.find('#iBeaconUUID'));
    };

    this.clearCreateLocationForm = function() {
        this.geometry = null;
        this.tagsSelector.clear();
        $locationPage.find('#countryId, #gmapSearch, #searchTitle, #iBeaconUUID, #iBeaconMajor, #iBeaconMinor').val('');
    };

    this.setAddressAndCountryForLocation = function(location_id, address, country, country_name) {
        var $location;
        if (location_id) {
            $location = $locationsTable.find('.locationName[data-id="'+location_id+'"]');
        } else {
            $location = $locationPage.find('.addLocation');
        }

        if (!country) country = "";
        $location.find('.country').val(country);

        if (address) {
            $location.find('.address').val(address);
        }
    };


    this.tableFields = ['title', 'address', 'latitude', 'longitude', 'country', 'radius', 'ibeacon_uuid', 'ibeacon_major', 'ibeacon_minor', 'tags'];

    this.locationsInEditMode = {};

    this.initTableActions = function() {
        $(document).on('click', $locationsTable.find('.edit a').selector, function(event) {
            event.preventDefault();
            var $tr = $(this).parents('tr').eq(0);
            that.editLocationTableItem($tr);
            return false;
        });

        $(document).on('click', $locationsTable.find('.save a').selector, function(event) {
            event.preventDefault();
            var $tr = $(this).parents('tr').eq(0);
            that.saveLocationTableItem($tr);
            return false;
        });

        $(document).on('click', $locationsTable.find('.delete a').selector, function(event) {
            event.preventDefault();
            var $tr = $(this).parents('tr').eq(0);
            that.deleteLocationTableItem($tr);
            return false;
        });

        $(document).on('click', $locationsTable.find('.locate a').selector, function(event) {
            event.preventDefault();
            var $tr = $(this).parents('tr').eq(0);
            that.locateLocationTableItem($tr);
            return false;
        });

        $(document).on('change, keyup, blur, input', $locationsTable.find('.radius').selector, function(event) {
            event.preventDefault();
            var $tr = $(this).parents('tr').eq(0);

            var radius = parseInt($(this).val());
            if (radius < 1) radius = 1;
            if (radius > 50000) radius = 50000;

            that.chart.chart.markers[$tr.find('.locationName').data('id')].circle.setRadius(radius);
            $tr.find('.locationName').data('radius', radius);
            that.findLocation($tr.find('.locationName'), false);

            return false;
        });
    };

    this.initPayloads = function() {
        if (!that.twPayloadItem) {
            that.twPayloadItem = twig({
                id: "payloadListItem",
                href: VIEWS_DIR+"/location/views/location/templates/table-item-payloads-item.twig",
                async: false
            });
        }

        $locationsTable.find('.clonedFieldKey').each(function(i, field) {
            that.addPayloadAutocomplete(field);
        });

        $locationsTable.on('click', '.jsAddPayload', function(e){
            var index = parseInt($(this).closest('td').find('.jsPayload:last-child').data('index')) + 1;
            if (isNaN(index)) index = 0;
            var template = that.twPayloadItem.render({loop: {index: index}});
            var $container = $(this).closest('td').find('.jsPayloads');
            $container.show().append(template);
            that.addPayloadAutocomplete($container.find('.clonedFieldKey').last());
        });

        $locationsTable.on('click', '.jsDeletePayload', function(){
            var list = $(this).closest('ul');
            $(this).closest('li').eq(0).remove();
            if ($(list).find('li').length == 0) {
                $(list).hide();
            }
        });
    };

    this.addPayloadAutocomplete = function(field) {
        $(field).richAutocomplete({
            source: '/location/location-payload/autocomplete?project_id=' + that.chartSettings.project_id,
            searchOnFocus: {
                allowEmptyValue: true,
                allowFilledValue: true
            }
        });
    };

    this.createLocationTableItem = function(item) {
        if (!this.itemTemplate) {
            twig({
                id: 'item',
                href: VIEWS_DIR+'/location/views/location/templates/table-item.twig?v='+APP_VERSION,
                load: function(template) {
                    that.itemTemplate = template;
                },
                async: false
            });
        }

        var row = $(this.itemTemplate.render({
            item: item,
            manage: $locationPage.data('manage')
        }));

        var existing_row = $locationsTable.find('tr[data-id="'+item.id+'"]');
        if (existing_row.length) {
            existing_row.replaceWith(row);
        } else {
            $locationsTable.find('tbody').prepend(row);
        }

        var tags = item.groups_str.split(',');
        for (var i in tags) {
            if (that.tags.indexOf(tags[i]) < 0) {
                that.tags[that.tags.length] = {title: tags[i]};
            }
        }
    };

    this.editLocationTableItem = function($tr) {
        that.locationsInEditMode[$tr.data('id')] = true;
        var $content = $tr.find('.locationUpdateContent');

        if (!$tr.data('init')) {
            $tr.data('init', 1);
            that.initTagsSelector($tr.find('.tags'));
            that.initIBeaconUUIDSelector($tr.find('.ibeacon_uuid'));
        }

        that.chart.chart.markers[$tr.find('.locationName').data('id')].marker.setDraggable(true);

        that.findLocation($tr.find('.locationName'), false);

        for (var i in that.tableFields) {
            $content.find('.'+that.tableFields[i]).attr('prev', $content.find('.'+that.tableFields[i]).val());
        }

        $tr.find('.edit').hide();
        $tr.find('.save').show();

        $tr.find('.locationName').removeClass('closed');
        $tr.find('.locationName span').hide();
        $content.show();
    };

    this.saveLocationTableItem = function($tr) {
        delete that.locationsInEditMode[$tr.data('id')];
        var $content = $tr.find('.locationUpdateContent');

        that.chart.chart.markers[$tr.find('.locationName').data('id')].marker.setDraggable(false);

        // for (var i in that.tableFields) {
        // if ($content.find('.'+that.tableFields[i]).attr('prev') != $content.find('.'+that.tableFields[i]).val()) {
        that.updateLocation($tr.find('.locationName'));
        return;
        // }
        // }

        $tr.find('.save').hide();
        $tr.find('.edit').show();

        $tr.find('.locationName').addClass('closed');
        $content.hide();
        $tr.find('.locationName span').show();
    };

    this.deleteLocationTableItem = function($tr) {
        if (confirm(t('Common__Delete'))) {
            that.deleteLocation($tr.find('.locationName'));
        }
    };

    this.locateLocationTableItem = function($tr) {
        that.findLocation($tr.find('.locationName'));
    };

    this.initLocationTable = function() {
        for (var locationId in that.locationsInEditMode) {
            that.chart.chart.markers[locationId].marker.setDraggable(false);
        }
    };


    this.initTags = function() {
        $.ajax({
            url: '/location/location/groups?project_id=' + this.chartSettings.project_id,
            async: false,
            success: function(data) {
                that.tags = data.tags;
            }
        });
    };

    this.initTagsSelector = function(element) {
        return element.selectize({
            plugins: ['remove_button'],
            delimiter: ',',
            persist: false,
            load: function(query, callback) {
                callback(that.tags);
            },
            labelField: "title",
            valueField: "title",
            sortField: 'title',
            searchField: 'title',
            create: function(input) {
                var item = {
                    title: input
                };
                if (that.tags.indexOf(input) < 0) {
                    that.tags[that.tags.length] = item;
                }
                return item;
            }
        })[0].selectize;
    };

    this.initIBeaconUUIDSelector = function(element) {
        $(element).richAutocomplete({
            source: '/location/ibeacon-uuid/autocomplete?project_id='+this.chartSettings.project_id,
            searchOnFocus: {
                allowEmptyValue: true,
                allowFilledValue: true
            }
        });
    };


    this.updateLocation = function(el) {
        var form = $(el).parents('tr').find('form');
        $.ajax({
            type: "POST",
            url: 'update?project_id='+that.chartSettings.project_id+'&id='+$(el).data('id'),
            data: form.serialize(),
            async: false,
            success: function(data) {
                that.createLocationTableItem(data.object);
                that.chart.chart.markers[data.object.id].circle.setRadius(parseInt(data.object.radius));
                that.findLocation($locationsTable.find('.locationName[data-id="'+data.object.id+'"]'));
            },
            error: function(data) {
                alert(JSON.parse(data.responseText).message);
            }
        });
    };

    this.deleteLocation = function(el) {
        $.ajax({
            type: "POST",
            url: 'delete?project_id='+that.chartSettings.project_id+'&id='+$(el).data('id'),
            async: false,
            success: function(data) {
                that.chart.chart.removeMarker($(el).data('id'));
                $locationsTable.find('tr[data-id="'+$(el).data('id')+'"]').remove();
            },
            error: function(data) {
                alert(JSON.parse(data.responseText).message);
            }
        });
    };

    this.createLocation = function() {
        var form = $locationPage.find('#createLocationForm');

        var title = form.find('#searchTitle').val();
        if (!title) {
            alert(t('yii', '{attribute} cannot be blank.', {'attribute': t('all','LocationLocation__title')}));
            return false;
        }

        if (!this.geometry) {
            alert(t('all', 'LocationLocationController__Find_or_pin'));
            return false;
        }
        form.find('#latitude').val(this.geometry.latitude);
        form.find('#longitude').val(this.geometry.longitude);

        $.ajax({
            type: "POST",
            url: 'create?project_id='+that.chartSettings.project_id,
            data: form.serialize(),
            async: true,
            success: function(data) {
                that.chart.chart.removeMarker(0);
                that.chart.chart.addMarker(data.object.id, data.object.latitude, data.object.longitude, data.object.radius, data.object.title);

                form.find('input[type=submit]').prop('disabled', false);
                that.clearCreateLocationForm();

                that.createLocationTableItem(data.object);
                that.findLocation($locationsTable.find('.locationName[data-id="'+data.object.id+'"]'));
            },
            error: function(data) {
                form.find('input[type=submit]').prop('disabled', false);
                alert(JSON.parse(data.responseText).message);
            }
        });

        return true;
    };


    this.initGmapSearch = function() {
        LoadGoogleMaps(function (google) {
            that.geocoder = new google.maps.Geocoder();

            $locationPage.find('#gmapSearch').autocomplete({
                source: function (request, response) {
                    that.geocoder.geocode({ 'address': request.term }, function (results) {
                        response($.map(results, function (item) {
                            return {
                                label: item.formatted_address,
                                value: item.formatted_address,
                                geocode: item
                            }
                        }));
                    })
                },
                select: function (event, ui) {
                    that.geocodeLookup({ 'address': ui.item.value }, function (g, address, country, country_name) {
                        that.geometry = { latitude: g.location.lat(), longitude: g.location.lng() };
                        that.updateTmpMarker();

                        that.setAddressAndCountryForLocation(0, address, country, country_name);
                    });
                }
            });

            $locationPage.find('#gmapSearch').change(function () {
                $locationPage.find('#searchTitle').val($(this).val());
            });
        });
    };

    this.geocodeLookup = function(request, callback) {
        this.geocoder.geocode(request, function(results, status) {
            if (results[0]) {
                var country = null;
                var country_name = "";
                for (var i in results[0].address_components) {
                    if (results[0].address_components[i].types[0] == 'country') {
                        country = results[0].address_components[i].short_name;
                        country_name = results[0].address_components[i].long_name;
                    }
                }

                if (typeof callback == 'function') {
                    callback(results[0].geometry, results[0].formatted_address, country, country_name);
                }
            } else {
                if (typeof callback == 'function') {
                    callback(request.latLng, "", "", "");
                }
            }
        });
    };

    this.updateTmpMarker = function(dont_fit) {
        this.chart.chart.removeMarker(0);

        if (this.geometry) {
            this.chart.chart.addMarker(0, this.geometry.latitude, this.geometry.longitude, parseInt($locationPage.find('#PushLocation_radius_field').val()), 'New location', '#b9b3b3', true);

            if (!dont_fit) {
                this.chart.chart.fitWithPositionAndRadius(this.geometry.latitude, this.geometry.longitude, parseInt($locationPage.find('#PushLocation_radius_field').val()));
            }
        }
    };

    this.findLocation = function(el, jump) {
        this.chart.chart.fitWithPositionAndRadius($(el).data('latitude'), $(el).data('longitude'), $(el).data('radius'), jump);
    };

    this.markerMoved = function(marker_id, pos) {
        if (marker_id==0) {
            this.geometry = {latitude: pos.lat(), longitude: pos.lng()};
        } else {
            $locationsTable.find('.locationName[data-id="'+marker_id+'"] .latitude').val(pos.lat());
            $locationsTable.find('.locationName[data-id="'+marker_id+'"] .longitude').val(pos.lng());
        }

        that.geocodeLookup({'latLng': pos}, function(g, address, country, country_name){
            that.setAddressAndCountryForLocation(marker_id, address, country, country_name);
        });
    };

    this.markerOnClick = function(pos) {
        this.geometry = {latitude: pos.lat(), longitude: pos.lng()};

        that.geocodeLookup({'latLng': pos}, function(g, address, country, country_name){
            that.setAddressAndCountryForLocation(0, address, country, country_name);
        });

        this.updateTmpMarker(true);
    };
};
