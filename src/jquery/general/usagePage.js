export default function () {

    var that = this;

    this.container = null;

    this.init = function () {
        this.container = $('#usagePage');

        this.initPlanSelectors();

        this.initAddonSelectors();

        return this;
    };

    this.initPlanSelectors = function() {
        this.container.find('.plan_button input').click(function(){
            that.selectPlan($(this).parent().data('id'));
        });
    };

    this.initAddonSelectors = function() {
        this.container.find('.addon input').change(function(){
            that.selectAddons();
        });
    };


    this.selectPlan = function(id) {
        var plan = this.container.find('.plan_button[data-id="'+id+'"]');
        var by_agreement = plan.data('by_agreement');

        if (by_agreement) {
            window.location = "mailto:"+xp.provider.email;
        }

        else {
            window.location = "/payment/account/update-subscription?plan_update=1&plan_id="+id;
        }
    };

    this.selectAddons = function() {
        var addons = [];

        var selected_addons = this.container.find('.addon input:checked');
        selected_addons.each(function(){
            addons[addons.length] = $(this).val();
        });

        window.location = "/payment/account/update-subscription?plan_update=1&addons="+addons.join(',');
    };
};
