export default function () {

    var $inboxSettingsPage = null;
    var self = this;

    this.init = function() {
        $inboxSettingsPage = $('#inboxSettingsPage');
        this.initElements();
        this.initColorPickers();
    };

    this.initElements = function() {
        $inboxSettingsPage.find('.jsInboxStatusContainer input').on('select', function() {
            if ($(this).prop('checked')) {
                if ($(this).val() == 0) {
                    $(this).parents('.jsInboxContainer').find('.jsInboxSettingsContainer').hide();
                } else {
                    $(this).parents('.jsInboxContainer').find('.jsInboxSettingsContainer').show();
                }
            }
        }).trigger('select');

        $inboxSettingsPage.find('.jsInboxTypeStatusContainer input').on('select', function() {
            if ($(this).prop('checked')) {
                if ($(this).val() == 0) {
                    $(this).parents('.jsInboxTypeContainer').find('.jsInboxTypeSettingsContainer').hide();
                } else {
                    $(this).parents('.jsInboxTypeContainer').find('.jsInboxTypeSettingsContainer').show();
                }
            }
        }).trigger('select');

        $inboxSettingsPage.find('.jsInboxColorContainer input').on('select', function() {
            if ($(this).prop('checked')) {
                if ($(this).val() == 'custom') {
                    $(this).parents('.jsInboxTypeContainer').find('.jsInboxColorsContainer').show();
                } else {
                    $(this).parents('.jsInboxTypeContainer').find('.jsInboxColorsContainer').hide();
                }
            }
        }).trigger('select');
    };

    this.initColorPickers = function() {
        var $caller;
        $inboxSettingsPage.find('span.colorpicker-field').click(function() {
            $(this).siblings('input.colorpicker-field').click();
        });
        $inboxSettingsPage.find('input.colorpicker-field').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val('#' + hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function() {
                $(this).ColorPickerSetColor(this.value);
                $caller = $(this);
            },
            onShow: function(colpkr) {
                $(colpkr).fadeIn(200);
                return false;
            },
            onHide: function(colpkr) {
                $(colpkr).fadeOut(200);
                return false;
            },
            onChange: function(hsb, hex, rgb) {
                self.updateColorPickerPreview($caller, hex);
            }
        }).bind('keyup', function() {
            $(this).ColorPickerSetColor(this.value);
            self.updateColorPickerPreview(this);
        }).bind('blur', function() {
            if (!$(this).val().match(/^#[0-9a-f]{6}$/)) {
                $(this).val('');
                self.updateColorPickerPreview(this);
            }
        }).each(function(i, elem) {
            self.updateColorPickerPreview(elem);
        });
    };

    this.updateColorPickerPreview = function($caller, color) {
        $caller = $($caller);
        if (color == undefined) {
            color = $caller.val();
        }
        if (color.indexOf('#') !== 0 && color.length > 0) {
            color = '#' + color;
        }
        $caller.val(color);
        $caller.next().css('background', color);
    };
};