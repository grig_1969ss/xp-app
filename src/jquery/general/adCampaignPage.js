export default function () {
    var that = this;
    var $adCampaignPage;

    this.init = function() {
        $adCampaignPage = $('#adCampaignPage');

        this.loadTags();
        this.initAds();
    };

    this.tags = [];

    this.loadTags = function() {
        $.ajax({
            type: "POST",
            url: '/attribution/ad-campaign/tags?project_id=' + $adCampaignPage.data('project-id'),
            async: false,
            success: function(data) {
                that.tags = data.tags;
            }
        });
    };

    this.initAds = function () {
        $(document).on('click', $adCampaignPage.find('.jsAddAdGroup').selector, function() {
            var baseAdGroupElement = $adCampaignPage.find('.jsAdGroupTemplate .jsAdGroup');
            var newAdGroupElement = baseAdGroupElement.clone();
            $adCampaignPage.find('.jsAdGroups').append(newAdGroupElement);
            that.updateAdsIndexes();
            newAdGroupElement.find('.jsAd').each(function(){
                that.initAd($(this));
            });
        });

        $(document).on('click', $adCampaignPage.find('.jsRemoveAdGroup').selector, function() {
            $(this).closest('.jsAdGroup').remove();
            that.updateAdsIndexes();
        });

        $(document).on('click', $adCampaignPage.find('.jsAddAd').selector, function() {
            var sourceAdElement = $(this).closest('.jsAdGroup').find('.jsAd').last();
            var baseAdElement = $adCampaignPage.find('.jsAdTemplate .jsAd');
            var newAdElement = baseAdElement.clone();
            if (sourceAdElement.length) {
                newAdElement.find('.jsPlatform').val(sourceAdElement.find('.jsPlatform').val());
                newAdElement.find('.jsNetwork').val(sourceAdElement.find('.jsNetwork').val());
            }
            $(this).closest('.jsAdGroup').find('.jsAds').append(newAdElement);
            that.updateAdsIndexes();
            that.initAd(newAdElement);
        });

        $(document).on('click', $adCampaignPage.find('.jsRemoveAd').selector, function() {
            $(this).closest('.jsAd').remove();
            that.updateAdsIndexes();
        });

        that.updateAdsIndexes();

        $adCampaignPage.find('.jsAdGroups .jsAd').each(function(){
            that.initAd($(this));
        });
    };

    this.updateAdsIndexes = function() {
        $adCampaignPage.find('.jsAdGroups .jsAdGroup').each(function(){
            that.updateAdGroupIndexes($(this));
        });
    };

    this.updateAdGroupIndexes = function(adGroup) {
        var index = adGroup.index();
        adGroup.find('input, select').each(function(){
            var name = $(this).attr('name');
            if (name) {
                $(this).attr('name', name.replace(/\[NewAdGroups\]\[[0-9]{0,}\]/, "[NewAdGroups]["+index+"]"));
            }
        });
        adGroup.find('.jsAdGroupIndex').text(index + 1);
        adGroup.find('.jsAd').each(function(){
            that.updateAdIndexes($(this));
        });
    };

    this.updateAdIndexes = function(ad) {
        var index = ad.index();
        ad.find('input, select').each(function(){
            var name = $(this).attr('name');
            if (name) {
                $(this).attr('name', name.replace(/\[NewAds\]\[[0-9]{0,}\]/, "[NewAds]["+index+"]"));
            }
        });
        ad.find('.jsAdIndex').text(index + 1);
    };

    this.initAd = function($ad) {
        this.initTagsElement($ad);
        this.initNetworkElement($ad);
        this.initUrlElement($ad);
    };

    this.initTagsElement = function($ad) {
        return $ad.find('.jsTags').selectize({
            plugins: ['remove_button'],
            delimiter: ',',
            persist: false,
            load: function(query, callback) {
                console.log(that.tags);
                callback(that.tags);
            },
            labelField: "title",
            valueField: "title",
            sortField: 'title',
            searchField: 'title',
            create: function(input) {
                var item = {
                    title: input
                };
                if (that.tags.indexOf(input) < 0) {
                    that.tags[that.tags.length] = item;
                }
                return item;
            }
        })[0].selectize;
    };

    this.initNetworkElement = function($ad) {
        $ad.find('.jsPlatform').change(function(){
            $ad.find('.jsNetwork').val("");
            that.updateNetworkElement($ad);
        });

        $ad.find('.jsNetwork').focus(function(){
            if (!$ad.find('.jsPlatform').val()) {
                $ad.find('.jsPlatform').focus();
                $ad.find('.jsPlatform').blur();
            }
        });

        $ad.find('.jsNetwork').networkAutocomplete();
        that.updateNetworkElement($ad);
    };

    this.updateNetworkElement = function($ad) {
        if ($ad.find('.jsPlatform').val()) {
            $ad.find('.jsNetwork').networkAutocomplete({
                source: '/attribution/network/autocomplete?platform=' + $ad.find('.jsPlatform').val()
            });
        } else {
            $ad.find('.jsNetwork').networkAutocomplete('option', 'source', null);
        }
    };

    this.initUrlElement = function($ad) {
        $ad.find('.jsUrl').click(function(){
            $(this).select();
        });
    };
};

$.widget("custom.networkAutocomplete", $.custom.richAutocomplete, {
    _renderItem: function (ul, item) {
        var $el = $("<li>");
        //var img = $("<img>").attr("src", item.logo).css("height", "20px").css("float", "left").css("margin-right", "8px");
        var link = $("<a>").text(item.label);
        //link.prepend(img);
        $el.append(link);
        return $el.appendTo(ul);
    }
});