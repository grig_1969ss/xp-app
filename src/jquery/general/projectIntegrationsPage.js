export default function () {
    let $projectIntegrationsContainer = null

    this.init = function () {
        $projectIntegrationsContainer = $('#projectIntegrationsPage')
        this.initElements()
    }

    this.initElements = function () {
        $projectIntegrationsContainer.find('.jsTogglePassword input').click(function () {
            let container = $(this).closest('.jsTogglePassword')
            if (container.find('input').attr('type') === 'password') {
                container.find('span').addClass('icon-lock').removeClass('icon-eye')
                container.find('input').attr('type', 'text')
            }
        })

        $projectIntegrationsContainer.find('.jsTogglePassword span').click(function () {
            let container = $(this).closest('.jsTogglePassword')
            if (container.find('input').attr('type') === 'password') {
                container.find('span').addClass('icon-lock').removeClass('icon-eye')
                container.find('input').attr('type', 'text')
            } else {
                container.find('span').addClass('icon-eye').removeClass('icon-lock')
                container.find('input').attr('type', 'password')
            }
        })

        $projectIntegrationsContainer.find('.jsCopyToken').click(function () {
            let container = $(this).siblings('.jsTogglePassword')
            if (container.find('input').attr('type') === 'password') {
                container.find('input').attr('type', 'text')
                container.find('input').select()
                document.execCommand('copy')
                container.find('input').attr('type', 'password')
            } else {
                container.find('input').select()
                document.execCommand('copy')
            }
            $(this).focus()
        })
    }
}