export function addCommas(amount, decimals)
{
    amount += '';
    var x = amount.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

export function addUnit(amount, unit)
{
    if (unit) {
        if (unit == '$' || unit == '€' || unit == '£') {
            return unit + '' + addCommas(amount);
        } else if (unit == '%') {
            return addCommas(amount) + '' + unit;
        } else {
            return addCommas(amount) + ' ' + unit;
        }
    } else {
        return addCommas(amount);
    }
}

export function formatPeriod(number)
{
    var hours = Math.floor(number / 3600);
    var minutes = Math.floor((number - hours * 3600) / 60);
    var seconds = number - hours * 3600 - minutes * 60;

    if (!hours && !minutes) {
        return seconds+'s';
    } else if (!hours) {
        return minutes+'m '+seconds+'s';
    } else if (hours < 100) {
        return hours+'h '+minutes+'m';
    } else {
        return addCommas(hours)+'h';
    }
}

export function randomString(length)
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

export function escapeHTML(str)
{
    return $("<div></div>").text(str).html();
}

export function buildURL(url, params) {
    for (var i in params) {
        var sep = (url.indexOf('?') > -1) ? '&' : '?';
        url += sep + i + '=' + encodeURIComponent(params[i]);
    }
    return url;
}




export function getHashParam(key) {
    var params = parseHashParams(window.location.hash);
    return params[key];
}

export function setHashParam(key, value) {
    var params = parseHashParams(window.location.hash);
    if (value) {
        params[key] = value;
    } else {
        delete params[key];
    }
    window.location = buildHashString(params);
}

export function parseHashParams(str) {
    var pieces = str.substr(1).split("&"), data = {}, i, parts;
    for (i = 0; i < pieces.length; i++) {
        parts = pieces[i].split("=");
        if (parts[0]) {
            data[decodeURIComponent(parts[0])] = decodeURIComponent(parts[1]);
        }
    }
    return data;
}

export function buildHashString(params) {
    var pieces = [];
    for (var key in params) {
        if (params[key]) {
            pieces.push(encodeURIComponent(key) + "=" + encodeURIComponent(params[key]));
        }
    }
    return '#'+pieces.join("&");
}