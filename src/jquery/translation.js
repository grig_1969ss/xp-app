
window.translation = (function ($) {
    return {
        translations: null,

        language: null,
        provider_id: null,

        load: function() {
            $.ajax({
                url: '/translations/jquery.' + this.language + '.json',
                async: false,
                dataType: 'json',
            })
              .success(data => this.translations = data);
        },

        translate: function(message, params) {
            if (this.translations === null) {
                this.load();
            }
            if (!this.translations || this.translations[message] === undefined) {
                return this.missing(message);
            }
            return this.adjust(this.translations[message], params);
        },

        adjust: function(message, params) {
            if (params) {
                for (var i in params) {
                    var re = new RegExp('{'+i+'}', 'g');
                    message = message.replace(re, params[i]);
                }
            }
            return message;
        },

        missing: function(message) {
            $.ajax({
                url: '/translation/system/missing-translation',
                data: {
                  category: 'jquery',
                  message: message
                }
            });
            return message;
        },

        init: function(language, provider_id) {
            this.language = language;
            this.provider_id = provider_id;
        }
    };
})(jQuery);

window.t = function(category, message, params) {

  // The category param is not required any more, but may still be be sent by legacy code
  // TODO: Tidy this up once the campaign and analytics sections have been rebuilt
  let categories = ['all', 'email', 'inbox-mobile', 'vue', 'web-sdk', 'yii'];
  if (!categories.includes(category)) {
    return window.translation.translate(category, message);
  }

  return window.translation.translate(message, params);
}

window.translation.init($('html').attr('lang'), xp.provider.id);
