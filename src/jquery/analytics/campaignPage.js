import CampaignVariantsTable from './charts/campaignVariantsTable'
import CampaignVariantsChart from './charts/campaignVariantsChart'
import CampaignLinksTable from './charts/campaignLinksTable'
import CampaignClickMap from './charts/campaignClickMap'
import CampaignDomainsTable from './charts/campaignDomainsTable'
import CampaignBouncesTable from './charts/campaignBouncesTable'
import CampaignActsTable from './charts/campaignActsTable'
import SelectorBox from './selectorBox'
import TabContent from '@/jquery/widgets/tabContent'

export default function () {

    var that = this;
    var $analyticsContainer;

    this.chartSettings = {
        project_id  : null,
        platform    : null,
        startDate   : null,
        endDate     : null
    };

    this.chartOptions = {
        campaign_id : null,
        variant_key: null
    };

    this.campaignVariantsChart = null;
    this.campaignVariantsTable = null;

    this.tab = null;
    this.tabContent = null;
    this.variant = null;
    this.variantsContent = null;

    this.campaignLinksTable = null;
    this.campaignClickMap = null;
    this.campaignDomainsTable = null;
    this.campaignBouncesTable = null;
    this.campaignActsTable = null;

    this.returnToAll = false;

    this.init = function() {
        $analyticsContainer = $('#analyticsContent');

        this.chartSettings.project_id  = $analyticsContainer.attr('data-project-id');
        this.chartOptions.campaign_id = $analyticsContainer.attr('data-campaign-id');

        this.campaignVariantsTable = new CampaignVariantsTable({
            element: $analyticsContainer.find('.campaignVariantsTable'),
            show_mobile_push: ($analyticsContainer.attr('data-show-mobile-push') == '1'),
            show_web_push: ($analyticsContainer.attr('data-show-web-push') == '1'),
            show_inapp: ($analyticsContainer.attr('data-show-inapp') == '1'),
            show_inbox: ($analyticsContainer.attr('data-show-inbox') == '1'),
            show_email: ($analyticsContainer.attr('data-show-email') == '1'),
            show_sms: ($analyticsContainer.attr('data-show-sms') == '1'),
            show_facebook: ($analyticsContainer.attr('data-show-facebook') == '1'),
            show_whatsapp: ($analyticsContainer.attr('data-show-whatsapp') == '1'),
            show_conversions: ($analyticsContainer.attr('data-show-conversions') == '1'),
            show_app_opens: ($analyticsContainer.attr('data-show-app-opens') == '1'),
            show_inweb: ($analyticsContainer.attr('data-show-inweb') == '1'),
            on_select_items: function(items) {
                that.campaignVariantsChart.showItems(items);
                that.reloadCampaignVariantsChart();
            }
        });

        this.campaignVariantsChart = new CampaignVariantsChart({
            element: $analyticsContainer.find('.campaignVariantsChart'),
            show_mobile_push: ($analyticsContainer.attr('data-show-mobile-push') == '1'),
            show_web_push: ($analyticsContainer.attr('data-show-web-push') == '1'),
            show_inapp: ($analyticsContainer.attr('data-show-inapp') == '1'),
            show_inbox: ($analyticsContainer.attr('data-show-inbox') == '1'),
            show_email: ($analyticsContainer.attr('data-show-email') == '1'),
            show_sms: ($analyticsContainer.attr('data-show-sms') == '1'),
            show_facebook: ($analyticsContainer.attr('data-show-facebook') == '1'),
            show_whatsapp: ($analyticsContainer.attr('data-show-whatsapp') == '1'),
            show_conversions: ($analyticsContainer.attr('data-show-conversions') == '1'),
            show_app_opens: ($analyticsContainer.attr('data-show-app-opens') == '1'),
            show_inweb: ($analyticsContainer.attr('data-show-inweb') == '1'),
            mode: 'timeline',
            modifier:   ($analyticsContainer.attr('data-show-web-push') == '1') ? 'web_push_sent' :
                        ($analyticsContainer.attr('data-show-mobile-push') == '1') ? 'mobile_push_sent' :
                        ($analyticsContainer.attr('data-show-inapp') == '1') ? 'inapp_sent' :
                        ($analyticsContainer.attr('data-show-inbox') == '1') ? 'inbox_sent' :
                        ($analyticsContainer.attr('data-show-email') == '1') ? 'email_sent' :
                        ($analyticsContainer.attr('data-show-sms') == '1') ? 'sms_sent' :
                        ($analyticsContainer.attr('data-show-facebook') == '1') ? 'facebook_sent' :
                        ($analyticsContainer.attr('data-show-whatsapp') == '1') ? 'whatsapp_sent' :
                        ($analyticsContainer.attr('data-show-inweb') == '1') ? 'inweb_sent' : ''
        });

        this.campaignVariantsChart.chartTimeline.updateTitle($analyticsContainer.attr('data-campaign-title'));

        if ($analyticsContainer.attr('data-show-email') == '1') {
            this.campaignLinksTable = new CampaignLinksTable({
                element: $analyticsContainer.find('.campaignLinksTable')
            });

            this.campaignClickMap = new CampaignClickMap(this.chartOptions.campaign_id, {
                container: $analyticsContainer.find('.campaignClickMap')
            });

            this.campaignDomainsTable = new CampaignDomainsTable({
                element: $analyticsContainer.find('.campaignDomainsTable')
            });

            this.campaignBouncesTable = new CampaignBouncesTable({
                element: $analyticsContainer.find('.campaignBouncesTable')
            });

            this.tabContent = new TabContent({
                element: $analyticsContainer.find('.jsEmailStats'),
                tabs_element: $analyticsContainer.find('.jsEmailStatsTabs'),
                contents_element: $analyticsContainer.find('.jsEmailStatsContents'),
                save_state: true,
                default_tab: 'links',
                on_change: function(tab) {
                    if (that.tab) {
                        that.tab = tab;
                        if (that.tab == 'click-map') {
                            that.variantsContent.disableTab('all');
                            if (that.variant == 'all') {
                                that.variantsContent.selectTab('A');
                                that.returnToAll = true;
                            } else {
                                that.redrawActiveTab();
                            }
                        } else {
                            that.variantsContent.enableTab('all');
                            if (that.returnToAll) {
                                that.returnToAll = false;
                                that.variantsContent.selectTab('all');
                            } else {
                                that.redrawActiveTab();
                            }
                        }
                    } else {
                        if (that.tab == 'click-map') {
                            that.variantsContent.disableTab('all');
                        }
                        that.tab = tab;
                    }
                }
            });

            this.variantsContent = new TabContent({
                element: $analyticsContainer.find('.jsEmailStatsVariants'),
                tabs_element: $analyticsContainer.find('.jsEmailStatsVariantsTabs'),
                contents_element: $analyticsContainer.find('.jsEmailStatsVariantsContents'),
                save_state: true,
                default_tab: 'all',
                hash_param: 'variant',
                on_change: function(variant) {
                    that.returnToAll = false;
                    if (that.variant) {
                        that.variant = variant;
                        that.chartOptions.variant_key = (variant == 'all') ? null : variant;
                        that.redrawActiveTab();
                    } else {
                        that.variant = variant;
                        that.chartOptions.variant_key = (variant == 'all') ? null : variant;
                    }
                }
            });
        }
        else
        {
            this.campaignActsTable = new CampaignActsTable({
                element: $analyticsContainer.find('.campaignActsTable')
            });

            this.variantsContent = new TabContent({
                element: $analyticsContainer.find('.jsActsVariants'),
                save_state: true,
                default_tab: 'all',
                hash_param: 'variant',
                on_change: function(variant) {
                    if (that.variant) {
                        that.variant = variant;
                        that.chartOptions.variant_key = (variant == 'all') ? null : variant;
                        that.redrawActiveTab();
                    } else {
                        that.variant = variant;
                        that.chartOptions.variant_key = (variant == 'all') ? null : variant;
                    }
                }
            });
        }

      this.initButtons();

        SelectorBox().init();

        return this;
    };

    this.reloadCampaignVariantsChart = function() {
        this.campaignVariantsChart.reloadByParam(this.chartSettings, this.chartOptions);
    };

    this.reloadCampaignVariantsTable = function() {
        this.campaignVariantsTable.reloadByParam(this.chartSettings, this.chartOptions);
    };

    this.reloadCampaignActsTable = function() {
        this.campaignActsTable.reloadByParam(this.chartSettings, this.chartOptions);
    };

    this.reloadCampaignLinksTable = function() {
        this.campaignLinksTable.reloadByParam(this.chartSettings, this.chartOptions);
    };

    this.reloadCampaignClickMap = function() {
        this.campaignClickMap.reloadByParam(this.chartSettings, this.chartOptions);
    };

    this.reloadCampaignDomainsTable = function() {
        this.campaignDomainsTable.reloadByParam(this.chartSettings, this.chartOptions);
    };

    this.reloadCampaignBouncesTable = function() {
        this.campaignBouncesTable.reloadByParam(this.chartSettings, this.chartOptions);
    };

    this.redrawBackButton = function() {
        this.campaignVariantsChart.chartTimeline.addTitleBackButton('/analytics/analytics/campaigns?project_id='+this.chartSettings.project_id+SelectorBox().getHashedSelector());
    };

    this.redraw = function() {
        this.reloadCampaignVariantsChart();
        this.reloadCampaignVariantsTable();
        this.redrawBackButton();
        this.redrawActiveTab();
    };

    this.redrawActiveTab = function () {
        if ($analyticsContainer.attr('data-show-email') == '1') {
            switch (this.tab) {
                case 'links':
                    this.reloadCampaignLinksTable();
                    break;
                case 'click-map':
                    this.reloadCampaignClickMap();
                    break;
                case 'domains':
                    this.reloadCampaignDomainsTable();
                    break;
                case 'bounces':
                    this.reloadCampaignBouncesTable();
                    break;
            }
        } else {
            this.reloadCampaignActsTable();
        }
    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };

    this.initButtons = function () {
        $analyticsContainer.find('.opt2Table').click(function () {
            window.location = that.campaignVariantsTable.requestLink() + '&download=1'
        });
    };
};
