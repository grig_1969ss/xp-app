import FunnelsTable from './charts/funnelsTable'
import SelectorBox from './selectorBox'

export default function () {

    var $analyticsContainer;

    this.chartSettings = {
        project_id  : null,
        platform    : null,
        startDate   : null,
        endDate     : null
    };

    this.funnelsTable = null;

    this.init = function () {
        $analyticsContainer = $('#analyticsContent');

        this.chartSettings.project_id  = $analyticsContainer.attr('data-project-id');

        this.funnelsTable = new FunnelsTable({
            element: $analyticsContainer.find('.funnelsTable')
        });

        SelectorBox().init();

        return this;
    };

    this.reloadFunnelsTable = function() {
        this.funnelsTable.reloadByParam(this.chartSettings);
    };

    this.redraw = function() {
        this.reloadFunnelsTable();
    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };
};
