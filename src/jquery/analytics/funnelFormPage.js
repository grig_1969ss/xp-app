import SelectorBox from './selectorBox'

export default function () {

    var that = this;
    var $funnelFormPage;

    this.chartSettings = {
        project_id  : null,
        platform    : null,
        startDate   : null,
        endDate     : null
    };

    this.init = function() {
        $funnelFormPage = $('#funnelFormPage');

        this.chartSettings.project_id  = $funnelFormPage.attr('data-project-id');

        this.initSteps();

        SelectorBox().init();
    };

    this.initSteps = function () {
        $(document).on('click', $funnelFormPage.find('.jsAddStep').selector, function() {
            var baseStepElement = $funnelFormPage.find('.jsStepTemplate .jsStep');
            var newStepElement = baseStepElement.clone();
            $funnelFormPage.find('.jsSteps').append(newStepElement);
            that.updateStepsIndexes();
            that.initStep(newStepElement);
        });

        $(document).on('click', $funnelFormPage.find('.jsRemoveStep').selector, function() {
            $(this).closest('.jsStep').remove();
            that.updateStepsIndexes();
        });

        $(document).on('change', $funnelFormPage.find('.jsOptional').selector, function() {
            /*if ($(this).data('first')) {
                alert(t('AnalyticsFunnel__First_step_cant_be_optional'));
                $(this).prop('checked', false);
            }*/
            if ($(this).data('last')) {
                alert(t('AnalyticsFunnel__Last_step_cant_be_optional'));
                $(this).prop('checked', false);
            }
        });

        that.updateStepsIndexes();

        $funnelFormPage.find('.jsSteps .jsStep').each(function(){
            that.initStep($(this));
        });

        $funnelFormPage.find('.jsSteps').sortable({
            update: function() {
                that.updateStepsIndexes();
            }
        });
    };

    this.updateStepsIndexes = function() {
        $funnelFormPage.find('.jsSteps .jsStep').each(function(){
            var index = $(this).index();
            $(this).find('input').each(function(){
                var name = $(this).attr('name');
                if (name) {
                    $(this).attr('name', name.replace(/\[steps\]\[[0-9]{0,}\]/, "[steps]["+index+"]"));
                }
                var id = $(this).attr('id');
                if (id) {
                    $(this).attr('id', id.replace(/steps_[0-9]{0,}/, "steps_"+index));
                }
            });
            $(this).find('label').each(function(){
                var forid = $(this).attr('for');
                if (forid) {
                    $(this).attr('for', forid.replace(/steps_[0-9]{0,}/, "steps_"+index));
                }
            });
            $(this).find('.jsStepIndex').text(index + 1);
        });

        $funnelFormPage.find('.jsSteps .jsStep .jsOptional').data('first', 0).data('last', 0);
        //$funnelFormPage.find('.jsSteps .jsStep:first .jsOptional').data('first', 1).prop('checked', false);
        $funnelFormPage.find('.jsSteps .jsStep:last .jsOptional').data('last', 1).prop('checked', false);
    };

    this.initStep = function($step) {
        $step.find('.jsTag').tagAutocomplete({
            source: '/api/tag/tag/autocomplete?project_id='+$funnelFormPage.data('project-id'),
        });
    };

    this.redraw = function() {

    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };
};