import DeviceAttributeChart from './charts/deviceAttributeChart'
import DeviceAttributeTable from './charts/deviceAttributeTable'
import PlatformsChart from './charts/platformsChart'
import SelectorBox from './selectorBox'
import TabContent from '@/jquery/widgets/tabContent'

export default function () {

    var that = this;
    var $analyticsContainer;

    this.chartSettings = {
        project_id  : null
    };

    this.charts = {};
    this.tables = {};
    this.tabContent = null;

    this.init = function () {
        $analyticsContainer = $('#analyticsContent');

        this.chartSettings.project_id  = $analyticsContainer.attr('data-project-id');

        this.tabContent = new TabContent({
            element: $analyticsContainer.find('.tabContentWrapper'),
            save_state: true,
            default_tab: 'device_os',
            on_change: function() {
                $(window).trigger('resize');
            }
        });

        this.charts['platforms'] = new PlatformsChart({
            element: $analyticsContainer.find('.platformsChart .graphWrapper')
        });

        this.charts['device_os_ios'] = new DeviceAttributeChart({
            element: $analyticsContainer.find('.deviceOsIosChart .graphWrapper')
        }, 'device_os_chart', 'ios');
        this.tables['device_os_ios'] = new DeviceAttributeTable({
            element: $analyticsContainer.find('.deviceOsIosTable'),
            on_select_items: function(items) {
                this.reloadTableChart(that.charts['device_os_ios'], items, that.chartSettings);
            }
        }, 'device_os_table', 'ios');

        this.charts['device_os_android'] = new DeviceAttributeChart({
            element: $analyticsContainer.find('.deviceOsAndroidChart .graphWrapper')
        }, 'device_os_chart', 'android');
        this.tables['device_os_android'] = new DeviceAttributeTable({
            element: $analyticsContainer.find('.deviceOsAndroidTable'),
            on_select_items: function(items) {
                this.reloadTableChart(that.charts['device_os_android'], items, that.chartSettings);
            }
        }, 'device_os_table', 'android');

        this.charts['device_model_ios'] = new DeviceAttributeChart({
            element: $analyticsContainer.find('.deviceModelIosChart .graphWrapper')
        }, 'device_model_chart', 'ios');
        this.tables['device_model_ios'] = new DeviceAttributeTable({
            element: $analyticsContainer.find('.deviceModelIosTable'),
            on_select_items: function(items) {
                this.reloadTableChart(that.charts['device_model_ios'], items, that.chartSettings);
            }
        }, 'device_model_table', 'ios');

        this.charts['device_model_android'] = new DeviceAttributeChart({
            element: $analyticsContainer.find('.deviceModelAndroidChart .graphWrapper')
        }, 'device_model_chart', 'android');
        this.tables['device_model_android'] = new DeviceAttributeTable({
            element: $analyticsContainer.find('.deviceModelAndroidTable'),
            on_select_items: function(items) {
                this.reloadTableChart(that.charts['device_model_android'], items, that.chartSettings);
            }
        }, 'device_model_table', 'android');

        SelectorBox().init(false, false, false);

        return this;
    };

    this.reloadCharts = function() {
        delete this.chartSettings['platform'];
        delete this.chartSettings['startDate'];
        delete this.chartSettings['endDate'];

        this.charts['platforms'].reloadByParam(this.chartSettings);
        this.charts['device_os_ios'].reloadByParam(this.chartSettings);
        this.tables['device_os_ios'].reloadByParam(this.chartSettings);
        this.charts['device_os_android'].reloadByParam(this.chartSettings);
        this.tables['device_os_android'].reloadByParam(this.chartSettings);
        this.charts['device_model_ios'].reloadByParam(this.chartSettings);
        this.tables['device_model_ios'].reloadByParam(this.chartSettings);
        this.charts['device_model_android'].reloadByParam(this.chartSettings);
        this.tables['device_model_android'].reloadByParam(this.chartSettings);
    };

    this.redraw = function() {
        that.reloadCharts();
    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };
};