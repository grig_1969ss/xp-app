import UsersEBSChart from './charts/usersEBSChart'
import UsersEBLAChart from './charts/usersEBLAChart'
import SelectorBox from './selectorBox'
import TabContent from '@/jquery/widgets/tabContent'

export default function () {

    var that = this;
    var $analyticsContainer;

    this.chartSettings = {
        project_id  : null,
        platform    : null,
        startDate   : null,
        endDate     : null
    };

    this.charts = {};
    this.tabContent = null;
    this.tab = null;

    this.init = function () {
        $analyticsContainer = $('#analyticsContent');

        this.chartSettings.project_id  = $analyticsContainer.attr('data-project-id');

        SelectorBox().init();

        this.tabContent = new TabContent({
            element: $analyticsContainer.find('.tabContentWrapper'),
            save_state: true,
            default_tab: 'sessions',
            on_change: function(tab) {
                if (!that.charts[tab]) {
                    switch(tab) {
                        case 'sessions':
                            that.charts[tab] = new UsersEBSChart({
                                element: $analyticsContainer.find('.usersEBSChart .graphWrapper'),
                                table: $analyticsContainer.find('.usersEBSTable')
                            });
                            break;
                        case 'last_active':
                            that.charts[tab] = new UsersEBLAChart({
                                element: $analyticsContainer.find('.usersEBLAChart .graphWrapper'),
                                table: $analyticsContainer.find('.usersEBLATable')
                            });
                            break;
                    }
                }
                
                if (tab === 'last_active') {
                    SelectorBox().disableDateSelector('selectorAlltime');
                } else {
                    SelectorBox().enableDateSelector();
                }

                if (that.tab != tab) {
                    that.tab = tab;
                    that.reloadChart();
                }
            }
        });

        return this;
    };

    this.reloadChart = function() {
        if (this.charts[this.tab]) {
            this.charts[this.tab].reloadByParam(this.chartSettings);
        }
    };

    this.redraw = function() {
        that.reloadChart();
    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };
};
