import TagValuesTable from './charts/tagValuesTable'
import TagValuesChart from './charts/tagValuesChart'
import SelectorBox from './selectorBox'

export default function () {

  var that = this;
  var $analyticsContainer;

  this.chartSettings = {
    project_id  : null,
    platform    : null,
    startDate   : null,
    endDate     : null
  };

  this.chartOptions = {
    tag_id : null
  };

  this.tagValuesChart = null;
  this.tagValuesTable = null;

  this.initiated = false;

  this.init = function () {
    $analyticsContainer = $('#analyticsContent');

    this.chartSettings.project_id = $analyticsContainer.attr('data-project-id');
    this.chartOptions.tag_id = $analyticsContainer.attr('data-tag-id');

    this.tagValuesTable = new TagValuesTable({
      element: $analyticsContainer.find('.tagValuesTable'),
      on_select_items: function(items) {
        that.tagValuesChart.showItems(items);
        that.reloadTagValuesChart();
      }
    });

    this.tagValuesChart = new TagValuesChart({
      element: $analyticsContainer.find('.tagValuesChart'),
      mode: 'totals',
      modifier: 'total_hits',
      on_change: function() {
        if (this.modifier && that.tagValuesTable.modifier != this.modifier) {
          that.tagValuesTable.setModifier(this.modifier);
          if (that.initiated) {
            that.reloadTagValuesTable();
          }
        }
      }
    });

    let title = t('Analytics__Tag_values') + ': ' + $analyticsContainer.attr('data-tag-title');
    this.tagValuesChart.chartTimeline.updateTitle(title);
    this.tagValuesChart.chartTimelineUnique.updateTitle(title);
    this.tagValuesChart.chartTotals.updateTitle(title);
    this.tagValuesChart.chartTotalsUnique.updateTitle(title);

    SelectorBox().init();

    this.initiated = true;

    return this;
  };

  this.reloadTagValuesChart = function() {
    this.tagValuesChart.reloadByParam(this.chartSettings, this.chartOptions);
  };

  this.reloadTagValuesTable = function() {
    this.tagValuesTable.reloadByParam(this.chartSettings, this.chartOptions);
  };

  this.redrawBackButton = function() {
    let backButton = '/analytics/analytics/tags?project_id='+this.chartSettings.project_id+SelectorBox().getHashedSelector();
    this.tagValuesChart.chartTimeline.addTitleBackButton(backButton);
    this.tagValuesChart.chartTimelineUnique.addTitleBackButton(backButton);
    this.tagValuesChart.chartTotals.addTitleBackButton(backButton);
    this.tagValuesChart.chartTotalsUnique.addTitleBackButton(backButton);
  };

  this.redraw = function() {
    this.reloadTagValuesTable();
    this.reloadTagValuesChart();
    this.redrawBackButton();
  };

  this.setSettings = function(key,value) {
    this.chartSettings[key] = value;
  };

  this.getSettings = function(key) {
    return this.chartSettings[key];
  };
};
