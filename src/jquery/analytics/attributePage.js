import DeviceAttributeChart from './charts/deviceAttributeChart'
import DeviceAttributeTable from './charts/deviceAttributeTable'
import SelectorBox from './selectorBox'

export default function () {

    var that = this;
    var $analyticsContainer;

    this.chartSettings = {
        project_id  : null
    };

    this.chart = null;
    this.table = null;

    this.init = function () {
        $analyticsContainer = $('#analyticsContent');
        var attribute_key = $analyticsContainer.data('attribute');

        this.chartSettings.project_id  = $analyticsContainer.attr('data-project-id');

        this.chart = new DeviceAttributeChart({
            element: $analyticsContainer.find('.graphWrapper')
        }, attribute_key + '_chart');
        this.table = new DeviceAttributeTable({
            element: $analyticsContainer.find('.attributeTable'),
            on_select_items: function(items) {
                this.reloadTableChart(that.chart, items, that.chartSettings);
            }
        }, attribute_key + '_table');

        this.chart.updateTitle(t("Analytics__Users_by_" + attribute_key));

        SelectorBox().init(false);

        return this;
    };

    this.reloadChart = function() {
        delete this.chartSettings['startDate'];
        delete this.chartSettings['endDate'];

        this.chart.reloadByParam(this.chartSettings);
        this.table.reloadByParam(this.chartSettings);
    };

    this.redraw = function() {
        that.reloadChart();
    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };
};