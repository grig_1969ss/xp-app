import OptInsChart from './charts/optInsChart'
import OptOutsChart from './charts/optOutsChart'
import PushesStatisticsChart from './charts/pushesStatisticsChart'
import SelectorBox from './selectorBox'
import TabContent from '@/jquery/widgets/tabContent'

export default function () {

    var that = this;
    var $analyticsContainer;

    this.chartSettings = {
        project_id  : null,
        platform    : null,
        startDate   : null,
        endDate     : null
    };

    this.charts = {};
    this.tabContent = null;
    this.tab = null;

    this.init = function () {
        $analyticsContainer = $('#analyticsContent');

        this.chartSettings.project_id  = $analyticsContainer.attr('data-project-id');

        SelectorBox().init();

        this.tabContent = new TabContent({
            element: $analyticsContainer.find('.tabContentWrapper'),
            save_state: true,
            default_tab: 'pushes_statistics',
            on_change: function(tab) {
                if (!that.charts[tab]) {
                    switch(tab) {
                        case 'opt_ins':
                            that.charts[tab] = new OptInsChart({
                                element: $analyticsContainer.find('.optInsChart .graphWrapper'),
                                table: $analyticsContainer.find('.optInsTable')
                            });
                            break;
                        case 'opt_outs':
                            that.charts[tab] = new OptOutsChart({
                                element: $analyticsContainer.find('.optOutsChart .graphWrapper'),
                                table: $analyticsContainer.find('.optOutsTable')
                            });
                            break;
                        case 'pushes_statistics':
                            that.charts[tab] = new PushesStatisticsChart({
                                element: $analyticsContainer.find('.pushesStatisticsChart .graphWrapper'),
                                table: $analyticsContainer.find('.pushesStatisticsTable'),
                                show_push_deliveries: ($analyticsContainer.attr('data-show-push-deliveries') == '1')
                            });
                            break;
                    }
                }

                if (that.tab != tab) {
                    that.tab = tab;
                    that.reloadChart();
                }
            }
        });

        return this;
    };

    this.reloadChart = function() {
        if (this.charts[this.tab]) {
            this.charts[this.tab].reloadByParam(this.chartSettings);
        }
    };

    this.redraw = function() {
        that.reloadChart();
    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };
};
