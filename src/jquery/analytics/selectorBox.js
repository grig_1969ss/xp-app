import { getHashParam, setHashParam, parseHashParams, buildHashString } from '@/jquery/utils'
import moment from 'vendor/moment/moment-with-langs.js'

var selectorBox = new function() {

    var that = this;
    var $chartSelectorsPanel = null, $chartSelectorsStatus = null;
    var platformSelector = null, mobilePlatformSelector = null;
    var dateSelector = null, dateStart = null, dateEnd = null;
    var enable_device_selector = true, enable_date_selector = true, enable_mobile_device_selector = true;
    var date_selector_disabled_state_active_button = null, platform_selector_disabled_state_active_button = null;
    var auto_update_analytics_enabled = false, update_analytics_timer = null, update_analytics_timeout = 60000;

    var monthNames = [ t("Common__January"), t("Common__February"), t("Common__March"), t("Common__April"), t("Common__May"), t("Common__June"), t("Common__July"), t("Common__August"), t("Common__September"), t("Common__October"), t("Common__November"), t("Common__December")];

    var dateSelectorTimeDiff = moment($('#date-selector').data('date')).unix() - moment().unix() + 1;

    var server_moment = function() {
        return moment().add('seconds', dateSelectorTimeDiff);
    };

    var updateAnalytics = function() {
        if (auto_update_analytics_enabled) {
            updateDateString();
            window.analyticPage.redraw();
        }
    };

    var initChartSelector = function () {

        /*if ($.cookie('auto_update_analytics_enabled')) {
            auto_update_analytics_enabled = $.cookie('auto_update_analytics_enabled');
            $('#update-analytics-checkbox').prop('checked', auto_update_analytics_enabled);
        }*/

        update_analytics_timer = setInterval(updateAnalytics, update_analytics_timeout);
        enable_device_selector = $chartSelectorsPanel.data('disabled') ? false : enable_device_selector;

        if (enable_device_selector) {
            $('#device-selector .dbutton').click(function () {
                $('#device-selector .dbutton').removeClass('active');
                $(this).addClass('active');

                setHashParam('platform', $(this).data('value'));
                selectPlatform($(this).data('value'));

                if ($(this).data('value') == 'mobile') {
                    $('#mobile-device-selector').show();
                    window.analyticPage.redraw();
                } else {
                    if ($('#mobile-device-selector').length) {
                        $('#mobile-device-selector').hide();
                        $('#mobile-device-selector .dbutton[data-value=""]').click();
                    } else {
                        window.analyticPage.redraw();
                    }
                }
            });
        } else {
            $('#device-selector').addClass('disabled');
        }

        if (enable_mobile_device_selector) {
            $('#mobile-device-selector .dbutton').click(function () {
                $('#mobile-device-selector .dbutton').removeClass('active');
                $(this).addClass('active');

                setHashParam('mobilePlatform', $(this).data('value'));
                selectMobilePlatform($(this).data('value'));

                window.analyticPage.redraw();
            });
        } else {
            $('#mobile-device-selector').addClass('disabled');
        }

        if (enable_date_selector) {

            $('#update-analytics-checkbox').change(function() {
                auto_update_analytics_enabled = $(this).prop('checked');
                $.cookie('auto_update_analytics_enabled', auto_update_analytics_enabled);
            });

            $('#date-selector .sbutton').click(function() {
                $('#date-selector .dbutton').removeClass('active');
                $(this).addClass('active');

                setHashParam('date', $(this).data('value'));
                selectDate($(this).data('value'));

                window.analyticPage.redraw();
            });

            var initDatePicker = function() {
                $(window).click(function() {
                    $("#date-picker").hide();
                });

                $("#date-picker").click(function(e) {
                    e.stopPropagation();
                });

                $("#date-picker-button").click(function(e) {
                    $( "#date-picker" ).toggle();
                    e.stopPropagation();
                });

                $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );

                var dateTo = $( "#date-to" ).datepicker({
                    numberOfMonths: 1,
                    showOtherMonths: true,
                    maxDate: server_moment().toDate(),
                    defaultDate: server_moment().toDate(),
                    onSelect: function( selectedDate ) {
                        var instance = $( this ).data("datepicker"),
                            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings),
                            fromLimit = moment(selectedDate).toDate();

                        dateFrom.datepicker("option", "maxDate", fromLimit);
                    },
                    beforeShowDay: function (date) {
                        var ds = $("#date-from").datepicker("getDate"),
                            de = $("#date-to").datepicker("getDate");
                        if (date >= ds && date <= de) {
                            return [true, 'ui-datepicker-inrange', ''];
                        }
                        return [true, '', ''];
                    },
                    monthNames: monthNames
                });

                var dateFrom = $( "#date-from" ).datepicker({
                    numberOfMonths: 1,
                    showOtherMonths: true,
                    maxDate: server_moment().toDate(),
                    defaultDate: server_moment().toDate(),
                    onSelect: function( selectedDate ) {
                        var instance = $(this).data("datepicker"),
                            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings );

                        dateTo.datepicker("option", "minDate", date);
                    },
                    beforeShowDay: function (date) {
                        var ds = $("#date-from").datepicker("getDate"),
                            de = $("#date-to").datepicker("getDate");
                        if (date >= ds && date <= de) {
                            return [true, 'ui-datepicker-inrange', ''];
                        }
                        return [true, '', ''];
                    },
                    monthNames: monthNames
                });

                dateTo.datepicker("refresh");

                $("#date-submit").click(function(event) {
                    $('#date-selector .dbutton').removeClass('active');
                    $('#date-picker-button').addClass('active');

                    var date_start = moment(dateFrom.datepicker("getDate")).format("YYYY-MM-DD");
                    var date_end = moment(dateTo.datepicker( "getDate" )).format("YYYY-MM-DD");

                    setHashParam('date', 'selectorCustom');
                    setHashParam('date_start', date_start);
                    setHashParam('date_end', date_end);
                    selectDate('selectorCustom', date_start, date_end);

                    $("#date-picker").hide();

                    window.analyticPage.redraw();
                });
            };
            initDatePicker();
        } else {
            $('#date-selector').addClass('disabled');
        }

        var device_selector = $chartSelectorsPanel.data("platform");
        if (enable_device_selector && getHashParam('platform')) device_selector = getHashParam('platform');
        $('#device-selector div[data-value="'+device_selector+'"]').addClass('active');
        selectPlatform(device_selector);

        var mobile_device_selector = '';
        if (device_selector == 'mobile') {
            $('#mobile-device-selector').show();
            if (enable_device_selector && getHashParam('mobilePlatform')) mobile_device_selector = getHashParam('mobilePlatform');
            selectMobilePlatform(mobile_device_selector);
        }
        $('#mobile-device-selector div[data-value="'+mobile_device_selector+'"]').addClass('active');

        var date_selector = 'selectorAlltime';
        if (enable_date_selector) {
            if ($chartSelectorsPanel.data('date')) {
                date_selector = 'selectorPlusMinus0Days';
            } else if ($chartSelectorsPanel.data('fixed')) {
                date_selector = 'selectorAlltime';
            } else {
                date_selector = 'selectorToday';
            }
        }
        if (enable_date_selector && getHashParam('date')) {
            date_selector = getHashParam('date');
        }
        if (date_selector=='selectorCustom') {
            if(getHashParam('date_start') && getHashParam('date_end')) {
                selectDate(date_selector, getHashParam('date_start'), getHashParam('date_end'));
                $("#date-to").datepicker("setDate", getHashParam('date_end'));
                $("#date-from").datepicker("setDate", getHashParam('date_start'));
                $("#date-to").datepicker("setDate", getHashParam('date_end'));
            } else {
                date_selector = 'selectorLast30Days';
                selectDate(date_selector);
            }
        } else {
            selectDate(date_selector);
        }
        $('#date-selector div[data-value="'+date_selector+'"]').addClass('active');

        window.analyticPage.redraw();
    };

    var dateRangeString = function(dateSelector, startDate, endDate) {
        var dateStart, dateEnd, dateString;

        if (startDate) {
            dateStart = moment(startDate).toDate();
        } else {
            dateStart = null;
        }

        if (endDate) {
            dateEnd = moment(endDate).toDate();
        } else {
            dateEnd = server_moment().toDate();
        }

        var dateNow = server_moment().toDate();

        if (dateSelector=='selectorAlltime') {
            dateString = t("Analytics__All_time");
        } else if (dateSelector=='selectorToday') {
            dateString = "00:00 - " + (dateNow.getHours()<10?'0':'') + dateNow.getHours() + ":" + (dateNow.getMinutes()<10?'0':'') + dateNow.getMinutes();
        } else {
            var dateStartString = "";
            if (dateStart.getFullYear() != dateEnd.getFullYear() || dateStart.getFullYear() != dateNow.getFullYear()) dateStartString += dateStart.getFullYear() + ", ";
            dateStartString += monthNames[dateStart.getMonth()] + " " + dateStart.getDate();

            var dateEndString = "";
            if (dateStart.getFullYear() != dateEnd.getFullYear() || dateEnd.getFullYear() != dateNow.getFullYear()) dateEndString += dateEnd.getFullYear() + ", ";
            dateEndString += monthNames[dateEnd.getMonth()] + " " + dateEnd.getDate();

            if (dateStartString == dateEndString) {
                return dateStartString;
            } else {
                return dateStartString + " - " + dateEndString;
            }
        }
        return dateString;
    };

    var setDateInLastNDays = function(num) {
        dateStart = server_moment().subtract('days', num).format('YYYY-MM-DD');
        dateEnd = server_moment().format('YYYY-MM-DD');
        window.analyticPage.setSettings('startDate', dateStart);
        window.analyticPage.setSettings('endDate', dateEnd);
    };

    var setDateInLastNMonth = function(num) {
        dateStart = server_moment().subtract('month', num - 1).startOf('month').format('YYYY-MM-DD');
        dateEnd = server_moment().format('YYYY-MM-DD');
        window.analyticPage.setSettings('startDate', dateStart);
        window.analyticPage.setSettings('endDate', dateEnd);
    };

    var setCustomSelectorValue = function(start, end) {
        dateStart = start;
        dateEnd = end;
        setHashParam('date_start', start);
        setHashParam('date_end', end);
        window.analyticPage.setSettings('startDate', dateStart);
        window.analyticPage.setSettings('endDate', dateEnd);
    };

    var setDatePlusMinusNDays = function(num) {
        dateStart = moment($chartSelectorsPanel.data("date-start")).subtract('days', num).format('YYYY-MM-DD');
        dateEnd = moment($chartSelectorsPanel.data("date-end")).add('days', num).format('YYYY-MM-DD');
        var dateEndMax = server_moment().format('YYYY-MM-DD');
        if (dateEnd > dateEndMax) dateEnd = dateEndMax;
        window.analyticPage.setSettings('startDate', dateStart);
        window.analyticPage.setSettings('endDate', dateEnd);
    };

    var setDatePlusMinusNMonth = function(num) {
        dateStart = moment($chartSelectorsPanel.data("date-start")).subtract('month', num - 1).startOf('month').format('YYYY-MM-DD');
        dateEnd = moment($chartSelectorsPanel.data("date-end")).add('month', num - 1).endOf('month').format('YYYY-MM-DD');
        var dateEndMax = server_moment().format('YYYY-MM-DD');
        if (dateEnd > dateEndMax) dateEnd = dateEndMax;
        window.analyticPage.setSettings('startDate', dateStart);
        window.analyticPage.setSettings('endDate', dateEnd);
    };


    var updateDateString = function() {
      $('.chartSelectorsStatus span').text(dateRangeString(dateSelector, dateStart, dateEnd));

      if (window.location.pathname === '/analytics/analytics/index-old') {
        $('.chartSelectorsStatus .dashboard-switcher').show();
      }
    };

    var selectDate = function(selector, custom_start, custom_end) {
        dateSelector = selector;

        if (selector == 'selectorToday') {
            $('.update-analytics-holder').show();
        } else {
            $('.update-analytics-holder').hide();
        }

        switch(selector) {
            case 'selectorToday':
                setDateInLastNDays(0);
                break;
            case 'selectorLast7Days':
                setDateInLastNDays(6);
                break;
            case 'selectorLast14Days':
                setDateInLastNDays(13);
                break;
            case 'selectorLast30Days':
                setDateInLastNDays(29);
                break;
            case 'selectorLast3Month':
                setDateInLastNMonth(3);
                break;
            case 'selectorLast12Month':
                setDateInLastNMonth(12);
                break;
            case 'selectorPlusMinus0Days':
                setDatePlusMinusNDays(0);
                break;
            case 'selectorPlusMinus1Days':
                setDatePlusMinusNDays(1);
                break;
            case 'selectorPlusMinus3Days':
                setDatePlusMinusNDays(3);
                break;
            case 'selectorPlusMinus7Days':
                setDatePlusMinusNDays(7);
                break;
            case 'selectorPlusMinus14Days':
                setDatePlusMinusNDays(14);
                break;
            case 'selectorPlusMinus30Days':
                setDatePlusMinusNDays(30);
                break;
            case 'selectorPlusMinus3Month':
                setDatePlusMinusNMonth(3);
                break;
            case 'selectorAlltime':
                setCustomSelectorValue("", "");
                break;
            case 'selectorCustom':
                setCustomSelectorValue(custom_start, custom_end);
                break;
        }

        updateDateString();
    };

    var selectPlatform = function(selector) {
        platformSelector = selector;
        window.analyticPage.setSettings('platform', selector);
    };

    var selectMobilePlatform = function (selector) {
        mobilePlatformSelector = selector;
        window.analyticPage.setSettings('mobilePlatform', selector);
    }

    this.init = function(_enable_date_selector, _enable_device_selector, _enable_mobile_device_selector) {
        if (!(_enable_device_selector===undefined)) enable_device_selector = _enable_device_selector;
        if (!(_enable_date_selector===undefined)) enable_date_selector = _enable_date_selector;
        if (!(_enable_mobile_device_selector===undefined)) enable_mobile_device_selector = _enable_mobile_device_selector;

        $chartSelectorsPanel = $('#chartSelectors');
        $chartSelectorsStatus = $('.chartSelectorsStatus');

        initChartSelector();
    };



    var addDisabledStateLayer = function(selector) {
        selector.append('<span id="splash" style="display: block; position: absolute; top; 0; left: 0; width: 100%; height: 100%;"></span>');
    };

    this.disableDateSelector = function(active_button_selector) {
        $('#date-selector').addClass('disabled');
        date_selector_disabled_state_active_button = $('#date-selector .dbutton.active').data('value');

        if (!(active_button_selector===undefined)) {
            selectDate(active_button_selector, getHashParam('date_start'), getHashParam('date_end'));
            $('#date-selector .dbutton').removeClass('active');
            $('#date-selector .dbutton[data-value="' + active_button_selector + '"]').addClass('active');
        }

        addDisabledStateLayer($('#date-selector'));
    };

    this.enableDateSelector = function() {
        $('#date-selector').removeClass('disabled');
        $('#date-selector #splash').remove();

        if (date_selector_disabled_state_active_button) {
            selectDate(date_selector_disabled_state_active_button, getHashParam('date_start'), getHashParam('date_end'));
            $('#date-selector .dbutton').removeClass('active');
            $('#date-selector .dbutton[data-value="' + date_selector_disabled_state_active_button + '"]').addClass('active');
            date_selector_disabled_state_active_button = null;
        }
    };

    this.disablePlatformSelector = function(active_button_index) {
        if (!$chartSelectorsStatus) this.init();
        $('#device-selector').addClass('disabled');
        platform_selector_disabled_state_active_button = $('#device-selector .dbutton.active').data('value');

        if (!(active_button_index===undefined)) {
            selectPlatform(active_button_index);
            $('#device-selector .dbutton').removeClass('active');
            $('#device-selector .dbutton:eq(' + active_button_index + ')').addClass('active');
        }

        addDisabledStateLayer($('#device-selector'));
    };

    this.enablePlatformSelector = function() {
        if (!$chartSelectorsStatus) this.init();
        $('#device-selector').removeClass('disabled');
        $('#device-selector #splash').remove();

        if (platform_selector_disabled_state_active_button) {
            selectPlatform(platform_selector_disabled_state_active_button);
            $('#device-selector .dbutton').removeClass('active');
            $('#device-selector .dbutton[data-value="' + platform_selector_disabled_state_active_button + '"]').addClass('active');
            platform_selector_disabled_state_active_button = null;
        }
    };

    var cookie = function(key, value) {
        key += '_' + window.analyticPage.chartSettings.project_id;
        if ($chartSelectorsPanel.data('id')) {
            key += '_' + $chartSelectorsPanel.data('id');
        }
        return $.cookie(key, value);
    };

    this.getHashedSelector = function () {
        var params = parseHashParams(window.location.hash);
        var hashParams = {
            'platform': params['platform'],
            'date': params['date'],
            'date_start': params['date_start'],
            'date_end': params['date_end']
        };
        if (params['platform'] == 'mobile' && params['mobilePlatform']) {
            hashParams['mobilePlatform'] = params['mobilePlatform'];
        }
        return buildHashString(hashParams);
    };

    this.getDateSelector = function() {
        return dateSelector;
    };
}

export default function () {
    return selectorBox;
}
