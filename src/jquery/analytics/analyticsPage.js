import OnelineChart from './charts/onelineChart'
import SelectorBox from './selectorBox'
import { getHashParam, setHashParam, addCommas, formatPeriod } from '@/jquery/utils'
import { ajaxLoaderShow, ajaxLoaderHide } from '@/jquery/widgets/ajaxLoader'
import '@/font/Phoca/phoca-flags.css'

export default function () {

  var that = this;
  var $analyticsContainer,
    $analyticsMenu
      = null;

  var tab = null;

  this.chartSettings = {
    project_id: null,
    platform: null,
    mobilePlatform: null,
    startDate: null,
    endDate: null,
    method: null,
    value_formatter: null
  };

  this.chart = null;

  this.init = function () {
    $analyticsContainer = $('#analyticsContent');
    $analyticsMenu = $analyticsContainer.find('.menuDevice');
    $analyticsMenu.show();

    this.chartSettings.project_id = $analyticsContainer.attr('data-project-id');

    this.chart = new OnelineChart({
      element: $analyticsContainer.find('.graphWrapper'),
      mode: $analyticsMenu.data('default-mode')
    });

    $analyticsMenu.find('a').click(function () {
      that.applyNewSettings($(this));
      that.reloadChart();
      return false;
    });

    tab = getHashParam('tab');
    if (!tab || !$analyticsMenu.find('a[data-value="' + tab + '"]').length) {
      this.applyNewSettings($analyticsMenu.find('a[data-default="1"]').eq(0));
    }
    else {
      this.applyNewSettings($analyticsMenu.find('a[data-value="' + tab + '"]').eq(0));
    }

    SelectorBox().init();

    return this;
  };

  this.applyNewSettings = function (el) {
    setHashParam('tab', $(el).data('value'));
    $analyticsMenu.find('a').removeClass('active');
    $(el).addClass('active');

    that.chart.updateTitle($(el).data('title'));
    that.chart.chartSettings.series[0].name = $(el).data('seria');
    that.chartSettings.method = $(el).data('value');
    that.chartSettings.value_formatter = $(el).data('formatter');
    if (!that.chartSettings.value_formatter) that.chartSettings.value_formatter = null;
  };

  this.reloadChart = function () {
    this.chart.reloadByParam(this.chartSettings);
  };

  this.redraw = function () {
    ajaxLoaderShow(function () {
      that.reloadPlatform();
      that.updateNavigationValues();
      that.reloadChart();
    });
  };

  this.reloadPlatform = function () {
    var activeTab = $analyticsMenu.find('a[data-value="' + getHashParam('tab') + '"]').parent();

    $analyticsMenu.find('li').hide();
    if (this.chartSettings['platform'] === 'mobile') {
      $analyticsMenu.removeClass('menuDeviceWeb').find('li.menuItemMobile').show();
      if (!activeTab.hasClass('menuItemMobile')) {
        this.applyNewSettings($analyticsMenu.find('a[data-default="1"]').eq(0));
      }
      $analyticsContainer.find('.statusWeb').hide();
      $analyticsContainer.find('.statusMobile').show();
    } else {
      $analyticsMenu.addClass('menuDeviceWeb').find('li.menuItemWeb').show();
      if (!activeTab.hasClass('menuItemWeb')) {
        this.applyNewSettings($analyticsMenu.find('a[data-default-web="1"]').eq(0));
      }
      $analyticsContainer.find('.statusMobile').hide();
      $analyticsContainer.find('.statusWeb').show();
    }
  };

  this.updateNavigationValues = function () {
    var sendData = {};
    var fields = ['project_id', 'platform', 'mobilePlatform', 'startDate', 'endDate'];
    for (var i in fields) {
      if (this.chartSettings[fields[i]]) {
        sendData[fields[i]] = this.chartSettings[fields[i]];
      }
    }

    $.ajax(API_URL + '/analytics/analytics/stat', {
      data: sendData
    }).success(function (data) {
      console.log(data);
      var keys = [];
      $analyticsMenu.find('a').each(function () {
        keys.push($(this).data('value'));
      });
      for (var i in keys) {
        var total = 0;
        if (data[keys[i]]) {
          data[keys[i]].totals.amount = parseInt(data[keys[i]].totals.amount);
          //data[keys[i]].previous_totals.amount = parseInt(data[keys[i]].previous_totals.amount);

          var formatter = $analyticsMenu.find('a[data-value="' + keys[i] + '"]').data("formatter");
          total = data[keys[i]].totals.amount;
        }
        if (formatter == 'period') {
          total = formatPeriod(total);
        } else if (formatter == 'percent') {
          total = total + '%';
        } else {
          total = addCommas(total);
        }

        $analyticsMenu.find('a[data-value="' + keys[i] + '"] .fotterDevice span').text(total);

        /*var progress = {};
         if (data[keys[i]].previous_totals.amount < 0) {
         progress.style = 'None';
         progress.icon = 'up';
         progress.value = '<div class="progressUnd"><i class="icon-down-dir"></i></div>';
         } else if (data[keys[i]].totals.amount == data[keys[i]].previous_totals.amount) {
         progress.style = 'Up';
         progress.icon = 'up';
         progress.value = '0%';
         } else if (data[keys[i]].previous_totals.amount == 0) {
         progress.style = 'Up';
         progress.icon = 'up';
         progress.value = '∞';
         } else if (data[keys[i]].totals.amount > data[keys[i]].previous_totals.amount) {
         progress.style = 'Up';
         progress.icon = 'up';
         progress.value = Math.round((data[keys[i]].totals.amount / data[keys[i]].previous_totals.amount - 1) * 100 * 10) / 10;
         progress.is_number = true;
         } else {
         progress.style = 'Down';
         progress.icon = 'down';
         progress.value = -Math.round((1 - data[keys[i]].totals.amount / data[keys[i]].previous_totals.amount) * 100 * 10) / 10;
         progress.is_number = true;
         }
         if (progress.is_number) {
         if (progress.value >= 1000) {
         progress.value = '>1000';
         } else if (progress.value < 100 && progress.value > -100) {
         progress.value = progress.value.toFixed(1);
         } else if (progress.value < -100) {
         progress.value = -100;
         } else {
         progress.value = Math.floor(progress.value);
         }
         progress.value += '%';
         }

         $analyticsMenu.find('a[data-value="'+keys[i]+'"] .progressIcon').removeAttr('class').addClass('progressIcon').addClass('progress'+progress.style);
         $analyticsMenu.find('a[data-value="'+keys[i]+'"] .progressImg').removeAttr('class').addClass('progressImg').addClass('icon-'+progress.icon+'-dir');
         $analyticsMenu.find('a[data-value="'+keys[i]+'"] .progressValue').html(progress.value);
         $analyticsMenu.find('a[data-value="'+keys[i]+'"] .progressIcon').attr('title', data[keys[i]].previous_totals.period_title);*/
      }
      ajaxLoaderHide();
    }).error(function () {
      $analyticsMenu.find('a .fotterDevice span').html("?");
      ajaxLoaderHide();
    });
  };

  this.setSettings = function (key, value) {
    this.chartSettings[key] = value;
  };

  this.getSettings = function (key) {
    return this.chartSettings[key];
  };
};
