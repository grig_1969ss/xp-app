import CountriesChart from './charts/countriesChart'
import SelectorBox from './selectorBox'

export default function () {

    var that = this;
    var $analyticsContainer;

    this.chartSettings = {
        project_id  : null,
        platform    : null
    };

    this.chart = null;

    this.init = function () {
        $analyticsContainer = $('#analyticsContent');

        this.chartSettings.project_id  = $analyticsContainer.attr('data-project-id');

        this.chart = new CountriesChart({
            element: $analyticsContainer.find('.graphWrapper'),
            table: $analyticsContainer.find('.table'),
            show_addressable: ($analyticsContainer.attr('data-show-addressable') == '1')
        });

        SelectorBox().init(false);

        return this;
    };

    this.reloadChart = function() {
        delete this.chartSettings.startDate;
        delete this.chartSettings.endDate;
        this.chart.reloadByParam(this.chartSettings);
    };

    this.redraw = function() {
        that.reloadChart();
    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };
};
