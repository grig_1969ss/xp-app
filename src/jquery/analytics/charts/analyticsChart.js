import DataTable from '@/jquery/widgets/dataTable'
import { ajaxLoaderShow, ajaxLoaderHide } from '@/jquery/widgets/ajaxLoader'
import { addCommas, formatPeriod } from '@/jquery/utils'
import { twig } from 'vendor/twig/twig'

export default function (settings, inherit) {

    var that = this;
    var $chartContainer = this.chartContainer = settings.element;
    var $tableContainer = this.tableContainer = settings.table;
    this.initiated = false;

    this.method  = settings.method;
    this.addTotals = settings.addTotals;
    this.addPreviousTotals = settings.addPreviousTotals;

    this.project_id = settings.project_id;
    this.platform   = settings.platform;
    this.startDate  = settings.startDate;
    this.endDate    = settings.endDate;
    this.mobilePlatform = settings.mobilePlatform;

    this.options    = null;
    this.ids        = null;

    this.chart      = null;
    this.chartBox   = null;
    this.data  = null;
    this.chartType  = (typeof Highcharts != 'undefined') ? Highcharts.Chart : null;
    this.chartSettings = {
        chart: {
            type: 'column',
            backgroundColor: 'transparent',
            borderRadius: 0
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: {
            categories: [],
            lineColor: '#eee',
            labels: {
                rotation: -45,
                align: 'center',
                y: 30,
                style: {
                    fontSize: '11px',
                    fontFamily: 'Verdana, sans-serif',
                    color:'#32404d'
                }
            }
        },
        yAxis: {
            title: null,
            allowDecimals:false,
            maxPadding: 0,
            tickPixelInterval: 20,
            gridLineWidth: 1,
            gridLineColor: '#eee',
            labels: {
                style: {
                    color:'#32404d'
                }
            },
            min: 0
        },
        tooltip: {
            formatter: function() {
                var point = this.point;

                var val = this.y;
                if (point.high || point.low) val = point.low - point.high;
                if (!(point.y_displayed===undefined)) val = point.y_displayed;
                if (that.value_formatter=='period') {
                    val = formatPeriod(val);
                } else if (that.value_formatter=='percent') {
                    val = val + '%';
                } else {
                    val = addCommas(val);
                }

                var str = "";
                if (that.chartSettings.tooltip.index_title) {
                    if (!point.n && point.category) point.n = point.category;
                    str += '<span style="font-size: 10px; color: #666666;">'+that.chartSettings.tooltip.index_title+': '+point.n+'</span>';
                    str += '<br>';
                }
                str += '<span style="color: '+this.series.color+'">'+$('<div>').text(this.series.name).html()+':</span> <span style="font-weight: bold">'+$('<div>').text(val).html()+'</span>';

                return str;
            },
            index_title: t("Common__Date")
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        series: [
            {
                color: '#1d87c8',
                lineColor: '#1d87c8',
                marker: {fillColor: '#ffffff', lineColor: '#1d87c8', lineWidth: 2, radius: 3, symbol: 'circle'},
                data: [],
                type: 'line'
            }
        ],
        exporting: {
            enabled: false
        },
        colorsByIndex: {},
        settings: settings.chartSettings
    };

    this.table = null;
    this.tableSettings = {
        data: 'data',
        row_sort: true,
        row_sort_default_column: 0,
        row_sort_default_order: 'asc'
    };

    this.process_point = null;
    this.value_formatter  = settings.value_formatter;

    this.on_load_data = settings.on_load_data;


    this.init = function() {
        this.initiated = true;
        if ($chartContainer)
        {
            this.chartBox = $chartContainer.find('.graph');
            this.chartSettings.chart.renderTo = this.chartBox[0];

            if (this.value_formatter) {
                this.setValueFormatter(this.value_formatter);
            }

            this.initButtons();
            this.initTotals();
        }

        this.initTable();
    };


    this.load = function() {
        if (this.chart) return;

        ajaxLoaderShow(function(){
            that.dataRequest(
                function(data) {
                    that.data = data;

                    if (typeof that.on_load_data == 'function') {
                        $.proxy(that.on_load_data(), this);
                    }

                    that.buildGraph();

                    $(window).trigger('resize');
                    ajaxLoaderHide();
                }
            );
        });
    };

    this.buildGraph = function() {
        if(this.data) {
            this.processData();
            if (this.chartBox) {
                this.chart = new this.chartType(this.chartSettings);
                this.renderTotals();
            }
            this.renderTable();
        }
    };

    this.processData = function () {
        for (var i=0; i<this.chartSettings.series.length; i++) {
            this.chartSettings.series[i].data = [];
        }
        this.chartSettings.xAxis.categories = [];

        $.each(this.data.data,function(index,value) {
            var add_index = false;

            for (var i=0; i<that.chartSettings.series.length; i++)
            {
                if (that.chartSettings.series[i].chart === undefined || that.chartSettings.series[i].chart)
                {
                    var val = undefined;
                    var key = that.chartSettings.series[i].key;
                    if (that.chartSettings.series[i].value) {
                        val = that.chartSettings.series[i].value(index, value);
                    } else {
                        if (!key) {
                            if (!isNaN(parseFloat(value))) {
                                val = parseFloat(value);
                            } else {
                                key = 'amount';
                            }
                        }
                        if (val===undefined) {
                            if (typeof key == 'function') {
                                val = key(index, value);
                            } else {
                                if (value === null) {
                                    val = null;
                                } else if (value[key]===undefined) {
                                    val = 0;
                                } else {
                                    val = parseFloat(value[key]);
                                }
                            }
                        }
                    }

                    var point = {
                        y: val,
                        i: index,
                        n: index,
                        v: value,
                        k: key
                    };

                    if (point.v !== null && point.v.title) {
                        point.n = point.v.title;
                    } else {
                        point.n = point.i;
                    }
                    if (that.chartSettings.colorsByIndex[point.i]) {
                        point.color = that.chartSettings.colorsByIndex[point.i];
                    }

                    if (typeof that.process_point == 'function') {
                        point = that.process_point(point);
                    }
                    if (point) {
                        that.chartSettings.series[i].data.push(point);
                        add_index = true;
                    }
                }
            }

            if (add_index) {
                that.chartSettings.xAxis.categories.push(index);
            }
        });
    };

    this.reload = function() {
        if (!this.initiated) {
            this.init();
        }
        else {
            this.data = null;

            if (this.chart) {
                if (typeof this.chart.destroy == 'function') {
                    this.chart.destroy();
                } else if (typeof this.chart.remove == 'function') {
                    this.chart.remove();
                }

                this.chart = null;
            }

            this.load();
        }
    };

    this.reloadByParam = function(settings, options) {

        if (!(settings===undefined))
        {
            if (!(settings.platform===undefined)) {
                this.platform = settings.platform;
            }

            if (!(settings.mobilePlatform===undefined)) {
                this.mobilePlatform = settings.mobilePlatform;
            }

            if (!(settings.startDate===undefined)) {
                this.startDate = settings.startDate;
            }

            if (!(settings.endDate===undefined)) {
                this.endDate = settings.endDate;
            }

            if (!(settings.method===undefined)) {
                this.method = settings.method;
            }

            if (!(settings.project_id===undefined)) {
                this.project_id = settings.project_id;
            }

            if (!(settings.value_formatter===undefined)) {
                this.setValueFormatter(settings.value_formatter);
            }
        }

        if (!(options===undefined)) {
            this.options = options;
        }

        this.reload();
    };

    this.requestLink = function(requestData) {
        let requestLink = API_URL + '/analytics/analytics/call';
        requestData = requestData ? requestData : this.requestData();

        let first = true;
        for (let i in requestData) {
            if (first) requestLink += '?'; else requestLink += '&'; first = false;
            requestLink += i + '=' + encodeURI(requestData[i]);
        }

        return requestLink;
    };

    this.requestData = function() {
        var requestData = {};

        requestData['project_id'] = this.project_id;
        requestData['method'] = this.method;
        if (this.addTotals) {
            requestData['addTotals'] = 1;
        }
        if (this.addPreviousTotals) {
            requestData['addPreviousTotals'] = 1;
        }

        if (this.platform) {
            requestData['platform'] = this.platform;
        }
        if (this.mobilePlatform) {
            requestData['mobilePlatform'] = this.mobilePlatform;
        }
        if (this.startDate) {
            requestData['startDate'] = this.startDate;
        }
        if (this.endDate) {
            requestData['endDate'] = this.endDate;
        }

        if (this.options) {
            $.each(this.options,function(index,value) {
                if (value != null) {
                    requestData[index] = value;
                }
            });
        }

        if (!(this.ids===null)) {
            requestData['ids'] = this.ids;
        }

        return requestData;
    };

    this.downloadLinkFormat = function(link) {
        return (link ? link : this.requestLink()) + '&download=1';
    };

    this.exportTableLink = function(method) {
        let requestData = {
            'project_id': this.project_id,
            'method': method,
        };

        if (this.startDate) {
            requestData['startDate'] = this.startDate;
        }
        if (this.endDate) {
            requestData['endDate'] = this.endDate;
        }
        if (this.options.campaign_id) {
            requestData['campaign_id'] = this.options.campaign_id;
        }

        return this.downloadLinkFormat(this.requestLink(requestData));
    };

    this.dataRequest = function(doneCallback,errorCallback) {

        var requestLink = this.requestLink();

        console.log(requestLink);

        $.ajax(requestLink).success(function(data){
            console.log(data);
            if (typeof doneCallback == 'function') {
                doneCallback(data);
            }
        }).error(function(){
            if (typeof errorCallback == 'function') {
                errorCallback();
            }
            ajaxLoaderHide();
        });

    };


    this.initTotals = function() {
        $chartContainer.find('.graphChoice').html("");

        var totals_num = 0;

        for (var i=0; i<this.chartSettings.series.length; i++)
        {
            if (this.chartSettings.totalsOrder) var si = this.chartSettings.totalsOrder[i]; else si = i;
            if (this.chartSettings.series[si].totals)
            {
                if (!this.twTotalsItem)
                {
                    twig({
                        href: VIEWS_DIR+"/analytics/views/analytics/templates/chart-total-item.twig",
                        load: function(template) {
                            that.twTotalsItem = template;
                        },
                        async: false
                    });
                }

                if (this.chartSettings.series[si].show_progress===undefined) this.chartSettings.series[si].show_progress = false;

                totals_num++;
                var totalsItem = this.twTotalsItem.render({
                    title: this.chartSettings.series[si].name,
                    key: this.chartSettings.series[si].key,
                    color: this.chartSettings.series[si].lineColor,
                    show_progress: this.chartSettings.series[si].show_progress
                });
                $chartContainer.find('.graphChoice').append(totalsItem);
            }
        }

        var totalsItemWidth = Math.round((100 / totals_num) * 100) / 100;
        $chartContainer.find('.choiceWrapper').css('width', totalsItemWidth+'%');
    };

    this.renderTotals = function() {
        if (!this.chartSettings.series) return;
        for (var i=0; i<this.chartSettings.series.length; i++)
        {
            var key = this.chartSettings.series[i].key;
            if (!key) key = 'amount';

            if (this.chartSettings.series[i].totals && this.data.totals && (!(this.data.totals[key]===undefined) || this.chartSettings.series[i].value))
            {
                var total;
                if (this.chartSettings.series[i].value) {
                    total = this.chartSettings.series[i].value();
                } else {
                    total = addCommas(this.data.totals[key]);
                }
                $chartContainer.find('.choiceWrapper[data-value="'+key+'"] .choiceValue').text(total);

                if (this.data.previous_totals && !(this.data.previous_totals[key]===undefined))
                {
                    this.data.totals[key] = parseInt(this.data.totals[key]);
                    this.data.previous_totals[key] = parseInt(this.data.previous_totals[key]);

                    var progress = {};
                    if (this.data.previous_totals[key] < 0) {
                        progress.style = 'None';
                        progress.icon = 'up';
                        progress.value = '<div class="progressUnd"><i class="icon-down-dir"></i></div>';
                    } else if (this.data.totals[key] == this.data.previous_totals[key]) {
                        progress.style = 'Up';
                        progress.icon = 'up';
                        progress.value = '0%';
                    } else if (this.data.previous_totals[key] == 0) {
                        progress.style = 'Up';
                        progress.icon = 'up';
                        progress.value = '∞';
                    } else if (this.data.totals[key] > this.data.previous_totals[key]) {
                        progress.style = 'Up';
                        progress.icon = 'up';
                        progress.value = Math.round((this.data.totals[key] / this.data.previous_totals[key] - 1) * 100 * 10) / 10;
                        progress.is_number = true;
                    } else {
                        progress.style = 'Down';
                        progress.icon = 'down';
                        progress.value = -Math.round((1 - this.data.totals[key] / this.data.previous_totals[key]) * 100 * 10) / 10;
                        progress.is_number = true;
                    }
                    if (progress.is_number) {
                        if (progress.value >= 1000) {
                            progress.value = '>1000';
                        } else if (progress.value < 100 && progress.value > -100) {
                            progress.value = progress.value.toFixed(1);
                        } else if (progress.value < -100) {
                            progress.value = -100;
                        } else {
                            progress.value = Math.floor(progress.value);
                        }
                        progress.value += '%';
                    }
                    if (this.chartSettings.series[i].progress_invert) {
                        if (progress.style=='Down') {
                            progress.style = 'Up;'
                        } else if (progress.style=='Up') {
                            progress.style = 'Down';
                        }
                    }

                    $chartContainer.find('.choiceWrapper[data-value="'+key+'"] .progressIcon').removeAttr('class').addClass('progressIcon').addClass('progress'+progress.style);
                    $chartContainer.find('.choiceWrapper[data-value="'+key+'"] .progressImg').removeAttr('class').addClass('progressImg').addClass('icon-'+progress.icon+'-dir');
                    $chartContainer.find('.choiceWrapper[data-value="'+key+'"] .progressValue').html(progress.value);
                    $chartContainer.find('.choiceWrapper[data-value="'+key+'"] .progressIcon').attr('title', this.data.previous_totals.period_title);
                }
            }
        }
    };


    this.initTable = function() {
        if ($tableContainer) {
            this.table = new DataTable(jQuery.extend(this.tableSettings, {
                element: $tableContainer,
                empty_label: t("Analytics__There_is_no_" + this.method)
            }));
        }
    };

    this.renderTable = function() {
        if ($tableContainer) {
            this.table.data = this.data[this.tableSettings.data];
            this.table.reload();
        }
    };


    this.updateTitle = function(title) {
        $chartContainer.find('.graphTitle span:first').text(title);
    };

    this.addTitleBackButton = function(link) {
        var $link = $("<a>").addClass('backButton').attr('href', link).append("<i class='icon-level-up'></i>");
        $chartContainer.find('.backButton').remove();
        $chartContainer.find('.graphTitle').prepend($link);
    };

    this.initButtons = function() {
        this.chartContainer.find('.opt2').click(function() {
            window.location = that.downloadLinkFormat();
        });

        this.chartContainer.find('.opt2Table').click(function() {
            let method = $(this).closest('[data-export-table-method]').data('export-table-method');
            if (method) {
                window.location = that.exportTableLink(method);
            }
        });
    };

    this.setValueFormatter = function(formatter) {
        this.value_formatter = formatter;
        if (this.value_formatter=='period') {
            this.chartSettings.yAxis.labels.formatter = function() {
                return formatPeriod(this.value);
            };
        }
        else if (this.value_formatter=='percent') {
            this.chartSettings.yAxis.labels.formatter = function() {
                return this.value+'%';
            };
        }
        else {
            delete this.chartSettings.yAxis.labels.formatter;
        }
    }

    this.showIndexes = function(indexes) {
        for (var i in indexes) {
            indexes[i] = indexes[i].toString();
        }
        var redraw = false;
        $(this.chart.series).each(function(){
            $(this.data).each(function(){
                var visible = ($.inArray(this.i, indexes) >= 0);
                if (visible != this.visible) {
                    this.update({
                        visible: visible
                    }, false);
                    redraw = true;
                }
            });
        });
        if (redraw) {
            this.chart.redraw();
        }
    }


    if (!inherit) {
        this.init();
    }

    return this;
};
