import AnalyticsChart from './analyticsChart'
import moment from 'vendor/moment/moment-with-langs.js'

export default function (settings) {

  var chart = new AnalyticsChart(settings, true);

  chart.method = 'email_statistics';

  chart.options = {

  };

  chart.chartSettings.plotOptions = {
    column: {
      stacking: 'normal',
      borderWidth: 0
    }
  };

  chart.chartSettings.series = [
    {
      color: '#6BB3DE',
      lineColor: '#6BB3DE',
      marker: {fillColor: '#ffffff', lineColor: '#6BB3DE', lineWidth: 2, radius: 3, symbol: 'circle'},
      name: t('Analytics__Email_sent'),
      data: [],
      visible: true,
      type: 'column',
      key: 'email_sent',
      totals: true
    },{
      color: '#1d87c8',
      lineColor: '#1d87c8',
      marker: {fillColor: '#ffffff', lineColor: '#1d87c8', lineWidth: 2, radius: 3, symbol: 'circle'},
      name: t('Analytics__Email_opened'),
      data: [],
      visible: true,
      type: 'column',
      key: 'email_opened',
      totals: true
    },{
      color: '#055483',
      lineColor: '#055483',
      marker: {fillColor: '#055483', lineColor: '#055483', lineWidth: 2, radius: 2, symbol: 'circle'},
      name: t('Analytics__Email_clicked'),
      data: [],
      visible: true,
      type: 'column',
      key: 'email_clicked',
      totals: true
    }
  ];

  chart.tableSettings.columns = [
    {
      type: 'string',
      value: 'index',
      min_width: '200px',
      sort: true,
      sort_default: 'asc',
      search: true,
      name: t('Analytics__Date')
    },{
      type: 'separator',
      width: '20px',
    },{
      type: 'number',
      value: 'email_sent',
      width: '80px',
      name: t('all', 'Analytics__Campaigns_table_email_sent'),
      class: 'smallHeader centerHeader',
      group: true,
      sort: true,
      sort_default: 'desc'
    },{
      type: 'number',
      value: 'email_delivered',
      width: '80px',
      name: t('all', 'Analytics__Campaigns_table_email_delivered'),
      class: 'smallHeader centerHeader',
      group: true,
      sort: true,
      sort_default: 'desc'
    },{
      type: 'number',
      value: 'email_opened',
      width: '80px',
      name: t('all', 'Analytics__Campaigns_table_email_opened'),
      class: 'smallHeader centerHeader',
      group: true,
      sort: true,
      sort_default: 'desc'
    },{
      type: 'percent',
      value: 'email_open_rate',
      width: '80px',
      name: t('all', 'Analytics__Campaigns_table_email_open_rate'),
      class: 'smallHeader centerHeader',
      group: true,
      sort: true,
      sort_default: 'desc'
    },{
      type: 'number',
      value: 'email_clicked',
      width: '80px',
      name: t('all', 'Analytics__Campaigns_table_email_clicked'),
      class: 'smallHeader centerHeader',
      group: true,
      sort: true,
      sort_default: 'desc'
    },{
      type: 'percent',
      value: 'email_click_rate',
      width: '80px',
      name: t('all', 'Analytics__Campaigns_table_email_click_rate'),
      class: 'smallHeader centerHeader',
      group: true,
      sort: true,
      sort_default: 'desc'
    },{
      type: 'number',
      value: 'email_failed',
      width: '80px',
      name: t('all', 'Analytics__Campaigns_table_email_failed'),
      class: 'smallHeader centerHeader',
      group: true,
      sort: true,
      sort_default: 'desc'
    },{
      type: 'number',
      value: 'email_unsubscribed',
      width: '80px',
      name: t('all', 'Analytics__Campaigns_table_email_unsubscribed'),
      class: 'smallHeader centerHeader',
      group: true,
      sort: true,
      sort_default: 'desc'
    },{
      type: 'number',
      value: 'email_complained',
      width: '80px',
      name: t('all', 'Analytics__Campaigns_table_email_complained'),
      class: 'smallHeader centerHeader',
      group: true,
      sort: true,
      sort_default: 'desc'
    }
  ];

  chart.process_point = function(point) {
    if (point.v === null) {
      return [point, null, null];
    }
    if (point.k=='email_sent') {
      point.y_displayed = point.v.email_sent;
      point.y = parseInt(point.v.email_sent) - parseInt(point.v.email_opened);
    }
    if (point.k=='email_opened') {
      point.y_displayed = point.v.email_opened;
      point.y = parseInt(point.v.email_opened) - parseInt(point.v.email_clicked);
    }
    return point;
  };

  chart.updateTitle(t("Analytics__Email_statistics"));

  chart.init();

  return chart;
};