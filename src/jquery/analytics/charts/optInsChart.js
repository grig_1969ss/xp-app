import AnalyticsChart from './analyticsChart'

export default function (settings) {

    var chart = new AnalyticsChart(settings, true);

    chart.method = 'opt_ins';
    chart.addTotals = true;

    chart.options = {

    };

    chart.chartSettings.series = [
        {
            name: t("Analytics__Opt_ins"),
            color: '#1d87c8',
            lineColor: '#1d87c8',
            marker: {fillColor: '#ffffff', lineColor: '#1d87c8', lineWidth: 2, radius: 3, symbol: 'circle'},
            totals: true,
            key: 'amount',
            type: 'column'
        }
    ];

    chart.tableSettings.columns = [
        {
            type: 'number',
            value: 'index',
            sort: true,
            sort_value: 'index_key',
            name: t('Common__Date'),
            width: '50%'
        },{
            type: 'number',
            value: 'amount',
            sort: true,
            name: t('Analytics__Opt_ins'),
            width: '50%'
        }
    ];

    chart.initFilter = function() {
        chart.chartContainer.find('.options').append(
            '<button class="graphType graphTypeFirst graphTypeLast date-picker-button" style="width: 140px"><i class="icon icon-search"></i> '+t('Analytics__Pushes_statistics_filter')+'</button>'
        );

        chart.chartContainer.append(
            '<div class="date-picker-period" style="position:absolute; z-index:10; right: 7px; top: 53px; display: none;">'+
            '   <div style="overflow:auto; margin-top:6px; padding:3px;">'+
            '       <div style="float: left; margin: 0 5px;"><input class="browser-opt-version" type="text" style="width: 219px; height: 31px; line-height: 31px" placeholder="'+t('Analytics__Browser_opt_version')+'"></div>'+
            '       <div style="float: right; margin: 0 5px;"><input class="browser-version" type="text" style="width: 219px; height: 31px; line-height: 31px" placeholder="'+t('Analytics__Browser_version')+'"></div>'+
            '   </div>'+
            '   <div style="overflow:auto; margin-top:6px; padding:3px;">'+
            '       <div style="float: left; margin: 0 5px;"><input class="browser-os-opt-version" type="text" style="width: 219px; height: 31px; line-height: 31px" placeholder="'+t('Analytics__Browser_os_opt_version')+'"></div>'+
            '       <div style="float: right; margin: 0 5px;"><input class="browser-os-version" type="text" style="width: 219px; height: 31px; line-height: 31px" placeholder="'+t('Analytics__Browser_os_version')+'"></div>'+
            '   </div>'+
            '   <div style="overflow:auto; margin-top:6px; padding:3px; text-align: center;">'+
            '       <div style="display: inline-block; border: 1px solid #c2c4c6; border-radius: 4px; padding: 6px 12px; margin-right: 4px; cursor: pointer;" class="date-reset cly-button-dark button"><i class="icon icon-cancel"></i> '+t('Common__Reset')+'</div>'+
            '       <div style="display: inline-block; border: 1px solid #c2c4c6; border-radius: 4px; padding: 6px 12px; margin-left: 4px; cursor: pointer;" class="date-submit cly-button-dark button"><i class="icon icon-check"></i> '+t('Common__Apply')+'</div>'+
            '   </div>'+
            '</div>'
        );

        var initDatePicker = function() {
            $(window).click(function() {
                chart.chartContainer.find('.date-picker-period').hide();
            });

            chart.chartContainer.find('.date-picker-period').click(function(e) {
                e.stopPropagation();
            });

            chart.chartContainer.find('.date-picker-button').click(function(e) {
                chart.chartContainer.find('.date-picker-period').toggle();
                e.stopPropagation();
            });

            chart.chartContainer.find('.date-submit').click(function(event) {
                var browser_version = chart.chartContainer.find('.browser-version').val();
                var browser_opt_version = chart.chartContainer.find('.browser-opt-version').val();
                var browser_os_version = chart.chartContainer.find('.browser-os-version').val();
                var browser_os_opt_version = chart.chartContainer.find('.browser-os-opt-version').val();

                chart.chartContainer.find('.date-picker-button').addClass('active');
                chart.chartContainer.find('.date-picker-period').hide();

                chart.options.browserVersion = browser_version;
                chart.options.browserOptVersion = browser_opt_version;
                chart.options.browserOsVersion = browser_os_version;
                chart.options.browserOsOptVersion = browser_os_opt_version;
                chart.method = 'opt_ins_filter';

                window.analyticPage.redraw();
            });

            chart.chartContainer.find('.date-reset').click(function(event) {
                chart.chartContainer.find('.date-picker-button').removeClass('active');
                chart.chartContainer.find('.date-picker-period').hide();
                chart.chartContainer.find('.browser-version').val('');
                chart.chartContainer.find('.browser-opt-version').val('');
                chart.chartContainer.find('.browser-os-version').val('');
                chart.chartContainer.find('.browser-os-opt-version').val('');

                delete chart.options.browserVersion;
                delete chart.options.browserOptVersion;
                delete chart.options.browserOsVersion;
                delete chart.options.browserOsOptVersion;
                chart.method = 'opt_ins';

                window.analyticPage.redraw();
            });
        };
        initDatePicker();

        chart.chartContainer.find('.graphMode').click(function() {
            chart.chartContainer.find('.graphMode').removeClass('active');
            $(this).addClass('active');
            chart.setMode($(this).data('mode'));
            chart.reload();
        });
    };

    chart.updateTitle(t("Analytics__Opt_ins"));

    chart.initFilter();

    chart.init();

    return chart;
};