import AnalyticsChart from './analyticsChart'
import geoChart from './geoChart'

export default function (settings, inherit) {

    var chart = new AnalyticsChart(settings, true);

    chart.chartType = geoChart;

    chart.method = 'locations_map';

    chart.updateTitle(t("Analytics__Locations"));

    if (!inherit) {
        chart.init();
    }

    return chart;
}
