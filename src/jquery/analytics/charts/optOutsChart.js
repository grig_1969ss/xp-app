import AnalyticsChart from './analyticsChart'
import moment from 'vendor/moment/moment-with-langs.js'

export default function (settings) {

    var chart = new AnalyticsChart(settings, true);

    chart.method = 'opt_outs';
    chart.addTotals = true;

    chart.options = {

    };

    chart.chartSettings.series = [
        {
            name: t("Analytics__Opt_outs"),
            color: '#A2050D',
            lineColor: '#A2050D',
            marker: {fillColor: '#ffffff', lineColor: '#A2050D', lineWidth: 2, radius: 3, symbol: 'circle'},
            totals: true,
            key: 'amount',
            type: 'column'
        }
    ];

    chart.tableSettings.columns = [
        {
            type: 'number',
            value: 'index',
            sort: true,
            sort_value: 'index_key',
            name: t('Common__Date'),
            width: '50%'
        },{
            type: 'number',
            value: 'amount',
            sort: true,
            name: t('Analytics__Opt_outs'),
            width: '50%'
        }
    ];

    chart.initFilter = function() {
        chart.chartContainer.find('.options').append(
            '<button class="graphType graphTypeFirst graphTypeLast date-picker-button" style="width: 140px"><i class="icon icon-search"></i> '+t('Analytics__Pushes_statistics_filter')+'</button>'
        );

        chart.chartContainer.append(
            '<div class="date-picker-period" style="position:absolute; z-index:10; right: 7px; top: 53px; display: none;">'+
            '   <div style="text-align: center; font-weight: bold;">'+t('Analytics__Pushes_statistics_optin_date_filter')+'</div>'+
            '   <div style="overflow: auto; margin:3px; margin-top:5px; border: 0; ">'+
            '       <div class="date-from" style="float:left; margin-right:3px;"></div>'+
            '       <div class="date-to" style="float:left; margin-left:3px;"></div>'+
            '   </div>'+
            '   <div style="overflow:auto; margin-top:6px; padding:3px;">'+
            '       <div style="float: left; margin-left: 4px;"><input class="browser-opt-version" type="text" style="width: 219px; height: 31px; line-height: 31px" placeholder="'+t('Analytics__Browser_opt_version')+'"></div>'+
            '       <div style="float: right; margin-right: 4px;"><input class="browser-version" type="text" style="width: 219px; height: 31px; line-height: 31px" placeholder="'+t('Analytics__Browser_version')+'"></div>'+
            '   </div>'+
            '   <div style="overflow:auto; margin-top:6px; padding:3px;">'+
            '       <div style="float: left; margin: 0 5px;"><input class="browser-os-opt-version" type="text" style="width: 219px; height: 31px; line-height: 31px" placeholder="'+t('Analytics__Browser_os_opt_version')+'"></div>'+
            '       <div style="float: right; margin: 0 5px;"><input class="browser-os-version" type="text" style="width: 219px; height: 31px; line-height: 31px" placeholder="'+t('Analytics__Browser_os_version')+'"></div>'+
            '   </div>'+
            '   <div style="overflow:auto; margin-top:6px; padding:3px; text-align: center;">'+
            '       <div style="display: inline-block; border: 1px solid #c2c4c6; border-radius: 4px; padding: 6px 12px; margin-right: 4px; cursor: pointer;" class="date-reset cly-button-dark button"><i class="icon icon-cancel"></i> '+t('Common__Reset')+'</div>'+
            '       <div style="display: inline-block; border: 1px solid #c2c4c6; border-radius: 4px; padding: 6px 12px; margin-left: 4px; cursor: pointer;" class="date-submit cly-button-dark button"><i class="icon icon-check"></i> '+t('Common__Apply')+'</div>'+
            '   </div>'+
            '</div>'
        );

        var initDatePicker = function() {
            $(window).click(function() {
                chart.chartContainer.find('.date-picker-period').hide();
            });

            chart.chartContainer.find('.date-picker-period').click(function(e) {
                e.stopPropagation();
            });

            chart.chartContainer.find('.date-picker-button').click(function(e) {
                chart.chartContainer.find('.date-picker-period').toggle();
                e.stopPropagation();
            });

            $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );

            var dateTo = chart.chartContainer.find('.date-to').datepicker({
                numberOfMonths: 1,
                showOtherMonths: true,
                onSelect: function( selectedDate ) {
                    var instance = $( this ).data("datepicker"),
                        date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings),
                        fromLimit = moment(selectedDate).toDate();

                    dateFrom.datepicker("option", "maxDate", fromLimit);
                },
                beforeShowDay: function (date) {
                    var ds = chart.chartContainer.find('.date-from').datepicker("getDate"),
                        de = chart.chartContainer.find('.date-to').datepicker("getDate");
                    if (date >= ds && date <= de) {
                        return [true, 'ui-datepicker-inrange', ''];
                    }
                    return [true, '', ''];
                }
            });

            var dateFrom = chart.chartContainer.find('.date-from').datepicker({
                numberOfMonths: 1,
                showOtherMonths: true,
                onSelect: function( selectedDate ) {
                    var instance = $(this).data("datepicker"),
                        date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings );

                    dateTo.datepicker("option", "minDate", date);
                },
                beforeShowDay: function (date) {
                    var ds = chart.chartContainer.find('.date-from').datepicker("getDate"),
                        de = chart.chartContainer.find('.date-to').datepicker("getDate");
                    if (date >= ds && date <= de) {
                        return [true, 'ui-datepicker-inrange', ''];
                    }
                    return [true, '', ''];
                }
            });

            dateFrom.datepicker("setDate", null);
            dateTo.datepicker("setDate", null);
            dateFrom.datepicker("refresh");
            dateTo.datepicker("refresh");

            chart.chartContainer.find('.date-submit').click(function(event) {
                var date_start = dateFrom.datepicker("getDate") ? moment(dateFrom.datepicker("getDate")).format("YYYY-MM-DD") : "";
                var date_end = dateTo.datepicker("getDate") ? moment(dateTo.datepicker("getDate")).format("YYYY-MM-DD") : "";
                var browser_version = chart.chartContainer.find('.browser-version').val();
                var browser_opt_version = chart.chartContainer.find('.browser-opt-version').val();
                var browser_os_version = chart.chartContainer.find('.browser-os-version').val();
                var browser_os_opt_version = chart.chartContainer.find('.browser-os-opt-version').val();

                chart.chartContainer.find('.date-picker-button').addClass('active');
                chart.chartContainer.find('.date-picker-period').hide();

                chart.options.startOptInDate = date_start;
                chart.options.endOptInDate = date_end;
                chart.options.browserVersion = browser_version;
                chart.options.browserOptVersion = browser_opt_version;
                chart.options.browserOsVersion = browser_os_version;
                chart.options.browserOsOptVersion = browser_os_opt_version;
                chart.method = 'opt_outs_filter';

                window.analyticPage.redraw();
            });

            chart.chartContainer.find('.date-reset').click(function(event) {
                chart.chartContainer.find('.date-picker-button').removeClass('active');
                chart.chartContainer.find('.date-picker-period').hide();
                chart.chartContainer.find('.browser-version').val('');
                chart.chartContainer.find('.browser-opt-version').val('');
                chart.chartContainer.find('.browser-os-version').val('');
                chart.chartContainer.find('.browser-os-opt-version').val('');

                dateFrom.datepicker("setDate", null);
                dateTo.datepicker("setDate", null);
                dateFrom.datepicker("refresh");
                dateTo.datepicker("refresh");

                delete chart.options.startOptInDate;
                delete chart.options.endOptInDate;
                delete chart.options.browserVersion;
                delete chart.options.browserOptVersion;
                delete chart.options.browserOsVersion;
                delete chart.options.browserOsOptVersion;
                chart.method = 'opt_outs';

                window.analyticPage.redraw();
            });
        };
        initDatePicker();

        chart.chartContainer.find('.graphMode').click(function() {
            chart.chartContainer.find('.graphMode').removeClass('active');
            $(this).addClass('active');
            chart.setMode($(this).data('mode'));
            chart.reload();
        });
    };

    chart.updateTitle(t("Analytics__Opt_outs"));

    chart.initFilter();

    chart.init();

    return chart;
};