import DataTable from '@/jquery/widgets/dataTable'

export default function (settings) {

    var table = new DataTable(settings, true);

    table.data_source = '/location/location/datatable';

    table.row_template_path = VIEWS_DIR+'/location/views/location/templates/table-item.twig?v='+APP_VERSION;
    table.row_template_params = {manage: settings.manage};

    table.row_search = true;
    table.row_search_type_backend = true;
    table.row_search_save = true;

    table.pagination = true;
    table.pagination_show_more = true;
    table.pagination_per_page = 25;

    table.columns = [
        {
            name: t('Analytics__Location')
        },
        {
            width: settings.manage ? '90px' : '40px'
        }
    ];

    table.init();

    return table;
};