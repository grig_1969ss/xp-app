import AnalyticsChart from './analyticsChart'

export default function (settings) {

    var chart = new AnalyticsChart(settings, true);

    chart.method = 'users';
    chart.addTotals = true;

    chart.chartSettings.series = [
        {
            color: '#1d87c8',
            lineColor: '#1d87c8',
            marker: {fillColor: '#ffffff', lineColor: '#1d87c8', lineWidth: 2, radius: 3, symbol: 'circle'},
            name: t("Analytics__New_users"),
            data: [],
            type: 'arearange',
            visible: true,
            key: 'new',
            totals: true,
            stack: 'new_returning'
        },{
            color: '#6BB3DE',
            lineColor: '#6BB3DE',
            marker: {fillColor: '#ffffff', lineColor: '#6BB3DE', lineWidth: 2, radius: 3, symbol: 'circle'},
            name: t("Analytics__Returning_users"),
            data: [],
            type: 'arearange',
            visible: true,
            key: 'returning',
            totals: true,
            stack: 'new_returning'
        },{
            color: '#055483',
            lineColor: '#055483',
            marker: {fillColor: '#055483', lineColor: '#055483', lineWidth: 2, radius: 2, symbol: 'circle'},
            name: t("Analytics__Active_users"),
            data: [],
            visible: true,
            key: 'active',
            totals: true,
            stack: 'active'
        }
    ];

    chart.chartSettings.totalsOrder = [2,0,1];

    chart.chartSettings.plotOptions = {
        column: {
            stacking: 'normal'
        }
    };

    chart.tableSettings.columns = [
        {
            type: 'number',
            value: 'index',
            sort: true,
            sort_value: 'index_key',
            name: t('Common__Date'),
            width: '25%'
        },{
            type: 'number',
            value: 'active',
            sort: true,
            name: t('Analytics__Active_users'),
            width: '25%'
        },{
            type: 'number',
            value: 'new',
            sort: true,
            name: t('Analytics__New_users'),
            width: '25%'
        },{
            type: 'number',
            value: 'returning',
            sort: true,
            name: t('Analytics__Returning_users'),
            width: '25%'
        }
    ];

    chart.on_load_data = function() {
        if (Object.keys(this.data.data).length>1) {
            this.chartSettings.series[0].type='arearange';
            this.chartSettings.series[1].type='arearange';

            this.process_point = function(point) {
                if (point.v === null) {
                    return [point, null, null];
                }
                if (point.k=='new') {
                    var l = parseInt(point.v.active) - parseInt(point.v.new);
                    if (l<0) l = 0;
                    point = [point, parseInt(point.v.active), l];
                }
                if (point.k=='returning') {
                    var h = parseInt(point.v.active) - parseInt(point.v.new);
                    if (h<0) h=0;
                    point = [point, h, 0];
                }
                return point;
            };

        } else {
            this.chartSettings.series[0].type='column';
            this.chartSettings.series[1].type='column';

            this.process_point = function(point) {
                return point;
            };
        }
    };


    chart.updateTitle(t("Analytics__Users"));

    chart.init();

    return chart;
}