import AnalyticsChart from './analyticsChart'
import { addCommas } from '@/jquery/utils'

export default function (settings, inherit) {

    var chart = new AnalyticsChart(settings, true);

    chart.addTotals = 1;

    chart.chartSettings.plotOptions = {
        column: {
            stacking: 'normal',
            lineColor: '#c24c3'
        }
    };

    chart.chartSettings.xAxis.labels.rotation = 0;
    chart.chartSettings.xAxis.labels.y = 18;

    chart.chartSettings.series = [
        {
            color: '#1d87c8',
            name: t("Analytics__Number_of_users"),
            data: [],
            visible: true,
            key: 'total',
            type: 'column'
        },
        {
            color: '#1d87c8',
            name: t("Analytics__Addressable_users"),
            data: [],
            visible: false,
            key: 'active',
            type: 'column'
        }
    ];

    chart.tableSettings.columns = [
        {
            type: 'number',
            value: 'index',
            sort: true,
            sort_value: 'index_key',
            width: '33%'
        },{
            type: 'number',
            value: function(item) {
                return addCommas(item.total);
            },
            sort: true,
            sort_value: 'total',
            name: t('Analytics__Number_of_users'),
            width: '33%'
        },{
            type: 'number',
            value: function(item) {
                if (!chart.data.totals.total) {
                    return '-';
                } else {
                    var percent = (item.total / chart.data.totals.total) * 100;
                    return percent.toFixed(1)+'%';
                }
            },
            sort: true,
            sort_value: function(item) {
                if (!chart.data.totals.total) {
                    return 0;
                } else {
                    return item.total / chart.data.totals.total;
                }
            },
            name: t('Analytics__Percent'),
            width: '34%'
        }
    ];

    /*chart.process_point = function(point) {
        if (point.k=='total') {
            point.y_displayed = point.v.total;
            point.y = point.v.total - point.v.active;
        }
        return point;
    };*/

    if (!inherit) {
        chart.init();
    }

    return chart;
}