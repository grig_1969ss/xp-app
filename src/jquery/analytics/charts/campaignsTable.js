import AnalyticsTable from './analyticsTable'
import SelectorBox from '@/jquery/analytics/selectorBox'
import { escapeHTML } from '@/jquery/utils'

export default function (settings, inherit) {

    var table = new AnalyticsTable(settings, true);

    table.method = 'campaigns_list';

    table.columns = [
        {
            checkbox: true
        },{
            type: 'string',
            value:function(item) {
                var str = '<a href="/analytics/analytics/campaign?project_id='+table.project_id+'&id='+item.id+SelectorBox().getHashedSelector()+'">'+escapeHTML(item.title)+'</a>';
                str += '<div class="description">' + (parseInt(item.active) ? '<span style="color: #1d87c8">'+t('all', 'Analytics__Campaigns_live')+'</span>, ' : '') + item.description + '</div>';
                return str;
            },
            min_width: '300px',
            sort_value: 'date',
            sort: true,
            sort_default: 'desc',
            search: true,
            name: t('Analytics__Campaign')
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'string',
            value: 'first_send_date',
            width: '120px',
            sort: true,
            sort_default: 'desc',
            name: t('all', 'Analytics__Campaign_table_first_send_date'),
            class: 'smallHeader centerHeader',
            group: true,
        },{
            type: 'string',
            value: 'last_send_date',
            width: '120px',
            sort: true,
            sort_default: 'desc',
            name: t('all', 'Analytics__Campaign_table_last_send_date'),
            class: 'smallHeader centerHeader',
            group: true,
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'number',
            value: 'mobile_push_sent',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_mobile_push_sent'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'mobile_push_opened',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_mobile_push_opened'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'percent',
            value: 'mobile_push_open_rate',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_mobile_push_open_rate'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'number',
            value: 'web_push_sent',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_web_push_sent'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'web_push_delivered',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_web_push_delivered'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'web_push_opened',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_web_push_opened'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'percent',
            value: 'web_push_open_rate',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_web_push_open_rate'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'number',
            value: 'sms_sent',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_sms_sent'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'sms_delivered',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_sms_delivered'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'number',
            value: 'inbox_sent',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_inbox_sent'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'inbox_delivered',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_inbox_delivered'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'inbox_opened',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_inbox_opened'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'percent',
            value: 'inbox_open_rate',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_inbox_open_rate'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'number',
            value: 'inweb_sent',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_inweb_sent'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'inweb_opened',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_inweb_clicked'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'percent',
            value: 'inweb_open_rate',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_inweb_click_rate'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'number',
            value: 'inapp_sent',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_inapp_sent'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'inapp_delivered',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_inapp_delivered'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'inapp_opened',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_inapp_opened'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'percent',
            value: 'inapp_open_rate',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_inapp_open_rate'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'number',
            value: 'email_sent',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_email_sent'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'email_delivered',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_email_delivered'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'email_opened',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_email_opened'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'percent',
            value: 'email_open_rate',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_email_open_rate'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'email_clicked',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_email_clicked'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'percent',
            value: 'email_click_rate',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_email_click_rate'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'email_failed',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_email_failed'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'email_unsubscribed',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_email_unsubscribed'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'email_complained',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_email_complained'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'number',
            value: 'facebook_sent',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_facebook_sent'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'facebook_delivered',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_facebook_delivered'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'facebook_opened',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_facebook_opened'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'percent',
            value: 'facebook_open_rate',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_facebook_open_rate'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'number',
            value: 'whatsapp_sent',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_whatsapp_sent'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'whatsapp_delivered',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_whatsapp_delivered'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'whatsapp_opened',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_whatsapp_opened'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'percent',
            value: 'whatsapp_open_rate',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_whatsapp_open_rate'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'number',
            value: 'actions_converted_unique',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_actions_converted_unique'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'actions_sent_unique',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_actions_sent_unique'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'percent',
            value: 'actions_conversion',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_actions_conversion'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'number',
            value: 'goals',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_goals'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'number',
            value: 'goals_value',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_goals_value'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'number',
            value: 'app_opens_direct',
            width: '70px',
            name: t('all', 'Analytics__Campaigns_table_app_opens_direct'),
            class: 'smallHeader centerHeader',
            group: true,
            sort: true,
            sort_default: 'desc'
        },{
            type: 'separator',
            width: '20px',
        },{
            type: 'string',
            value: function(item) {
                if (item.ab == 1) {
                    return '<div style="text-align: center"><a href="/analytics/analytics/campaign?project_id='+table.project_id+'&id='+item.id+SelectorBox().getHashedSelector()+'"><i class="iconp-chart-line"></i></a></div>';
                } else {
                    return '';
                }
            },
            name: t('all', 'Analytics__Campaigns_table_ab'),
            width: '40px'
        }
    ];

    let i = 0;

    table.columns_groups = [
        {
            columns: [i++]
        },{
            columns: [i++]
        },{
            columns: [i++]
        },{
            name: t('all', 'Analytics__Campaign_table_send_date'),
            columns: [i++,i++],
            width: '240px'
        },{
            columns: [i++]
        },{
            name: t('all', 'Analytics__Campaigns_table_mobile_push'),
            columns: [i++,i++,i++],
            width: '210px'
        },{
            columns: [i++]
        },{
            name: t('all', 'Analytics__Campaigns_table_web_push'),
            columns: [i++,i++,i++,i++],
            width: '280px'
        },{
            columns: [i++]
        },{
            name: t('all', 'Analytics__Campaigns_table_sms'),
            columns: [i++,i++],
            width: '140px'
        },{
            columns: [i++]
        },{
            name: t('all', 'Analytics__Campaigns_table_inbox'),
            columns: [i++,i++,i++,i++],
            width: '280px'
        },{
            columns: [i++]
        },{
            name: t('all', 'Analytics__Campaigns_table_inweb'),
            columns: [i++,i++,i++],
            width: '210px'
        },{
            columns: [i++]
        },{
            name: t('all', 'Analytics__Campaigns_table_inapp'),
            columns: [i++,i++,i++,i++],
            width: '280px'
        },{
            columns: [i++]
        },{
            name: t('all', 'Analytics__Campaigns_table_email'),
            columns: [i++,i++,i++,i++,i++,i++,i++,i++,i++],
            width: '700px'
        },{
            columns: [i++]
        },{
            name: t('all', 'Analytics__Campaigns_table_facebook'),
            columns: [i++,i++,i++,i++],
            width: '280px'
        },{
            columns: [i++]
        },{
            name: t('all', 'Analytics__Campaigns_table_whatsapp'),
            columns: [i++,i++,i++,i++],
            width: '280px'
        },{
            columns: [i++]
        },{
            name: t('all', 'Analytics__Campaigns_table_conversion'),
            columns: [i++,i++,i++],
            width: '210px'
        },{
            columns: [i++]
        },{
            name: t('all', 'Analytics__Campaigns_table_goal'),
            columns: [i++,i++],
            width: '140px'
        },{
            columns: [i++]
        },{
            name: t('all', 'Analytics__Campaigns_table_app_opens'),
            columns: [i++],
            width: '70px'
        },{
            columns: [i++]
        },{
            columns: [i++]
        }
    ];

    let groupMobilePush = 5;
    let groupWebPush = 7;
    let groupSms = 9;
    let groupInbox = 11;
    let groupInweb = 13;
    let groupInapp = 15;
    let groupEmail = 17;
    let groupFacebook = 19;
    let groupWhatsapp = 21;
    let groupActions = 23;
    let groupGoals = 25;
    let groupAppOpens = 27;

    function deleteGroup(number) {
        for (let i in table.columns_groups[number].columns) delete table.columns[table.columns_groups[number].columns[i]];
        for (let i in table.columns_groups[number-1].columns) delete table.columns[table.columns_groups[number-1].columns[i]];
        delete table.columns_groups[number];
        delete table.columns_groups[number-1];
    }

    if (settings.show_web_push === false) {
        deleteGroup(groupWebPush);
    }

    if (settings.show_mobile_push === false) {
        deleteGroup(groupMobilePush);
    }

    if (settings.show_sms === false) {
        deleteGroup(groupSms);
    }

    if (settings.show_inbox === false) {
        deleteGroup(groupInbox);
    }

    if (settings.show_inweb === false) {
        deleteGroup(groupInweb);
    }

    if (settings.show_inapp === false) {
        deleteGroup(groupInapp);
    }

    if (settings.show_email === false) {
        deleteGroup(groupEmail);
    }

    if (settings.show_facebook === false) {
        deleteGroup(groupFacebook);
    }

    if (settings.show_whatsapp === false) {
        deleteGroup(groupWhatsapp);
    }

    if (settings.show_conversions === false) {
        deleteGroup(groupActions);
        deleteGroup(groupGoals);
    }

    if (settings.show_app_opens === false) {
        deleteGroup(groupAppOpens);
    }

    table.auto_layout = true;


    table.on_select_items = settings.on_select_items;
    table.row_selection_items = {};
    table.row_selection = true;
    table.row_selection_on_select_rows = function(ids) {
        for (var i in ids) {
            if (table.row_selection_items[ids[i]] === undefined) {
                var item = table.getRowById(ids[i]);
                if (item) {
                    table.row_selection_initiated = true;
                    table.row_selection_items[ids[i]] = {
                        id: item.id,
                        title: item.title
                    };
                }
            }
        }
        if (typeof table.on_select_items == 'function') {
            table.on_select_items(table.row_selection_items);
        }
    };
    table.row_selection_on_deselect_rows = function(ids) {
        if (ids === null) {
            table.row_selection_items = {};
        } else {
            for (var i in ids) {
                delete table.row_selection_items[ids[i]];
            }
        }
        if (typeof table.on_select_items == 'function') {
            table.on_select_items(table.row_selection_items);
        }
    };

    table.row_sort = true;
    table.row_sort_default_column = 1;
    table.row_sort_default_order = 'desc';
    table.row_sort_type_backend = true;

    table.row_search = true;
    table.row_search_save = true;
    table.row_search_type_backend = true;

    table.pagination = true;
    table.pagination_show_more = true;

    if (!inherit) {
        table.init();
    }

    return table;
}
