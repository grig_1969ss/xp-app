import AnalyticsChart from './analyticsChart'
import LoadGoogleMaps from 'vendor/gmaps/loader'

export default function (settings, inherit) {

    var chart = new AnalyticsChart(settings, true);
    var google;

    chart.settings = settings;

    chart.markers = {};
    chart.ids = [];

    chart.infoWindow = null;

    chart.categoriesAmount = 10;

    chart.categoriesColors = [
        '#d20000',
        '#ff0000',
        '#ff4949',

        '#3b9944',
        '#41a64b',
        '#47b351',

        '#069ed8',
        '#07b0f1',
        '#31c2f9',

        '#999999'
    ];

    chart._init = chart.init;
    chart.init = function () {
        this.initiated = true;
        LoadGoogleMaps(function (googleApi) {
            google = googleApi

            chart.mapSettings = {
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: false,
                fullscreenControl: false,
                MapType: false,
                styles: settings.styles
            };

            chart._init();
        })
    };

    chart.buildGraph = function() {
        LoadGoogleMaps(function () {
            chart.createMap();

            chart.processData();

            chart.removeMarkers();

            chart.calculateCategories();

            chart.createLegend();

            chart.createInfoBox();

            chart.createMarkers();

            chart.centerMap();
        })
    };


    // MAP

    chart.createMap = function () {
        $(chart.chartSettings.chart.renderTo).height(485);
        $(chart.chartSettings.chart.renderTo).parent().find('.placeholder').remove();
        if (!chart.gmap) {
            chart.gmap = new google.maps.Map(chart.chartSettings.chart.renderTo, chart.mapSettings);
        }
    };

    chart.destroyMap = function () {
        $(chart.chartSettings.chart.renderTo).empty();
        if (chart.gmap) {
            delete chart.gmap;
        }
    };


    // LOGIC

    chart.calculateCategories = function() {
        chart.categories = {};

        var data = chart.data.data;
        var dataLength = data.length;
        if (!dataLength) { return false; }
        var min, max = min = parseInt(data[0].amount);

        $(data).each(function (i, point) {
            if (parseInt(point.amount) > max) { max = parseInt(point.amount); }
            if (parseInt(point.amount) < min) { min = parseInt(point.amount); }
            if (point.amount == 0) {
                if (chart.categories[chart.categoriesAmount - 1] == undefined) {
                    chart.categories[chart.categoriesAmount - 1] = [{
                        ids: [],
                        label: t('Analytics__None')
                    }]; // category for zero-values
                }
                chart.categories[chart.categoriesAmount - 1].push(point);
                chart.categories[chart.categoriesAmount - 1][0].ids.push(point.id);
                point.category = chart.categoriesAmount - 1;
            }
        });

        data.sort(function(a, b) {
            return parseInt(a.amount) - parseInt(b.amount);
        });

        var nonZeroDataLength = dataLength, nonZeroPointer = 0.00001;
        // except zero-category
        for (var pointer = 0; pointer < dataLength; nonZeroPointer++, pointer++) {
            var v = data[pointer];
            if (v.amount == 0) {
                nonZeroPointer--;
                nonZeroDataLength--;
                continue;
            }
            var category = Math.floor((1 - nonZeroPointer / (nonZeroDataLength - 1 + 0.00001)) * (chart.categoriesAmount - 1));
            if (chart.categories[category] == undefined) {
                chart.categories[category] = [{max: +v.amount, min: +v.amount, ids: []}];
            }
            chart.categories[category].push(v);
            chart.categories[category][0].ids.push(v.id);
            if (+v.amount > chart.categories[category][0].max) {
                chart.categories[category][0].max = +v.amount;
            }
            if (+v.amount < chart.categories[category][0].min) {
                chart.categories[category][0].min = +v.amount;
            }
            chart.categories[category][0].label =
                chart.categories[category][0].max > chart.categories[category][0].min
                    ? (chart.categories[category][0].min + ' - ' + chart.categories[category][0].max)
                    : chart.categories[category][0].min;
            v.category = category;
        }
    };

    chart.centerMap = function() {
        var visibleMarkers = false;
        var bounds = new google.maps.LatLngBounds();
        $.each(chart.markers, function() {
            if (this.visible) {
                visibleMarkers = true;
                bounds.union(this.bounds);
            }
        });

        if (visibleMarkers) {
            chart.gmap.fitBounds(bounds);
        } else {
            var defCenter = new google.maps.LatLng(0, 0);
            chart.gmap.setZoom(2);
            chart.gmap.setCenter(defCenter);
        }
    };


    // CONTROLS

    chart.createLegend = function() {
        var $list = $('<ul />').addClass('mapLegend');
        for (var i = 0; i < chart.categoriesAmount; i++) {
            if (chart.categories[i] == undefined) { continue; } // miss legend labels for empty categorys

            var $item = $('<li />').addClass('mapLegendItem').data('category', i).html(
                    '<span class="categoryColor category' + i + '"'
                    + ' style="background: ' + chart.categoriesColors[i] + '"'
                    + ' data-category="' + i + '">&nbsp;</span> ' + chart.categories[i][0].label);
            $list.append($item);
        }

        chart.legendBlock = $('<div />', {
            class: 'mapLegendContainer',
            html: $list
        });

        chart.gmap.controls[google.maps.ControlPosition.TOP_RIGHT].clear();
        chart.gmap.controls[google.maps.ControlPosition.TOP_RIGHT].push(chart.legendBlock[0]);

        chart.legendBlock.find('.mapLegendItem').click(function() {
            var isDisabled, category = $(this).data('category');
            if ($(this).hasClass('disabled')) {
                $(this).removeClass('disabled');
                isDisabled = false;
            } else {
                $(this).addClass('disabled');
                isDisabled = true;
            }

            $.each(chart.categories[category][0].ids, function(i, id) {
                if (isDisabled) {
                    chart.hideMarker(id, false);
                } else {
                    chart.showMarker(id);
                }
            });
        });
    };

    chart.createInfoBox = function() {
        if (!chart.infoWindow) {
            chart.infoWindow = new google.maps.InfoWindow();
        }
    };


    // MARKERS

    chart.createMarkers = function() {
        $.each(chart.data.data, function(index, point) {
            chart.createMarker(point);
        });
    };

    chart.createMarkerViaCircle = function(point) {
        var id = point.id,
            latitude = point.latitude,
            longitude = point.longitude,
            radius = parseInt(point.radius),
            uuid = point.ibeacon_uuid,
            title = point.title,
            amount = point.amount;

        var position = new google.maps.LatLng(latitude, longitude);

        var color = chart.categoriesColors[point.category];

        var opacity, weight;
        if (radius >= 5000) {
            opacity = 0.1;
            weight = 40;
        } else if (radius >= 2500) {
            opacity = 0.2;
            weight = 30;
        } else if (radius >= 1000) {
            opacity = 0.3;
            weight = 20;
        } else if (radius >= 500) {
            opacity = 0.4;
            weight = 15;
        } else {
            opacity = 0.5;
            weight = 10;
        }

        var circle = new google.maps.Circle({
            id: id,
            center: position,
            radius: radius,
            fillOpacity: 0,
            fillColor: color,
            strokeColor: color,
            strokeOpacity: opacity,
            strokeWeight: weight,
            map: chart.gmap
        });

        google.maps.event.addListener(circle, 'mousedown', function(e) {
            console.log(this);
            console.log(e);
        });

        chart.markers[id] = {
            circle: circle,
            visible: true
        };

        return chart.markers[id];
    };

    chart.createMarker = function(point) {
        var id = point.id,
            latitude = point.latitude,
            longitude = point.longitude,
            radius = parseInt(point.radius);

        var position = new google.maps.LatLng(latitude, longitude);

        var color = chart.categoriesColors[point.category];

        var location = new google.maps.Circle({
            center: position,
            radius: radius
        });

        var index = chart.categoriesAmount - point.category;

        var circle;
        if (chart.options.location_type == 2) {
            circle = new google.maps.Circle({
                id: id,
                center: position,
                radius: radius,
                fillOpacity: 0.5,
                fillColor: color,
                strokeOpacity: 0.2,
                strokeColor: color,
                strokeWeight: 10,
                zIndex: index,
                map: chart.gmap
            });

        } else {
            var opacity, weight;
            if (radius >= 5000) {
                opacity = 0.1;
                weight = 40;
            } else if (radius >= 2500) {
                opacity = 0.2;
                weight = 30;
            } else if (radius >= 1000) {
                opacity = 0.3;
                weight = 20;
            } else if (radius >= 500) {
                opacity = 0.4;
                weight = 15;
            } else {
                opacity = 0.5;
                weight = 10;
            }

            circle = new google.maps.Polyline({
                id: id,
                path: chart.getCirclePath(position, radius, 1),
                strokeColor: color,
                strokeOpacity: opacity,
                strokeWeight: weight,
                zIndex: index,
                map: chart.gmap
            });
        }


        var infoWindowTimeout;

        google.maps.event.addListener(circle, 'click', (function(circle, point) {
            return function(e) {
                infoWindowTimeout = setTimeout(function() {
                    var infoWindowContent = '<p style="text-align: left; font-size: 16px">'
                        + t('Analytics__Location_info_title') + ': <b>' + point.title + '</b><br />';
                    if (chart.options.location_type == 2) {
                        infoWindowContent
                            +=t('Analytics__Ibeacon_info_uuid') + ': <b>' + point.ibeacon_uuid + '</b><br />'
                            + t('Analytics__Ibeacon_info_major') + ': <b>' + point.ibeacon_major + '</b><br />'
                            + t('Analytics__Ibeacon_info_minor') + ': <b>' + point.ibeacon_minor + '</b><br />'
                    }
                    infoWindowContent += t('Analytics__Location_info_amount') + ': <b>' + point.amount + '</b></p>';

                    chart.infoWindow.setContent(infoWindowContent);
                    chart.infoWindow.setPosition(e.latLng);
                    chart.infoWindow.open(chart.gmap, circle);
                }, 200);
            }
        })(circle, point));

        google.maps.event.addListener(circle, 'dblclick', function() {
            clearTimeout(infoWindowTimeout);
        });

        chart.markers[id] = {
            visible: true,
            circle: circle,
            bounds: location.getBounds()
        };

        return chart.markers[id];
    };

    chart.getCirclePath = function (point, radius, dir) {
        var d2r = Math.PI / 180;   // degrees to radians
        var r2d = 180 / Math.PI;   // radians to degrees
        var earthsradius = 6371000; // the radius of the earth in meters

        var points = 32 + Math.floor(radius / 800);

        // find the radius in lat/lon
        var rlat = (radius / earthsradius) * r2d;
        var rlng = rlat / Math.cos(point.lat() * d2r);

        var extp = [];
        var start = 0;
        var end=points + 1;

        for (var i=start; (dir==1 ? i < end : i > end); i=i+dir) {
            var theta = Math.PI * (i / (points/2));
            var ey = point.lng() + (rlng * Math.cos(theta)); // center a + radius x * cos(theta)
            var ex = point.lat() + (rlat * Math.sin(theta)); // center b + radius y * sin(theta)
            extp.push(new google.maps.LatLng(ex, ey));
        }

        return extp;
    };

    chart.showMarker = function(id) {
        if (chart.markers[id]) {
            chart.markers[id].visible = true;
            if (chart.markers[id].circle) {
                chart.markers[id].circle.setMap(chart.gmap);
            }
        }
    };

    chart.hideMarker = function(id) {
        if (chart.markers[id]) {
            chart.markers[id].visible = false;
            if (chart.markers[id].circle) {
                chart.markers[id].circle.setMap(null);
            }
        }
    };

    chart.removeMarkers = function () {
        $.each(chart.markers, function(id) {
            if (chart.markers[id].circle) {
                chart.markers[id].circle.setMap(null);
            }
        });
        chart.markers = {};
    };



    // INTERFACE

    chart.fitWithPositionAndRadius = function(latitude, longitude, radius, jump) {
        if (jump === undefined || jump) {
            $('html, body').scrollTop($(chart.chartSettings.chart.renderTo).offset().top - 145);
        }

        var bounds = new google.maps.LatLngBounds();
        var position = new google.maps.LatLng(latitude, longitude);
        var circle = new google.maps.Circle({center: position, radius: radius * 1.3});
        bounds.extend(position);
        bounds.union(circle.getBounds());
        chart.gmap.fitBounds(bounds);
    };

    chart.destroy = function() {
        chart.destroyMap();
    };

    chart.showItems = function(items) {
        chart.ids = [];
        for (var i in items) {
            if (items[i].title) {
                chart.ids.push(items[i].id);
            }
        }
    };

    if (!inherit) {
        chart.init();
    }

    return chart;
};
