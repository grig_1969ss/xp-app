import LoadGoogleMaps from 'vendor/gmaps/loader'

export default function (settings, inherit) {

    var that = this;
    var google;

    this.settings = settings;

    if (!settings.settings) settings.settings = {};
    this.onDrag = settings.settings.onDrag;
    this.onDblClick = settings.settings.onDblClick;
    this.onRightClick = settings.settings.onRightClick;

    this.markers = {};
    this.infoWindow = null;

    this.init = function() {
        LoadGoogleMaps(function (googleApi) {
            google = googleApi

            that.mapSettings = {
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            that.gmap = new google.maps.Map(settings.chart.renderTo, that.mapSettings);
            that.infowindow = new google.maps.InfoWindow();

            $.each(that.settings.series[0].data, function(index, point) {
                var marker = that.addMarker(point.v.id, point.v.latitude, point.v.longitude, point.v.radius, point.v.title);
            });

            that.centerMap();

            google.maps.event.addListener(that.gmap, "dblclick", function(e){
                if (typeof that.onDblClick == 'function')
                {
                    that.onDblClick(e.latLng);
                }
            });

            google.maps.event.addListener(that.gmap, "rightclick", function(e){
                if (typeof that.onRightClick == 'function')
                {
                    that.onRightClick(e.latLng);
                }
            });
        })
    };

    this.centerMap = function() {
        var visible_markers = false;
        var bounds = new google.maps.LatLngBounds();

        $.each(this.markers, function() {
            if (this.visible)
            {
                visible_markers = true;
                bounds.extend(this.marker.position);
                bounds.union(this.circle.getBounds());
            }
        });

        if (visible_markers) {
            this.gmap.fitBounds(bounds);
        } else {
            var defCenter = new google.maps.LatLng(0, 0);
            this.gmap.setZoom(1);
            this.gmap.setCenter(defCenter);
        }
    };

    this.addMarker = function(id, latitude, longitude, radius, title, color, draggable) {
        if (!draggable) draggable = false;

        var position = new google.maps.LatLng(latitude, longitude);

        var marker = new google.maps.Marker({
            position: position,
            map: this.gmap,
            id: id,
            title: title,
            draggable: draggable
        });

        if (!color) color = '#FF0000';

        var circle = new google.maps.Circle({
            center: position,
            radius: parseInt(radius),
            fillOpacity: 0.35,
            fillColor: color,
            strokeColor: color,
            strokeOpacity: 0.5,
            strokeWeight: 1,
            map: this.gmap
        });

        if (marker.title)
        {
            google.maps.event.addListener(marker, 'click', (function(marker) {
                return function() {
                    that.infowindow.setContent($('<div>').text(marker.title).html());
                    that.infowindow.open(that.gmap, marker);
                }
            })(marker));
        }

        google.maps.event.addListener(marker, 'dblclick', (function(marker, circle) {
            return function() {
                that.fitWithPositionAndRadius(marker.getPosition().lat(), marker.getPosition().lng(), circle.radius);
            }
        })(marker, circle));

        google.maps.event.addListener(marker, 'dragend', $.proxy(function(pos) {
            if (that.markers[this.id] && that.markers[this.id].circle) {
                that.markers[this.id].circle.setCenter(this.getPosition());
            }

            if (typeof that.onDrag == 'function') {
                that.onDrag(this.id, pos.latLng);
            }
        }), marker);

        this.markers[id] = {marker: marker, circle: circle, visible: true};

        return this.markers[id];
    };


    this.removeMarker = function(id) {
        if (this.markers[id])
        {
            if (this.markers[id].marker) {
                this.markers[id].marker.setMap(null);
            }
            if (this.markers[id].circle) {
                this.markers[id].circle.setMap(null);
            }
            delete this.markers[id];
        }
    };

    this.setMapOnClick = function(callback) {
        return google.maps.event.addListener(this.gmap, "click", callback);
    };

    this.destroy = function() {
        $(this.settings.chart.renderTo).empty();
        delete this.gmap;
    };

    this.fitWithPositionAndRadius = function(latitude, longitude, radius, jump) {
        if (jump === undefined || jump) {
            $('html, body').scrollTop($(this.settings.chart.renderTo).offset().top - 145);
        }

        var bounds = new google.maps.LatLngBounds();
        var position = new google.maps.LatLng(latitude, longitude);
        var circle = new google.maps.Circle({center: position, radius: radius * 1.3});
        bounds.extend(position);
        bounds.union(circle.getBounds());
        this.gmap.fitBounds(bounds);
    };

    this.showMarkers = function(ids) {
        var update = false;
        for (var i in this.markers)
        {
            var visible = ($.inArray(parseInt(i), ids) >= 0);
            if (this.markers[i].visible != visible) {
                update = true;
                this.markers[i].visible = visible;
                if (this.markers[i].visible) {
                    this.markers[i].marker.setMap(this.gmap);
                    this.markers[i].circle.setMap(this.gmap);
                } else {
                    this.markers[i].marker.setMap(null);
                    this.markers[i].circle.setMap(null);
                }
            }
        }
        if (update) {
            this.centerMap();
        }
    };

    if (!inherit) {
        this.init();
    }

    return this;
}
