import AnalyticsTable from './analyticsTable'

export default function (settings) {

    var table = new AnalyticsTable(settings, true);

    table.method = null;
    table.modifier = null;

    table.columns = [
        {
            checkbox: true,
        },{
            type: 'string',
            value: function(item) {
                var groups = "";
                if (item.groups_str) {
                    var tags = item.groups_str.split(",");
                    for (var i in tags) {
                        if (tags[i]) {
                            groups += '<span class="group">#'+$('<div>').text(jQuery.trim(tags[i])).html()+'</span>';
                        }
                    }
                }
                return '<span>'+$('<div>').text(item.title).html()+'</span>'+groups;
            },
            search: true,
            sort: true,
            sort_value: 'title',
            name: t('Common__Title')
        },{
            type: 'number',
            value: 'amount',
            sort: true,
            name: t('Analytics__Locations_enters'),
            width: '200px'
        },{
            sort: false,
            value: function (item) {
                return '<i class="ico icon-location" style="cursor: pointer" onclick="analyticPage.clickOnTableMarker(' + item.latitude + ',' + item.longitude + ',' + item.radius + ');"></i>';
            },
            name: t('Common__Map'),
            width: '50px'
        }
    ];

    table.setModifier = function(modifier) {
        table.modifier = modifier;
        if (table.modifier == 'total_hits') {
            table.method = 'locations_list';
            table.columns[2].name = t('Analytics__Locations_enters');
        } else {
            table.method = 'locations_unique_list';
            table.columns[2].name = t('Analytics__Locations_unique_enters');
        }
        table.table.find('.tableHeaders th').eq(2).text(table.columns[2].name);
        table.updateSortControls();
    };

    if (settings.modifier) {
        table.setModifier(settings.modifier);
    }

    table.on_select_items = settings.on_select_items;
    table.row_selection_items = {};
    table.row_selection = true;
    table.row_selection_on_select_rows = function(ids) {
        for (var i in ids) {
            if (table.row_selection_items[ids[i]] === undefined) {
                var item = table.getRowById(ids[i]);
                if (item) {
                    table.row_selection_initiated = true;
                    table.row_selection_items[ids[i]] = {
                        id: item.id,
                        title: item.title
                    };
                }
            }
        }
        if (typeof table.on_select_items == 'function') {
            table.on_select_items(table.row_selection_items);
        }
    };
    table.row_selection_on_deselect_rows = function(ids) {
        if (ids === null) {
            table.row_selection_items = {};
        } else {
            for (var i in ids) {
                delete table.row_selection_items[ids[i]];
            }
        }
        if (typeof table.on_select_items == 'function') {
            table.on_select_items(table.row_selection_items);
        }
    };

    table.row_sort = true;
    table.row_sort_default_column = 1;
    table.row_sort_default_order = 'asc';
    table.row_sort_type_backend = true;

    table.row_search = true;
    table.row_search_save = true;
    table.row_search_type_backend = true;

    table.pagination = true;
    table.pagination_show_more = true;

    table.init();

    return table;
}