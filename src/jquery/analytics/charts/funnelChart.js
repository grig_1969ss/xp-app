import AnalyticsChart from './analyticsChart'
import { addCommas } from '@/jquery/utils'

export default function (settings) {

    var chart = new AnalyticsChart(settings, true);

    chart.method = 'funnel';
    chart.type_switcher = false;

    chart.chartSettings.chart.inverted = true;
    chart.chartSettings.chart.marginLeft = 200;
    chart.chartSettings.chart.marginRight = 100;

    chart.chartSettings.xAxis.labels = jQuery.extend({}, chart.chartSettings.yAxis.labels);
    chart.chartSettings.xAxis.labels.align = 'right';
    chart.chartSettings.xAxis.labels.formatter = function() {
        var str = chart.data.data[this.value].title;
        str = str.replace(' ', '&nbsp;');
        return '<div style="width: 180px; overflow: hidden; text-align: right; white-space: normal; word-wrap: break-word; line-height: 14px; max-height: 42px;">'+str+'</div>';
    };
    chart.chartSettings.xAxis.labels.useHTML = true;

    chart.chartSettings.yAxis.labels = {enabled: false};
    chart.chartSettings.yAxis.tickPixelInterval = 60;
    chart.chartSettings.yAxis.gridLineWidth = 0;
    chart.chartSettings.yAxis.minorGridLineWidth = 0;
    chart.chartSettings.yAxis.title = {enabled: true, text: t('AnalyticsFunnel__Users'), margin: 16, style: {color: "#1d87c8"}};

    chart.chartSettings.plotOptions = {
        column: {
            dataLabels: {
                enabled: true,
                crop: false,
                overflow: 'none',
                useHTML: true,
                color: '#32404d',
                formatter: function() {
                    var percent = chart.getCompletePercentFromTotal(this.x);
                    return "<b>" + addCommas(chart.data.data[this.x].users) + "</b><br>" + percent.toFixed(1) + "%";
                },
                inside: false
            },
            pointWidth: 30,
            states: {
                hover: {
                    brightness: 0.05
                }
            }
        },
        series: {
            stacking: 'normal'
        }
    };

    chart.chartSettings.tooltip.useHTML = true;
    chart.chartSettings.tooltip.formatter = function() {
        var str;
        if (this.series.index) {
            let percent = chart.getCompletePercentFromTotal(this.x);
            str = t('all', 'AnalyticsFunnel__{users}_completed_{hits}_times', {users: addCommas(this.y), hits: addCommas(chart.data.data[this.x].hits)});
            str += "<br>" + t('all', 'AnalyticsFunnel__Its_{percent}_of_total_users', {percent: percent.toFixed(1) + '%'});
        } else {
            let percent = chart.getDropoffPercentFromStep(this.x);
            str = t('all', 'AnalyticsFunnel__{users}_dropped_off_on_previous_step', {users: addCommas(this.y)});
            str += "<br>" + t('all', 'AnalyticsFunnel__Its_{percent}_of_users_completed_previous_step', {percent: percent.toFixed(1) + '%'});
        }
        return "<div class='highcharts-tooltip-custom'>"+str+"</div>";
    };
    chart.chartSettings.tooltip.style = {padding: '1px'};
    chart.chartSettings.tooltip.backgroundColor = 'rgb(255, 255, 255)';
    chart.chartSettings.tooltip.followPointer = true;

    chart.on_load_data = function() {
        this.chartContainer.find('.graph').css('height', (Object.keys(this.data.data).length * 55 + 100)+'px');

        if (!this.data.totals.entered) {
            chart.chartSettings.yAxis.max = 1;
        } else {
            chart.chartSettings.yAxis.max = undefined;
        }
    };

    chart.chartSettings.series = [
        {
            color: '#eaeaea',
            lineColor: '#eaeaea',
            data: [],
            visible: true,
            type: 'column',
            key: 'dropoff',
            value: function(index, value) {
                var dropoff = chart.getDropoff(index);
                return dropoff ? dropoff : 0;
            },
            dataLabels: false
        },{
            color: '#1d87c8',
            lineColor: '#1d87c8',
            data: [],
            visible: true,
            type: 'column',
            key: 'users'
        },{
            color: '#1d87c8',
            lineColor: '#1d87c8',
            name: t('AnalyticsFunnel__Users_entered'),
            totals: true,
            visible: false,
            chart: false,
            key: 'entered'
        },{
            color: '#1d87c8',
            lineColor: '#1d87c8',
            name: t('AnalyticsFunnel__Users_completed'),
            totals: true,
            visible: false,
            chart: false,
            key: 'completed'
        },{
            color: '#1d87c8',
            lineColor: '#1d87c8',
            name: t('AnalyticsFunnel__Completion_rate'),
            totals: true,
            visible: false,
            chart: false,
            key: 'rate',
            value: function() {
                var percent = chart.getCompletePercent();
                return percent.toFixed(1) + '%';
            }
        }
    ];

    chart.tableSettings.columns = [
        {
            type: 'number',
            value: function(item) {
                return item.index_key + 1;
            },
            name: '#',
            sort: true,
            group: true,
            width: '40px',
            class: 'alignCenter noSortIcon'
        },{
            type: 'string',
            value: 'title',
            name: t('all', 'AnalyticsFunnelTable__Step_title'),
            sort: true,
            group: true
        },{
            type: 'number',
            value: 'users',
            name: t('all', 'AnalyticsFunnelTable__Users_amount'),
            hint: t('all', 'AnalyticsFunnelTable__Users_amount_hint'),
            sort: true,
            width: '90px',
            class: 'smallHeader centerHeader',
            group: true
        },{
            type: 'number',
            value: function(item) {
                var percent = chart.getCompletePercentFromTotal(item.index_key);
                return percent.toFixed(1) + '%';
            },
            sort: true,
            sort_value: function(item) {
                return chart.getCompletePercentFromTotal(item.index_key);
            },
            name: t('all', 'AnalyticsFunnelTable__Users_percent_from_total'),
            hint: t('all', 'AnalyticsFunnelTable__Users_percent_from_total_hint'),
            width: '90px',
            class: 'smallHeader centerHeader',
            group: true
        },{
            type: 'number',
            value: function(item) {
                var percent = chart.getCompletePercentFromStep(item.index_key);
                if (percent === undefined) return t('all', 'Common__Na');
                return percent.toFixed(1) + '%';
            },
            sort: true,
            sort_value: function(item) {
                var percent = chart.getCompletePercentFromStep(item.index_key);
                return percent ? percent : 0;
            },
            name: t('all', 'AnalyticsFunnelTable__Users_percent_from_prev_step'),
            hint: t('all', 'AnalyticsFunnelTable__Users_percent_from_prev_step_hint'),
            width: '90px',
            class: 'smallHeader centerHeader',
            group: true
        },{
            type: 'number',
            value: function(item) {
                var dropoff = chart.getDropoff(item.index_key);
                if (dropoff === undefined) return t('all', 'Common__Na');
                return addCommas(dropoff);
            },
            sort: true,
            sort_value: function(item) {
                var dropoff = chart.getDropoff(item.index_key);
                return dropoff ? dropoff : 0;
            },
            name: t('all', 'AnalyticsFunnelTable__Dropoff_amount'),
            hint: t('all', 'AnalyticsFunnelTable__Dropoff_amount_hint'),
            width: '90px',
            class: 'smallHeader centerHeader',
            group: true
        },{
            type: 'number',
            value: function(item) {
                var percent = chart.getDropoffPercentFromTotal(item.index_key);
                if (percent === undefined) return t('all', 'Common__Na');
                return percent.toFixed(1) + '%';
            },
            sort: true,
            sort_value: function(item) {
                var percent = chart.getDropoffPercentFromTotal(item.index_key);
                return percent ? percent : 0;
            },
            name: t('all', 'AnalyticsFunnelTable__Dropoff_percent_from_total'),
            hint: t('all', 'AnalyticsFunnelTable__Dropoff_percent_from_total_hint'),
            width: '90px',
            class: 'smallHeader centerHeader',
            group: true
        },{
            type: 'number',
            value: function(item) {
                var percent = chart.getDropoffPercentFromStep(item.index_key);
                if (percent === undefined) return t('all', 'Common__Na');
                return percent.toFixed(1) + '%';
            },
            sort: true,
            sort_value: function(item) {
                var percent = chart.getDropoffPercentFromStep(item.index_key);
                return percent ? percent : 0;
            },
            name: t('all', 'AnalyticsFunnelTable__Dropoff_percent_from_prev_step'),
            hint: t('all', 'AnalyticsFunnelTable__Dropoff_percent_from_prev_step_hint'),
            width: '90px',
            class: 'smallHeader centerHeader',
            group: true
        }
    ];

    chart.tableSettings.columns_groups = [
        {
            columns: [0,1],
            name: t('all', 'AnalyticsFunnelTable__Step')
        },{
            name: t('all', 'AnalyticsFunnelTable__Users_completed'),
            columns: [2,3,4],
            width: '270px'
        },{
            name: t('all', 'AnalyticsFunnelTable__Users_dropped_off'),
            columns: [5,6,7],
            width: '270px'
        }
    ];

    chart.tableSettings.auto_layout = true;

    chart.getCompletePercentFromTotal = function(index) {
        if (!chart.data.totals.entered) return 0;
        return (chart.data.data[index].users / chart.data.totals.entered) * 100;
    };

    chart.getCompletePercentFromStep = function(index) {
        if (!index) return undefined;
        var step_users = chart.data.data[index-1].users;
        if (!step_users) return 0;
        return (chart.data.data[index].users / step_users) * 100;
    };

    chart.getDropoff = function(index) {
        if (!chart.data.data[index-1] || !chart.data.data[index]) return undefined;
        return (chart.data.data[index-1].users - chart.data.data[index].users);
    };

    chart.getDropoffPercentFromTotal = function(index) {
        var dropoff = chart.getDropoff(index);
        if (dropoff === undefined) return undefined;
        var total_dropoff = chart.data.totals.entered - chart.data.totals.completed;
        if (!total_dropoff) return 0;
        return (dropoff / total_dropoff) * 100;
    };

    chart.getDropoffPercentFromStep = function(index) {
        var dropoff = chart.getDropoff(index);
        if (dropoff === undefined) return undefined;
        var step_users = chart.data.data[index-1].users;
        if (!step_users) return 0;
        return (dropoff / step_users) * 100;
    };

    chart.getCompletePercent = function() {
        if (!chart.data.totals.entered) return 0;
        return (chart.data.totals.completed / chart.data.totals.entered) * 100;
    };

    chart.initManageButtons = function() {
        chart.chartContainer.find('.options').append(
            '<button class="graphType graphTypeFirst graphTypeLast funnelEdit" style="width: auto;">' + t("AnalyticsFunnel__Edit_funnel") + '</button>'
        );
        chart.chartContainer.find('.funnelEdit').click(function() {
            location = '/analytics/funnel/update?project_id='+chart.project_id+'&id='+chart.options.funnel_id;
        });
    };

    chart.init();

    chart.initManageButtons();

    return chart;
}
