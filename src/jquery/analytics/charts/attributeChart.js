import PieChart from './pieChart'

export default function (settings, inherit) {

    var chart = new PieChart(settings, true);

    chart.addTotals = true;

    chart.chartSettings.plotOptions.pie.minPercentage = 1;

    chart.chartSettings.series[0].key = 'total';

    chart.tableSettings.columns = [
        {
            checkbox: true
        },{
            type: 'string',
            value: 'title',
            search: true,
            sort: true,
            name: t('Analytics__Attribute_'+chart.method)
        },{
            type: 'number',
            value: 'total',
            sort: true,
            sort_value: 'total',
            name: t('Analytics__Total_users'),
            width: '20%'
        },{
            type: 'number',
            value: 'active',
            sort: true,
            sort_value: 'active',
            name: t('Analytics__Addressable_users'),
            width: '20%'
        },{
            type: 'number',
            value: function(item) {
                if (!chart.data.totals.total) {
                    return '-';
                } else {
                    var percent = (item.total / chart.data.totals.total) * 100;
                    return percent.toFixed(1)+'%';
                }
            },
            sort: true,
            sort_value: function(item) {
                if (!chart.data.totals.total) {
                    return 0;
                } else {
                    return item.total / chart.data.totals.total;
                }
            },
            name: t('Analytics__Percent'),
            width: '20%'
        }
    ];

    chart.tableSettings.row_search = true;
    chart.tableSettings.row_search_save = true;

    chart.visible_indexes = {};
    chart.tableSettings.row_selection = true;
    chart.tableSettings.row_selection_all_by_default = true;
    chart.tableSettings.row_selection_on_select_rows = function(ids) {
        for (var i in ids) {
            chart.visible_indexes[ids[i]] = 1;
        }
        chart.showIndexes(Object.keys(chart.visible_indexes));
    };
    chart.tableSettings.row_selection_on_deselect_rows = function(ids) {
        if (ids === null) {
            chart.visible_indexes = {};
        } else {
            for (var i in ids) {
                delete chart.visible_indexes[ids[i]];
            }
        }
        chart.showIndexes(Object.keys(chart.visible_indexes));
    };
    chart.tableSettings.on_build_table = function() {
        chart.table.selectAllVisibleRows();
    };

    if (chart.method == 'app_version') {
        chart.tableSettings.row_sort_default_column = 1;
        chart.tableSettings.row_sort_default_order = 'desc';
        chart.tableSettings.row_sort_force = true;
    } else {
        chart.tableSettings.row_sort_default_column = 2;
        chart.tableSettings.row_sort_default_order = 'desc';
        chart.tableSettings.row_sort_force = true;
    }

    if (!inherit) {
        chart.init();
    }

    return chart;
}