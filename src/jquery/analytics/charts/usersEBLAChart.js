import UsersEBChart from './usersEBChart'

export default function (settings, inherit) {

    var chart = new UsersEBChart(settings, true);

    chart.method = 'users_ebla';

    chart.tableSettings.columns[0].name = t('Analytics__Time_last_active');

    chart.chartSettings.tooltip.index_title = t("Analytics__Time_last_active");

    chart.updateTitle(t("Analytics__Users_engagement_by_last_active"));

    if (!inherit) {
        chart.init();
    }

    return chart;
}