import ItemsTimelineChart from './itemsTimelineChart'
import ItemsTotalsChart from './itemsTotalsChart'

export default function (settings) {

    var chart = this;

    chart.chartContainer = settings.element;
    chart.on_change = settings.on_change;
    chart.modifier = null;
    chart.mode = null;
    chart.modeChart = null;

    delete settings.element;

    chart.chartTimeline = new ItemsTimelineChart(jQuery.extend({
        method: 'tags',
        element: chart.chartContainer.find('.timelineChart')
    }, settings), true);

    chart.chartTimelineUnique = new ItemsTimelineChart(jQuery.extend({
        method: 'tags_unique',
        element: chart.chartContainer.find('.timelineUniqueChart')
    }, settings), true);

    chart.chartTotals = new ItemsTotalsChart(jQuery.extend({
        method: 'tags_total',
        element: chart.chartContainer.find('.totalsChart')
    }, settings), true);

    chart.chartTotalsUnique = new ItemsTotalsChart(jQuery.extend({
        method: 'tags_unique_total',
        element: chart.chartContainer.find('.totalsUniqueChart')
    }, settings), true);

    chart.initModeButtons = function() {
        chart.chartContainer.find('.options').append(
            '<button class="graphType graphTypeFirst graphMode" data-mode="totals" style="width: auto;">' + t("Analytics__Overall") + '</button>' +
            '<button class="graphType graphTypeLast graphMode" data-mode="timeline" style="width: auto;">' + t("Analytics__Timeline") + '</button>' +
            '<button class="graphType graphTypeFirst graphModifier" data-modifier="total_hits" style="width: auto;">' + t("Analytics__Total_hits") + '</button>' +
            '<button class="graphType graphTypeLast graphModifier" data-modifier="unique_hits" style="width: auto;">' + t("Analytics__Unique_hits") + '</button>'
        );
        chart.chartContainer.find('.graphMode').click(function() {
            chart.setMode($(this).data('mode'));
            chart.reload();
        });
        chart.chartContainer.find('.graphModifier').click(function() {
            chart.setModifier($(this).data('modifier'));
            chart.reload();
        });
    };

    chart.setMode = function(mode) {
        chart.mode = mode;
        chart.chartContainer.find('.graphMode').removeClass('active');
        chart.chartContainer.find('.graphMode[data-mode="'+mode+'"]').addClass('active');
        chart.changeChart();
    };

    chart.setModifier = function(modifier) {
        chart.modifier = modifier;
        chart.chartContainer.find('.graphModifier').removeClass('active');
        chart.chartContainer.find('.graphModifier[data-modifier="'+modifier+'"]').addClass('active');
        chart.changeChart();
    };

    chart.changeChart = function() {
        if (chart.modeChart) {
            chart.modeChart.chartContainer.hide();
        }
        if (chart.mode == 'timeline' && chart.modifier == 'total_hits') {
            chart.modeChart = chart.chartTimeline;
        } else if (chart.mode == 'timeline' && chart.modifier == 'unique_hits') {
            chart.modeChart = chart.chartTimelineUnique;
        } else if (chart.mode == 'totals' && chart.modifier == 'total_hits') {
            chart.modeChart = chart.chartTotals;
        } else if (chart.mode == 'totals' && chart.modifier == 'unique_hits') {
            chart.modeChart = chart.chartTotalsUnique;
        } else {
            chart.modeChart = null;
        }
        if (chart.modeChart) {
            chart.modeChart.chartContainer.show();
        }
        if (typeof chart.on_change == 'function') {
            chart.on_change();
        }
    };

    chart.reload = function() {
        chart.reloadByParam(chart.settings, chart.options);
    };

    chart.reloadByParam = function(settings, options) {
        if (!(settings === undefined)) {
            chart.settings = settings;
        }
        if (!(options === undefined)) {
            chart.options = options;
        }
        chart.modeChart.reloadByParam(chart.settings, chart.options);
    };

    chart.showItems = function(items) {
        chart.chartTimeline.showItems(items);
        chart.chartTimelineUnique.showItems(items);
        chart.chartTotals.showItems(items);
        chart.chartTotalsUnique.showItems(items);
    };

    chart.initModeButtons();

    chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Total_hits');
    chart.chartTimeline.updateTitle(t('Analytics__Tag_hits'));
    chart.chartTimeline.init();

    chart.chartTimelineUnique.chartSettings.tooltip.title = t('Analytics__Unique_hits');
    chart.chartTimelineUnique.updateTitle(t('Analytics__Tag_hits'));
    chart.chartTimelineUnique.init();

    chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Total_hits');
    chart.chartTotals.updateTitle(t('Analytics__Tag_hits'));
    chart.chartTotals.init();

    chart.chartTotalsUnique.chartSettings.tooltip.title = t('Analytics__Unique_hits');
    chart.chartTotalsUnique.updateTitle(t('Analytics__Tag_hits'));
    chart.chartTotalsUnique.init();

    if (settings.mode) {
        chart.setMode(settings.mode);
    }

    if (settings.modifier) {
        chart.setModifier(settings.modifier);
    }

    return chart;
}