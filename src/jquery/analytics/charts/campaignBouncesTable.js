import AnalyticsTable from './analyticsTable'

export default function (settings) {

    var table = new AnalyticsTable(settings, true);

    table.method = 'campaign_variant_bounces_list';

    table.columns = [
        {
            type: 'string',
            value: 'id',
            sort: true,
            name: t('Analytics__Bounce_type'),
        }, {
            type: 'number',
            value: 'amount',
            sort: true,
            name: t('Analytics__Amount'),
            width: '200px'
        }
    ];

    table.row_sort = true;
    table.row_sort_default_column = 1;
    table.row_sort_default_order = 'desc';

    table.init();

    return table;
};