import ItemsTimelineChart from './itemsTimelineChart'
import ItemsTotalsChart from './itemsTotalsChart'

export default function (settings) {

    var chart = this;

    chart.chartContainer = settings.element;
    chart.on_change = settings.on_change;
    chart.modifier = null;
    chart.mode = null;
    chart.modeChart = null;

    delete settings.element;

    chart.chartTimeline = new ItemsTimelineChart(jQuery.extend({
        method: 'metrics',
        element: chart.chartContainer.find('.timelineChart'),
        itemValueFormatter: {unit: true, decimals: 2}
    }, settings), true);

    chart.chartTimelineUsers = new ItemsTimelineChart(jQuery.extend({
        method: 'metrics_users',
        element: chart.chartContainer.find('.timelineUsersChart')
    }, settings), true);

    chart.chartTimelineAverage = new ItemsTimelineChart(jQuery.extend({
        method: 'metrics_average',
        element: chart.chartContainer.find('.timelineAverageChart'),
        itemValueFormatter: {unit: true, decimals: 2}
    }, settings), true);

    chart.chartTimelineParticipation = new ItemsTimelineChart(jQuery.extend({
        method: 'metrics_participation',
        element: chart.chartContainer.find('.timelineParticipationChart'),
        itemValueFormatter: {unit: '%', decimals: 2}
    }, settings), true);

    chart.chartTotals = new ItemsTotalsChart(jQuery.extend({
        method: 'metrics_total',
        element: chart.chartContainer.find('.totalsChart'),
        itemValueFormatter: {unit: true, decimals: 2}
    }, settings), true);

    chart.chartTotalsUsers = new ItemsTotalsChart(jQuery.extend({
        method: 'metrics_users_total',
        element: chart.chartContainer.find('.totalsUsersChart')
    }, settings), true);

    chart.chartTotalsAverage = new ItemsTotalsChart(jQuery.extend({
        method: 'metrics_average_total',
        element: chart.chartContainer.find('.totalsAverageChart'),
        itemValueFormatter: {unit: true, decimals: 2}
    }, settings), true);

    chart.chartTotalsParticipation = new ItemsTotalsChart(jQuery.extend({
        method: 'metrics_participation_total',
        element: chart.chartContainer.find('.totalsParticipationChart'),
        itemValueFormatter: {unit: '%', decimals: 2}
    }, settings), true);

    chart.initModeButtons = function() {
        chart.chartContainer.find('.options').append(
            '<button class="graphType graphTypeFirst graphMode" data-mode="totals" style="width: auto;">' + t("Analytics__Overall") + '</button>' +
            '<button class="graphType graphTypeLast graphMode" data-mode="timeline" style="width: auto;">' + t("Analytics__Timeline") + '</button>' +
            '<button class="graphType graphTypeFirst graphModifier" data-modifier="amount" style="width: auto;">' + t("Analytics__Metrics_total") + '</button>' +
            '<button class="graphType graphModifier" data-modifier="users" style="width: auto;">' + t("Analytics__Metrics_users") + '</button>' +
            '<button class="graphType graphModifier" data-modifier="average" style="width: auto;">' + t("Analytics__Metrics_average") + '</button>' +
            '<button class="graphType graphTypeLast graphModifier" data-modifier="participation" style="width: auto;">' + t("Analytics__Metrics_participation") + '</button>'
        );
        chart.chartContainer.find('.graphMode').click(function() {
            chart.setMode($(this).data('mode'));
            chart.reload();
        });
        chart.chartContainer.find('.graphModifier').click(function() {
            chart.setModifier($(this).data('modifier'));
            chart.reload();
        });
    };

    chart.setMode = function(mode) {
        chart.mode = mode;
        chart.chartContainer.find('.graphMode').removeClass('active');
        chart.chartContainer.find('.graphMode[data-mode="'+mode+'"]').addClass('active');
        chart.changeChart();
    };

    chart.setModifier = function(modifier) {
        chart.modifier = modifier;
        chart.chartContainer.find('.graphModifier').removeClass('active');
        chart.chartContainer.find('.graphModifier[data-modifier="'+modifier+'"]').addClass('active');
        chart.changeChart();
    };

    chart.changeChart = function() {
        if (chart.modeChart) {
            chart.modeChart.chartContainer.hide();
        }
        if (chart.mode == 'timeline' && chart.modifier == 'amount') {
            chart.modeChart = chart.chartTimeline;
        } else if (chart.mode == 'timeline' && chart.modifier == 'users') {
            chart.modeChart = chart.chartTimelineUsers;
        } else if (chart.mode == 'timeline' && chart.modifier == 'average') {
            chart.modeChart = chart.chartTimelineAverage;
        } else if (chart.mode == 'timeline' && chart.modifier == 'participation') {
            chart.modeChart = chart.chartTimelineParticipation;
        } else if (chart.mode == 'totals' && chart.modifier == 'amount') {
            chart.modeChart = chart.chartTotals;
        } else if (chart.mode == 'totals' && chart.modifier == 'users') {
            chart.modeChart = chart.chartTotalsUsers;
        } else if (chart.mode == 'totals' && chart.modifier == 'average') {
            chart.modeChart = chart.chartTotalsAverage;
        } else if (chart.mode == 'totals' && chart.modifier == 'participation') {
            chart.modeChart = chart.chartTotalsParticipation;
        } else {
            chart.modeChart = null;
        }
        if (chart.modeChart) {
            chart.modeChart.chartContainer.show();
        }
        if (typeof chart.on_change == 'function') {
            chart.on_change();
        }
    };

    chart.reload = function() {
        chart.reloadByParam(chart.settings, chart.options);
    };

    chart.reloadByParam = function(settings, options) {
        if (!(settings === undefined)) {
            chart.settings = settings;
        }
        if (!(options === undefined)) {
            chart.options = options;
        }
        chart.modeChart.reloadByParam(chart.settings, chart.options);
    };

    chart.showItems = function(items) {
        chart.chartTimeline.showItems(items);
        chart.chartTimelineUsers.showItems(items);
        chart.chartTimelineAverage.showItems(items);
        chart.chartTimelineParticipation.showItems(items);
        chart.chartTotals.showItems(items);
        chart.chartTotalsUsers.showItems(items);
        chart.chartTotalsAverage.showItems(items);
        chart.chartTotalsParticipation.showItems(items);
    };

    chart.initModeButtons();

    chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Metrics_total');
    chart.chartTimeline.updateTitle(t('Analytics__Metrics'));
    chart.chartTimeline.init();

    chart.chartTimelineUsers.chartSettings.tooltip.title = t('Analytics__Metrics_users');
    chart.chartTimelineUsers.updateTitle(t('Analytics__Metrics'));
    chart.chartTimelineUsers.init();

    chart.chartTimelineAverage.chartSettings.tooltip.title = t('Analytics__Metrics_average');
    chart.chartTimelineAverage.updateTitle(t('Analytics__Metrics'));
    chart.chartTimelineAverage.init();

    chart.chartTimelineParticipation.chartSettings.tooltip.title = t('Analytics__Metrics_participation');
    chart.chartTimelineParticipation.updateTitle(t('Analytics__Metrics'));
    chart.chartTimelineParticipation.init();

    chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Metrics_total');
    chart.chartTotals.updateTitle(t('Analytics__Metrics'));
    chart.chartTotals.init();

    chart.chartTotalsUsers.chartSettings.tooltip.title = t('Analytics__Metrics_users');
    chart.chartTotalsUsers.updateTitle(t('Analytics__Metrics'));
    chart.chartTotalsUsers.init();

    chart.chartTotalsAverage.chartSettings.tooltip.title = t('Analytics__Metrics_average');
    chart.chartTotalsAverage.updateTitle(t('Analytics__Metrics'));
    chart.chartTotalsAverage.init();

    chart.chartTotalsParticipation.chartSettings.tooltip.title = t('Analytics__Metrics_participation');
    chart.chartTotalsParticipation.updateTitle(t('Analytics__Metrics'));
    chart.chartTotalsParticipation.init();

    if (settings.mode) {
        chart.setMode(settings.mode);
    }

    if (settings.modifier) {
        chart.setModifier(settings.modifier);
    }

    return chart;
}