import AnalyticsChart from './analyticsChart'
import LoadGoogleMaps from 'vendor/gmaps/loader'

export default function (settings, inherit) {
  var chart = new AnalyticsChart(settings, true);
  var google;
  chart.settings = settings;

  if (!settings.settings) settings.settings = {};

  chart.updateAddress = settings.chartSettings.setAddress;
  chart.mapReady = settings.chartSettings.mapReady;

  chart.marker = {};
  chart.infoWindow = null;
  chart._init = chart.init;

  chart.init = function () {
    this.initiated = true;
    LoadGoogleMaps(function (googleApi) {
      google = googleApi
      chart.mapSettings = {
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: true,
        fullscreenControl: true,
        MapType: false,
        styles: settings.styles
      };
      chart._init();
    })
  };

  chart.buildGraph = function() {
    LoadGoogleMaps(function () {
      chart.createMap();
      chart.centerMap();
      chart.mapReady();
    })
  };

  chart.createMap = function () {
    $(chart.chartSettings.chart.renderTo).height(835);
    $(chart.chartSettings.chart.renderTo).parent().find('.placeholder').remove();
    if (!chart.gmap) {
      chart.gmap = new google.maps.Map(chart.chartSettings.chart.renderTo, chart.mapSettings);
    }
    chart.infowindow = new google.maps.InfoWindow();
  };

  chart.destroyMap = function () {
    $(chart.chartSettings.chart.renderTo).empty();
    if (chart.gmap) {
      delete chart.gmap;
    }
  };

  chart.centerMap = function() {
    var visible_markers = false;
    var bounds = new google.maps.LatLngBounds();

    if (chart.marker.visible)
    {
      visible_markers = true;
      bounds.extend(chart.marker.marker.position);
      bounds.union(chart.marker.circle.getBounds());
    }

    if (visible_markers) {
      chart.gmap.fitBounds(bounds);
    } else {
      var defCenter = new google.maps.LatLng(0, 0);
      chart.gmap.setZoom(1);
      chart.gmap.setCenter(defCenter);
    }
  };

  chart.fitWithPositionAndRadius = function(latitude, longitude, radius, jump) {
    if (jump === undefined || jump) {
      $('html, body').scrollTop($(chart.chartSettings.chart.renderTo).offset().top - 145);
    }
    var bounds = new google.maps.LatLngBounds();
    var position = new google.maps.LatLng(latitude, longitude);
    var circle = new google.maps.Circle({center: position, radius: radius * 1.3});
    bounds.extend(position);
    bounds.union(circle.getBounds());
    chart.gmap.fitBounds(bounds);

    chart.updateMarker(0, latitude, longitude, radius, settings.title)
  };

  chart.updateMarker = function(id, latitude, longitude, radius, title, color){

    var position = new google.maps.LatLng(latitude, longitude);

    var marker = new google.maps.Marker({
      position: position,
      map: chart.gmap,
      id: id,
      title: title,
      draggable: true
    });

    if (!color) color = '#FF0000';

    var circle = new google.maps.Circle({
      center: position,
      radius: parseInt(radius),
      fillOpacity: 0.35,
      fillColor: color,
      strokeColor: color,
      strokeOpacity: 0.5,
      strokeWeight: 1,
      map: chart.gmap
    });

    if (marker.title)
    {
      google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
          chart.infowindow.setContent($('<div>').text(marker.title).html());
          chart.infowindow.open(chart.gmap, marker);
        }
      })(marker));
    }

    google.maps.event.addListener(marker, 'dblclick', (function(marker, circle) {
      return function() {
        chart.fitWithPositionAndRadius(marker.getPosition().lat(), marker.getPosition().lng(), circle.radius);
      }
    })(marker, circle));

    google.maps.event.addListener(marker, 'dragend', $.proxy(function(pos) {
      if (chart.marker && chart.marker.circle) {
        chart.marker.circle.setCenter(this.getPosition());
      }
      if (typeof chart.onDrag == 'function') {
        chart.onDrag(pos.latLng, radius);
      }
    }), marker);

    chart.marker = {marker: marker, circle: circle, visible: true};

    return chart.marker;
  }

  chart.removeMarker = function() {
    if (chart.marker)
    {
      if (chart.marker.marker) {
        chart.marker.marker.setMap(null);
      }
      if (chart.marker.circle) {
        chart.marker.circle.setMap(null);
      }
      delete chart.marker;
    }
  };

  chart.refreshMap = function(latitude, longitude, radius){
    chart.removeMarker()
    chart.fitWithPositionAndRadius(latitude, longitude, radius)
  }

  chart.onDrag = function(pos, radius){
    chart.refreshMap(pos.lat(), pos.lng(), radius)
    chart.setNewAddress(pos)
  };

  chart.setNewAddress = function(pos){
    let geocoder = null
    LoadGoogleMaps(function(google) {
      geocoder = new google.maps.Geocoder()
      geocoder.geocode({'latLng': pos}, results => {
        if (results[0]) {
          var country = null;
          var country_name = "";
          for (var i in results[0].address_components) {
            if (results[0].address_components[i].types[0] == 'country') {
              country = results[0].address_components[i].short_name;
              country_name = results[0].address_components[i].long_name;
            }
          }
          chart.updateAddress(results[0].geometry,  results[0].formatted_address, country)
        } else {
          chart.updateAddress(request.latLng, "", "");
        }
      })
    })
  }

  chart.destroy = function() {
    chart.destroyMap();
  };

  if (!inherit) {
    chart.init();
  }

  return chart;
};
