import ItemsTimelineChart from './itemsTimelineChart'
import ItemsTotalsChart from './itemsTotalsChart'

export default function (settings, inherit) {

    var chart = this;

    chart.chartContainer = settings.element;
    chart.modifier = null;
    chart.mode = null;
    chart.modeChart = null;

    chart.method_prefix = 'campaigns_';

    delete settings.element;

    chart.chartTimeline = new ItemsTimelineChart(jQuery.extend({
        element: chart.chartContainer.find('.timelineChart')
    }, settings), true);

    chart.chartTotals = new ItemsTotalsChart(jQuery.extend({
        element: chart.chartContainer.find('.totalsChart')
    }, settings), true);

    chart.initModeButtons = function() {
        var select = $('<select class="graphModifierSelect"></select>');
        select.append('<option value="">'+t("Analytics__Select_metric_dropdown")+'</option>');
        if (settings.show_mobile_push) {
            select.append('<option value="mobile_push_sent">'+t("Analytics__Campaigns_mobile_push_sent")+'</option>');
            select.append('<option value="mobile_push_opened">'+t("Analytics__Campaigns_mobile_push_opened")+'</option>');
            select.append('<option value="mobile_push_open_rate">'+t("Analytics__Campaigns_mobile_push_open_rate")+'</option>');
        }
        if (settings.show_web_push) {
            select.append('<option value="web_push_sent">'+t("Analytics__Campaigns_web_push_sent")+'</option>');
            select.append('<option value="web_push_delivered">'+t("Analytics__Campaigns_web_push_delivered")+'</option>');
            select.append('<option value="web_push_opened">'+t("Analytics__Campaigns_web_push_opened")+'</option>');
            select.append('<option value="web_push_open_rate">'+t("Analytics__Campaigns_web_push_open_rate")+'</option>');
        }
        if (settings.show_sms) {
            select.append('<option value="sms_sent">'+t("Analytics__Campaigns_sms_sent")+'</option>');
            select.append('<option value="sms_delivered">'+t("Analytics__Campaigns_sms_delivered")+'</option>');
        }
        if (settings.show_inbox) {
            select.append('<option value="inbox_sent">'+t("Analytics__Campaigns_inbox_sent")+'</option>');
            select.append('<option value="inbox_delivered">'+t("Analytics__Campaigns_inbox_delivered")+'</option>');
            select.append('<option value="inbox_opened">'+t("Analytics__Campaigns_inbox_opened")+'</option>');
            select.append('<option value="inbox_open_rate">'+t("Analytics__Campaigns_inbox_open_rate")+'</option>');
        }
        if (settings.show_inweb) {
            select.append('<option value="inweb_sent">'+t("Analytics__Campaigns_inweb_sent")+'</option>');
            select.append('<option value="inweb_opened">'+t("Analytics__Campaigns_inweb_clicked")+'</option>');
            select.append('<option value="inweb_open_rate">'+t("Analytics__Campaigns_inweb_click_rate")+'</option>');
        }
        if (settings.show_inapp) {
            select.append('<option value="inapp_sent">'+t("Analytics__Campaigns_inapp_sent")+'</option>');
            select.append('<option value="inapp_delivered">'+t("Analytics__Campaigns_inapp_delivered")+'</option>');
            select.append('<option value="inapp_opened">'+t("Analytics__Campaigns_inapp_opened")+'</option>');
            select.append('<option value="inapp_open_rate">'+t("Analytics__Campaigns_inapp_open_rate")+'</option>');
        }
        if (settings.show_email) {
            select.append('<option value="email_sent">'+t("Analytics__Campaigns_email_sent")+'</option>');
            select.append('<option value="email_delivered">'+t("Analytics__Campaigns_email_delivered")+'</option>');
            select.append('<option value="email_opened">'+t("Analytics__Campaigns_email_opened")+'</option>');
            select.append('<option value="email_open_rate">'+t("Analytics__Campaigns_email_open_rate")+'</option>');
            select.append('<option value="email_clicked">'+t("Analytics__Campaigns_email_clicked")+'</option>');
            select.append('<option value="email_click_rate">'+t("Analytics__Campaigns_email_click_rate")+'</option>');
            select.append('<option value="email_failed">'+t("Analytics__Campaigns_email_failed")+'</option>');
            select.append('<option value="email_unsubscribed">'+t("Analytics__Campaigns_email_unsubscribed")+'</option>');
            select.append('<option value="email_complained">'+t("Analytics__Campaigns_email_complained")+'</option>');
        }
        if (settings.show_facebook) {
            select.append('<option value="facebook_sent">'+t("Analytics__Campaigns_facebook_sent")+'</option>');
            select.append('<option value="facebook_delivered">'+t("Analytics__Campaigns_facebook_delivered")+'</option>');
            select.append('<option value="facebook_opened">'+t("Analytics__Campaigns_facebook_opened")+'</option>');
            select.append('<option value="facebook_open_rate">'+t("Analytics__Campaigns_facebook_open_rate")+'</option>');
        }
        if (settings.show_whatsapp) {
            select.append('<option value="whatsapp_sent">'+t("Analytics__Campaigns_whatsapp_sent")+'</option>');
            select.append('<option value="whatsapp_delivered">'+t("Analytics__Campaigns_whatsapp_delivered")+'</option>');
            select.append('<option value="whatsapp_opened">'+t("Analytics__Campaigns_whatsapp_opened")+'</option>');
            select.append('<option value="whatsapp_open_rate">'+t("Analytics__Campaigns_whatsapp_open_rate")+'</option>');
        }
        if (settings.show_conversions) {
            select.append('<option value="actions_converted_unique">'+t("Analytics__Campaigns_actions_converted_unique")+'</option>');
            select.append('<option value="actions_sent_unique">'+t("Analytics__Campaigns_actions_sent_unique")+'</option>');
            select.append('<option value="actions_conversion">'+t("Analytics__Campaigns_actions_conversion")+'</option>');
            select.append('<option value="goals">'+t("Analytics__Campaigns_goals")+'</option>');
            select.append('<option value="goals_value">'+t("Analytics__Campaigns_goals_value")+'</option>');
        }
        if (settings.show_app_opens) {
            select.append('<option value="app_opens_direct">'+t("Analytics__Campaigns_app_opens_direct")+'</option>');
        }
        if (settings.modifier) {
            select.val(settings.modifier)
        }
        chart.chartContainer.find('.options').append(select);
        chart.chartContainer.find('.graphMode').click(function() {
            chart.setMode($(this).data('mode'));
            chart.reload();
        });
        chart.chartContainer.find('.graphModifierSelect').change(function() {
            chart.setModifier($(this).val());
            chart.reload();
        });
    };

    chart.setMode = function(mode) {
        chart.mode = mode;
        chart.chartContainer.find('.graphMode').removeClass('active');
        chart.chartContainer.find('.graphMode[data-mode="'+mode+'"]').addClass('active');
        chart.changeChart();
    };

    chart.setModifier = function(modifier) {
        if (chart.onModifierChange) {
            chart.onModifierChange(modifier)
        }

        chart.modifier = modifier;
        chart.chartContainer.find('.graphModifier').removeClass('active');
        chart.chartContainer.find('.graphModifier[data-modifier="'+modifier+'"]').addClass('active');
        chart.changeChart();
    };

    chart.changeChart = function() {
        if (chart.modeChart) {
            chart.modeChart.chartContainer.hide();
        }

        switch (chart.mode) {
            case 'timeline':
                chart.modeChart = chart.chartTimeline;
                break;
            case 'totals':
                chart.modeChart = chart.chartTotals;
                break;
            default:
                break;
        }

        chart.chartTimeline.method = null;
        chart.chartTotals.method = null;
        chart.chartTimeline.chartSettings.tooltip.title = null;
        chart.chartTotals.chartSettings.tooltip.title = null;
        chart.chartTimeline.itemValueFormatter = {};
        chart.chartTimeline.setValueFormatter(null);
        chart.chartTotals.setValueFormatter(null);

        switch (chart.modifier) {
            case 'web_push_sent':
                chart.chartTimeline.method = chart.method_prefix + 'web_push_sent';
                chart.chartTotals.method = chart.method_prefix + 'web_push_sent_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_web_push_sent');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_web_push_sent');
                break;
            case 'web_push_delivered':
                chart.chartTimeline.method = chart.method_prefix + 'web_push_delivered';
                chart.chartTotals.method = chart.method_prefix + 'web_push_delivered_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_web_push_delivered');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_web_push_delivered');
                break;
            case 'web_push_opened':
                chart.chartTimeline.method = chart.method_prefix + 'web_push_opened';
                chart.chartTotals.method = chart.method_prefix + 'web_push_opened_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_web_push_opened');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_web_push_opened');
                break;
            case 'web_push_open_rate':
                chart.chartTimeline.method = chart.method_prefix + 'web_push_open_rate';
                chart.chartTotals.method = chart.method_prefix + 'web_push_open_rate_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_web_push_open_rate');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_web_push_open_rate');
                chart.chartTimeline.itemValueFormatter = {decimals: 2, unit: '%'};
                chart.chartTimeline.setValueFormatter('percent');
                chart.chartTotals.setValueFormatter('percent');
                break;
            case 'mobile_push_sent':
                chart.chartTimeline.method = chart.method_prefix + 'mobile_push_sent';
                chart.chartTotals.method = chart.method_prefix + 'mobile_push_sent_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_mobile_push_sent');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_mobile_push_sent');
                break;
            case 'mobile_push_opened':
                chart.chartTimeline.method = chart.method_prefix + 'mobile_push_opened';
                chart.chartTotals.method = chart.method_prefix + 'mobile_push_opened_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_mobile_push_opened');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_mobile_push_opened');
                break;
            case 'mobile_push_open_rate':
                chart.chartTimeline.method = chart.method_prefix + 'mobile_push_open_rate';
                chart.chartTotals.method = chart.method_prefix + 'mobile_push_open_rate_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_mobile_push_open_rate');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_mobile_push_open_rate');
                chart.chartTimeline.itemValueFormatter = {decimals: 2, unit: '%'};
                chart.chartTimeline.setValueFormatter('percent');
                chart.chartTotals.setValueFormatter('percent');
                break;
            case 'inweb_sent':
                chart.chartTimeline.method = chart.method_prefix + 'inweb_sent';
                chart.chartTotals.method = chart.method_prefix + 'inweb_sent_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_inweb_sent');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_inweb_sent');
                break;
            case 'inweb_delivered':
                chart.chartTimeline.method = chart.method_prefix + 'inweb_delivered';
                chart.chartTotals.method = chart.method_prefix + 'inweb_delivered_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_inweb_delivered');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_inweb_delivered');
                break;
            case 'inweb_opened':
                chart.chartTimeline.method = chart.method_prefix + 'inweb_opened';
                chart.chartTotals.method = chart.method_prefix + 'inweb_opened_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_inweb_clicked');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_inweb_clicked');
                break;
            case 'inweb_open_rate':
                chart.chartTimeline.method = chart.method_prefix + 'inweb_open_rate';
                chart.chartTotals.method = chart.method_prefix + 'inweb_open_rate_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_inweb_click_rate');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_inweb_click_rate');
                chart.chartTimeline.itemValueFormatter = {decimals: 2, unit: '%'};
                chart.chartTimeline.setValueFormatter('percent');
                chart.chartTotals.setValueFormatter('percent');
                break;
            case 'inapp_sent':
                chart.chartTimeline.method = chart.method_prefix + 'inapp_sent';
                chart.chartTotals.method = chart.method_prefix + 'inapp_sent_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_inapp_sent');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_inapp_sent');
                break;
            case 'inapp_delivered':
                chart.chartTimeline.method = chart.method_prefix + 'inapp_delivered';
                chart.chartTotals.method = chart.method_prefix + 'inapp_delivered_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_inapp_delivered');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_inapp_delivered');
                break;
            case 'inapp_opened':
                chart.chartTimeline.method = chart.method_prefix + 'inapp_opened';
                chart.chartTotals.method = chart.method_prefix + 'inapp_opened_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_inapp_opened');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_inapp_opened');
                break;
            case 'inapp_open_rate':
                chart.chartTimeline.method = chart.method_prefix + 'inapp_open_rate';
                chart.chartTotals.method = chart.method_prefix + 'inapp_open_rate_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_inapp_open_rate');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_inapp_open_rate');
                chart.chartTimeline.itemValueFormatter = {decimals: 2, unit: '%'};
                chart.chartTimeline.setValueFormatter('percent');
                chart.chartTotals.setValueFormatter('percent');
                break;
            case 'inbox_sent':
                chart.chartTimeline.method = chart.method_prefix + 'inbox_sent';
                chart.chartTotals.method = chart.method_prefix + 'inbox_sent_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_inbox_sent');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_inbox_sent');
                break;
            case 'inbox_delivered':
                chart.chartTimeline.method = chart.method_prefix + 'inbox_delivered';
                chart.chartTotals.method = chart.method_prefix + 'inbox_delivered_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_inbox_delivered');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_inbox_delivered');
                break;
            case 'inbox_opened':
                chart.chartTimeline.method = chart.method_prefix + 'inbox_opened';
                chart.chartTotals.method = chart.method_prefix + 'inbox_opened_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_inbox_opened');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_inbox_opened');
                break;
            case 'inbox_open_rate':
                chart.chartTimeline.method = chart.method_prefix + 'inbox_open_rate';
                chart.chartTotals.method = chart.method_prefix + 'inbox_open_rate_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_inbox_open_rate');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_inbox_open_rate');
                chart.chartTimeline.itemValueFormatter = {decimals: 2, unit: '%'};
                chart.chartTimeline.setValueFormatter('percent');
                chart.chartTotals.setValueFormatter('percent');
                break;
            case 'email_sent':
                chart.chartTimeline.method = chart.method_prefix + 'email_sent';
                chart.chartTotals.method = chart.method_prefix + 'email_sent_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_email_sent');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_email_sent');
                break;
            case 'email_failed':
                chart.chartTimeline.method = chart.method_prefix + 'email_failed';
                chart.chartTotals.method = chart.method_prefix + 'email_failed_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_email_failed');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_email_failed');
                break;
            case 'email_delivered':
                chart.chartTimeline.method = chart.method_prefix + 'email_delivered';
                chart.chartTotals.method = chart.method_prefix + 'email_delivered_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_email_delivered');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_email_delivered');
                break;
            case 'email_opened':
                chart.chartTimeline.method = chart.method_prefix + 'email_opened';
                chart.chartTotals.method = chart.method_prefix + 'email_opened_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_email_opened');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_email_opened');
                break;
            case 'email_open_rate':
                chart.chartTimeline.method = chart.method_prefix + 'email_open_rate';
                chart.chartTotals.method = chart.method_prefix + 'email_open_rate_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_email_open_rate');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_email_open_rate');
                chart.chartTimeline.itemValueFormatter = {decimals: 2, unit: '%'};
                chart.chartTimeline.setValueFormatter('percent');
                chart.chartTotals.setValueFormatter('percent');
                break;
            case 'email_clicked':
                chart.chartTimeline.method = chart.method_prefix + 'email_clicked';
                chart.chartTotals.method = chart.method_prefix + 'email_clicked_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_email_clicked');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_email_clicked');
                break;
            case 'email_click_rate':
                chart.chartTimeline.method = chart.method_prefix + 'email_click_rate';
                chart.chartTotals.method = chart.method_prefix + 'email_click_rate_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_email_click_rate');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_email_click_rate');
                chart.chartTimeline.itemValueFormatter = {decimals: 2, unit: '%'};
                chart.chartTimeline.setValueFormatter('percent');
                chart.chartTotals.setValueFormatter('percent');
                break;
            case 'email_unsubscribed':
                chart.chartTimeline.method = chart.method_prefix + 'email_unsubscribed';
                chart.chartTotals.method = chart.method_prefix + 'email_unsubscribed_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_email_unsubscribed');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_email_unsubscribed');
                break;
            case 'email_complained':
                chart.chartTimeline.method = chart.method_prefix + 'email_complained';
                chart.chartTotals.method = chart.method_prefix + 'email_complained_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_email_complained');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_email_complained');
                break;
            case 'sms_sent':
                chart.chartTimeline.method = chart.method_prefix + 'sms_sent';
                chart.chartTotals.method = chart.method_prefix + 'sms_sent_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_sms_sent');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_sms_sent');
                break;
            case 'sms_delivered':
                chart.chartTimeline.method = chart.method_prefix + 'sms_delivered';
                chart.chartTotals.method = chart.method_prefix + 'sms_delivered_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_sms_delivered');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_sms_delivered');
                break;
            case 'sms_opened':
                chart.chartTimeline.method = chart.method_prefix + 'sms_opened';
                chart.chartTotals.method = chart.method_prefix + 'sms_opened_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_sms_opened');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_sms_opened');
                break;
            case 'sms_open_rate':
                chart.chartTimeline.method = chart.method_prefix + 'sms_open_rate';
                chart.chartTotals.method = chart.method_prefix + 'sms_open_rate_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_sms_open_rate');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_sms_open_rate');
                chart.chartTimeline.itemValueFormatter = {decimals: 2, unit: '%'};
                chart.chartTimeline.setValueFormatter('percent');
                chart.chartTotals.setValueFormatter('percent');
                break;
            case 'facebook_sent':
                chart.chartTimeline.method = chart.method_prefix + 'facebook_sent';
                chart.chartTotals.method = chart.method_prefix + 'facebook_sent_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_facebook_sent');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_facebook_sent');
                break;
            case 'facebook_delivered':
                chart.chartTimeline.method = chart.method_prefix + 'facebook_delivered';
                chart.chartTotals.method = chart.method_prefix + 'facebook_delivered_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_facebook_delivered');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_facebook_delivered');
                break;
            case 'facebook_opened':
                chart.chartTimeline.method = chart.method_prefix + 'facebook_opened';
                chart.chartTotals.method = chart.method_prefix + 'facebook_opened_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_facebook_opened');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_facebook_opened');
                break;
            case 'facebook_open_rate':
                chart.chartTimeline.method = chart.method_prefix + 'facebook_open_rate';
                chart.chartTotals.method = chart.method_prefix + 'facebook_open_rate_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_facebook_open_rate');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_facebook_open_rate');
                chart.chartTimeline.itemValueFormatter = {decimals: 2, unit: '%'};
                chart.chartTimeline.setValueFormatter('percent');
                chart.chartTotals.setValueFormatter('percent');
                break;
            case 'whatsapp_sent':
                chart.chartTimeline.method = chart.method_prefix + 'whatsapp_sent';
                chart.chartTotals.method = chart.method_prefix + 'whatsapp_sent_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_whatsapp_sent');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_whatsapp_sent');
                break;
            case 'whatsapp_delivered':
                chart.chartTimeline.method = chart.method_prefix + 'whatsapp_delivered';
                chart.chartTotals.method = chart.method_prefix + 'whatsapp_delivered_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_whatsapp_delivered');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_whatsapp_delivered');
                break;
            case 'whatsapp_opened':
                chart.chartTimeline.method = chart.method_prefix + 'whatsapp_opened';
                chart.chartTotals.method = chart.method_prefix + 'whatsapp_opened_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_whatsapp_opened');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_whatsapp_opened');
                break;
            case 'whatsapp_open_rate':
                chart.chartTimeline.method = chart.method_prefix + 'whatsapp_open_rate';
                chart.chartTotals.method = chart.method_prefix + 'whatsapp_open_rate_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_whatsapp_open_rate');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_whatsapp_open_rate');
                chart.chartTimeline.itemValueFormatter = {decimals: 2, unit: '%'};
                chart.chartTimeline.setValueFormatter('percent');
                chart.chartTotals.setValueFormatter('percent');
                break;
            case 'app_opens_direct':
                chart.chartTimeline.method = chart.method_prefix + 'app_opens_direct';
                chart.chartTotals.method = chart.method_prefix + 'app_opens_direct_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_app_opens_direct');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_app_opens_direct');
                break;
            case 'actions_sent_unique':
                chart.chartTimeline.method = chart.method_prefix + 'actions_sent_unique';
                chart.chartTotals.method = chart.method_prefix + 'actions_sent_unique_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_actions_sent_unique');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_actions_sent_unique');
                break;
            case 'actions_converted_unique':
                chart.chartTimeline.method = chart.method_prefix + 'actions_converted_unique';
                chart.chartTotals.method = chart.method_prefix + 'actions_converted_unique_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_actions_converted_unique');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_actions_converted_unique');
                break;
            case 'actions_conversion':
                chart.chartTimeline.method = chart.method_prefix + 'actions_conversion';
                chart.chartTotals.method = chart.method_prefix + 'actions_conversion_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_actions_conversion');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_actions_conversion');
                chart.chartTimeline.itemValueFormatter = {decimals: 2, unit: '%'};
                chart.chartTimeline.setValueFormatter('percent');
                chart.chartTotals.setValueFormatter('percent');
                break;
            case 'goals':
                chart.chartTimeline.method = chart.method_prefix + 'goals';
                chart.chartTotals.method = chart.method_prefix + 'goals_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_goals');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_goals');
                break;
            case 'goals_value':
                chart.chartTimeline.method = chart.method_prefix + 'goals_value';
                chart.chartTotals.method = chart.method_prefix + 'goals_value_total';
                chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Campaigns_goals_value');
                chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Campaigns_goals_value');
                break;
        }

        if (chart.modeChart) {
            chart.modeChart.chartContainer.show();
        }
    };

    chart.reload = function() {
        chart.reloadByParam(chart.settings, chart.options);
    };

    chart.reloadByParam = function(settings, options) {
        if (!(settings === undefined)) {
            chart.settings = settings;
        }
        if (!(options === undefined)) {
            chart.options = options;
        }
        chart.modeChart.reloadByParam(chart.settings, chart.options);
    };

    chart.showItems = function(items) {
        chart.chartTimeline.showItems(items);
        chart.chartTotals.showItems(items);
    };

    chart.init = function() {
        chart.initModeButtons();

        chart.chartTimeline.updateTitle(t('Analytics__Campaigns_statistics'));
        chart.chartTimeline.init();

        chart.chartTotals.updateTitle(t('Analytics__Campaigns_statistics'));
        chart.chartTotals.init();

        if (settings.mode) {
            chart.setMode(settings.mode);
        }

        if (settings.modifier) {
            chart.setModifier(settings.modifier);
        }
    };

    if (!inherit) {
        chart.init();
    }

    return chart;
}
