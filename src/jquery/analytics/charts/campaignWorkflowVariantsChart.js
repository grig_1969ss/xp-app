import CampaignsChart from './campaignsChart'

export default function (settings) {

  var chart = new CampaignsChart(settings, true);

  chart.method_prefix = 'campaign_variants_workflow_';

  chart.init();

  return chart;
}
