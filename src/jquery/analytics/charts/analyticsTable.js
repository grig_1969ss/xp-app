import DataTable from '@/jquery/widgets/dataTable'

export default function (settings, inherit) {

    var table = new DataTable(settings, true);

    table.method  = settings.method;
    table.addTotals = settings.addTotals;
    table.addPreviousTotals = settings.addPreviousTotals;

    table.project_id = settings.project_id;
    table.platform   = settings.platform;
    table.startDate  = settings.startDate;
    table.endDate    = settings.endDate;
    table.mobilePlatform = settings.mobilePlatform;

    table.reloadByParam = function(settings, options) {
        if (!(settings===undefined))
        {
            if (!(settings.platform===undefined)) {
                this.platform = settings.platform;
            }

            if (!(settings.mobilePlatform===undefined)) {
                this.mobilePlatform = settings.mobilePlatform;
            }

            if (!(settings.startDate===undefined)) {
                this.startDate = settings.startDate;
            }

            if (!(settings.endDate===undefined)) {
                this.endDate = settings.endDate;
            }

            if (!(settings.method===undefined)) {
                this.method = settings.method;
            }

            if (!(settings.project_id===undefined)) {
                this.project_id = settings.project_id;
            }
        }

        if (!(options===undefined)) {
            this.reloadByOptions(options);
        } else {
            this.reload();
        }
    };

    table.data_source = '/analytics/analytics/call';

    table.requestDataDefault = table.requestData;
    table.requestData = function() {
        var requestData = this.requestDataDefault();

        requestData['project_id'] = this.project_id;
        requestData['method'] = this.method;
        if (this.addTotals) {
            requestData['addTotals'] = 1;
        }
        if (this.addPreviousTotals) {
            requestData['addPreviousTotals'] = 1;
        }

        if (this.platform) {
            requestData['platform'] = this.platform
        }
        if (this.mobilePlatform) {
            requestData['mobilePlatform'] = this.mobilePlatform
        }
        if (this.startDate) {
            requestData['startDate'] = this.startDate
        }
        if (this.endDate) {
            requestData['endDate'] = this.endDate
        }

        return requestData;
    };

    table.process_request_link = function() {
        var requestLink = API_URL + this.data_source;



        if (this.options) {
            $.each(this.options,function(index,value) {
                if (value != null) {
                    sendData[index] = value;
                }
            });
        }

        var first = true;
        for (var i in sendData) {
            if (first) requestLink += '?'; else requestLink += '&'; first = false;
            requestLink += i + '=' + sendData[i];
        }

        return requestLink;
    };

    table.empty_label = t("Analytics__There_is_no_items");

    if (!inherit) {
        table.init();
    }

    return table;
}