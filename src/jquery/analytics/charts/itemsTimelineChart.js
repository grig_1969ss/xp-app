import AnalyticsChart from './analyticsChart'
import { addUnit } from '@/jquery/utils'

export default function (settings, inherit) {

    var chart = new AnalyticsChart(settings, true);

    chart.chartSettings.legend = {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        itemMarginTop: 5,
        itemMarginBottom: 4,
        borderColor: '#eee',
        maxHeight: 220,
        width: 300,
        useHTML: false,
        labelFormatter: function() {
            var name = this.name
                .replace("[\n]", " ")
                .replace("[\d]", "(")
                .replace("[/\d", ")");
            var display_name = name;
            if (display_name.length > 40) {
                display_name = display_name.substr(0, 39)+'...';
            }
            display_name = $('<div>').text(display_name).html();
            name = $('<div>').text(name).html();
            return '<span title="'+name+'">'+display_name+'</span>';
        }
    };

    chart.chartSettings.tooltip.useHTML = true;
    chart.chartSettings.tooltip.formatter = function() {
        var val = chart.itemValue(this.y, this.point.k);
        var name = this.series.name;
        name = $('<div>').text(name).html()
            .replace("[\n]", '<br>')
            .replace("[\d]", '<span style="font-size: 9px; color: #888888;">')
            .replace("[/\d]", '</span>');
        var str = '<span style="color: '+this.series.color+'">'+chart.chartSettings.tooltip.title+': </span> <span style="font-weight: bold">'+$('<div>').text(val).html()+'</span>';
        str += '<div style="width: 300px; white-space: normal; color: #666666; font-size: 10px;">'+this.x+', '+name+'</div>';
        return str;
    };

    chart.ids = [];

    chart.items = {};

    chart.itemValueFormatter = settings.itemValueFormatter || {};

    chart.itemValue = function(val, index) {
        if (chart.itemValueFormatter.decimals) val = parseFloat(val).toFixed(chart.itemValueFormatter.decimals);
        return addUnit(val, chart.itemUnit(index));
    };

    chart.itemUnit = function(index) {
        if (!chart.itemValueFormatter.unit) return undefined;
        if (typeof chart.itemValueFormatter.unit == 'string') return chart.itemValueFormatter.unit;
        return chart.items[index] ? chart.items[index].unit : undefined;
    };


    chart.chartSettings.series = [];

    chart.showItems = function(items) {
        chart.items = items;

        var colors = ['#627ed9','#3fa3d2','#3fc2d2','#3fd2b8','#3fd270','#a7d23f','#cdd23f','#d2bf3f','#d2733f','#d2733f','#d2733f','#d2733f','#d2733f','#d2733f','#d2733f','#d2733f','#d2733f','#d2733f','#d2733f','#d2733f','#d2733f','#d2733f','#d2733f','#d2733f'];
        this.chartSettings.series = [];
        for (var i in this.items) {
            if (this.items[i].title) {
                var color = colors[i];
                this.chartSettings.series[this.chartSettings.series.length] = {
                    color: color,
                    lineColor: color,
                    marker: {fillColor: '#ffffff', lineColor: color, lineWidth: 1, radius: 0, symbol: 'circle'},
                    name: this.items[i].title,
                    data: [],
                    visible: true,
                    key: this.items[i].id
                };
            }
        }

        this.ids = [];
        for (var i in items) {
            if (items[i].title) {
                this.ids.push(items[i].id);
            }
        }

        if (Object.keys(items).length) {
            this.chartContainer.find('.graph').css('height', '320px');
            this.chartContainer.find('.placeholder').remove();
        } else {
            this.chartContainer.find('.graph').css('height', '100px');
            this.chartContainer.find('.graphWrapper').append('<div class="placeholder">'+t('Analytics__Select_items_to_display_on_the_graph')+'</div>');
        }
    };

    chart.chartContainer.find('.graph').css('height', '100px');
    chart.chartContainer.find('.graphWrapper').append('<div class="placeholder">'+t('Analytics__Select_items_to_display_on_the_graph')+'</div>');


    if (!inherit) {
        chart.init();
    }

    return chart;
}