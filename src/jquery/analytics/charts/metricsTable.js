import AnalyticsTable from './analyticsTable'
import { addUnit } from '@/jquery/utils'

export default function (settings, inherit) {

    var table = new AnalyticsTable(settings, true);

    table.method = 'metrics_list';

    table.columns = [
        {
            checkbox: true
        },{
            type: 'string',
            value: 'title',
            sort: true,
            search: true,
            name: t('Analytics__Metric')
        },{
            type: 'number',
            value: function(item) {
                return addUnit(parseFloat(item.amount).toFixed(2), item.unit);
            },
            sort_value: 'amount',
            sort: true,
            width: '140px',
            name: t('Analytics__Metrics_total')
        },{
            type: 'number',
            value: 'users',
            width: '140px',
            name: t('Analytics__Metrics_users')
        },{
            type: 'number',
            value: function(item) {
                return addUnit(parseFloat(item.average).toFixed(2), item.unit);
            },
            sort_value: 'average',
            width: '140px',
            name: t('Analytics__Metrics_average')
        },{
            type: 'number',
            value: function(item) {
                return parseFloat(item.participation).toFixed(2) + '%';
            },
            width: '140px',
            name: t('Analytics__Metrics_participation')
        }
    ];

    table.on_select_items = settings.on_select_items;
    table.row_selection_items = {};
    table.row_selection = true;
    table.row_selection_on_select_rows = function(ids) {
        for (var i in ids) {
            if (table.row_selection_items[ids[i]] === undefined) {
                var item = table.getRowById(ids[i]);
                if (item) {
                    table.row_selection_initiated = true;
                    table.row_selection_items[ids[i]] = {
                        id: item.id,
                        title: item.title,
                        unit: item.unit
                    };
                }
            }
        }
        if (typeof table.on_select_items == 'function') {
            table.on_select_items(table.row_selection_items);
        }
    };
    table.row_selection_on_deselect_rows = function(ids) {
        if (ids === null) {
            table.row_selection_items = {};
        } else {
            for (var i in ids) {
                delete table.row_selection_items[ids[i]];
            }
        }
        if (typeof table.on_select_items == 'function') {
            table.on_select_items(table.row_selection_items);
        }
    };
    table.first_build_table = true;
    table.on_build_table = function() {
        if (table.first_build_table) {
            if (table.data.length <= 10) {
                table.selectAllVisibleRows();
            }
        }
        table.first_build_table = false;
    };

    table.row_sort = true;
    table.row_sort_default_column = 2;
    table.row_sort_default_order = 'desc';
    table.row_sort_type_backend = true;

    table.row_search = true;
    table.row_search_save = true;
    table.row_search_type_backend = true;

    table.pagination = true;
    table.pagination_show_more = true;

    if (!inherit) {
        table.init();
    }

    return table;
}