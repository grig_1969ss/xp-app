import AnalyticsTable from './analyticsTable'

export default function (settings) {

    var table = new AnalyticsTable(settings, true);

    table.method = 'campaign_variant_domains_list';

    table.columns = [
        {
            type: 'string',
            value: 'id',
            sort: true,
            name: t('Analytics__Domains_table_domain'),
            search: true
        }, {
            type: 'number',
            value: 'email_sent',
            sort: true,
            name: t('Analytics__Domains_table_email_sent'),
            width: '70px',
            class: 'smallHeader centerHeader'
        }, {
            type: 'number',
            value: 'email_failed',
            sort: true,
            name: t('Analytics__Domains_table_email_failed'),
            width: '70px',
            class: 'smallHeader centerHeader'
        }, {
            type: 'percent',
            value: 'email_fail_rate',
            sort: true,
            name: t('Analytics__Domains_table_email_fail_rate'),
            width: '70px',
            class: 'smallHeader centerHeader'
        }, {
            type: 'number',
            value: 'email_delivered',
            sort: true,
            name: t('Analytics__Domains_table_email_delivered'),
            width: '70px',
            class: 'smallHeader centerHeader'
        }, {
            type: 'percent',
            value: 'email_delivery_rate',
            sort: true,
            name: t('Analytics__Domains_table_email_delivery_rate'),
            width: '70px',
            class: 'smallHeader centerHeader'
        }, {
            type: 'number',
            value: 'email_opened',
            sort: true,
            name: t('Analytics__Domains_table_email_opened'),
            width: '70px',
            class: 'smallHeader centerHeader'
        }, {
            type: 'percent',
            value: 'email_open_rate',
            sort: true,
            name: t('Analytics__Domains_table_email_open_rate'),
            width: '70px',
            class: 'smallHeader centerHeader'
        }, {
            type: 'number',
            value: 'email_clicked',
            sort: true,
            name: t('Analytics__Domains_table_email_clicked'),
            width: '70px',
            class: 'smallHeader centerHeader'
        }, {
            type: 'percent',
            value: 'email_click_rate',
            sort: true,
            name: t('Analytics__Domains_table_email_click_rate'),
            width: '70px',
            class: 'smallHeader centerHeader'
        }, {
            type: 'number',
            value: 'email_unsubscribed',
            sort: true,
            name: t('Analytics__Domains_table_email_unsubscribed'),
            width: '70px',
            class: 'smallHeader centerHeader'
        }, {
            type: 'percent',
            value: 'email_unsubscribe_rate',
            sort: true,
            name: t('Analytics__Domains_table_email_unsubscribe_rate'),
            width: '70px',
            class: 'smallHeader centerHeader'
        }, {
            type: 'number',
            value: 'email_complained',
            sort: true,
            name: t('Analytics__Domains_table_email_complained'),
            width: '70px',
            class: 'smallHeader centerHeader'
        }, {
            type: 'percent',
            value: 'email_complaint_rate',
            sort: true,
            name: t('Analytics__Domains_table_email_complaint_rate'),
            width: '70px',
            class: 'smallHeader centerHeader'
        }
    ];

    table.row_sort = true;
    table.row_sort_default_column = 1;
    table.row_sort_default_order = 'desc';

    table.row_search = true;
    table.row_search_save = true;

    table.init();

    return table;
};