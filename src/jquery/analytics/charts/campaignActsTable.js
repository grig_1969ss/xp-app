import AnalyticsTable from './analyticsTable'

export default function (settings) {

    var table = new AnalyticsTable(settings, true);

    table.method = 'campaign_variant_acts_list';

    table.columns = [
        {
            type: 'string',
            value: function(item) {
                return item.id.charAt(0).toUpperCase() + item.id.slice(1);
            },
            sort: true,
            name: t('Analytics__Acts_table_act')
        },{
            type: 'number',
            value: 'amount_total',
            sort: true,
            name: t('Analytics__Acts_table_amount_total'),
            width: '200px'
        },{
            type: 'number',
            value: 'amount_unique',
            sort: true,
            name: t('Analytics__Acts_table_amount_unique'),
            width: '200px'
        }
    ];

    table.row_sort = true;
    table.row_sort_default_column = 1;
    table.row_sort_default_order = 'desc';

    table.init();

    return table;
};