import AnalyticsTable from './analyticsTable'

export default function (settings, attribute, platform) {

    var table = new AnalyticsTable(settings, true);

    table.method = attribute;
    table.platform = platform;

    if (jQuery.inArray(table.method, ['app_version_table', 'language_table', 'carrier_name_table', 'browser_table']) != -1) {
        table.columns = [
            {
                checkbox: true
            },{
                type: 'string',
                value: 'title',
                search: true,
                sort: true,
                sort_value: 'value',
                name: t('Analytics__Attribute_'+table.method)
            },{
                type: 'number',
                value: 'total',
                sort: true,
                name: t('Analytics__Total_users'),
                width: '20%'
            },{
                type: 'number',
                value: 'active',
                sort: true,
                name: t('Analytics__Addressable_users'),
                width: '20%'
            },{
                type: 'number',
                value: 'total_percent',
                sort: true,
                sort_value: 'total',
                name: t('Analytics__Percent'),
                width: '20%'
            }
        ];
    } else {
        table.columns = [
            {
                checkbox: true
            }, {
                type: 'string',
                value: 'title',
                sort: true,
                sort_value: 'value',
                search: true,
                name: t('Analytics__Attribute_' + table.method),
                width: '50%'
            }, {
                type: 'number',
                value: 'total',
                sort: true,
                name: t('Analytics__Number_of_users'),
                width: '50%'
            }
        ];
    }

    table.on_select_items = settings.on_select_items;
    table.row_selection_items = {};
    table.row_selection = true;
    table.row_selection_on_select_rows = function(ids) {
        for (var i in ids) {
            if (table.row_selection_items[ids[i]] === undefined) {
                var item = table.getRowById(ids[i]);
                if (item) {
                    table.row_selection_initiated = true;
                    table.row_selection_items[ids[i]] = {
                        title: item.title
                    };
                }
            }
        }
        if (typeof table.on_select_items === 'function') {
            table.on_select_items(table.row_selection_items);
        }
    };
    table.row_selection_on_deselect_rows = function(ids) {
        if (ids === null) {
            table.row_selection_items = {};
        } else {
            for (var i in ids) {
                delete table.row_selection_items[ids[i]];
            }
        }
        if (typeof table.on_select_items === 'function') {
            table.on_select_items(table.row_selection_items);
        }
    };

    table.row_sort = true;
    table.row_sort_default_column = 2;
    table.row_sort_default_order = 'desc';
    table.row_sort_type_backend = true;

    table.row_search = true;
    table.row_search_save = true;
    table.row_search_type_backend = true;

    table.pagination = true;
    table.pagination_show_more = true;

    table.reloadTableChart = function(chart, items, chartSettings) {
        chart.ids = [];
        for (var i in items) {
            if (items[i].title) {
                chart.ids.push(encodeURIComponent(i));
            }
        }
        chart.reloadByParam(chartSettings);
    };

    table.init();

    return table;
};