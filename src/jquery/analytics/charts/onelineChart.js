import AnalyticsChart from './analyticsChart'

export default function (settings) {

    var chart = new AnalyticsChart(settings, true);

    chart.initMode = function () {
        if (!settings.mode) {
            settings.mode = 'line';
        }
        chart.chartSettings.chart.type = settings.mode;
        for (var i in chart.chartSettings.series) {
            chart.chartSettings.series[i].type = settings.mode;
        }
    };

    chart.initModeButtons = function() {
        chart.chartContainer.find('.options').append(
            '<button class="graphType graphTypeFirst '+(settings.mode == 'line' ? 'active' : '')+' graphMode graphLine icon-chart-line" data-mode="line"></button>' +
            '<button class="graphType graphTypeLast '+(settings.mode == 'column' ? 'active' : '')+' graphMode graphBar icon-chart-bar" data-mode="column"></button>'
        );
        chart.chartContainer.find('.graphMode').click(function() {
            chart.chartContainer.find('.graphMode').removeClass('active');
            $(this).addClass('active');
            chart.setMode($(this).data('mode'));
            chart.reload();
        });
    };

    chart.setMode = function(mode) {
        chart.mode = mode;
        chart.chartSettings.chart.type = chart.mode;
        for (var i in chart.chartSettings.series) {
            chart.chartSettings.series[i].type = chart.mode;
        }
        $(chart.chart.series).each(function(){
            this.update({
                type: chart.mode
            }, false);
        });
        chart.chart.redraw();
    };

    chart.initMode();
    chart.initModeButtons();
    chart.init();

    return chart;
};
