import AnalyticsChart from './analyticsChart'

import 'vendor/jvectormap/jquery-jvectormap-2.0.0.css'
import 'vendor/jvectormap/jquery-jvectormap-2.0.0.min.js'
import 'vendor/jvectormap/jquery-jvectormap-world-mill-en.js'

export default function (settings) {

    var that = this;

    var chart = new AnalyticsChart(settings, true);

    chart.chartType = window.jvm.Map;

    chart.method = 'country';
    chart.addTotals = 1;

    chart.chartSettings.container = settings.element.find('.graph');
    chart.chartSettings.map = 'world_mill_en';
    chart.chartSettings.zoomOnScroll = false;
    chart.chartSettings.backgroundColor = '#ffffff';
    chart.chartSettings.color = 'red';
    chart.chartSettings.regionStyle = {
        initial: {
            fill: '#c5def9'
        }
    };

    chart.chartSettings.series = {
        regions: [{
            values: {},
            attribute: 'fill',
            scale: ['#7aa3d0', '#103e73'],
            normalizeFunction: 'polynomial',
            legend: {
                vertical: true,
                labelRender: function(v) {
                    return v.toFixed(1).replace('.0', '');
                }
            }
        }]
    };

    chart.chartSettings.onRegionTipShow = function(e, el, code) {
        var val = chart.chartSettings.series.regions[0].values[code];
        if (!val) val = 0;
        el.html(el.html() + ': <b>'+val+'</b>');
    };

    chart.tableSettings.columns = [
        {
            type: 'string',
            value: 'title',
            search: true,
            sort: true,
            name: t('Analytics__Attribute_country')
        },{
            type: 'number',
            value: 'total',
            sort: true,
            sort_value: 'total',
            name: t('Analytics__Total_users'),
            width: '20%'
        },{
            type: 'number',
            value: 'active',
            sort: true,
            sort_value: 'active',
            name: t('Analytics__Addressable_users'),
            width: '20%'
        },{
            type: 'number',
            value: function(item) {
                if (!chart.data.totals.total) {
                    return '-';
                } else {
                    var percent = (item.total / chart.data.totals.total) * 100;
                    return percent.toFixed(1)+'%';
                }
            },
            sort: true,
            sort_value: function(item) {
                if (!chart.data.totals.total) {
                    return 0;
                } else {
                    return item.total / chart.data.totals.total;
                }
            },
            name: t('Analytics__Percent'),
            width: '20%'
        }
    ];

    if (settings.show_addressable === false) {
        chart.tableSettings.columns[1].width = '30%';
        chart.tableSettings.columns[3].width = '30%';
        delete chart.tableSettings.columns[2];
    }

    chart.tableSettings.row_sort_default_column = 1;
    chart.tableSettings.row_sort_default_order = 'desc';

    chart.tableSettings.row_search = true;
    chart.tableSettings.row_search_save = true;

    chart.processData = function() {
        this.chartSettings.series.regions[0].values = {};

        $.each(this.data.data, $.proxy(function(index, value) {
            this.chartSettings.series.regions[0].values[index] = value.total;
        }, this));
    };

    chart.updateTitle(t("Analytics__Users_by_country"));

    chart.init();

    return chart;
}
