import AnalyticsChart from './analyticsChart'
import { addUnit } from '@/jquery/utils'

export default function (settings, inherit) {

    var chart = new AnalyticsChart(settings, true);

    chart.chartSettings.chart.inverted = true;

    chart.chartSettings.chart.marginLeft = 200;
    chart.chartSettings.chart.marginRight = 100;
    chart.chartSettings.xAxis.labels = jQuery.extend({}, chart.chartSettings.yAxis.labels);
    chart.chartSettings.xAxis.labels.align = 'right';
    chart.chartSettings.xAxis.labels.formatter = function() {
        var str;
        if (chart.items[this.value]) {
            if (chart.items[this.value].title.length > 120) {
                str = chart.items[this.value].title.substr(0, 120) + ' ...';
            } else {
                str = chart.items[this.value].title;
            }
        } else {
            str = this.value;
        }
        str = $('<div>').text(str).html()
            .replace(' ', '&nbsp;')
            .replace("[\n]", '<br>')
            .replace("[\d]", '<span style="font-size: 9px; color: #888888;">')
            .replace("[/\d]", '</span>');
        return '<div style="width: 180px; overflow: hidden; text-align: right; white-space: normal; word-wrap: break-word; line-height: 14px; max-height: 42px;">'+str+'</div>';
    };
    chart.chartSettings.xAxis.labels.useHTML = true;

    chart.chartSettings.yAxis.labels = {enabled: false};
    chart.chartSettings.yAxis.tickPixelInterval = 60;
    chart.chartSettings.yAxis.gridLineWidth = 0;
    chart.chartSettings.yAxis.minorGridLineWidth = 0;

    chart.chartSettings.tooltip.useHTML = true;
    chart.chartSettings.tooltip.formatter = function() {
        var val = chart.itemValue(this.y, this.x);
        var name = chart.items[this.x] ? chart.items[this.x].title : this.x;
        name = $('<div>').text(name).html()
            .replace("[\n]", '<br>')
            .replace("[\d]", '<span style="font-size: 9px; color: #888888;">')
            .replace("[/\d]", '</span>');
        var str = '<span style="color: '+this.series.color+'">'+chart.chartSettings.tooltip.title+': </span> <span style="font-weight: bold">'+$('<div>').text(val).html()+'</span>';
        str += '<div style="width: 300px; white-space: normal; color: #666666; font-size: 10px;">'+name+'</div>';
        return str;
    };
    chart.chartSettings.tooltip.followPointer = true;

    chart.chartSettings.plotOptions = {
        column: {
            dataLabels: {
                enabled: true,
                crop: false,
                overflow: 'none',
                inside: false,
                color: '#32404d',
                formatter: function() {
                    return chart.itemValue(this.y, this.x);
                }
            },
            pointWidth: 30,
            states: {
                hover: {
                    brightness: 0.05
                }
            }
        }
    };

    chart.ids = [];

    chart.items = {};

    chart.itemValueFormatter = settings.itemValueFormatter || {};

    chart.itemValue = function(val, index) {
        if (chart.itemValueFormatter.decimals) val = parseFloat(val).toFixed(chart.itemValueFormatter.decimals);
        return addUnit(val, chart.itemUnit(index));
    };

    chart.itemUnit = function(index) {
        if (!chart.itemValueFormatter.unit) return undefined;
        if (typeof chart.itemValueFormatter.unit == 'string') return chart.itemValueFormatter.unit;
        return chart.items[index] ? chart.items[index].unit : undefined;
    };

    chart.showItems = function(items) {
        chart.items = items;

        this.chartSettings.series = [
            {
                color: '#1d87c8',
                data: [],
                visible: true,
                type: 'column'
            }
        ];

        this.ids = [];
        for (var i in items) {
            if (items[i].title) {
                this.ids.push(items[i].id);
            }
        }

        if (Object.keys(items).length) {
            this.chartContainer.find('.graph').css('height', (Object.keys(items).length * 55 + 100)+'px');
            this.chartContainer.find('.placeholder').remove();
        } else {
            this.chartContainer.find('.graph').css('height', '100px');
            this.chartContainer.find('.graphWrapper').append('<div class="placeholder">'+t('Analytics__Select_items_to_display_on_the_graph')+'</div>');
        }
    };

    chart.chartContainer.find('.graph').css('height', '100px');
    chart.chartContainer.find('.graphWrapper').append('<div class="placeholder">'+t('Analytics__Select_items_to_display_on_the_graph')+'</div>');


    if (!inherit) {
        chart.init();
    }

    return chart;
}
