import CampaignsTable from './campaignsTable'
import { escapeHTML } from '@/jquery/utils'

export default function (settings) {

    var table = new CampaignsTable(settings, true);

    table.method = 'campaign_variants_list';

    table.columns[1] = {
        type: 'string',
        value: function(item) {
            return escapeHTML(item.key + (item.title ? ' - ' + item.title : ''));
        },
        min_width: '200px',
        name: t('Analytics__Campaigns_variants_table_variant')
    };

    let abButton = table.columns_groups.length - 1;
    for (var i in table.columns_groups[abButton].columns) delete table.columns[table.columns_groups[abButton].columns[i]];
    delete table.columns_groups[abButton];

    table.on_select_items = function(items) {
        for (var i in items) {
            if (!items[i].transformed) {
                items[i].title = items[i].id + (items[i].title ? ' - ' + items[i].title : '');
                items[i].transformed = true;
            }
        }
        if (typeof settings.on_select_items == 'function') {
            settings.on_select_items(items);
        }
    };

    table.row_sort = false;
    table.row_search = false;
    table.pagination = false;

    table.init();

    return table;
}
