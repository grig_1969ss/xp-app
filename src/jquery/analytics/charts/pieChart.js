import AnalyticsChart from './analyticsChart'
import { addCommas } from '@/jquery/utils'

export default function (settings) {

    var that = this;

    var chart = new AnalyticsChart(settings, true);

    chart.chartSettings.chart.type = 'pie';

    chart.chartSettings.tooltip.formatter = function() {
        var val = addCommas(this.y);
        return '<span style="color: '+this.series.color+'">'+$('<div>').text(this.point.n).html()+':</span> <span style="font-weight: bold">'+$('<div>').text(val).html()+'</span>';
    };

    Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')]
            ]
        };
    });

    chart.chartSettings.plotOptions = {
        pie: {
            minPercentage: 0,
            maxDataLabelLength: null,
            colors: ['#1d87c8','#3fa3d2','#3fc2d2','#3fd2b8','#3fd270','#a7d23f','#cdd23f','#d2bf3f','#f4ae00','#ff8b00','#ff6e2e','#ff5331','#ff3333','#ea1b5b','#ff3198','#ff57d0','#ff99f3','#f386fc','#d67dff','#b269ff','#8a55ff','#4848ed','#4b6be0','#4b80dd'],
            dataLabels: {
                enabled: true,
                color: '#32404d',
                formatter: function() {
                    if (this.percentage < chart.chartSettings.plotOptions.pie.minPercentage) return null;
                    var name = this.point.n;
                    if (chart.chartSettings.plotOptions.pie.maxDataLabelLength) {
                        name = name.substr(0, chart.chartSettings.plotOptions.pie.maxDataLabelLength)+'...';
                    }
                    name = $('<div>').text(name).html();
                    return '<b>'+ name +'</b>: '+ (Math.round(this.percentage * 100) / 100) +'%';
                }
            },
            borderWidth: 0,
            startAngle: 270,
            endAngle: 630,
            minSize: 180,
            center: ['50%', '50%'],
            allowPointSelect: true
        }
    };

    chart.chartSettings.series = [{
        type: 'pie',
        innerSize: '40%',
        data: [],
        turboThreshold: 2000
    }];

    return chart;
}