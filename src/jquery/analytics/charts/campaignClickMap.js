import { ajaxLoaderShow, ajaxLoaderHide } from '@/jquery/widgets/ajaxLoader'
import { twig } from 'vendor/twig/twig'

export default function (campaignId, settings) {
    var that = this;

    this.container = settings.container;
    this.rendered = false;

    this.init = function () {

    };

    this.render = function (variant) {
        ajaxLoaderShow(function () {
            $.get(API_URL + '/push/campaign/preview-email-variants?project_id=' + $('#analyticsContent').attr('data-project-id') + '&campaign_id=' + campaignId, function (data) {
                $.each(data, function (i, item) {
                    var iframeContainer = that.container.find('#campaignVariantPreview' + i);
                    var iframe = iframeContainer.find('iframe');
                    var iframeDoc = iframe[0].contentDocument || iframe[0].contentWindow.document;

                    var resizeIframe = function () {
                        that.container.show();
                        $(iframe).innerHeight($(iframeDoc).find('body').prop('scrollHeight'));
                    };

                    iframe[0].addEventListener("load", resizeIframe);
                    iframeDoc.write(item);
                    iframeDoc.close();
                    resizeIframe();

                    var iframeHtml = iframe.contents().find('html');
                    var styles = '.campaignClickMapCircle { border:2px solid #FFFFFF; border-radius:7px; display:inline-block; background-color:#c2c4c6; width:12px; height:12px; pointer-events:none; position:absolute; margin-left:5px; pointer-events:all; }';
                    var circle = '<span class="campaignClickMapCircle">&nbsp;</span>';
                    iframeHtml.prepend('<style>' + styles + '</style>');
                    iframeHtml.find('a').prepend(circle);

                    var twClickMapTooltip = twig({
                        id: "campaignClickMapTooltipContainer" + i,
                        href: VIEWS_DIR + "/analytics/views/analytics/templates/email-preview-tooltip.twig",
                        async: false
                    });

                    iframe.contents().find('a > span.campaignClickMapCircle').hover(function () {
                        var iframeCoords = $(this).offset();
                        var percent = iframeContainer.find('.campaignVariantTooltipContainer').data('total_clicked') ? 100 * $(this).data('amount_total') / iframeContainer.find('.campaignVariantTooltipContainer').data('total_clicked') : 0;
                        var template = twClickMapTooltip.render({
                            data: {
                                amount_total: $(this).data('amount_total'),
                                amount_unique: $(this).data('amount_unique'),
                                click_percent: percent.toFixed(1)
                            }
                        });

                        iframeContainer.find('.campaignVariantTooltipContainer').html(template);
                        iframeContainer.find('.campaignClickMapTooltipContainer .campaignClickMapTooltip').css({
                            'top': iframeCoords.top - 75,
                            'left': iframeCoords.left + 8,
                            'visibility': 'visible'
                        });
                    }, function () {
                        iframeContainer.find('.campaignVariantTooltipContainer').html('');
                    });
                    iframe.contents().find('a').click(false);
                });
                that.container.addClass('isInit');
                ajaxLoaderHide();

                that.reloadByParam(window.analyticPage.chartSettings, window.analyticPage.chartOptions);
            }, 'json');
        });
    };

    this.reloadByParam = function (settings, options) {
        if (!this.rendered) {
            this.rendered = true;
            return this.render(options.variant_key, function () {
                that.reloadByParam(settings, options)
            });
        }

        var requestLink = API_URL + '/analytics/analytics/call';
        var requestData = {
            method: 'campaign_variant_links_url'
        };

        $.each([settings, options], function (i, requestArray) {
            if (!(requestArray === undefined)) {
                $.each(requestArray, function (index, value) {
                    if (value) {
                        requestData[index] = value;
                    }
                });
            }
        });

        var first = true;
        for (var i in requestData) {
            if (first) requestLink += '?'; else requestLink += '&';
            first = false;
            requestLink += i + '=' + requestData[i];
        }

        var iframes = that.container.find('.campaignVariantPreviewContainer iframe');
        $.each(iframes, function (i, iframe) {
            var iframeHtml = $(iframe).contents().find('html');
            iframeHtml.find('a > span.campaignClickMapCircle')
                .data('amount_total', '0')
                .data('amount_unique', '0')
                .css({'background-color': '#c2c4c6'});
        });

        $.get(requestLink, function (response) {
            if (response && response.data) {
                var clickedTotal = {};
                $.each(response.data, function (i, item) {
                    if (clickedTotal[item.message_variant] === undefined) {
                        clickedTotal[item.message_variant] = parseInt(item.amount_total);
                    } else {
                        clickedTotal[item.message_variant] += parseInt(item.amount_total);
                    }
                    var iframe = that.container.find('#campaignVariantPreview' + item.message_variant + ' iframe');
                    var iframeHtml = iframe.contents().find('html');
                    iframeHtml.find('a[href="' + item.link_url + '"] > span.campaignClickMapCircle')
                        .data('amount_total', item.amount_total)
                        .data('amount_unique', item.amount_unique);
                });
                $.each(clickedTotal, function (i, item) {
                    that.container.find('#campaignVariantPreview' + i + ' .campaignVariantTooltipContainer').data('total_clicked', item);
                });
                $.each(response.data, function (i, item) {
                    var iframe = that.container.find('#campaignVariantPreview' + item.message_variant + ' iframe');
                    var iframeHtml = iframe.contents().find('html');
                    var tooltip = that.container.find('#campaignVariantPreview' + item.message_variant + ' .campaignVariantTooltipContainer');
                    var circle = iframeHtml.find('a[href="' + item.link_url + '"] > span.campaignClickMapCircle');
                    var percent = circle.data('amount_total') / tooltip.data('total_clicked');
                    circle.css({'background-color': 'rgb(255, ' + Math.round(200 * (1 - percent)) + ', 0)'});
                });
            }
        }, 'json');
    };

    return this;
};