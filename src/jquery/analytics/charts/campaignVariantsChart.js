import CampaignsChart from './campaignsChart'

export default function (settings) {

    var chart = new CampaignsChart(settings, true);

    chart.method_prefix = 'campaign_variants_';

    chart.init();

    return chart;
}
