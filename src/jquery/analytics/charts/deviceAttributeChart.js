import AttributeChart from './attributeChart'

export default function (settings, attribute, platform) {

    settings.method = attribute;
    settings.platform = platform;

    var chart = new AttributeChart(settings, true);

    chart.chartSettings.series[0].innerSize = '30%';

    chart.tableSettings.columns = [
        {
            checkbox: true
        },{
            type: 'string',
            value: 'title',
            search: true,
            sort: true,
            name: t('Analytics__Attribute_' + chart.method),
            width: '50%'
        },{
            type: 'number',
            value: 'total',
            sort: true,
            name: t('Analytics__Number_of_users'),
            width: '50%'
        }
    ];

    if (chart.method == 'device_os' || chart.method == 'device_os_chart') {
        chart.tableSettings.row_sort_default_column = 1;
        chart.tableSettings.row_sort_default_order = 'desc';
        chart.updateTitle(t("Analytics__Platform_"+platform)+ " " + t("Analytics__Versions"));
    }
    else if (chart.method == 'device_model' || chart.method == 'device_model_chart') {
        chart.tableSettings.row_sort_default_column = 2;
        chart.tableSettings.row_sort_default_order = 'desc';
        chart.updateTitle(t("Analytics__Platform_"+platform) + " " + t("Analytics__Device_models"));
    }

    chart.init();

    return chart;
}