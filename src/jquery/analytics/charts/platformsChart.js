import AnalyticsChart from './analyticsChart'

export default function (settings) {

    var chart = new AnalyticsChart(settings, true);

    chart.method = 'type';

    chart.chartSettings.xAxis.labels.rotation = 0;
    chart.chartSettings.xAxis.labels.y = 18;

    chart.chartSettings.tooltip.index_title = t("Analytics__Platform");

    chart.chartSettings.colorsByIndex = {
        'ios': '#1d87c8',
        'android': '#1d87c8'
    };

    chart.chartSettings.series = [
        {
            color: '#7487c2',
            name: t("Analytics__Number_of_users"),
            data: [],
            visible: true,
            key: 'total',
            type: 'column'
        }
    ];

    chart.process_point = function(point) {
        if (point.v.title) {
            point.n = point.v.title;
        } else {
            point.n = point.i;
        }
        return point;
    };

    chart.updateTitle(t("Analytics__Users_by_type"));

    chart.init();

    return chart;
}