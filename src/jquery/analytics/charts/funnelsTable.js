import AnalyticsTable from './analyticsTable'

export default function (settings) {

    var table = new AnalyticsTable(settings, true);

    table.method = 'funnels_list';

    table.columns = [
        {
            type: 'string',
            value: 'title',
            sort: true,
            search: true,
            name: t('AnalyticsAnalyticsController__Funnel_title')
        }
    ];

    table.columns_headers = false;

    table.row_click = function(item) {
        location = '/analytics/analytics/funnel?project_id='+table.project_id+'&id='+item.id;
    };

    table.row_sort = true;
    table.row_sort_default_column = 0;
    table.row_sort_default_order = 'asc';
    table.row_sort_type_backend = true;

    table.row_search = true;
    table.row_search_save = true;
    table.row_search_type_backend = true;

    table.pagination = true;
    table.pagination_show_more = true;

    table.init();

    return table;
}