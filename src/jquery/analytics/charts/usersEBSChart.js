import UsersEBChart from './usersEBChart'

export default function (settings, inherit) {

    var chart = new UsersEBChart(settings, true);

    chart.method = 'users_ebs';

    chart.tableSettings.columns[0].name = t('Analytics__Number_of_sessions');

    chart.chartSettings.tooltip.index_title = t("Analytics__Number_of_sessions");

    chart.updateTitle(t("Analytics__Users_engagement_by_sessions"));

    if (!inherit) {
        chart.init();
    }

    return chart;
}