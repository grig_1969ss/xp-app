import AnalyticsTable from './analyticsTable'

export default function (settings) {

    var table = new AnalyticsTable(settings, true);

    table.method = 'campaign_variant_links_list';

    table.columns = [
        {
            type: 'string',
            value: 'url',
            sort: true,
            name: t('Analytics__Link')
        },{
            type: 'number',
            value: 'amount_total',
            sort: true,
            name: t('Analytics__Clicks_total'),
            width: '200px'
        },{
            type: 'number',
            value: 'amount_unique',
            sort: true,
            name: t('Analytics__Clicks_unique'),
            width: '200px'
        }
    ];

    table.row_sort = true;
    table.row_sort_default_column = 1;
    table.row_sort_default_order = 'desc';

    table.init();

    return table;
};