import PieChart from './pieChart'

export default function (settings) {

    var chart = new PieChart(settings, true);

    chart.method = 'opt_ins_outs_current';
    chart.addTotals = 1;

    chart.chartSettings.colorsByIndex = {
        'ios_addressable': '#31854d',
        'ios_non_addressable': '#A2050D',
        'android_addressable': '#31854d',
        'android_non_addressable': '#A2050D'
    };

    chart.tableSettings.row_sort_default_column = undefined;
    chart.tableSettings.row_sort_default_order = undefined;

    chart.tableSettings.columns = [
        {
            type: 'string',
            value: 'platform',
            sort: true,
            name: t('Analytics__Platform'),
            width: '25%'
        },{
            type: 'string',
            value: function(item) {
                if (item.addressable) {
                    return 'Yes';
                } else {
                    return 'No';
                }
            },
            sort: true,
            name: t('Analytics__Addressable'),
            width: '25%'
        },{
            type: 'number',
            value: 'amount',
            sort: true,
            name: t('Analytics__Number_of_users'),
            width: '25%'
        }, {
            type: 'number',
            value: function(item) {
                if (!chart.data.totals.amount) {
                    return '-';
                } else {
                    var percent = (item.amount / chart.data.totals.amount) * 100;
                    return percent.toFixed(1)+'%';
                }
            },
            sort: true,
            sort_value: function(item) {
                if (!chart.data.totals.amount) {
                    return 0;
                } else {
                    return item.amount / chart.data.totals.amount;
                }
            },
            name: t('Analytics__Percent'),
            width: '25%'
        }
    ];

    chart.updateTitle(t("Analytics__Push_acceptance")+' ('+t("Analytics__Current_status")+')');

    chart.init();

    return chart;
}