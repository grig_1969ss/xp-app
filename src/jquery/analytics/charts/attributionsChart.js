import ItemsTimelineChart from './itemsTimelineChart'
import ItemsTotalsChart from './itemsTotalsChart'

export default function (settings) {

    var chart = this;

    chart.chartContainer = settings.element;
    chart.modifier = settings.modifier;
    chart.mode = null;
    chart.modeChart = null;

    delete settings.element;

    switch (chart.modifier) {
        case 'ads':
            chart.method = 'attributions_ads';
            break;
        case 'ads_groups':
            chart.method = 'attributions_ads_groups';
            break;
        case 'ads_campaigns':
            chart.method = 'attributions_ads_campaigns';
            break;
        case 'networks':
            chart.method = 'attributions_networks';
            break;
        case 'tags':
            chart.method = 'attributions_ads_tags';
            break;
    }

    chart.chartTimeline = new ItemsTimelineChart(jQuery.extend({
        method: chart.method,
        element: chart.chartContainer.find('.timelineChart')
    }, settings), true);

    chart.chartTotals = new ItemsTotalsChart(jQuery.extend({
        method: chart.method + '_total',
        element: chart.chartContainer.find('.totalsChart')
    }, settings), true);

    chart.initModeButtons = function() {
        chart.chartContainer.find('.options').append(
            '<button class="graphType graphTypeFirst graphMode" data-mode="totals" style="width: auto;">' + t("Analytics__Overall") + '</button>' +
            '<button class="graphType graphTypeLast graphMode" data-mode="timeline" style="width: auto;">' + t("Analytics__Timeline") + '</button>'
        );
        chart.chartContainer.find('.graphMode').click(function() {
            chart.setMode($(this).data('mode'));
            chart.reload();
        });
    };

    chart.setMode = function(mode) {
        chart.mode = mode;
        chart.chartContainer.find('.graphMode').removeClass('active');
        chart.chartContainer.find('.graphMode[data-mode="'+mode+'"]').addClass('active');

        if (chart.modeChart) {
            chart.modeChart.chartContainer.hide();
        }
        if (chart.mode == 'timeline') {
            chart.modeChart = chart.chartTimeline;
        } else {
            chart.modeChart = chart.chartTotals;
        }
        chart.modeChart.chartContainer.show();
    };

    chart.reload = function() {
        chart.reloadByParam(chart.settings, chart.options);
    };

    chart.reloadByParam = function(settings, options) {
        if (!(settings === undefined)) {
            chart.settings = settings;
        }
        if (!(options === undefined)) {
            chart.options = options;
        }
        chart.modeChart.reloadByParam(chart.settings, chart.options);
    };

    chart.showItems = function(items) {
        chart.chartTimeline.showItems(items);
        chart.chartTotals.showItems(items);
    };

    chart.initModeButtons();

    chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Installs');
    chart.chartTimeline.updateTitle(t('Analytics__Install_attributions'));
    chart.chartTimeline.init();

    chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Installs');
    chart.chartTotals.updateTitle(t('Analytics__Install_attributions'));
    chart.chartTotals.init();

    if (settings.mode) {
        chart.setMode(settings.mode);
    }

    return chart;
}
