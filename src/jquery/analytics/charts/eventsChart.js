import ItemsTimelineChart from './itemsTimelineChart'
import ItemsTotalsChart from './itemsTotalsChart'

export default function (settings) {

  var chart = this;

  chart.chartContainer = settings.element;
  chart.mode = null;
  chart.modeChart = null;

  delete settings.element;

  chart.chartTimeline = new ItemsTimelineChart(jQuery.extend({
    method: 'events',
    element: chart.chartContainer.find('.timelineChart')
  }, settings), true);

  chart.chartTotals = new ItemsTotalsChart(jQuery.extend({
    method: 'events_totals',
    element: chart.chartContainer.find('.totalsChart')
  }, settings), true);

  chart.initModeButtons = function() {
    chart.chartContainer.find('.options').append(
      '<button class="graphType graphTypeFirst graphMode" data-mode="totals" style="width: auto;">' + t("Analytics__Overall") + '</button>' +
      '<button class="graphType graphTypeLast graphMode" data-mode="timeline" style="width: auto;">' + t("Analytics__Timeline") + '</button>'
    );
    chart.chartContainer.find('.graphMode').click(function() {
      chart.setMode($(this).data('mode'));
      chart.reload();
    });
  };

  chart.setMode = function(mode) {
    chart.mode = mode;
    chart.chartContainer.find('.graphMode').removeClass('active');
    chart.chartContainer.find('.graphMode[data-mode="'+mode+'"]').addClass('active');
    chart.changeChart();
  };

  chart.changeChart = function() {
    if (chart.modeChart) {
      chart.modeChart.chartContainer.hide();
    }
    if (chart.mode == 'timeline') {
      chart.modeChart = chart.chartTimeline;
    } else if (chart.mode == 'totals') {
      chart.modeChart = chart.chartTotals;
    } else {
      chart.modeChart = null;
    }
    if (chart.modeChart) {
      chart.modeChart.chartContainer.show();
    }
  };

  chart.reload = function() {
    chart.reloadByParam(chart.settings, chart.options);
  };

  chart.reloadByParam = function(settings, options) {
    if (!(settings === undefined)) {
      chart.settings = settings;
    }
    if (!(options === undefined)) {
      chart.options = options;
    }
    chart.modeChart.reloadByParam(chart.settings, chart.options);
  };

  chart.showItems = function(items) {
    chart.chartTimeline.showItems(items);
    chart.chartTotals.showItems(items);
  };

  chart.initModeButtons();

  chart.chartTimeline.chartSettings.tooltip.title = t('Analytics__Hits');
  chart.chartTimeline.updateTitle(t('Analytics__Event_hits'));
  chart.chartTimeline.init();

  chart.chartTotals.chartSettings.tooltip.title = t('Analytics__Hits');
  chart.chartTotals.updateTitle(t('Analytics__Event_hits'));
  chart.chartTotals.init();

  if (settings.mode) {
    chart.setMode(settings.mode);
  }

  return chart;
}