import AnalyticsTable from './analyticsTable'

export default function (settings) {

    var table = new AnalyticsTable(settings, true);

    table.on_select_items = settings.on_select_items;

    table.method = 'attributions_ads_tags_list';

    table.columns = [
        {
            checkbox: true
        },{
            type: 'string',
            value: 'title',
            sort: true,
            sort_value: 'title',
            search: true,
            name: t('Analytics__Install_campaign_tag')
        },{
            type: 'number',
            value: 'amount',
            sort: true,
            name: t('Analytics__Installs'),
            width: '200px'
        }
    ];

    table.row_selection_items = {};
    table.row_selection = true;
    table.row_selection_on_select_rows = function(ids) {
        for (var i in ids) {
            if (table.row_selection_items[ids[i]] === undefined) {
                var item = table.getRowById(ids[i]);
                if (item) {
                    table.row_selection_initiated = true;
                    table.row_selection_items[ids[i]] = {
                        id: item.id,
                        title: item.title
                    };
                }
            }
        }
        if (typeof table.on_select_items == 'function') {
            table.on_select_items(table.row_selection_items);
        }
    };
    table.row_selection_on_deselect_rows = function(ids) {
        if (ids === null) {
            table.row_selection_items = {};
        } else {
            for (var i in ids) {
                delete table.row_selection_items[ids[i]];
            }
        }
        if (typeof table.on_select_items == 'function') {
            table.on_select_items(table.row_selection_items);
        }
    };

    table.row_sort = true;
    table.row_sort_default_column = 2;
    table.row_sort_default_order = 'desc';
    table.row_sort_type_backend = true;

    table.row_search = true;
    table.row_search_save = true;
    table.row_search_type_backend = true;

    table.pagination = true;
    table.pagination_show_more = true;

    table.init();

    return table;
}