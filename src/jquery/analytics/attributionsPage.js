import AttributionsChart from './charts/attributionsChart'
import AttributionsNetworksTable from './charts/attributionsNetworksTable'
import SelectorBox from './selectorBox'

export default function () {

    var that = this;
    var $analyticsContainer;

    this.chartSettings = {
        project_id  : null,
        platform    : null,
        startDate   : null,
        endDate     : null
    };

    this.tabContent = null;
    this.tab = null;

    this.chart = null;
    this.table = null;

    this.init = function () {
        $analyticsContainer = $('#analyticsContent');

        this.chartSettings.project_id = $analyticsContainer.attr('data-project-id');

        this.chart = new AttributionsChart({
            element: $analyticsContainer.find('.attributionsNetworksChart'),
            modifier: 'networks',
            mode: 'totals'
        });

        this.table = new AttributionsNetworksTable({
            element: $analyticsContainer.find('.attributionsNetworksTable'),
            on_select_items: function(items) {
                that.chart.showItems(items);
                that.reloadChart();
            }
        });

        SelectorBox().init();

        return this;
    };

    this.reloadChart = function() {
        this.chart.reloadByParam(this.chartSettings);
    };

    this.reloadTable = function() {
        this.table.reloadByParam(this.chartSettings);
    };


    this.redraw = function() {
      this.reloadTable();
      this.reloadChart();
    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };
};
