import LocationsChart from './charts/locationsChart'
import LocationsTable from './charts/locationsTable'
import SelectorBox from './selectorBox'
import TabContent from '@/jquery/widgets/tabContent'

export default function () {

    var that = this;
    var $analyticsContainer;

    this.chartSettings = {
        project_id  : null,
        platform    : null,
        startDate   : null,
        endDate     : null
    };

    this.chartOptions = { };

    this.chart = null;
    this.tab = null;

    this.initiated = false;

    this.init = function () {
        $analyticsContainer = $('#analyticsContent');

        this.chartSettings.project_id  = $analyticsContainer.attr('data-project-id');

        this.table = new LocationsTable({
            element: $analyticsContainer.find('.locationsTable'),
            on_select_items: function(items) {
                that.chart.showItems(items);
                that.reloadChart();
            }
        });

        this.chart = new LocationsChart({
            element: $analyticsContainer.find('.locationsChart'),
            mode: 'map',
            modifier: 'total_hits',
            on_change: function(reload) {
                if (reload !== false && this.modifier && that.table.modifier != this.modifier) {
                    that.table.setModifier(this.modifier);
                    if (that.initiated) {
                        that.reloadTable();
                    }
                }
            }
        });

        this.tabContent = new TabContent({
            element: $analyticsContainer.find('.tabContentWrapper'),
            save_state: true,
            default_tab: 'locations',
            on_change: function(tab) {
                if (tab === 'locations') {
                    that.chartOptions.location_type = 0;
                } else if (tab == 'ibeacons') {
                    that.chartOptions.location_type = 2;
                } else {
                    that.chartOptions.location_type = undefined;
                }

                if (that.tab) {
                    that.tab = tab;
                    that.reloadChart();
                    that.reloadTable();
                } else {
                    that.tab = tab;
                }
            }
        });

        SelectorBox().init();

        this.initiated = true;

        return this;
    };

    this.reloadChart = function() {
        this.chart.reloadByParam(this.chartSettings, this.chartOptions);
    };

    this.reloadTable = function() {
        this.table.reloadByParam(this.chartSettings, this.chartOptions);
    };

    this.redraw = function() {
        this.reloadChart();
        this.reloadTable();
    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };

    this.clickOnTableMarker = function(lat, lon, radius) {
        this.chart.setMode('map', false);
        this.chart.map.fitWithPositionAndRadius(lat, lon, radius);
    };
};