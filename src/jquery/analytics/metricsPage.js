import MetricsChart from './charts/metricsChart'
import MetricsTable from './charts/metricsTable'
import SelectorBox from './selectorBox'

export default function () {

    var that = this;
    var $analyticsContainer;

    this.chartSettings = {
        project_id  : null,
        platform    : null,
        startDate   : null,
        endDate     : null
    };

    this.metricsChart = null;
    this.metricsTable = null;

    this.init = function () {
        $analyticsContainer = $('#analyticsContent');

        this.chartSettings.project_id  = $analyticsContainer.attr('data-project-id');

        this.metricsTable = new MetricsTable({
            element: $analyticsContainer.find('.metricsTable'),
            on_select_items: function(items) {
                that.metricsChart.showItems(items);
                that.reloadMetricsChart();
            }
        });

        this.metricsChart = new MetricsChart({
            element: $analyticsContainer.find('.metricsChart'),
            mode: 'totals',
            modifier: 'amount'
        });

        SelectorBox().init();

        return this;
    };

    this.showHideMetricsChart = function() {

    };

    this.reloadMetricsChart = function() {
        this.metricsChart.reloadByParam(this.chartSettings);
    };

    this.reloadMetricsTable = function() {
        this.metricsTable.reloadByParam(this.chartSettings);
    };

    this.redraw = function() {
        this.showHideMetricsChart();
        this.reloadMetricsTable();
        this.reloadMetricsChart();
    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };
};
