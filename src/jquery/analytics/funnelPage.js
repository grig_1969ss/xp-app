import FunnelChart from './charts/funnelChart'
import SelectorBox from './selectorBox'

export default function () {

    var $analyticsContainer;

    this.chartSettings = {
        project_id  : null,
        platform    : null,
        startDate   : null,
        endDate     : null
    };

    this.chartOptions = {
        funnel_id : null
    };

    this.funnelChart = null;

    this.init = function () {
        $analyticsContainer = $('#analyticsContent');

        this.chartSettings.project_id  = $analyticsContainer.attr('data-project-id');
        this.chartOptions.funnel_id = $analyticsContainer.attr('data-funnel-id');

        this.funnelChart = new FunnelChart({
            element: $analyticsContainer.find('.funnelChart'),
            table: $analyticsContainer.find('.funnelTable')
        });

        this.funnelChart.updateTitle($analyticsContainer.attr('data-funnel-title'));
        this.funnelChart.addTitleBackButton('/analytics/analytics/funnels?project_id='+this.chartSettings.project_id);

        SelectorBox().init();

        return this;
    };

    this.reloadFunnelChart = function() {
        this.funnelChart.reloadByParam(this.chartSettings, this.chartOptions);
    };

    this.redraw = function() {
        this.reloadFunnelChart();
    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };
};
