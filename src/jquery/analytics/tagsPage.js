import TagsTable from './charts/tagsTable'
import TagsChart from './charts/tagsChart'
import ImpressionsTable from './charts/impressionsTable'
import ImpressionsChart from './charts/impressionsChart'
import EventsTable from './charts/eventsTable'
import EventsChart from './charts/eventsChart'
import SelectorBox from './selectorBox'
import TabContent from '@/jquery/widgets/tabContent'

export default function () {

    var that = this;
    var $analyticsContainer;

    this.chartSettings = {
        project_id  : null,
        platform    : null,
        startDate   : null,
        endDate     : null
    };

    this.tabContent = null;
    this.tab = null;

    this.tagsChart = null;
    this.tagsTable = null;
    this.impressionsChart = null;
    this.impressionsTable = null;
    this.eventsChart = null;
    this.eventsTable = null;

    this.initiated = false;

    this.init = function () {
        $analyticsContainer = $('#analyticsContent');

        this.chartSettings.project_id  = $analyticsContainer.attr('data-project-id');

        this.tagsTable = new TagsTable({
            element: $analyticsContainer.find('.tagsTable'),
            on_select_items: function(items) {
                that.tagsChart.showItems(items);
                that.reloadTagsChart();
            }
        });

        this.tagsChart = new TagsChart({
            element: $analyticsContainer.find('.tagsChart'),
            mode: 'totals',
            modifier: 'total_hits',
            on_change: function() {
                if (this.modifier && that.tagsTable.modifier != this.modifier) {
                    that.tagsTable.setModifier(this.modifier);
                    if (that.initiated) {
                        that.reloadTagsTable();
                    }
                }
            }
        });

        this.impressionsChart = new ImpressionsChart({
            element: $analyticsContainer.find('.impressionsChart'),
            mode: 'totals'
        });

        this.impressionsTable = new ImpressionsTable({
            element: $analyticsContainer.find('.impressionsTable'),
            on_select_items: function(items) {
                that.impressionsChart.showItems(items);
                that.reloadImpressionsChart();
            }
        });

        this.eventsChart = new EventsChart({
            element: $analyticsContainer.find('.eventsChart'),
            mode: 'totals'
        });

        this.eventsTable = new EventsTable({
            element: $analyticsContainer.find('.eventsTable'),
            on_select_items: function(items) {
                that.eventsChart.showItems(items);
                that.reloadEventsChart();
            }
        });

        this.tabContent = new TabContent({
            element: $analyticsContainer.find('.tabContentWrapper'),
            save_state: true,
            default_tab: 'tags',
            on_change: function(tab) {
                if (that.tab) {
                    that.tab = tab;
                    that.redraw();
                } else {
                    that.tab = tab;
                }
            }
        });

        SelectorBox().init();

        this.initiated = true;

        return this;
    };

    this.showHideTagsChart = function() {

    };

    this.reloadTagsChart = function() {
        this.tagsChart.reloadByParam(this.chartSettings);
    };

    this.reloadTagsTable = function() {
        this.tagsTable.reloadByParam(this.chartSettings);
    };

    this.reloadImpressionsChart = function() {
        this.impressionsChart.reloadByParam(this.chartSettings);
    };

    this.reloadImpressionsTable = function() {
        this.impressionsTable.reloadByParam(this.chartSettings);
    };

    this.reloadEventsChart = function() {
        this.eventsChart.reloadByParam(this.chartSettings);
    };

    this.reloadEventsTable = function() {
        this.eventsTable.reloadByParam(this.chartSettings);
    };

    this.redraw = function() {
        if (this.tab == 'tags') {
            this.showHideTagsChart();
            this.reloadTagsTable();
            this.reloadTagsChart();
        } else if (this.tab == 'impressions') {
            this.reloadImpressionsChart();
            this.reloadImpressionsTable();
        } else if (this.tab == 'events') {
            this.reloadEventsChart();
            this.reloadEventsTable();
        }
    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };
};
