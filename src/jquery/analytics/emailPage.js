import EmailStatisticsChart from './charts/emailStatisticsChart'
import SelectorBox from './selectorBox'

export default function () {

  var $analyticsContainer;

  this.chartSettings = {
    project_id  : null,
    startDate   : null,
    endDate     : null
  };

  this.chart = null;

  this.init = function () {
    $analyticsContainer = $('#analyticsContent');

    this.chartSettings.project_id  = $analyticsContainer.attr('data-project-id');

    this.chart = new EmailStatisticsChart({
      element: $analyticsContainer.find('.emailChart'),
      table: $analyticsContainer.find('.emailTable')
    });

    SelectorBox().init();

    return this;
  };

  this.reloadChart = function() {
    this.chart.reloadByParam(this.chartSettings);
  };

  this.redraw = function() {
    this.reloadChart();
  };

  this.setSettings = function(key,value) {
    this.chartSettings[key] = value;
  };

  this.getSettings = function(key) {
    return this.chartSettings[key];
  };
};
