import AttributionsChart from './charts/attributionsChart'
import AttributionsCampaignsTable from './charts/attributionsCampaignsTable'
import SelectorBox from './selectorBox'

export default function () {

  var that = this;
  var $analyticsContainer;

  this.chartSettings = {
    project_id  : null,
    platform    : null,
    startDate   : null,
    endDate     : null
  };

  this.chartOptions = {
    network_id : null,
    ids: ''
  };

  this.chart = null;
  this.table = null;

  this.init = function () {
    $analyticsContainer = $('#analyticsContent');

    this.chartSettings.project_id = $analyticsContainer.attr('data-project-id');
    this.chartOptions.network_id = $analyticsContainer.attr('data-network-id');

    this.chart = new AttributionsChart({
      element: $analyticsContainer.find('.attributionsCampaignsChart'),
      modifier: 'ads_campaigns',
      mode: 'totals'
    });

    this.chart.chartTimeline.updateTitle(t('Analytics__Install_attributions') + ' - ' + $analyticsContainer.attr('data-network-title'));
    this.chart.chartTotals.updateTitle(t('Analytics__Install_attributions') + ' - ' + $analyticsContainer.attr('data-network-title'));

    this.table = new AttributionsCampaignsTable({
      element: $analyticsContainer.find('.attributionsCampaignsTable'),
      on_select_items: function(items) {
        that.chart.showItems(items);
        that.reloadChart();
      }
    });

    SelectorBox().init();

    return this;
  };

  this.reloadChart = function() {
    this.chart.reloadByParam(this.chartSettings, this.chartOptions);
  };

  this.reloadTable = function() {
    this.table.reloadByParam(this.chartSettings, this.chartOptions);
  };

  this.redrawBackButton = function () {
    this.chart.chartTimeline.addTitleBackButton('/analytics/analytics/attributions?project_id='+this.chartSettings.project_id+SelectorBox().getHashedSelector());
    this.chart.chartTotals.addTitleBackButton('/analytics/analytics/attributions?project_id='+this.chartSettings.project_id+SelectorBox().getHashedSelector());
  };

  this.redraw = function() {
    this.reloadTable();
    this.reloadChart();
    this.redrawBackButton();
  };

  this.setSettings = function(key,value) {
    this.chartSettings[key] = value;
  };

  this.getSettings = function(key) {
    return this.chartSettings[key];
  };
};
