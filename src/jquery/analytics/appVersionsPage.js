import DeviceAttributeChart from './charts/deviceAttributeChart'
import DeviceAttributeTable from './charts/deviceAttributeTable'
import SelectorBox from './selectorBox'

export default function () {

    var that = this;
    var $analyticsContainer;

    this.chartSettings = {
        project_id  : null
    };

    this.charts = {};
    this.tables = {};

    this.init = function () {
        $analyticsContainer = $('#analyticsContent');

        this.chartSettings.project_id  = $analyticsContainer.attr('data-project-id');

        this.charts['ios'] = new DeviceAttributeChart({
            element: $analyticsContainer.find('.attributeIosChart .graphWrapper')
        }, 'app_version_chart', 'ios');
        this.tables['ios'] = new DeviceAttributeTable({
            element: $analyticsContainer.find('.attributeIosTable'),
            on_select_items: function(items) {
                this.reloadTableChart(that.charts['ios'], items, that.chartSettings);
            }
        }, 'app_version_table', 'ios');

        this.charts['android'] = new DeviceAttributeChart({
            element: $analyticsContainer.find('.attributeAndroidChart .graphWrapper')
        }, 'app_version_chart', 'android');
        this.tables['android'] = new DeviceAttributeTable({
            element: $analyticsContainer.find('.attributeAndroidTable'),
            on_select_items: function(items) {
                this.reloadTableChart(that.charts['android'], items, that.chartSettings);
            }
        }, 'app_version_table', 'android');

        this.charts['ios'].updateTitle(t("Analytics__Users_by_app_version_ios"));
        this.charts['android'].updateTitle(t("Analytics__Users_by_app_version_android"));

        SelectorBox().init(false, false, false);

        return this;
    };

    this.reloadChart = function() {
        delete this.chartSettings['platform'];
        delete this.chartSettings['startDate'];
        delete this.chartSettings['endDate'];

        this.charts['ios'].reloadByParam(this.chartSettings);
        this.tables['ios'].reloadByParam(this.chartSettings);
        this.charts['android'].reloadByParam(this.chartSettings);
        this.tables['android'].reloadByParam(this.chartSettings);
    };

    this.redraw = function() {
        that.reloadChart();
    };

    this.setSettings = function(key,value) {
        this.chartSettings[key] = value;
    };

    this.getSettings = function(key) {
        return this.chartSettings[key];
    };
};
