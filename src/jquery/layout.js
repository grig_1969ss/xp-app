window.$header                 = $('#headerWrap');
window.$content                = $('#content');
window.$modalWindowsContainer  = $('#modalWindowsContainer');

$('.jsPage[data-manage="0"]')
  .find('input, select, textarea')
  .prop('disabled', true)
  .addClass('viewable');
