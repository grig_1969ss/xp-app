import ContentSection from './contentSection'
import ContentEditor from '@/jquery/widgets/contentEditor'
import Dropzone from 'vendor/dropzone/package'
import { twig } from 'vendor/twig/twig'

export default function (campaignPage, platform, inherit) {

    var that = new ContentSection(campaignPage);

    var $campaignPage,
        $contentSection
            = null;

    var twPayloadItem = null;

    that.init = function() {
        $campaignPage = campaignPage.getLayout();
        $contentSection = $campaignPage.find('#content-'+platform+'_section');
        if (!$contentSection.length) return null;

        that.initSection($contentSection);
        return that;
    };

    that.initVariant = function(variant) {
        that.initTranslations(variant);
        that.initCategoryAutocomplete(variant);
        that.initImageUpload(variant);
        that.initFormInputs(variant);
        that.initClickAction(variant);
        that.initPayloads(variant);
        that.initPreview(variant);
        that.updatePreview(variant);
    };

    that.initTranslations = function(variant) {
        $(variant).off('init', '.jsTranslation').on('init', '.jsTranslation', function(event){
            if ($(event.target).hasClass('jsTranslation')) {
                that.initTranslation(variant, this);
            }
        });
        $(variant).find('.jsTranslations .jsTranslation').trigger('init');
        $(variant).find('.jsTranslationDefault .jsTranslation').trigger('init');

        $(variant).on('update', '.jsTranslationsContainer', function(event){
            if ($(event.target).hasClass('jsTranslationsContainer')) {
                that.updatePreview(variant);
            }
        });
    };

    that.initCategoryAutocomplete = function(variant) {
        var $field = $(variant).find('.jsPushCategoryAutocomplete');

        $field.comboboxAutocomplete({
            source: '/push/push-category/autocomplete?project_id=' + window.xp.project.id + '&PushCategory[type]=' + $field.data('type'),
            select: function(event, ui) {
                that.renderCategory(variant, ui.item.id);
            }
        });

        $field.on('cleaned', function() {
            that.renderCategory(variant, null);
        });
    };

    that.renderCategory = function (variant, category_id) {
        $.get('/push/campaign/render-push-category', {
            project_id: window.xp.project.id,
            category_id: category_id,
            variant_key: $(variant).data('key'),
            message_type: $(variant).closest('.jsMessage').data('type')
        }, function(response) {
            $(variant).find('.jsPushCategoryContent').html(response.data);
            that.updatePreview(variant);
        });
    };

    that.initTranslation = function(variant, translation) {
        that.initEditor(variant, translation);
    };

    that.initEditor = function(variant, translation) {
        new ContentEditor($(translation).find('.jsPushTitleContainer'), {
            personalization: true,
            emojis: true,
            snippets: $(variant).data('twig'),
            optimove: $campaignPage.data('optimove') ? true : false
        });

        new ContentEditor($(translation).find('.jsPushTextContainer'), {
            personalization: true,
            emojis: true,
            snippets: $(variant).data('twig'),
            optimove: $campaignPage.data('optimove') ? true : false
        });
    };

    that.initFormInputs = function(variant) {
      const variantContainer = $(variant);
      variantContainer.on('change', '.pushPictureSelector', function(e, customParam) {
        const value = $(this).val();
        const wrapper = $(this).closest('.imageSelectWrapper');
        wrapper.find('.choice').each(function (i, item) {
          $(item).hide();
          if (!$(item).hasClass('choice-' + value)) {
            $(item).find('input,select,textarea').attr("disabled", "disabled").trigger('disable');
          }
        });

        const choice = wrapper.find('.choice-' + value);
        choice.css('display', 'inline-block');
        choice.find('input,select,textarea').removeAttr("disabled").trigger('enable');

        variantContainer.find('.pushPicture').val(choice.find('.pushPictureUrl').val()).trigger('change', [customParam]);
      });

      variantContainer.on('change', '.pushPictureUrl', function() {
        variantContainer.find('.pushPicture').val($(this).val()).trigger('change');
      });

      variantContainer.find('.pushPictureSelector').trigger('change', ['doNotUpdatePreview']);
    };

    that.initImageUpload = function(variant) {
      Dropzone.autoDiscover = false;
      $(variant).find('.fake-input-file').not('.fake-input-file-custom').dropzone({
        url: "/push/campaign/upload-image?project_id="+window.xp.project.id,
        maxFilesize: 10,
        thumbnailWidth: null,
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        paramName: "CampaignMessage[image]",
        params: {
          _csrf: $(variant).parents("form").find('input[type=hidden][name=_csrf]').val(),
          button_id: $(this.element).data('type')
        },
        success: function(event, response, xhr) {
          $(this.element).addClass('pushImageWithRemove');
          $(this.element).find('input').val(response.filename).trigger('change');
          $(this.element).parent().find('.jsRemovePicture').show();
        }
      });
      $(variant).on('click', '.fake-input-file a', function() {
        $(this).parent().click();
      });
      $(variant).on('click', '.jsRemovePicture', function () {
        $(this).hide();
        $(this).parent().find('.fake-input-file').removeClass('pushImageWithRemove');
        $(this).parent().find('.pushPictureUrl').val('').trigger('change');
      });
    };

    that.initClickAction = function(variant) {
        $(variant).on('change', '.jsAction .action-selector', function() {
            var value = $(this).val();
            $(this).closest('.jsMessageVariant').find('.jsActionChoice').each(function (i, item) {
                if (!$(item).hasClass('jsActionChoice-' + value)) {
                    $(item).hide();
                    $(item).find('input').attr("disabled", "disabled");
                    $(item).find('select').attr("disabled", "disabled");
                } else {
                    $(item).show();
                    $(item).find('input,select').removeAttr("disabled");
                }
            });

            if (campaignPage.updateInappSection) {
                campaignPage.updateInappSection();
            }
        });

        $(variant).find('.jsAction .action-selector').trigger('change');
    };

    that.initPayloads = function(variant) {
        if (!twPayloadItem) {
            twPayloadItem = twig({
                id: "payloadListItem_"+platform,
                href: VIEWS_DIR+"/push/views/campaign/templates/content-payloads-item.twig",
                async: false
            });
        }

        $(variant).on('click', '.jsAddPayload', function(e){
            var index = parseInt($(this).parents('.jsMessageVariant').find('.jsPayload:last-child').data('index')) + 1;
            if (isNaN(index)) index = 0;
            var template = twPayloadItem.render({message_type: $(this).parents('.jsMessage').data('type'), variant_key: $(this).parents('.jsMessageVariant').data('key'), index: index});
            $(this).parents('.jsMessageVariant').find('.jsPayloads').show().append(template);
        });

        $(variant).on('click', '.jsDeletePayload', function(e){
            var list = $(this).parents('ul');
            $(this).parents('li').eq(0).remove();
            if ($(list).find('li').length == 0) {
                $(list).hide();
            }
        });
    };

    that.initPreview = function(variant) {
        $(variant).on('keyup', '.pushTitle, .pushMessage', function() {
            that.updatePreview(variant);
        });

        $(variant).on('change', '.pushTitle, .pushMessage, .pushIcon', function() {
            that.updatePreview(variant);
        });

        $(variant).on('change', '.updatePreview', function(e, customParam) {
          if (customParam === 'doNotUpdatePreview') {
            return;
          }

          that.updatePreview(variant);
        });

        $(variant).data('preview', $(variant).find('.jsMessagePreview').vue());
    };

    that.updatePreview = function(variant) {
        let preview = $(variant).data('preview');
        if (preview) {
            preview.message = that.buildMessage(variant);
        }
    };

    that.buildMessage = function(variant) {
        let title = $(variant).find('.jsTranslationLanguageDefault').find('div.pushTitle').length
            ? $("<div>").html($(variant).find('.jsTranslationLanguageDefault').find('div.pushTitle').html()).text()
            : $(variant).find('.jsTranslationLanguageDefault').find('textarea.pushTitle, input.pushTitle').val();
        if (!title) title = $(variant).find('.pushPreview .title').data('default');

        let message = $(variant).find('.jsTranslationLanguageDefault').find('div.pushMessage').length
            ? $("<div>").html($(variant).find('.jsTranslationLanguageDefault').find('div.pushMessage').html()).text()
            : $(variant).find('.jsTranslationLanguageDefault').find('textarea.pushMessage, input.pushMessage').val();
        if (!message) message = t('all', 'PushCampaign__Preview_message_text');

        let icon = $(variant).find('.pushIcon').val();
        if (icon === undefined || icon === "") icon = $(variant).find('.jsPushCategoryContent .jsPushIcon').val();
        if (icon === undefined || icon === "") icon = $(variant).closest('.jsMessage').data('project-icon');
        let picture = $(variant).find('.pushPicture').val();

        let actions = [];
        $(variant).find('.jsPushAction').each(function() {
            actions.push($(this).val());
        });

        return {
            title: title,
            text: message,
            icon: icon,
            picture: picture,
            actions: actions
        };
    };

    that.requiresInapp = function() {
        var status = false;
        $contentSection.find('.action-selector').each(function(){
             if ($(this).val() == 'inapp') {
                 status = true;
             }
        });
        return status;
    };

    if (inherit || inherit === undefined) {
        return that.init();
    }

    return that;
};
