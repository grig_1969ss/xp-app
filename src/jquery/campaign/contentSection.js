export default function (campaignPage) {
    var that = this;

    this.initSection = function($contentSection) {
        this.initAB($contentSection);
    };

    this.initAB = function($contentSection) {
        $contentSection.on('init', '.jsMessageVariant', function(event){
            if ($(event.target).hasClass('jsMessageVariant')) {
                that.initVariantCommon(this);
            }
        });
        $contentSection.find('.jsMessageVariants .jsMessageVariant').trigger('init');

        $contentSection.on('click', '.jsMessageVariantsTabButton', function (event) {
            event.preventDefault();
            that.enableVariant('.jsMessageVariant[data-key=' + $(this).data('key') + ']', event);
        });
    };

    this.initVariantCommon = function (variant) {
        this.initTemplateLoader(variant);
        this.initVariant(variant);
    };

    this.initVariant = function(variant) {
        // To be overwritten in child classes
    };

    this.enableVariant = function(variant) {
        // To be overwritten in child classes
    };

    this.initTemplateLoader = function(variant) {
        $(variant).find('.jsTemplateLoader').comboboxAutocomplete({
            source: '/push/campaign-template/autocomplete?project_id=' + window.xp.project.id + '&message_type=' + $(variant).parents('.jsMessage').eq(0).data('type'),
            select: function( event, ui ) {
                var languagesMulti = campaignPage.getLayout().find('.jsMultilanguage').prop('checked') ? 1 : 0;
                var languagesDefault = campaignPage.getLayout().find('.jsLanguageDefault:checked').val();
                var languagesList = [];
                campaignPage.getLayout().find('.jsLanguages .jsLanguageSelect').each(function(){
                    languagesList.push($(this).val());
                });

                $(variant).find('.jsTemplateLoader').blur();
                that.loadTemplate(variant, ui.item.id, languagesMulti, languagesList, languagesDefault);
                return false;
            }
        });
    };

    this.loadTemplate = function(variant, templateId, languagesMulti, languagesList, languagesDefault) {
        $.ajax({
            url: '/push/campaign-template/render',
            data: {
                project_id: window.xp.project.id,
                id: templateId,
                variant_key: $(variant).data('key'),
                languages_multi: languagesMulti,
                languages_list: languagesList,
                languages_default: languagesDefault
            },
            success: function(response) {
                if (response && response.data) {
                    if ($(variant).find('.jsTemplate').length) {
                        $(variant).find('.jsTemplate').html($('<div/>').html(response.data).find('.jsTemplate').html());
                    } else {
                        $(variant).html(response.data);
                    }
                    that.initVariantCommon(variant);
                }
            }
        });
    };

    return this;
};
