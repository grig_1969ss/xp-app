export default function (contentInapp) {

    var that = this;
    var TYPE_EDITOR = '3';

    this.initVariant = function(variant) {
        var $container = $(variant).find('.editor.tab');

        $container.find('.button').click(function() {
            $container.find('.editorSelectorContainer .button').removeClass('active');
            $(this).addClass('active');

            $container.find('.innerTab').hide();
            $container.find('.' + $(this).data('value')).show();
        });

        $container.find('.sortableTitle textarea').redactor({
            tabKey: false,
            enterKey: false,
            maxHeight: 50,
            buttons: ['bold', 'italic', 'underline'],
            callbacks: {
                init: function() {
                    var code = this.code.get();
                    if (code != "") {
                        $(variant).find('.inappPreview .editorContainer .title').html(code);
                    } else {
                        this.core.editor().find('p').attr('style', 'font-family: Arial !important; font-size: 18px; text-align: center');
                    }
                },
                change: function() {
                    $(variant).find('.inappPreview .editorContainer .title').html(this.code.get());
                },
                focus: function() {
                    $(variant).find('.inappPreview .editorContainer .title').html(this.code.get());
                }
            },
            plugins: ['alignment', 'fontsize', 'fontfamily', 'personalization']
        });

        $container.find('.sortableText textarea').redactor({
            minHeight: 55,
            maxHeight: 700,
            buttons: ['bold', 'italic', 'underline', 'ol', 'ul'],
            callbacks: {
                init: function() {
                    var code = this.code.get();
                    if (code != "") {
                        $(variant).find('.inappPreview .editorContainer .text').html(code);
                    } else {
                        this.core.editor().find('p').attr('style', 'font-family: Arial !important; font-size: 14px; text-align: center');
                    }
                },
                change: function() {
                    $(variant).find('.inappPreview .editorContainer .text').html(this.code.get());
                },
                focus: function() {
                    $(variant).find('.inappPreview .editorContainer .text').html(this.code.get());
                }
            },
            plugins: ['alignment', 'fontsize', 'fontfamily', 'personalization']
        });

        $container.find('.sortableButtons input[type=text]').keyup(function() {
            that.onEditorButtonKeyUp(variant, this);
        });
        $container.find('.sortableButtons input[type=text]').focus(function() {
            that.onEditorButtonKeyUp(variant, this);
        });
        $container.find('.sortableButtons input[type=checkbox]').change(function() {
            that.onEditorButtonCheckboxChanged(variant, this);
        });

        $container.find('.editorText').sortable({
            handle: '.sortButton',
            update: function() {
                that.updateEditorPreview(variant);
            }
        });

        var $caller;
        $container.find('span.colorpickerField').click(function() {
            $(this).siblings('input.colorpickerField').click();
        });
        $container.find('input.colorpickerField').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
                $caller = $(this);
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(200);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(200);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                that.updateInAppEditorColor(variant, $caller, hex);
            }
        }).bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
            that.updateInAppEditorColor(variant, this);
        }).bind('change', function() {
            if (this.value.search(/^#[0-9a-f]{6}$/i) == -1) {
                $(this).val("");
            }
            $(this).ColorPickerSetColor(this.value);
            that.updateInAppEditorColor(variant, this);
        }).each(function(i, elem) {
            that.updateInAppEditorColor(variant, elem);
        });

        $container.find('input.numberField').on('keyup keypress blur change', function() {
            if (!$.isNumeric(this.value) || Math.floor(this.value) != this.value) {
                $(this).val("");
            } else {
              let max = parseInt($(this).attr('max'));
              let min = parseInt($(this).attr('min'));
              $(this).val(Math.min(max, Math.max(min, $(this).val())));
            }
            that.updateInAppEditorStyle(variant, this);
        }).each(function(i, elem) {
            that.updateInAppEditorStyle(variant, elem);
        });

        $(variant).find('.editorImageHeight').keyup(function() {
            var $img = $(variant).find('.inappPreview .editorContainer .image img');
            $(this).val(parseInt($(this).val()) || '');
            if ($(this).val() == '' || $(this).val() == undefined) {
                $img.css('max-height', 'none');
                return false;
            }
            $img.css('max-height', $(this).val() + 'px');
        }).trigger('keyup');

        $(variant).find('.editorImageCheckbox').change(function() {
            var $imgContainer = $(variant).find('.inappPreview .editorContainer .image');
            if (this.checked) {
                $imgContainer.show();
            } else {
                $imgContainer.hide();
            }
        });

        return this;
    };

    this.updateInAppEditorColor = function(variant, $caller, color) {
        $caller = $($caller);
        if (color == undefined) {
            color = $caller.val();
        }
        if (color.length && color.indexOf('#') !== 0) {
            color = '#' + color;
        }
        $caller.val(color);

        var $elem;
        var target = $caller.data('target');
        var property = $caller.data('property');
        var $preview = $(variant).find('.inappPreview .editorContainer');
        var $overlay = $(variant).find('.inappPreview .editorOverlay');
        if (target == 'this') {
            $elem = $preview;
        } else if (target == 'container') {
            $elem = $preview.parent();
        } else if (target == 'overlay') {
            $elem = $overlay;
        } else {
            $elem = $preview.find(target);
        }
        if (property == 'border-color') {
            $elem.css('border', color.length ? '1px solid' : '0px');
        }
        $elem.css(property, color);
        $caller.next().css('background', color.length ? color : '#eeeeee');
    };

    this.updateInAppEditorStyle = function (variant, $caller, value) {
        $caller = $($caller);
        if (value == undefined) {
            value = $caller.val();
        }

        var $elem;
        var target = $caller.data('target');
        var property = $caller.data('property');
        var $preview = $(variant).find('.inappPreview .editorContainer');
        var $overlay = $(variant).find('.inappPreview .editorOverlay');
        if (target == 'this') {
            $elem = $preview;
        } else if (target == 'container') {
            $elem = $preview.parent();
        } else if (target == 'overlay') {
            $elem = $overlay;
        } else {
            $elem = $preview.find(target);
        }

        if (property == 'opacity') {
            value = parseFloat(value / 100);
        }
        if (property === 'border-radius') {
          value = parseInt(value) || 0
        }
        $elem.css(property, value);
    };

    this.onEditorButtonKeyUp = function(variant, elem, init) {
        var $previewContainer = $(variant).find('.inappPreview .editorContainer .buttons');
        var target = $(elem).data('target');

        var $targetButton = $previewContainer.find('.button.' + target);
        if ($(elem).val().length) {
            $targetButton.text($(elem).val());
        } else {
            if (!init) {
                $targetButton.html('&nbsp;');
            }
        }
    };

    this.onEditorButtonCheckboxChanged = function(variant, elem, init) {
        var $previewContainer = $(variant).find('.inappPreview .editorContainer .buttons');
        var $checkbox = $(elem);
        var $checkboxes = $(elem).parents('.sortableButtons').find('input[type=checkbox]');
        var $textfield = $checkbox.parents('.buttonBlock').find('input[type=text]');
        var target = $checkbox.data('target');

        var $targetButton = $previewContainer.find('.button.' + target);
        if ($checkbox[0].checked) {
            $targetButton.show();
        } else {
            $targetButton.hide();
        }
        if ($textfield.val().length) {
            $targetButton.text($textfield.val());
        } else {
            if (!init) {
                $targetButton.html('&nbsp;');
            }
        }

        var $buttons = $previewContainer.find('.button');
        var visibleButtonsLength = 0 + $checkboxes[0].checked + $checkboxes[1].checked;
        if (visibleButtonsLength == 2) {
            $buttons.css('width', '40%');
            $buttons.last().css('margin-left', '8px');
        } else {
            $buttons.css('width', '60%').css('margin', '0px');
        }
    };

    this.updateEditorPreview = function(variant, filename) {
        if (!$(variant).data('initiated') || $(variant).find('.inappType input:radio:checked').val() != TYPE_EDITOR) {
            return false;
        }
        var params = contentInapp.getScreenParams(variant);
        var orientation = params.or;
        var screen_width = params.width;
        var screen_height = params.height;
        if (orientation == "" || orientation == undefined) {
            return false;
        }

        $(variant).find('.orientationSelectorContainer').hide();

        var $container = $(variant).find('.inappPreview .portrait .container');
        var $preview = $container.find('.editorContainer');
        var $img = $preview.find('img');

        $(variant).find('.tab.editor input.colorpickerField').each(function(i, elem) {
            that.updateInAppEditorColor(variant, elem);
        });
        $(variant).find('.tab.editor input.numberField').each(function(i, elem) {
            that.updateInAppEditorStyle(variant, elem);
        });
        $container.find('.editorContainer').show();
        $container.find('span').show();

        $container.find('img.watermark').hide();
        $container.find('iframe').hide();
        $container.find('.background').hide();
        $container.find('.button').hide();

        if (filename != undefined && filename != "") {
            var onload = function(_bg) {
                return function() {
                    $container.find('.dz-preview').hide();

                    $(_bg).css("width", "auto")
                        .css("max-width", "100%")
                        .css("height", "auto")
                        .css("max-height", "100%")
                        .fadeIn(200);

                    $(variant).find(".editorImageFile").val(filename);
                    $container.find('.editorContainer .image img').attr('src', filename);
                    that.updateEditorPreview(variant);
                }
            };
            $img.off("load").one("load", onload($img)).attr('src', filename).each(function() {
                if (this.complete) $(this).load();
            });

            return false;
        }

        $container.css("width", screen_width + "px").css("height", screen_height + "px")
            .css("top", "0px").css("left", "0px").css("right", "auto").css("bottom", "auto");
        $preview.css('width', (screen_width - 40) + "px")
            .css('max-height', (screen_height - 40) + "px");

        var $input = $(variant).find(".editorImageFile");
        var $imgHeight = $(variant).find('.editorImageHeight');
        var filename = $input.val();
        if (filename != undefined && filename != "") {
            $img.attr('src', filename);
            $imgHeight.removeAttr('disabled');
        } else {
            $img.attr('src', '/img/inappeditor-placeholder.jpg');
            $imgHeight.val('').attr('disabled', 'disabled');
        }
        if ($(variant).find('.editorImageCheckbox')[0].checked) {
            $img.parent().show();
        } else {
            $img.parent().hide();
        }

        var elements = {
            image: $preview.find('.image'),
            title: $preview.find('.title'),
            text: $preview.find('.text'),
            buttons : $preview.find('.buttons')
        };
        $(variant).find('.editor.tab .sortable').each(function(i, field) {
            var sequence = $(field).data('sequence');
            $preview.append(elements[sequence]);
        });
        $(variant).find('.editor.tab .sortableButtons input.button-label').each(function(i, elem) {
            that.onEditorButtonKeyUp(variant, elem, true);
        });
        $(variant).find('.editor.tab .sortableButtons input[type=checkbox]').each(function(i, elem) {
            that.onEditorButtonCheckboxChanged(variant, elem, true);
        });
    };

    return this;
};
