import ContentSection from './contentSection'
import ContentEditor from '@/jquery/widgets/contentEditor'
import SmsCounter from '@/jquery/widgets/smsCounter'

export default function (campaignPage, platform, inherit) {

  let that = new ContentSection(campaignPage);

  let $campaignPage,
    $contentSection
      = null;

  that.init = function() {
    $campaignPage = campaignPage.getLayout();
    $contentSection = $campaignPage.find('#content-sms_section');
    if (!$contentSection.length) return null;

    that.initSection($contentSection);
    return that;
  };

  that.initVariant = function(variant) {
    that.initTranslations(variant);
    that.initPreview(variant);
    that.updatePreview(variant);
  };

  that.initTranslations = function(variant) {
    $(variant).off('init', '.jsTranslation').on('init', '.jsTranslation', function(event){
      if ($(event.target).hasClass('jsTranslation')) {
        that.initTranslation(variant, this);
      }
    });
    $(variant).find('.jsTranslations .jsTranslation').trigger('init');
    $(variant).find('.jsTranslationDefault .jsTranslation').trigger('init');

    $(variant).on('update', '.jsTranslationsContainer', function(event){
      if ($(event.target).hasClass('jsTranslationsContainer')) {
        that.updatePreview(variant);
      }
    });
  };

  that.initTranslation = function(variant, translation) {
    that.initFrom(variant, translation);
    that.initEditor(variant, translation);
    that.initCounter(variant, translation);
  };

  that.initFrom = function(variant, translation) {
    $(translation).find('.jsSmsFrom').richAutocomplete({
      source: '/api/sms/sms-from-name/autocomplete?project_id=' + window.xp.project.id,
      searchOnFocus: {
        allowEmptyValue: true
      }
    });
  };

  that.initEditor = function(variant, translation) {
    new ContentEditor($(translation).find('.jsSmsTextContainer'), {
      personalization: true,
      emojis: true,
      snippets: $(variant).data('twig'),
      optimove: !!$campaignPage.data('optimove')
    });
  };

  that.initPreview = function(variant) {
    $(variant).on('keyup', '.jsSmsFrom, .jsSmsText', function() {
      that.updatePreview(variant);
    });

    $(variant).on('change', '.jsSmsFrom, .jsSmsText', function() {
      that.updatePreview(variant);
    });

    $(variant).data('preview', $(variant).find('.jsMessagePreview').vue());
  };

  that.initCounter = function(variant, translation) {
    $(translation).on('keyup', '.jsSmsText', function () {
      that.updateCounter(variant, translation);
    });

    $(translation).on('change', '.jsSmsText', function () {
      that.updateCounter(variant, translation);
    });

    that.updateCounter(variant, translation);
  };

  that.updatePreview = function(variant) {
    let preview = $(variant).data('preview');
    if (preview) {
      preview.message = that.buildMessage(variant);
    }
  };

  that.updateCounter = function(variant, translation) {
    let textarea = $(translation).find('.jsSmsText');
    let target = $(translation).find('.jsSmsTextCounter');
    let counter = new SmsCounter(textarea);

    let counterInfo = counter.countSms(target);
    if (counterInfo['messages'] > 1) {
      $(translation).find('.jsSmsCounterParts').text(t('Campaign__Sms_message_parts'));
    } else {
      $(translation).find('.jsSmsCounterParts').text(t('Campaign__Sms_message_part'));
    }

    let text = textarea.val();
    if (text.indexOf('{{') !== -1 && text.indexOf('}}') !== -1) {
      $(translation).find('.jsSmsCounterEstimated').show();
    } else {
      $(translation).find('.jsSmsCounterEstimated').hide();
    }
  };

  that.buildMessage = function(variant) {
    let from = $(variant).find('.jsTranslationLanguageDefault').find('.jsSmsFrom').val();

    let text = $(variant).find('.jsTranslationLanguageDefault').find('.jsSmsText').val();
    if (!text) text = t('all', 'PushCampaign__Preview_sms_text');

    return {
      title: from,
      text: text,
    };
  };

  if (inherit || inherit === undefined) {
    return that.init();
  }

  return that;
};
