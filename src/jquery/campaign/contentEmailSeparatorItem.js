import ContentEmailColorPicker from './contentEmailColorPicker'

export default function (contentEmailSection, $editorItemContainer, $previewItemContainer) {
    var that = this;

    var $eItem = null,
        $pItem = null,
        $heightField = null,
        $lineWidthField = null,
        $borderStyleField = null,
        $borderWidthField = null,
        $borderColorPicker = null,
        $bgColorPicker = null;

    this.init = function() {
        $eItem = $editorItemContainer;
        $pItem = $previewItemContainer;

        $heightField = $eItem.find('.heightField');
        $lineWidthField = $eItem.find('.lineWidthField');
        $borderStyleField = $eItem.find('.borderStyleField');
        $borderWidthField = $eItem.find('.borderWidthField');

        var $line = $('<div/>');
        $pItem.append($line);

        var $colorpicker = $eItem.find('.colorpickerField.backgroundField');
        $bgColorPicker = new ContentEmailColorPicker($colorpicker, $pItem.parent(), 'background');
        $colorpicker = $eItem.find('.colorpickerField.borderColorField');
        $borderColorPicker = new ContentEmailColorPicker($colorpicker, $line, 'border-color');

        var $targetFields = [$heightField, $lineWidthField, $borderStyleField, $borderWidthField];
        $.each($targetFields, function(i, $elem) {
            $elem.on('change keyup', function() {
                that.render();
            });
        });

        return this;
    };

    this.render = function() {
        var $line = $pItem.find('div');
        var height = parseInt($heightField.val()) || 0;
        var borderWidth = parseInt($borderWidthField.val()) || 0;
        var lineWidth = $lineWidthField.val();

        $pItem.css('height', height + 'px');
        $line.css('height', (height - borderWidth) / 2 + 'px')
            .css('border-bottom-width', borderWidth + 'px')
            .css('border-bottom-style', $borderStyleField.val())
            .css('margin', '0 auto');

        if (lineWidth == 'edge') {
            $line.css('width', '100%');
            $pItem.css('padding', '0').css('max-width', 'none');
        } else if (lineWidth == 'full') {
            $line.css('width', '100%');
            $pItem.css('padding', '0 16px').css('max-width', '');
        } else {
            $line.css('width', lineWidth + '%');
            $pItem.css('padding', '0').css('max-width', '');
        }

        contentEmailSection.updateIframe();
    };

    return this.init();
};
