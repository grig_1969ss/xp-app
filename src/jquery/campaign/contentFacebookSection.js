import ContentSection from './contentSection'
import ContentEditor from '@/jquery/widgets/contentEditor'
import Dropzone from 'vendor/dropzone/package'

export default function (campaignPage, platform, inherit) {

  let that = new ContentSection(campaignPage);

  let $campaignPage,
    $contentSection
      = null;

  that.init = function() {
    $campaignPage = campaignPage.getLayout();
    $contentSection = $campaignPage.find('#content-facebook_section');
    if (!$contentSection.length) return null;

    that.initSection($contentSection);
    return that;
  };

  that.initVariant = function(variant) {
    that.initEditor(variant);
    that.initImageUpload(variant);
    that.initPreview(variant);
    that.updatePreview(variant);
  };

  that.initEditor = function(variant) {
    new ContentEditor($(variant).find('.jsFacebookTitleContainer'), {
      personalization: true,
      emojis: true,
      optimove: !!$campaignPage.data('optimove')
    });

    new ContentEditor($(variant).find('.jsFacebookSubtitleContainer'), {
      personalization: true,
      emojis: true,
      optimove: !!$campaignPage.data('optimove')
    });
  };

  that.initImageUpload = function(variant) {
    Dropzone.autoDiscover = false;
    $(variant).find('.fake-input-file').not('.fake-input-file-custom').dropzone({
      url: "/push/campaign/upload-image?project_id="+window.xp.project.id,
      maxFilesize: 10,
      thumbnailWidth: null,
      acceptedFiles: 'image/*',
      addRemoveLinks: true,
      paramName: "CampaignMessage[image]",
      params: {
        _csrf: $(variant).parents("form").find('input[type=hidden][name=_csrf]').val(),
        button_id: $(this.element).data('type')
      },
      success: function(event, response, xhr) {
        $(this.element).find('input').val(response.filename);
        that.updatePreview(variant);
      }
    });
    $(variant).on('click', '.fake-input-file a', function() {
      $(this).parent().click();
    });
  };

  that.initPreview = function(variant) {
    $(variant).on('keyup', 'input, textarea, select', function() {
      that.updatePreview(variant);
    });

    $(variant).on('change', 'input, textarea, select', function() {
      that.updatePreview(variant);
    });

    $(variant).data('preview', $(variant).find('.jsMessagePreview').vue());
  };

  that.updatePreview = function(variant) {
    let preview = $(variant).data('preview');
    if (preview) {
      preview.message = that.buildMessage(variant);
    }
  };

  that.buildMessage = function(variant) {
    let title = $(variant).find('.jsFacebookTitle').val();
    let subtitle = $(variant).find('.jsFacebookSubtitle').val();
    let image_url = $(variant).find('.jsFacebookImageUrl').val();

    return {
      title: title,
      subtitle: subtitle,
      image_url: image_url
    };
  };

  if (inherit || inherit === undefined) {
    return that.init();
  }

  return that;
};
