import ConditionsEditor from '@/jquery/widgets/conditionsEditor'

export default function (campaignPage) {
    var that = this;

    var $campaignPage,
        $segmentSection,
        $segmentsContainer,
        $conditionsEditor
            = null;

    this.init = function() {
        $campaignPage    = campaignPage.getLayout();
        $segmentSection = $campaignPage.find('#segment_section');
        $segmentsContainer = $segmentSection.find('#segmentsContainer');
        if (!$segmentSection.length) return null;
        var $targetApplicationsHiddenInput = $segmentSection.find('#target_applications');

        $segmentSection.find('.ctmSegmentOptions input').on('select', function(){
            if ($(this).prop('checked')) {
                if ($(this).val() == 0) {
                    $segmentsContainer.show();
                } else {
                    $segmentsContainer.hide();
                }
            }
        }).trigger('select');

        $conditionsEditor = new ConditionsEditor();
        $conditionsEditor.init($segmentsContainer, '/segment/segment/attribute-list-data');
        $segmentSection.find('.jsTargetApplication').selectize({
            valueField: 'id',
            labelField: 'value',
            searchField: 'value',
            create: false,
            preload: true,
            plugins: ['remove_button'],
            onChange: function(value) {
                $targetApplicationsHiddenInput.val(value)
            },
            load: function(query, callback) {
                var selectize = this
                $.ajax({
                    url: '/api/account/application/autocomplete?project_id=' + window.xp.project.id,
                    data: {
                        term: query,
                    },
                    type: 'GET',
                    error: function() {
                        callback()
                    },
                    success: function(res) {
                        callback(res)
                        selectize.setValue($targetApplicationsHiddenInput.val().split(','))
                    }
                })
            }
        })
        $(window).on('hashchange', function(e){
          if (location.hash === '#segment'){
            $conditionsEditor.initDOM()
          }
        });
        $( document ).ready(function() {
          $conditionsEditor.initDOM()
        });
        return this;
    };

    return this.init();
};
