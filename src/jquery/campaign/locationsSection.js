import { twig } from 'vendor/twig/twig'

export default function (campaignPage) {
    var that = this;

    var $campaignPage,
        $locationsSection,
        $locationsTable
            = null;

    var twLocationItem = null;

    var selectedLocations = [];

    this.init = function() {
        $campaignPage       = campaignPage.getLayout();
        $locationsSection   = $campaignPage.find('#locations_section');
        if (!$locationsSection.length) return null;

        $locationsTable = $locationsSection.find('.table').first();

        this.initLocations();

        return this;
    };

    this.initLocations = function() {
        if (!twLocationItem) {
            twLocationItem = twig({
                id: "locationItem",
                href: VIEWS_DIR+"/push/views/campaign/templates/locations-item.twig",
                async: false
            });
        }

        this.indexLocations();
        this.initAddLocation($locationsSection.find('.jsAddGeofence'), 0);
        this.initAddLocation($locationsSection.find('.jsAddIbeacon'), 2);

        $locationsTable.on('click', '.jsDeleteLocation', function(){
            $(this).parents('tr').eq(0).remove();
            that.indexLocations();
        });

        $locationsTable.on('click', '.jsDeleteAllLocations', function(){
            $locationsTable.find('tbody tr').remove();
            that.indexLocations();
        });
    };

    this.initAddLocation = function($el, type) {
        $el.comboboxAutocomplete({
            source: '/api/location/location/autocomplete?project_id='+window.xp.project.id+'&type='+type,
            allItem: true,
            noneItem: true,
            select: function( event, ui ) {
                if (ui.item.items) {
                    for (var i=0; i<ui.item.items.length; i++) {
                        if (ui.item.items[i].id) {
                            that.addLocation({
                                id: ui.item.items[i].id,
                                title: ui.item.items[i].value,
                                tags: ui.item.items[i].tags,
                                type: ui.item.items[i].type
                            });
                        }
                    }
                    setTimeout(function() {
                        $el.blur();
                    }, 0);
                } else {
                    if (ui.item.id) {
                        that.addLocation({
                            id: ui.item.id,
                            title: ui.item.value,
                            tags: ui.item.tags,
                            type: ui.item.type
                        });
                    }
                    setTimeout(function() {
                        $el.comboboxAutocomplete('search');
                    }, 0);
                }
                return false;
            }, processItem: function(item) {
                if ($.inArray(item.id.toString(), selectedLocations) !== -1) {
                    return null;
                } else {
                    return item;
                }
            }
        });
    };

    this.addLocation = function(location) {
        var template = twLocationItem.render(location);
        $locationsTable.find('tbody').append(template);
        this.indexLocations();
    };

    this.indexLocations = function() {
        selectedLocations = [];
        $locationsTable.find('input').each(function(index, element) {
            selectedLocations.push($(element).val());
        });

        if (selectedLocations.length) {
            $locationsTable.find('tfoot').hide();
        } else {
            $locationsTable.find('tfoot').show();
        }
    };

    return this.init();
};