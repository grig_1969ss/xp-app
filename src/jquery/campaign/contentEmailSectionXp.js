import ContentSection from './contentSection'
import ContentEmailColorPicker from './contentEmailColorPicker'
import ContentEmailImageItem from './contentEmailImageItem'
import ContentEmailMessageItem from './contentEmailMessageItem'
import ContentEmailSeparatorItem from './contentEmailSeparatorItem'
import ContentEmailSocialItem from './contentEmailSocialItem'
import ContentEmailButtonItem from './contentEmailButtonItem'
import ContentEditor from '@/jquery/widgets/contentEditor'
import { stdPlashes } from '@/jquery/widgets/general'

export default function(campaignPage, contentSection, variant) {
    var that = this;

    var $container,
        $editorContainer,
        $previewContainer,
        $previewContainerHidden, 
        $previewContainerCustom
            = null;

    var activeElements = {};

    var commonCss = '';
    var previewCss = '';

    const EMAIL_TEMPLATE_CENTER = 0;
    const EMAIL_TEMPLATE_FULL_WIDTH = 1;
    const EMAIL_TEMPLATE_CUSTOM = 2;


    /* INIT */

    this.init = function() {
        $container = $(variant).find('.emailStyle');
        $editorContainer = $container.find('.emailControls');
        $previewContainer = $container.find('.emailPreviewInnerContainer .emailPreview');
        $previewContainerCustom = $container.find('.emailPreviewInnerContainerCustom .emailPreview');
        $previewContainerHidden = $previewContainer.find('.emailPreviewHidden .container');

        this.initTemplateSelect();

        this.initContentSection();

        this.initStyleSection();

        this.initAttachmentSection();

        this.initPreview();

        this.updateIframe();

        $(contentSection).on('section-tab-click', function() {
            that.updateIframe();
        });

        return this;
    };

    this.initTemplateSelect = function() {
        if (!$(variant).find('.emailTemplates').data('stdPlashes')) {
            new stdPlashes($(variant).find('.emailTemplates'));
        }

        $(variant).find('.emailTemplates').on('select', function() {
            that.updateModeView();
            that.updateCss();
            that.updateIframe();
        });
        that.updateModeView();
    };

    this.initContentSection = function() {
        $editorContainer.on('click', '.button', function() {
            $(this).closest('.selectorContainer').find('.button').removeClass('active');
            $(this).addClass('active');

            var $tabContainer = $(this).closest('.selectorTabContainer');
            var $tabsInsideIncludedItems = $tabContainer.find('.jsItem .innerTab');

            $tabContainer.find('.innerTab').not($tabsInsideIncludedItems).hide();
            $tabContainer.find('.' + $(this).data('value')).show();
        });

        $editorContainer.find('.templates input, .templates textarea, .templates select').attr('disabled', 'disabled');

        $editorContainer.find('.activeItems .jsItem').each(function(i, v) {
            that.initItem($(v));
        });

        $editorContainer.find('.emailEditor .activeItems').sortable({
            handle: '.sortButton',
            update: function() {
                that.updatePreview();
            }
        });

        $editorContainer.find('.jsEmailTemplateCustomMode .jsEmailMessageTextContainer textarea').on('change', function() {
            that.updateIframe();
        });


        var $typesContainer = $editorContainer.find('.typesContainer');
        $editorContainer.find('.jsAddButton').click(function(event) {
            event.stopPropagation();

            if ($typesContainer.is(':visible')) {
                $typesContainer.fadeOut(200);
            } else if (event.which == 1) {
                $typesContainer.css('top', "40px")
                    .fadeIn(200);
            }
        });

        $typesContainer.find('.jsRemoveButton').mousedown(function() {
            $typesContainer.fadeOut(200);
        });

        $typesContainer.find('a').click(function() {
            var target = $(this).data('target');

            if (target) {
                that.addItem(target)
            }
            $typesContainer.fadeOut(200);
            return false;
        });

        $(document).on('mousedown', function() {
            $typesContainer.fadeOut(200);
        });

        $typesContainer.mousedown(function(event) {
            event.stopPropagation();
        });
    };

    this.initStyleSection = function() {
        var $colorpicker = $editorContainer.find('.emailColor .baseBackgroundFormElement');
        new ContentEmailColorPicker($colorpicker, $previewContainer.parent(), 'background');

        var $colorpicker2 = $editorContainer.find('.emailColor .bodyColorFormElement');
        new ContentEmailColorPicker($colorpicker2, $previewContainer, 'background');

        var $styleColorpickers = $editorContainer.find('.emailColor input.colorpickerField');
        $styleColorpickers.each(function(i, element) {
            var $input = $(element);
            new ContentEmailColorPicker($input, null, 'color');
            $input.on('colorUpdated', function() {
                that.updateCss();
                that.updateIframe();
            });
            // trigger event for last element only to avoid redundant updates
            if (i == $styleColorpickers.length - 1) {
                $input.trigger('colorUpdated');
            }
        });

        var $styleSelectors = $editorContainer.find('.emailColor .jsItem select');
        $styleSelectors.each(function(i, element) {
            var $element = $(element);
            $element.change(function() {
                that.updateCss();
                that.updateIframe();
            });
            // trigger event for last element only to avoid redundant updates
            if (i == $styleSelectors.length - 1) {
                $element.change();
            }
        });

        $editorContainer.find('.jsCss textarea').change(function () {
            that.updateCss();
            that.updateIframe();
        });

        $editorContainer.on('messageUpdated', function(event, data) {
            $editorContainer.find('.emailColor .jsItem select').trigger('change', data);
            $editorContainer.find('.emailColor input.colorpickerField')
                .trigger('colorUpdated', data);
        });
    };

    this.initPreview = function() {
        var iframe = $previewContainer.find('iframe');
        var iframeCustom = $previewContainerCustom.find('iframe');

        this.initIframe(iframe);
        this.initIframe(iframeCustom);

        commonCss = $(iframe).data('style');

        $container.on('click', '.emailPreviewSelectorContainer .button', function() {
            var $buttons = $(this).closest('.selectorContainer').find('.button');
            $buttons.removeClass('active');
            $(this).addClass('active');

            that.updateCss();
            that.updateIframe();
        });

        $container.on('click', '.emailBrowserPreviewContainer .button, .emailTextPreviewContainer .button', function() {
            var width = ($(variant).find('.emailPreviewSelectorContainer .button.active').data('value') == 'website') ? 700 : 320;
            var preview = window.open('', '', 'width=' + width + ', left=' + ((screen.width - width) / 2) + ', height=' + screen.height + ', toolbar=0, menubar=0, location=0, status=0');
            preview.document.open();
            preview.document.write('<div style="text-align: center">' + t('all', 'Common__Window_loading') + '</div>');
            preview.document.close();
            var url = API_URL + '/push/campaign/preview-email?project_id=' + window.xp.project.id + '&variant_key=' + $(variant).data('key');
            var isText = $(this).parent().hasClass('emailTextPreviewContainer');
            if (isText) url += '&text=1';
            var options = {
                url: url,
                success: function(data) {
                    preview.document.open();
                    preview.document.write(data);
                    preview.document.close();
                }
            };
            $(this).parents('form').ajaxSubmit(options);
        });
    };


    /* ITEMS */

    this.addItem = function(type) {
        var $item = $editorContainer.find('.templates .' + type).hide().clone();
        $editorContainer.find('.activeItems').append($item);

        var $staticIdElem = $editorContainer.find(".activeItems");
        $staticIdElem.attr("static-id", $staticIdElem.attr("static-id") == undefined
            ? $editorContainer.find(".activeItems .jsItem").length - 1
            : parseInt($staticIdElem.attr("static-id")) + 1);

        $item.fadeIn(200);
        that.updateItemsIndexes();
        that.initItem($item);
    };

    this.copyItem = function (elem) {
        var $parent = $(elem).closest('.jsItem');
        var $item = $editorContainer.find('.templates .' + $(elem).data('target')).hide().clone();

        // Copy html for rows for social item
        if ($item.find('.formElementType').val() == 'social') {
            var rows = $parent.find('.activeRows').clone();
            rows.find('input,select,textarea').each(function () {
                var itemName = $(this).attr('name').replace(/^(.+\[items\]\[)\d+(\].+$)/, "$1" + "$2");
                $(this).attr('name', itemName).removeAttr('static-id');
            });
            $item.find('.activeRows').html(rows.html());
        }

        // Copy values to templates for editable elements
        $item.find('input,select,textarea').each(function () {
            var itemName = $(this).attr('name');
            if (itemName) {
                var parentName = itemName.replace(/^(.+\[items\]\[)(\].+$)/, "$1" + $parent.attr('static-id') + "$2");
                $(this).val($parent.find('[name="' + parentName + '"]').val());
            }
        });
        $parent.after($item);

        var $staticIdElem = $editorContainer.find(".activeItems");
        $staticIdElem.attr("static-id", $staticIdElem.attr("static-id") == undefined
            ? $editorContainer.find(".activeItems .jsItem").length - 1
            : parseInt($staticIdElem.attr("static-id")) + 1);

        $item.fadeIn(200);
        that.updateItemsIndexes();
        that.initItem($item);
        that.updatePreview();
    };

    this.initItem = function($item) {
        $item.find('textarea,input,select').removeAttr('disabled');
        var type = $item.find('.formElementType').val();
        var staticId = $item.find('.formElementType').attr('static-id');
        var $previewDiv = $('<div/>').attr('static-id', staticId).addClass('item').addClass(type).css('overflow', 'hidden');
        var $previewDivContent = $('<div/>').addClass('content');
        var targetObject = null;
        $previewDiv.append($previewDivContent);
        $previewContainerHidden.append($previewDiv);
        switch (type) {
            case 'image':
                targetObject = new ContentEmailImageItem(that, $item, $previewDivContent);
                break;
            case 'message':
                targetObject = new ContentEmailMessageItem(that, $item, $previewDivContent);
                break;
            case 'button':
                targetObject = new ContentEmailButtonItem(that, $item, $previewDivContent);
                break;
            case 'social':
                targetObject = new ContentEmailSocialItem(that, $item, $previewDivContent);
                break;
            case 'separator':
                targetObject = new ContentEmailSeparatorItem(that, $item, $previewDivContent);
                break;
        }
        if (targetObject !== null)
            targetObject.render();
        activeElements[staticId] = targetObject;

        $item.find('.jsRemoveButton.removeItem').click(function() {
            $item.fadeOut(200, function() {
                $item.remove();
                $previewDiv.remove();
                that.updateIframe();
            });
        });

        $item.find('.jsCopyButton.copyButton').click(function() {
            that.copyItem(this);
        });

        $item.find('input.colorpickerField').on('colorUpdated', function() {
            that.updateCss();
            that.updateIframe();
        });
    };

    this.updateItemsIndexes = function() {
        var $staticIdElem = $editorContainer.find(".activeItems");
        var staticId = $staticIdElem.attr("static-id");

        $editorContainer.find('.activeItems .jsItem').each(function() {
            if ($(this).attr('static-id') == undefined) {
                $(this).attr('static-id', staticId);
            }
            $(this).find('input,select,textarea').each(function() {
                if ($(this).attr('static-id') == undefined) {
                    $(this).attr('static-id', staticId);
                    var fieldName = $(this).attr('name');
                    if (fieldName != undefined) {
                        fieldName = fieldName.replace(/^(.+\[items\]\[)(\].+$)/, "$1" + staticId + "$2");
                        $(this).attr('name', fieldName);
                    }
                }
            });
        });
    };


    /* ATTACHMENTS */

    this.initAttachmentSection = function () {
        $editorContainer.find('.jsAttachments .jsAttachment').each(function(index, item) {
            that.initAttachment($(item));
        });

        $editorContainer.find('.jsAttachments').sortable({
            handle: '.sortButton',
            update: function() {
                that.updateAttachmentIndexes();
            }
        });

        $editorContainer.find('.jsAttachmentAdd').click(function () {
            that.addAttachment();
        });
    };

    this.addAttachment = function(type) {
        var $item = $editorContainer.find('.jsAttachmentTemplate .jsAttachment').hide().clone();
        $editorContainer.find('.jsAttachments').append($item);

        that.updateAttachmentIndexes();
        that.initAttachment($item);
        $item.fadeIn(200);
    };

    this.initAttachment = function($item) {
        $item.find('input').removeAttr('disabled');

        $item.find('.jsAttachmentRemove').click(function() {
            $item.fadeOut(200, function() {
                $item.remove();
                that.updateAttachmentIndexes();
            });
        });

        $item.find('.fake-input-file').dropzone({
            url: "/push/campaign/upload-attachment?project_id="+window.xp.project.id,
            paramName: "attachment",
            params: {
                _csrf: $item.parents("form").find('input[type=hidden][name=_csrf]').val()
            },
            success: function(file, response) {
                if (response) {
                    console.log(response);
                    $item.find('.jsAttachmentFile').val(response.file);
                    $item.find('.jsAttachmentName').val(file.name);
                    $item.find('.jsAttachmentUpload').hide();
                    $item.find('.jsAttachmentDownload').show();
                } else {
                    $item.find('.jsAttachmentFile').val("");
                    $item.find('.jsAttachmentName').val("");
                    $item.find('.jsAttachmentUpload').show();
                    $item.find('.jsAttachmentDownload').hide();
                    alert("Error uploading file");
                }
            }
        });

        $item.find('.jsAttachmentUpload').click(function () {
            $(this).parent().click();
        });

        $item.find('.jsAttachmentDownload').click(function () {
            window.location = '/push/campaign/download-attachment?project_id='+window.xp.project.id +
                '&file='+encodeURIComponent($item.find('.jsAttachmentFile').val()) +
                '&name='+encodeURIComponent($item.find('.jsAttachmentName').val());
        });
    };

    this.updateAttachmentIndexes = function() {
        var index = 0;
        $editorContainer.find('.jsAttachments .jsAttachment').each(function() {
            $(this).find('.jsAttachmentNumber').text(index + 1);
            $(this).find('input').each(function() {
                var fieldName = $(this).attr('name');
                if (fieldName != undefined) {
                    fieldName = fieldName.replace(/^(.+\[attachments\]\[)(\].+$)/, "$1" + index + "$2");
                    $(this).attr('name', fieldName);
                }
            });
            index++;
        });
    };


    /* PREVIEW */

    this.updateCss = function() {
        previewCss = '';

        $editorContainer.find('.emailColor .jsItem').each(function(i, item) {
            var tag = $(item).data('tag');
            if (tag == 'text') tag = '.message';
            if (tag == 'link') tag = 'a';
            var cssRule = ' ' + tag + ' { ';
            $(item).find('input.colorpickerField, select').each(function(i, field) {
                if ($(field).val()) {
                    cssRule += $(field).data('property') + ': ' + $(field).val() + '; ';
                }
            });
            cssRule += '}';
            previewCss += cssRule;
        });

        // set links colors ('a' tags)
        $editorContainer.find('.activeItems > .jsMessageItem').each(function(i, elem) {
            var $elem = $(elem);
            var staticId = $elem.attr('static-id');
            var color = $elem.find('.linkColor').val();
            if (color.length) {
                previewCss += ' div[static-id="' + staticId + '"] a {color: ' + color + '} ';
            }
        });

        previewCss += $editorContainer.find('.jsCss textarea').val();

        var maxWidth;
        if ($(variant).find('.emailPreviewSelectorContainer .button.active').data('value') == 'website') {
            maxWidth = 600;
        } else {
            maxWidth = 320;
        }

        if ($(variant).find('.emailTemplates input:checked').val() == EMAIL_TEMPLATE_CENTER) {
            previewCss += '.container { padding: 8px; }';
            previewCss += ' .item { max-width: ' + maxWidth + 'px; margin: 0 auto; box-sizing: border-box; }';
            previewCss += ' .message .content { margin-left: 16px; margin-right: 16px; }';
        } else if ($(variant).find('.emailTemplates input:checked').val() == EMAIL_TEMPLATE_FULL_WIDTH) {
            previewCss += ' .content { max-width: ' + maxWidth + 'px; margin: 0 auto; box-sizing: border-box; }';
            previewCss += ' .message .content { max-width: ' + (maxWidth - 32) + 'px; }';
        } else if ($(variant).find('.emailTemplates input:checked').val() == EMAIL_TEMPLATE_CUSTOM) {
            previewCss = '';
        }

        $previewContainerCustom.css('width', maxWidth+'px');
    };

    this.updatePreview = function() {
        var $items = $editorContainer.find('.activeItems .jsItem');

        $items.each(function(i, v) {
            var $item = $(v);
            var staticId = $item.attr('static-id');
            $previewContainerHidden.append($previewContainerHidden.find('div[static-id=' + staticId + ']'));
        });

        this.updateIframe();
    };

    this.updateIframe = function() {
        var html, iframe;
        if ($(variant).find('.emailTemplates input:checked').val() == EMAIL_TEMPLATE_CUSTOM) {
            iframe = $previewContainerCustom.find('iframe');
            html = $editorContainer.find('.jsEmailTemplateCustomMode .jsEmailMessageTextContainer textarea').val();
        } else {
            iframe = $previewContainer.find('iframe');
            html = '<style type="text/css">' + commonCss + previewCss + '</style>' + $previewContainerHidden.parent().html();
        }
        that.fillIframe(iframe, html);
    };

    this.updateModeView = function() {
        if ($(variant).find('.emailTemplates input:checked').val() == EMAIL_TEMPLATE_CUSTOM) {
            $editorContainer.find('.jsEmailTemplateStandardMode').hide();
            $editorContainer.find('.jsEmailTemplateCustomMode').show();
            $previewContainer.parent().hide();
            $previewContainerCustom.parent().show();
        } else {
            $editorContainer.find('.jsEmailTemplateCustomMode').hide();
            $editorContainer.find('.jsEmailTemplateStandardMode').show();
            $previewContainerCustom.parent().hide();
            $previewContainer.parent().show();
        }
    };


    /* HELPERS */

    this.initIframe = function(iframe) {
        var loadListener = function() {
            that.resizeIframe(iframe);
            iframe[0].removeEventListener("load", loadListener);
        };
        iframe[0].addEventListener("load", loadListener);
    };

    this.fillIframe = function(iframe, html) {
        var iFrameDoc = iframe[0].contentDocument || iframe[0].contentWindow.document;
        iFrameDoc.write(html);
        iFrameDoc.close();
        that.resizeIframe(iframe);
    };

    this.resizeIframe = function(iframe) {
        var iFrameDoc = iframe[0].contentDocument || iframe[0].contentWindow.document;
        var el = $(iFrameDoc).find('.emailPreviewIframeContainer');
        if (!el.length) el = $(iFrameDoc).find('body');
        $(iFrameDoc).trigger('resize');
        $(iframe).innerHeight(el.prop('scrollHeight'));
    };

    this.getEditorContainer = function() {
        return $editorContainer;
    };

    this.getVariant = function() {
        return $(variant);
    };

    return this.init();
};
