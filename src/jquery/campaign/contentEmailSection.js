import ContentSection from './contentSection'
import ContentEmailSectionXp from './contentEmailSectionXp'
import ContentEmailSectionBee from './contentEmailSectionBee'
import ContentEditor from '@/jquery/widgets/contentEditor'

export default function (campaignPage) {
  var that = new ContentSection(campaignPage)

  var $campaignPage = null
  var $contentSection = null

  that.init = function() {
    $campaignPage = campaignPage.getLayout()
    $contentSection = $campaignPage.find('#content-email_section')
    if (!$contentSection.length) return null

    that.initSection($contentSection)

    return that
  }

  that.initVariant = function(variant) {
    that.initHeading(variant)

    if ($(variant).find('.jsTemplate').data('email-type') == 1) {
      new ContentEmailSectionBee(campaignPage, $contentSection, variant);
    } else {
      new ContentEmailSectionXp(campaignPage, $contentSection, variant);
    }
  }

  that.initHeading = function (variant) {
    if (!$(variant).data('init-heading')) {
      $(variant).data('init-heading', 1);

      new ContentEditor($(variant).find('.jsEmailSubjectContainer'), {
        personalization: true,
        emojis: true,
        snippets: $(variant).data('twig')
      });

      that.initEmailFields(variant);
    }
  }

  that.initEmailFields = function(variant) {
    let $fromName = $(variant).find('.pushEmailFromName');
    let $fromAddress = $(variant).find('.pushEmailFromAddress');
    let $replyToName = $(variant).find('.pushEmailReplyToName');
    let $replyToAddress = $(variant).find('.pushEmailReplyToAddress');

    $fromAddress.richAutocomplete({
      source: '/email/email-address/autocomplete?project_id=' + window.xp.project.id,
      select: function(event, ui) {
        if (ui.item.name && ui.item.value) {
          $fromName.add($replyToName.filter('[data-linked]')).val(ui.item.name);
          $replyToAddress.filter('[data-linked]').val(ui.item.value);
        }
      },
      searchOnFocus: {
        allowEmptyValue: true,
        allowFilledValue: true
      }
    });

    $fromAddress.on('change', function() {
      $replyToAddress.filter('[data-linked]').val($(this).val());
    });

    $fromName.on('change', function() {
      $replyToName.filter('[data-linked]').val($(this).val());
    });

    $replyToName.filter('[data-linked]').add($replyToAddress.filter('[data-linked]')).on('change', function() {
      $(this).removeAttr('data-linked');
    });
  };

  return that.init()
}
