import ContentSection from './contentSection'
import ContentEditor from '@/jquery/widgets/contentEditor'
import { twig } from 'vendor/twig/twig'

export default function (campaignPage) {

    var that = new ContentSection(campaignPage);

    var $campaignPage,
        $contentSection
            = null;

    var twBodyItem,
        twHeadersItem,
        twGetItem
            = null;

    that.init = function() {
        $campaignPage = campaignPage.getLayout();
        $contentSection = $campaignPage.find('#content-webhook_section');
        if (!$contentSection.length) return null;

        that.initSection($contentSection);
        return that;
    };

    that.initVariant = function(variant) {
        that.initClickAction(variant);
        that.initBodyItems(variant);
        that.initHeadersItems(variant);
        that.initGetItems(variant);
        that.updatePreview(variant);

        $contentSection.on('section-tab-click', function() {
            that.updatePreview(variant);
        });

        $(variant).on('change keyup', '.webhookUrl, .clonedFieldKey, .clonedFieldValue, .jsWebhookRaw', function(event) {
            that.updatePreview(variant);
        });

        $(variant).data('preview', $(variant).find('.jsMessagePreview').vue());
    };

    that.initClickAction = function(variant) {
        $(variant).find('.action-selector').on('change', function() {
            var value = $(this).val();
            $(this).parents('.input-controlBox').find('.choice').each(function(i, item) {
                $(item).hide();
                if (!$(item).hasClass('choice-' + value)) {
                    $(item).find('input[type=text]').attr("disabled", "disabled");
                    $(item).find('select').attr("disabled", "disabled");
                    $(item).find('textarea').attr("disabled", "disabled");
                }
            });
            $(this).parents('.input-controlBox').find('.choice-' + value).show()
                .find('input,select,textarea').removeAttr("disabled");
            that.updatePreview(variant);
        }).trigger('change');
    };

    that.initBodyItems = function(variant) {
        if (!twBodyItem) {
            twBodyItem = twig({
                id: "webhookBodyListItem",
                href: VIEWS_DIR + "/push/views/campaign/templates/content-webhook-body-item.twig",
                async: false
            });
        }

        that.addBodyItem = function(elem, type) {
            var index = parseInt($(elem).parents('.choice').find('.jsWebhookItem:last-child').data('index')) + 1;
            if (isNaN(index)) index = 0;
            var template = twBodyItem.render({
                message_type: $(elem).parents('.jsMessage').data('type'),
                variant_key: $(elem).parents('.jsMessageVariant').data('key'),
                index: index,
                type: type
            });
            $(elem).parents('.choice').find('.jsWebhook').show().append(template);
            that.initEditor($(elem).parents('.choice').find('.jsWebhook .jsWebhookItem:last-child .jsWebhookFieldValue'));
        };

        $(variant).find('.jsAddWebhookPostItem').on('click', function(e) {
            that.addBodyItem(this, 'post');
        });

        $(variant).find('.jsAddWebhookJsonItem').on('click', function(e) {
            that.addBodyItem(this, 'json');
        });

        $(variant).on('click', '.jsDeleteWebhookItem', function(e) {
            var list = $(this).parents('ul');
            $(this).parents('li').eq(0).remove();
            if ($(list).find('li').length == 0) {
                $(list).hide();
            }
            that.updatePreview(variant);
        });
    };

    that.initHeadersItems = function(variant) {
        if (!twHeadersItem) {
            twHeadersItem = twig({
                id: "webhookHeadersListItem",
                href: VIEWS_DIR + "/push/views/campaign/templates/content-webhook-headers-item.twig",
                async: false
            });
        }

        $(variant).find('.jsAddWebhookHeadersItem').on('click', function(e) {
            var index = parseInt($(this).parents('.jsWebhookHeaders').find('.jsWebhookItem:last-child').data('index')) + 1;
            if (isNaN(index)) index = 0;
            var template = twHeadersItem.render({
                message_type: $(this).parents('.jsMessage').data('type'),
                variant_key: $(this).parents('.jsMessageVariant').data('key'),
                index: index
            });
            $(this).parents('.jsWebhookHeaders').find('.jsWebhook').show().append(template);
            that.initEditor($(this).parents('.jsWebhookHeaders').find('.jsWebhook .jsWebhookItem:last-child .jsWebhookFieldValue'));
        });

        $(variant).on('click', '.jsDeleteWebhookHeadersItem', function(e) {
            var list = $(this).parents('ul');
            $(this).parents('li').eq(0).remove();
            if ($(list).find('li').length == 0) {
                $(list).hide();
            }
            that.updatePreview(variant);
        });
    };

    that.initGetItems = function(variant) {
        if (!twGetItem) {
            twGetItem = twig({
                id: "webhookGetListItem",
                href: VIEWS_DIR + "/push/views/campaign/templates/content-webhook-get-item.twig",
                async: false
            });
        }

        $(variant).find('.jsAddWebhookGetItem').on('click', function(e) {
            var index = parseInt($(this).parents('.jsWebhookUrl').find('.jsWebhookItem:last-child').data('index')) + 1;
            if (isNaN(index)) index = 0;
            var template = twGetItem.render({
                message_type: $(this).parents('.jsMessage').data('type'),
                variant_key: $(this).parents('.jsMessageVariant').data('key'),
                index: index
            });
            $(this).parents('.jsWebhookUrl').find('.jsWebhook').show().append(template);
            that.initEditor($(this).parents('.jsWebhookUrl').find('.jsWebhook .jsWebhookItem:last-child .jsWebhookFieldValue'));
        });

        $(variant).on('click', '.jsDeleteWebhookGetItem', function(e) {
            var list = $(this).parents('ul');
            $(this).parents('li').eq(0).remove();
            if ($(list).find('li').length == 0) {
                $(list).hide();
            }
            that.updatePreview(variant);
        });
    };

    that.updatePreview = function(variant) {
        var url = $(variant).find('.jsWebhookUrlContainer').find('input.webhookUrl').val();
        var getParams = [];
        var urlParams = $(variant).find('.jsWebhookUrl').find('.jsWebhookItem');
        if (urlParams.length) {
            urlParams.each(function(i, elem) {
                if ($(elem).find('.clonedFieldKey').val()) {
                    getParams.push($(elem).find('.clonedFieldKey').val() + '=' + $(elem).find('.clonedFieldValue').val());
                }
            });
            if (url) {
                var divider = url.indexOf('?') > -1 ? '&' : '?';
                url = url + divider + getParams.join('&');
            }
        }
        if (!url) url = '';

        var previewHeaders = '';
        var headers = $(variant).find('.jsWebhookHeaders').find('.jsWebhookItem');
        if (headers.length) {
            headers.each(function(i, elem) {
                if ($(elem).find('.clonedFieldKey').val() && $(elem).find('.clonedFieldValue').val()) {
                    previewHeaders += $(elem).find('.clonedFieldKey').val() + ': ' + $(elem).find('.clonedFieldValue').val() + '<br/>';
                }
            });
        }
        if (!previewHeaders) previewHeaders = '';

        var previewBody;
        if ($(variant).find('.jsWebhookRaw:visible').length) {
            previewBody = $(variant).find('.jsWebhookRaw:visible').val();
        } else if ($(variant).find('.jsWebhookPost:visible').length) {
            var body = $(variant).find('.jsWebhookPost:visible').find('.jsWebhookItem');
            if (body.length) {
                previewBody = [];
                body.each(function(i, elem) {
                    if ($(elem).find('.clonedFieldKey').val()) {
                        previewBody.push($(elem).find('.clonedFieldKey').val() + '=' + $(elem).find('.clonedFieldValue').val());
                    }
                });
                previewBody = previewBody.join('&');
            }
        } else if ($(variant).find('.jsWebhookJson:visible').length) {
            var body = $(variant).find('.jsWebhookJson:visible').find('.jsWebhookItem');
            if (body.length) {
                previewBody = {};
                body.each(function(i, elem) {
                    if ($(elem).find('.clonedFieldKey').val()) {
                        previewBody[$(elem).find('.clonedFieldKey').val()] = $(elem).find('.clonedFieldValue').val();
                    }
                });
                previewBody = $.isEmptyObject(previewBody) ? null : JSON.stringify(previewBody);
            }
        }
        if (!previewBody) previewBody = ''; else previewBody = '<br>' + previewBody;

        let preview = $(variant).data('preview');
        if (preview) {
            preview.message = {
                url: url,
                headers: previewHeaders,
                body: previewBody
            }
        }
    };

    that.initEditor = function(elem) {
        new ContentEditor(elem, {
            personalization: true
        });
    };

    return that.init();
};
