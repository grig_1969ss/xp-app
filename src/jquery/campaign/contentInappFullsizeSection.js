export default function (contentInapp) {

    var that = this;
    var TYPE_FULLSIZE = '1';

    this.initVariant = function(variant) {
        $(variant).find('.jsButton.backgroundButton input, .jsButton.backgroundButton select').each(function() {
            var fieldName = $(this).attr('name');
            fieldName = fieldName.replace(/^(.+\[)buttons\]\[button_(\].+$)/, "$1background$2");
            $(this).attr('name', fieldName);
        });

        $(variant).find('.fullsize.tab .background .mainSelect, .fullsize.tab .jsButtons .mainSelect')
                .trigger('change');

        $(variant).find('.fullsize.tab .jsAddButton').click(function() {
            var static_id_elem = $(variant).find(".fullsize.tab .jsButtons");
            static_id_elem.attr("static-id", static_id_elem.attr("static-id") == undefined ? 
                $(variant).find(".fullsize.tab .jsButton").length :
                parseInt(static_id_elem.attr("static-id")) + 1);
        
            var baseButtonElement = $(variant).find('.jsButtonTemplate .jsButton');
            var newButtonElement = baseButtonElement.clone();
            $(variant).find('.fullsize.tab .jsButtons').append(newButtonElement);
            contentInapp.updateItemsIndexes(variant, newButtonElement);
            contentInapp.initItem(variant, newButtonElement);
        });
        
        $(variant).on('click', '.fullsize.tab .jsButtons .jsRemoveButton', function() {
            var button = $(this).closest('.jsButton');
            var static_id = $(button).find('.imageFile').attr('static-id');
            $(variant).find('.container:visible .button.static-id-' + static_id).fadeOut(200, function() {
                $(this).remove();
            });
            button.fadeOut(200, function() {
                $(this).removeClass('jsItem');
                contentInapp.updateItemsIndexes(variant, button);
                $(this).remove();
            });
        });

        return this;
    };

    this.createButtonImage = function(variant, button, filename, left, top, orientation) {
        var button_image = $('<img style="display: none;" />');
        if (filename != undefined) {
            button_image.attr('src', filename);
        }
        var params = contentInapp.getScreenParams(variant, orientation);
        orientation = params.or;
        
        button_image.load(function() {
            var screen_width = params.width;
            var screen_height = params.height;
            var container = $(variant).find('.inappPreview .outerContainer.' + orientation + ' .container');
            var input = $(variant).find(".bgImageFile." + orientation);
            var resize = input.attr("resize-coef") || 1;
            
            // remove info about previous the button, if overwritten
            var static_id = $(button).find('.imageFile').attr('static-id');
            container.find('.button.static-id-' + static_id).remove();
            
            $(this).css('position', 'absolute').css('z-index', static_id)
                .attr('static-id', static_id).attr("original-width", this.width)
                .addClass('button').addClass('static-id-' + static_id);
            container.append(this);
            
            if (input.val() == "" || input.val() == undefined) {
                var original_width = screen_width;
                var original_height = screen_height;
            } else {
                var original_width = input.attr('original-width');
                var original_height = input.attr('original-height');
            }
            
            if (left == "" || top == "" || left == undefined || top == undefined) {
                var pixels = that.percentsToPixels(variant, $(this), 
                        // bind new button to the center on the background container
                        (original_width - this.width) / (2 * original_width) * 100, 
                        (original_height - this.height) / (2 * original_height) * 100);
                var button_left = pixels.left;
                var button_top = pixels.top;
            } else {
                // left an top values are in percents; convert them to pixels
                var pixels = that.percentsToPixels(variant, $(this), left, top);
                var button_left = pixels.left;
                var button_top = pixels.top;
            }
            $(this).css('left', button_left + "px").css('top', button_top + "px")
                    .width($(this).attr("original-width") / resize)
                    .fadeIn(300); 
                        
            button.find('.imageFile').val(filename);
            
            // left and top values are in pixels; convert them to percents
            var percents = that.pixelsToPercents(variant, $(this), button_left, button_top);
            button.find('input.left').val(percents.left);
            button.find('input.top').val(percents.top);
            var offsetX, offsetY, ccx, ccy, delta = 3, b_coords = [ ], line_color = "yellow";
            $(this).draggable({
                containment: container,
                scroll: false,
                start: function(event, ui) {
                    offsetX = event.offsetX; offsetY = event.offsetY;
                    ccx = container.width() / 2;  ccy = container.height() / 2;
                    // look for the buttons in container except the 
                    var buttons = container.find('.button[static-id!=' + static_id + ']');
                    buttons.each(function(i, button) {
                        var pos = $(button).position();
                        var x0 = pos.left,      x1 = pos.left + $(button).width();
                        var y0 = pos.top,       y1 = pos.top + $(button).height();
                        var xc = (x0 + x1) / 2, yc = (y0 + y1) / 2;
                        b_coords.push({
                            x0: x0, y0: y0,
                            x1: x1, y1: y1,
                            xc: xc, yc: yc,
                            static_id: $(button).attr("static-id")
                        });
                    });
                },
                drag: function(event, ui) {
                    var $this = $(this);
                    var thisPos = $(this).position();
                    var parentPos = container.position();
                    var parentOffset = container.offset();
                    var x0 = Math.round(event.pageX - parentOffset.left - offsetX);
                    var y0 = Math.round(event.pageY - parentOffset.top - offsetY);
                    var x1 = x0 + $(this).width();
                    var y1 = y0 + $(this).height();
                    var xc = (x0 + x1) / 2;
                    var yc = (y0 + y1) / 2;
                    
                    var right_border = container.width() - $(this).width();
                    var bottom_border = container.height() - $(this).height();
                    if (x0 >= 0 && y0 >= 0 && x1 <= container.width() && y1 <= container.height()) {
                        ui.position.left = x0;
                        ui.position.top = y0;
                    }
                    $.each(b_coords, function(i, coords) {
                        if (Math.abs(x0 - coords.x0) <= delta) {
                            if (!container.find('.button-' + coords.static_id + '.x0').length) {
                                var assist_line = $('<div class="assist-line button-' + coords.static_id 
                                        + ' x0 x"></div>');
                                container.append(assist_line);
                                $(assist_line).css("width", "1px").css("height", "100%").css("background", line_color)
                                        .css("position", "absolute").css("border", "none")
                                        .css("top", "0px").css("left", coords.x0 + "px").css("z-index", 9999);
                            }
                            ui.position.left = coords.x0;
                        } else {
                            container.find('.button-' + coords.static_id + '.x0').remove();
                        }
                        if (Math.abs(x1 - coords.x1) <= delta) {
                            if (!container.find('.button-' + coords.static_id + '.x1').length) {
                                var assist_line = $('<div class="assist-line button-' + coords.static_id 
                                        + ' x1 x"></div>');
                                container.append(assist_line);
                                $(assist_line).css("width", "1px").css("height", "100%").css("background", line_color)
                                        .css("position", "absolute").css("border", "none")
                                        .css("top", "0px").css("left", coords.x1 + "px").css("z-index", 9999);
                            }
                            ui.position.left = coords.x1 - $this.width();
                        } else {
                            container.find('.button-' + coords.static_id + '.x1').remove();
                        }
                        if (Math.abs(y0 - coords.y0) <= delta) {
                            if (!container.find('.button-' + coords.static_id + '.y0').length) {
                                var assist_line = $('<div class="assist-line button-' + coords.static_id 
                                        + ' y0 y"></div>');
                                container.append(assist_line);
                                $(assist_line).css("height", "1px").css("width", "100%").css("background", line_color)
                                        .css("position", "absolute").css("border", "none")
                                        .css("top", coords.y0 + "px").css("left", "0px").css("z-index", 9999);
                            }
                            ui.position.top = coords.y0;
                        } else {
                            container.find('.button-' + coords.static_id + '.y0').remove();
                        }
                        if (Math.abs(y1 - coords.y1) <= delta) {
                            if (!container.find('.button-' + coords.static_id + '.y1').length) {
                                var assist_line = $('<div class="assist-line button-' + coords.static_id 
                                        + ' y1 y"></div>');
                                container.append(assist_line);
                                $(assist_line).css("height", "1px").css("width", "100%").css("background", line_color)
                                        .css("position", "absolute").css("border", "none")
                                        .css("top", coords.y1 + "px").css("left", "0px").css("z-index", 9999);
                            }
                            ui.position.top = coords.y1 - $this.height();
                        } else {
                            container.find('.button-' + coords.static_id + '.y1').remove();
                        }
                        if (Math.abs(x1 - coords.x0) <= delta) {
                            if (!container.find('.button-' + coords.static_id + '.x1x0').length) {
                                var assist_line = $('<div class="assist-line button-' + coords.static_id 
                                        + ' x1x0 x"></div>');
                                container.append(assist_line);
                                $(assist_line).css("width", "1px").css("height", "100%").css("background", line_color)
                                        .css("position", "absolute").css("border", "none")
                                        .css("top", "0px").css("left", coords.x0 + "px").css("z-index", 9999);
                            }
                            ui.position.left = coords.x0 - $this.width();
                        } else {
                            container.find('.button-' + coords.static_id + '.x1x0').remove();
                        }
                        if (Math.abs(x0 - coords.x1) <= delta) {
                            if (!container.find('.button-' + coords.static_id + '.x0x1').length) {
                                var assist_line = $('<div class="assist-line button-' + coords.static_id 
                                        + ' x0x1 x"></div>');
                                container.append(assist_line);
                                $(assist_line).css("width", "1px").css("height", "100%").css("background", line_color)
                                        .css("position", "absolute").css("border", "none")
                                        .css("top", "0px").css("left", coords.x1 + "px").css("z-index", 9999);
                            }
                            ui.position.left = coords.x1;
                        } else {
                            container.find('.button-' + coords.static_id + '.x0x1').remove();
                        }
                        if (Math.abs(y1 - coords.y0) <= delta) {
                            if (!container.find('.button-' + coords.static_id + '.y1y0').length) {
                                var assist_line = $('<div class="assist-line button-' + coords.static_id 
                                        + ' y1y0 y"></div>');
                                container.append(assist_line);
                                $(assist_line).css("height", "1px").css("width", "100%").css("background", line_color)
                                        .css("position", "absolute").css("border", "none")
                                        .css("top", coords.y0 + "px").css("left", "0px").css("z-index", 9999);
                            }
                            ui.position.top = coords.y0 - $this.height();
                        } else {
                            container.find('.button-' + coords.static_id + '.y1y0').remove();
                        }
                        if (Math.abs(y0 - coords.y1) <= delta) {
                            if (!container.find('.button-' + coords.static_id + '.y0y1').length) {
                                var assist_line = $('<div class="assist-line button-' + coords.static_id 
                                        + ' y0y1 y"></div>');
                                container.append(assist_line);
                                $(assist_line).css("height", "1px").css("width", "100%").css("background", line_color)
                                        .css("position", "absolute").css("border", "none")
                                        .css("top", coords.y1 + "px").css("left", "0px").css("z-index", 9999);
                            }
                            ui.position.top = coords.y1;
                        } else {
                            container.find('.button-' + coords.static_id + '.y0y1').remove();
                        }
                        if (Math.abs(xc - coords.xc) <= delta) {
                            if (!container.find('.button-' + coords.static_id + '.xc').length) {
                                var assist_line = $('<div class="assist-line button-' + coords.static_id 
                                        + ' xc x"></div>');
                                container.append(assist_line);
                                $(assist_line).css("width", "1px").css("height", "100%").css("background", line_color)
                                        .css("position", "absolute").css("border", "none")
                                        .css("top", "0px").css("left", coords.xc + "px").css("z-index", 9999);
                            }
                            // stick not the side, but the center of the image to the assistant line
                            ui.position.left = coords.xc - $this.width() / 2;
                        } else {
                            container.find('.button-' + coords.static_id + '.xc').remove();
                        }
                        if (Math.abs(yc - coords.yc) <= delta) {
                            if (!container.find('.button-' + coords.static_id + '.yc').length) {
                                var assist_line = $('<div class="assist-line button-' + coords.static_id 
                                        + ' yc y"></div>');
                                container.append(assist_line);
                                $(assist_line).css("height", "1px").css("width", "100%").css("background", line_color)
                                        .css("position", "absolute").css("border", "none")
                                        .css("top", coords.yc + "px").css("left", "0px").css("z-index", 9999);
                            }
                            // stick not the side, but the center of the image to the assistant line
                            ui.position.top = coords.yc - $this.height() / 2;
                        } else {
                            container.find('.button-' + coords.static_id + '.yc').remove();
                        }
                    });
                    if (Math.abs(xc - ccx) <= delta) {
                        if (!container.find('.xcccx').length) {
                            var assist_line = $('<div class="assist-line xcccx x"></div>');
                            container.append(assist_line);
                            $(assist_line).css("width", "1px").css("height", "100%").css("background", line_color)
                                    .css("position", "absolute").css("border", "none")
                                    .css("top", "0px").css("left", ccx + "px").css("z-index", 9999);
                        }
                        // stick not the side, but the center of the image to the assistant line
                        ui.position.left = ccx - $this.width() / 2;
                    } else {
                        container.find('.xcccx').remove();
                    }
                    if (Math.abs(yc - ccy) <= delta) {
                        if (!container.find('.ycccy').length) {
                            var assist_line = $('<div class="assist-line ycccy y"></div>');
                            container.append(assist_line);
                            $(assist_line).css("height", "1px").css("width", "100%").css("background", line_color)
                                    .css("position", "absolute").css("border", "none")
                                    .css("top", ccy + "px").css("left", "0px").css("z-index", 9999);
                        }
                        // stick not the side, but the center of the image to the assistant line
                        ui.position.top = ccy - $this.height() / 2;
                    } else {
                        container.find('.ycccy').remove();
                    }
                    // left and top values are in pixels; convert them to percents
                    var percents = that.pixelsToPercents(variant, $(this), ui.position.left, ui.position.top);
                    button.find('.left').val(percents.left);
                    button.find('.top').val(percents.top);
                },
                stop: function(event, ui) {
                    container.find('.assist-line').remove();
                }
            });
        }).each(function() {
            if (this.complete) $(this).load();
        });
    };
    
    this.pixelsToPercents = function(variant, button_image, left, top, orientation) {
        var params = contentInapp.getScreenParams(variant, orientation);
        orientation = params.or;
        var screen_width = params.width;
        var screen_height = params.height;
        var container = $(variant).find('.inappPreview .outerContainer.' + orientation + ' .container');
        var input = $(variant).find(".bgImageFile." + orientation);
        
        var bg = (input.val() != "" && input.val() != undefined) ?
                container.find('.background') :
                container;
        var displayed_width = $(variant).find(".bgImageFile." + orientation)
                .attr("displayed-width") || screen_width;
        var displayed_height = $(variant).find(".bgImageFile." + orientation)
                .attr("displayed-height") || screen_height;

        var xdiff = Math.max(0, (displayed_width - $(container).width()) / 2);
        var ydiff = Math.max(0, (displayed_height - $(container).height()) / 2);

        var button_left = left || (parseInt($(button_image).css('left')));
        var button_top = top || (parseInt($(button_image).css('top')));

        left = (button_left + xdiff) / displayed_width * 100;
        top = (button_top + ydiff) / displayed_height * 100;

        return { left: left, top: top };
    };
    
    this.percentsToPixels = function(variant, button_image, left, top, orientation) {
        var params = contentInapp.getScreenParams(variant, orientation);
        orientation = params.or;
        var screen_width = params.width;
        var screen_height = params.height;

        var static_id = $(button_image).attr('static-id');
        if (left == undefined || top == undefined) {
            left = $(variant).find('.fullsize.tab .jsButtons .' + orientation
                    + ' input.left[static-id=' + static_id + ']').val();
            top = $(variant).find('.fullsize.tab .jsButtons .' + orientation
                    + ' input.top[static-id=' + static_id + ']').val();
        }

        var container = $(variant).find('.inappPreview .outerContainer.' + orientation + ' .container');
        var displayed_width = $(variant).find(".bgImageFile." + orientation)
                .attr("displayed-width") || screen_width;
        var displayed_height = $(variant).find(".bgImageFile." + orientation)
                .attr("displayed-height") || screen_height;

        var xdiff = Math.max(0, (displayed_width - $(container).width()) / 2);
        var ydiff = Math.max(0, (displayed_height - $(container).height()) / 2);

        var leftpx = Math.round(left / 100 * displayed_width - xdiff);
        var toppx = Math.round(top / 100 * displayed_height - ydiff);

        return { left: leftpx, top: toppx };
    };

    this.updateFullsizePreview = function(variant, filename, orientation, is_bg_updated, create_buttons) {
        if (!$(variant).data('initiated') || $(variant).find('.inappType input:radio:checked').val() != TYPE_FULLSIZE) {
            return false;
        }
        var params = contentInapp.getScreenParams(variant, orientation);
        orientation = params.or;
        if (orientation == "" || orientation == undefined) {
            return false;
        }
        var container = $(variant).find('.inappPreview .outerContainer.' + orientation + ' .container');
        var bg = container.find('img.background');
        var input = $(variant).find(".bgImageFile." + orientation);
        if (input.val() != "" && input.val() != undefined && input.attr("original-width") == undefined) {
            filename = input.val();
        }
        if (filename != undefined && filename != "") {
            var onload = function(_bg, ori) {
                return function() {
                    var container = $(variant).find('.inappPreview .outerContainer.' + ori + ' .container');
                    container.css('background', 'white');
                    container.find('img.watermark').hide();
                    container.find('.dz-preview').hide();
                    
                    $(_bg).css("width", "auto").css("height", "auto");
                    
                    // trick to get sizes of image in hidden div
                    _bg = $("<img />");
                    $("body").append(_bg);
                    _bg.off("load").attr("src", filename).one("load", function() {
                        $(variant).find(".bgImageFile." + ori)
                                .attr('original-width', $(_bg).width())
                                .attr('original-height', $(_bg).height())
                                .val(filename);
                        _bg.hide();
                        that.updateFullsizePreview(variant, undefined, ori, true, create_buttons);
                    }).each(function() {
                        if (this.complete) $(this).load();
                    });
                }
            };
            bg.off("load").attr("src", filename).one("load", onload(bg, orientation)).each(function() {
                if (this.complete) $(this).load();
            });
            
            return false;
        }
        var screen_width = params.width;
        var screen_height = params.height;
        
        if (input.val() == undefined || input.val() == "") {        
            container.css('background', '#1D87C8')
                    .css("width", screen_width + "px").css("height", screen_height + "px");
            container.find('.background').hide();
            
            var width = '50%';
            if (orientation == 'landscape') {
                var xdiff = (screen_width - screen_height) / 2;
                $(container).css("top", "0px").css("bottom", "0px")
                        .css("left", -xdiff + "px").css("right", xdiff + "px");
                width = '30%';
            }
            container.find('img.watermark')
                    .css('width', width).css('height', 'auto').show();
            container.find('span').show();
            
            return false;
        }
        if (bg.attr("src") != input.val()) {
            bg.attr("src", input.val()).show();
        }

        if (orientation == 'portrait') {
            var resize = Math.max(1, input.attr('original-height') / screen_height);
            input.attr("resize-coef", resize);
            if (resize > 1) {
                bg.height(screen_height).css("width", "auto");
            } else {
                bg.css("height", "auto").css("width", "auto");
                container.css("top", "0px").css("bottom", "0px").css("left", "0px").css("right", "0px");
            }
        } else {
            var resize = Math.max(1, input.attr('original-width') / screen_width);
            input.attr("resize-coef", resize);
            if (resize > 1) {
                bg.width(screen_width).css("height", "auto");
                var xdiff = (screen_width - screen_height) / 2;
                container.css("top", "0px").css("bottom", "0px")
                        .css("left", -xdiff + "px").css("right", xdiff + "px");
            } else {
                var img_width = input.attr("original-width");
                var img_height = input.attr("original-height");
                var xdiff = Math.max(0, ($(bg).width() - screen_height) / 2);
                container.css("top", "0px").css("bottom", "0px")
                        .css("left", -xdiff + "px").css("right", xdiff + "px");
            }
        }
        var orig_width = input.attr("original-width");
        var orig_height = input.attr("original-height");
        var displayed_width = orig_width / resize;
        var displayed_height = orig_height / resize;
        container.width(Math.min(displayed_width, screen_width))
                .height(Math.min(displayed_height, screen_height))
                .css("margin", "auto").css('min-height', '0px').css("background", "white");
                
        container.find("span").hide();
        container.find(".watermark").hide();
        container.find(".editorContainer").hide();
        container.find("iframe").hide();

        input.attr('displayed-width', displayed_width).attr('displayed-height', displayed_height);
        var xdiff = Math.round(Math.max(0, (displayed_width - container.width()) / 2));
        var ydiff = Math.round(Math.max(0, (displayed_height - container.height()) / 2));
        
        $(bg).css("left", -xdiff + "px").css("top", -ydiff + "px").css("position", "relative")
                .fadeIn(200);
        
        if (create_buttons) {
            // select buttons with uploaded images only
            var buttons = $(variant).find(".fullsize.tab .jsButtons .jsButton ."
                    + orientation + " .imageFile[value!='']").parents('.jsButton');
            $(buttons).each(function(i, button) {
                var input = $(button).find('.' + orientation + ' .fake-input-file');
                var filename = $(input).find('.imageFile').val();
                if (filename != undefined && filename != "") {
                    that.createButtonImage(variant, $(input), filename, $(input).find('.left').val(), 
                            $(input).find('.top').val(), orientation);
                }
            });
        } else {
            var buttons_images = $(variant).find('.inappPreview .outerContainer.'
                    + orientation + ' .container img.button');
            $(buttons_images).each(function(i, button_image) {
                if (is_bg_updated) {
                    var left = (parseInt($(button_image).css("left")) - container.width() / 2) / resize
                            + container.width() / 2;
                    var top = (parseInt($(button_image).css("top")) - container.height() / 2) / resize
                            + container.height() / 2;
                    var percents = that.pixelsToPercents(variant, button_image, left, top);
                    var static_id = $(button_image).attr("static-id");
                    $(variant).find(".fullsize.tab .jsButtons input.left[static-id=" + static_id + "]")
                            .val(percents.left);
                    $(variant).find(".fullsize.tab .jsButtons input.top[static-id=" + static_id + "]")
                            .val(percents.top);
                }
                var pixels = that.percentsToPixels(variant, button_image);
                $(button_image).width($(button_image).attr("original-width") / resize)
                        .css("left", pixels.left).css("top", pixels.top).fadeIn(200);
            });
        }
    };

    return this;
};
