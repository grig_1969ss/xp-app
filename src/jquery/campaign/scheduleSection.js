export default function (campaignPage) {
    var $campaignPage ,
        $scheduleSection,
        $startTime,
        $startTimeDatePicker,
        $endTime,
        $endTimeDatePicker,
        $sendTime,
        $sendTimeDatePicker,
        $startDate,
        $startDateDatePicker,
        $startDateTime,
        $startDateTimeDatePicker,
        $endDate,
        $endDateDatePicker,
        $endDateTime,
        $endDateTimeDatePicker,
        $endDateOptimove,
        $endDateOptimoveDatePicker,
        $endDateTimeOptimove,
        $endDateTimeOptimoveDatePicker,
        $sendDate,
        $sendDateDatePicker,
        $retryFor,
        $retryForPeriod,
        $retryUntilDate,
        $retryUntilDateDatePicker,
        $persistFor,
        $persistForPeriod,
        $persistUntilDate,
        $persistUntilDateDatePicker
            = null;

    this.init = function() {
        $campaignPage    = campaignPage.getLayout();
        $scheduleSection = $campaignPage.find('#schedule_section');
        if (!$scheduleSection.length) return null;

        $startTime = $scheduleSection.find("#PushMessage_startTime");
        $endTime   = $scheduleSection.find("#PushMessage_endTime");
        $sendTime  = $scheduleSection.find("#PushMessage_sendTime");
        $startDate = $scheduleSection.find("#PushMessage_startDate");
        $startDateTime = $scheduleSection.find("#PushMessage_startDateTime");
        $endDate   = $scheduleSection.find("#PushMessage_endDate");
        $endDateTime   = $scheduleSection.find("#PushMessage_endDateTime");
        $endDateOptimove = $scheduleSection.find("#PushMessage_endDateOptimove");
        $endDateTimeOptimove = $scheduleSection.find("#PushMessage_endDateTimeOptimove");
        $sendDate  = $scheduleSection.find("#PushMessage_sendDate");
        $retryFor = $scheduleSection.find("#PushMessage_retryFor");
        $retryForPeriod = $scheduleSection.find("#PushMessage_retryForPeriod");
        $retryUntilDate = $scheduleSection.find("#PushMessage_retryUntilDate");
        $persistFor = $scheduleSection.find("#PushMessage_persistFor");
        $persistForPeriod = $scheduleSection.find("#PushMessage_persistForPeriod");
        $persistUntilDate = $scheduleSection.find("#PushMessage_persistUntilDate");

        $scheduleSection.find('.ctmScheduleOptions input').on('select', function(){
            if ($(this).prop('checked')) {
                $scheduleSection.find('.sendOption').hide();
                $scheduleSection.find('.'+$(this).data('option')).show();

                let $optionToDisable = $scheduleSection.find('.sendOption.jsOptionToDisable');
                if ($optionToDisable.length) {
                    $optionToDisable.find('input, select').removeAttr('disabled');
                    $optionToDisable.filter(':not(.' + $(this).data('option') + ')').find('input, select').attr('disabled', true);
                }
            }
        }).trigger('select');

        $scheduleSection.find('.jsPrepareIntervalEnabled').on('change', function () {
            if ($(this).prop('checked')) {
              $scheduleSection.find('.jsPrepareIntervalWrapper').show();
            } else {
              $scheduleSection.find('.jsPrepareIntervalWrapper').hide();
              $scheduleSection.find('.jsPrepareInterval').val('');
            }
        });

        $scheduleSection.find('.jsRetryForToggle').on('deselect', function(){
            $retryFor.data('prev-value', $retryFor.val());
            $retryFor.val('');
        });
        $scheduleSection.find('.jsRetryForToggle').on('select', function(){
            if ($retryFor.data('prev-value')) {
                $retryFor.val($retryFor.data('prev-value'));
            }
        });

        $scheduleSection.find('.jsRetryUntilToggle').on('deselect', function(){
            $retryUntilDate.data('prev-value', $retryUntilDate.val());
            $retryUntilDate.val('');
        });
        $scheduleSection.find('.jsRetryUntilToggle').on('select', function(){
            if ($retryUntilDate.data('prev-value')) {
                $retryUntilDate.val($retryUntilDate.data('prev-value'));
            }
        });

        $scheduleSection.find('.jsPersistForToggle').on('deselect', function(){
            $persistFor.data('prev-value', $persistFor.val());
            $persistFor.val('');
        });
        $scheduleSection.find('.jssPersistForToggle').on('select', function(){
            if ($persistFor.data('prev-value')) {
                $persistFor.val($persistFor.data('prev-value'));
            }
        });

        $scheduleSection.find('.jsPersistUntilToggle').on('deselect', function(){
            $persistUntilDate.data('prev-value', $persistUntilDate.val());
            $persistUntilDate.val('');
        });
        $scheduleSection.find('.jsPersistUntilToggle').on('select', function(){
            if ($persistUntilDate.data('prev-value')) {
                $persistUntilDate.val($persistUntilDate.data('prev-value'));
            }
        });

        let datePicker = {
            dateFormat: "yy-mm-dd",
            altFormat: "yy-mm-dd"
        };

        let extendedDatePicker = function(timeField) {
            return $.extend({}, datePicker, {
                onSelect: function() {
                    if (timeField.length) {
                        timeField.val(timeField.data('default-value'));
                    }
                }
            });
        }

        let timePicker = {};

        $startTimeDatePicker = $startTime.timepicker(timePicker);
        $endTimeDatePicker = $endTime.timepicker(timePicker);
        $sendTimeDatePicker = $sendTime.timepicker(timePicker);
        $startDateDatePicker = $startDate.datepicker(extendedDatePicker($startDateTime));
        $startDateTimeDatePicker = $startDateTime.timepicker(timePicker);
        $endDateDatePicker = $endDate.datepicker(extendedDatePicker($endDateTime));
        $endDateTimeDatePicker = $endDateTime.timepicker(timePicker);
        $endDateOptimoveDatePicker = $endDateOptimove.datepicker(extendedDatePicker($endDateTimeOptimove));
        $endDateTimeOptimoveDatePicker = $endDateTimeOptimove.timepicker(timePicker);
        $sendDateDatePicker = $sendDate.datepicker(datePicker);
        $retryUntilDateDatePicker = $retryUntilDate.datetimepicker(datePicker);
        $persistUntilDateDatePicker = $persistUntilDate.datetimepicker(datePicker);

        return this;
    };

    this.enableRetryOptions = function() {
        $scheduleSection.find('.jsRetryOptions').show();
    };

    this.disableRetryOptions = function() {
        $scheduleSection.find('.jsRetryOptions').hide();
    };

    this.enablePersistOptions = function() {
        $scheduleSection.find('.jsPersistOptions').show();
    };

    this.disablePersistOptions = function() {
        $scheduleSection.find('.jsPersistOptions').hide();
    };

    return this.init();
};
