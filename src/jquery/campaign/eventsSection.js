import ConditionsEditor from '@/jquery/widgets/conditionsEditor';

export default function (campaignPage) {
    var that = this;

    var $campaignPage,
        $eventsSection
            = null;

    var $eventFilterContainer,
        $conditionsEditor
            = null;

    this.init = function() {
        $campaignPage = campaignPage.getLayout();
        $eventsSection  = $campaignPage.find('#events_section');
        if (!$eventsSection.length) return null;

        this.initTriggers();
        this.initEventConditions();

        return this;
    };

    this.initTriggers = function() {
        $('.jsEventSelect').autocomplete({
            source: '/api/event/event/autocomplete?project_id='+window.xp.project.id
        });
    };

    this.initEventConditions = function () {
        $eventFilterContainer = $eventsSection.find('#eventContainer');
        if (!$eventFilterContainer.length) return;

        $eventsSection.find('.ctmEventFilterOptions input').on('select', function(){
            if ($(this).prop('checked')) {
                if ($(this).val() == 0) {
                    $eventFilterContainer.hide();
                } else {
                    $eventFilterContainer.show();
                }
            }
        }).trigger('select');

        $conditionsEditor = new ConditionsEditor();
        $conditionsEditor.init($eventFilterContainer, '/push/event-filter/attribute-list-data');
    };

    return this.init();
};
