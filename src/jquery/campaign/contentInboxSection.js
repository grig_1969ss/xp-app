import ContentPushSection from './contentPushSection'
import ColorField from '@/jquery/widgets/colorField'
import { stdPlashes } from '@/jquery/widgets/general'

export default function (campaignPage, platform) {

    var that = new ContentPushSection(campaignPage, platform, false);

    var $campaignPage,
        $contentSection
            = null;

    var sameSelectUserEntered;

    that._init = that.init;
    that.init = function() {
        $campaignPage = campaignPage.getLayout();
        $contentSection = $campaignPage.find('#content-'+platform+'_section');
        if (!$contentSection.length) return null;

        that._init();
        that.initSameSelect();
        return that;
    };

    that.initSameSelect = function() {
        $contentSection.find('.jsInboxSame input').on('select', function(){
            that.updateSameSelect();
        });
        that.updateSameSelect();
    };

    that.disableSameSelect = function() {
        var sameSelect = $contentSection.find('.jsInboxSame input:checked').val();
        $contentSection.find('.jsInboxSame').hide();
        $contentSection.find('.jsInboxSameNo').prop('checked', true).trigger('change');
        sameSelectUserEntered = sameSelect;
    };

    that.enableSameSelect = function() {
        $contentSection.find('.jsInboxSame').show();
        if (sameSelectUserEntered == 1) {
            $contentSection.find('.jsInboxSameYes').prop('checked', true).trigger('change');
        } else {
            $contentSection.find('.jsInboxSameNo').prop('checked', true).trigger('change');
        }
    };

    that.updateSameSelect = function() {
        var sameSelect = $contentSection.find('.jsInboxSame input:checked').val();
        if (sameSelect == 1) {
            $contentSection.find('.jsInboxContent').hide();
        } else {
            $contentSection.find('.jsInboxContent').show();
        }
        sameSelectUserEntered = sameSelect;
    };

    that._initVariant = that.initVariant;
    that.initVariant = function(variant) {
        that._initVariant(variant);
        that.initColorPickers(variant);
        that.initTypeSelect(variant);
    };

    that.initTranslation = function(variant, translation) {
        that.initWysiwygs(variant, translation);
    };

    that.initFormInputs = function(variant) {
      // Intentionally Empty
    };

    that.initTypeSelect = function(variant) {
        if (!$(variant).find('.jsInboxTypeSelector').data('stdPlashes')) { new stdPlashes($(variant).find('.jsInboxTypeSelector')); }

        $(variant).find('.jsInboxTypeAlert').on('select', function(){ that.selectType(variant, 'alert'); });
        $(variant).find('.jsInboxTypeCard').on('select', function(){ that.selectType(variant, 'card'); });

        if ($(variant).find('.jsInboxTypeAlert').is(':checked')) { that.selectType(variant, 'alert'); }
        if ($(variant).find('.jsInboxTypeCard').is(':checked')) { that.selectType(variant, 'card'); }
    };

    that.selectType = function(variant, tab) {
        $(variant).find('.pushPreview .preview-tab').hide();
        $(variant).find('.pushPreview .preview-tab.' + tab).show();
        if (tab == 'alert') {
            $(variant).find('.device-selector .button[data-value=web-inbox-text]').click();
            $(variant).find('.web-inbox-color').hide();
        } else {
            $(variant).find('.web-inbox-color').show();
        }
        that.updatePreviewStyle(variant);
    };

    that.initWysiwygs = function(variant, translation) {
        var plugins = ['alignment', 'fontsize', 'fontfamily', 'fontcolor', 'emojis', 'personalization'];
        if ($(variant).data('twig')) plugins.push('snippet');
        if ($campaignPage.data('optimove')) plugins.push('optimove');

        that.initWysiwyg(variant, translation, $(translation).find('.jsPushTitleContainer'), {
            enterKey: false,
            tabKey: false,
            buttons: ['bold', 'italic', 'underline'],
            plugins: plugins
        });

        that.initWysiwyg(variant, translation, $(translation).find('.jsPushTextContainer'), {
            tabKey: false,
            minHeight: 90,
            buttons: ['bold', 'italic', 'underline'],
            plugins: plugins
        });
    };

    that.initWysiwyg = function(variant, translation, element, config) {
        $(element).find('textarea').redactor(
            $.extend({
                callbacks: {
                    change: function () {
                        that.updatePreview(variant);
                    }
                }
            }, config || {})
        );
    };

    that.initColorPickers = function (variant) {
        $(variant).find('.jsMessageBgColorField, .jsMessageTitleBgColorField').each(function() {
            new ColorField($(this), function () {
                that.updatePreviewStyle(variant);
            });
        })
    };

    that._updatePreview = that.updatePreview;
    that.updatePreview = function (variant) {
        that._updatePreview(variant);
        that.updatePreviewStyle(variant);
    };

    that.updatePreviewStyle = function (variant) {
      let preview = $(variant).data('preview');
        if (preview) {
          if ($(variant).find('.jsInboxTypeCard').prop('checked')) {
            preview.messageType = parseInt($(variant).find('.jsInboxTypeCard').val());
            preview.messageStyle = {
              bg:       $(variant).find('.jsMessageBgColor').val(),
              title_bg: $(variant).find('.jsMessageTitleBgColor').val(),
            };
          } else {
            preview.messageType = parseInt($(variant).find('.jsInboxTypeAlert').val());
            preview.messageStyle = {};
          }
        }
    };

    return that.init();
};
