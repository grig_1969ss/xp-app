import ContentEmailColorPicker from './contentEmailColorPicker'

export default function (contentEmailSection, $editorItemContainer, $previewItemContainer) {
    var that = this;

    var $eItem = null,
        $pItem = null,
        $textAlign = null,
        $textColorPicker = null,
        $bgColorPicker = null,
        $borderColorPicker = null,
        $borderStyle = null,
        $borderWidth = null,
        $linkColorField = null,
        $linkColorPicker = null,
        // we need this element to remove gap between message block and element below
        spanCode = '<span style="font-size:0">&nbsp;</span>';

    this.init = function() {
        $eItem = $editorItemContainer;
        $pItem = $previewItemContainer;
        var $container = contentEmailSection.getEditorContainer();

        $linkColorField = $eItem.find('.colorpickerField.linkColor');
        $linkColorPicker = new ContentEmailColorPicker($linkColorField, that.getCurrentLinks, 'color');
        $eItem.find('textarea').redactor({
            minHeight: 75,
            maxHeight: 700,
            toolbarFixed: false,
            removeComments: false,
            imageResizable: true,
            imagePosition: true,
            imageUpload: '/push/campaign/upload-image?project_id='+window.xp.project.id,
            imageUploadParam: 'CampaignMessage[image]',
            imageUploadFields: {
                "CampaignMessage[message_type]": 7, // constant Campaign::MESSAGE_TYPE_EMAIL == 7
                _csrf: contentEmailSection.getVariant()
                    .parents("form").find('input[type=hidden][name=_csrf]').val(),
            },
            imageManagerJson: '/push/campaign/choose-image?project_id='+window.xp.project.id,
            imageTag: 'p',
            buttons: ['ul', 'ol', 'image', 'link', 'format', 'bold', 'italic', 'underline', 'deleted'],
            plugins: ['source', 'alignment', 'fontsize', 'fontfamily', 'fontcolor', 'cleanformat',
                'lineheight', 'table', 'imagemanager', 'emojis', 'personalization'],
            formatting: ['p', 'h1', 'h2', 'h3', 'h4'],
            callbacks: {
                change: function() {
                    that.update(this.code.get());
                }
            }
        });

        $eItem.find('textarea').parent().find('.redactor-toolbar li').eq(9).after('<br>');

        var $colorpicker = $eItem.find('.colorpickerField.textColor');
        $textColorPicker = new ContentEmailColorPicker($colorpicker, $pItem.parent(), 'color');

        $colorpicker = $eItem.find('.colorpickerField.backgroundColor');
        $bgColorPicker = new ContentEmailColorPicker($colorpicker, $pItem.parent(), 'background');

        $colorpicker = $eItem.find('.colorpickerField.borderColor');
        $borderColorPicker = new ContentEmailColorPicker($colorpicker, $pItem, 'border-color');

        $textAlign = $eItem.find('.textAlign');
        $borderStyle = $eItem.find('.borderStyle');
        $borderWidth = $eItem.find('.borderWidth');
        $.each([$textAlign, $borderWidth], function(i, $elem) {
            $elem.on('change keyup', function() {
                $borderWidth.val(parseInt($borderWidth.val()) || 0);
                that.render();
            });
        });
        $borderStyle.change(function() {
            if ($(this).val() != 'none') {
                if (!parseInt($borderWidth.val())) {
                    $borderWidth.val('1');
                }
                if ($borderColorPicker.getElement().val() == '') {
                    $borderColorPicker.getElement().val('#000000');
                }
            }
            that.render();
        });

        return this;
    };

    this.render = function() {
        $pItem.empty()
            .css('box-sizing', 'border-box')
            .css('padding-left', '16px').css('padding-right', '16px')
            .css('text-align', $textAlign.val());

        if ($borderStyle.val() && $borderStyle.val() != 'none') $pItem
            .css('border-style', $borderStyle.val())
            .css('border-width', $borderWidth.val() + 'px')
            .css('margin-top', '8px').css('margin-bottom', '8px');

        $textColorPicker.render();
        $bgColorPicker.render();
        $borderColorPicker.render();

        this.update($eItem.find('textarea').val());
    };

    this.update = function (html) {
        $pItem.html(html);
        $pItem.find('a').attr('target', '_blank');

        var $allImages = $pItem.find('img');
        var imgCount = $allImages.length;
        if (imgCount) {
            $allImages.each(function(i, element) {
                $(element).load(function() {
                    if (--imgCount <= 0) {
                        contentEmailSection.updateIframe();
                    }
                });
                if (element.complete) $(element).load();
            });
        } else {
            contentEmailSection.updateIframe();
        }
    };

    return this.init();
};
