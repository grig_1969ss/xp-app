import ContentSection from './contentSection'
import ContentEditor from '@/jquery/widgets/contentEditor'
import { twig } from 'vendor/twig/twig'

export default function (campaignPage, platform, inherit) {

  let that = new ContentSection(campaignPage);

  let $campaignPage,
    $contentSection
      = null;

  let twParam = null;

  that.init = function() {
    $campaignPage = campaignPage.getLayout();
    $contentSection = $campaignPage.find('#content-whatsapp_section');
    if (!$contentSection.length) return null;

    that.initSection($contentSection);
    return that;
  };

  that.initVariant = function(variant) {
    that.initTranslations(variant);
    that.initPreview(variant);
    that.initStatusDropdown(variant);
    that.updatePreview(variant);
  };

  that.initStatusDropdown = function(variant) {
    $(variant).find('.jsWhatsappTemplateStatus').change(function () {
      if ($(this).val() === 'submitted') {
        $(variant).find('.jsWhatsappTemplateSubmitHint').show();
      } else {
        $(variant).find('.jsWhatsappTemplateSubmitHint').hide();
      }
    })
  }

  that.initTranslations = function(variant) {
    $(variant).off('init', '.jsTranslation').on('init', '.jsTranslation', function(event){
      if ($(event.target).hasClass('jsTranslation')) {
        that.initTranslation(variant, this);
      }
    });
    $(variant).find('.jsTranslations .jsTranslation').trigger('init');
    $(variant).find('.jsTranslationDefault .jsTranslation').trigger('init');

    $(variant).on('update', '.jsTranslationsContainer', function(event){
      if ($(event.target).hasClass('jsTranslationsContainer')) {
        that.updatePreview(variant);
      }
    });
  };

  that.initTranslation = function(variant, translation) {
    that.initEditor(variant, translation);
    that.initParams(variant, translation);
  };


  that.initEditor = function(variant, translation) {
    if (!$(translation).find('.jsWhatsappText').prop('readonly')) {
      new ContentEditor($(translation).find('.jsWhatsappTextContainer'), {
        emojis: true,
      });
    }
  };

  that.initParams = function(variant, translation) {
    if (!twParam) {
      twParam = twig({
        id: "whatsapp_param",
        href: VIEWS_DIR+"/push/views/campaign/templates/content-whatsapp-param.twig",
        async: false
      });
    }

    $(translation).on('change', '.jsWhatsappText', function() {
      that.updateParams(variant, translation);
    });

    that.initParamsEditor(variant, translation);
  }

  that.initParamsEditor = function(variant, translation) {
    $(translation).find('.jsWhatsappParamValueContainer').each(function () {
      new ContentEditor($(this), {
        personalization: true,
        optimove: !!$campaignPage.data('optimove')
      });
    })
  };

  that.updateParams = function(variant, translation) {
    let params = [];
    $(translation).find('.jsWhatsappParam').each(function() {
      let key = $(this).data('key');
      params[key] = $(this).find('input').val();
    });

    let textParams = [];
    let text = $(translation).find('.jsWhatsappText').val();
    let preg = /{{([0-9]+)}}/g;
    let match;
    do {
      match = preg.exec(text);
      if (match) {
        textParams.push(match[1]);
      }
    } while (match);

    $(translation).find('.jsWhatsappParams').html('').hide();

    textParams.forEach((paramId) => {
      let messageType = $(variant).parents('.jsMessage').data('type');
      let variantKey = $(variant).data('key');
      let language = $(translation).data('language');
      let value = params[paramId] || '';

      let template = twParam.render({
        message_type: messageType,
        variant_key: variantKey,
        language: language,
        key: paramId,
        value: value
      });

      $(translation).find('.jsWhatsappParams').append(template).show();
    });

    that.initParamsEditor(variant, translation);
  };

  that.initPreview = function(variant) {
    $(variant).on('keyup', 'input, textarea, select', function() {
      that.updatePreview(variant);
    });

    $(variant).on('change', 'input, textarea, select', function() {
      that.updatePreview(variant);
    });

    $(variant).data('preview', $(variant).find('.jsMessagePreview').vue());
  };

  that.updatePreview = function(variant) {
    let preview = $(variant).data('preview');
    if (preview) {
      preview.message = that.buildMessage(variant);
    }
  };

  that.buildMessage = function(variant) {
    let translation = $(variant).find('.jsTranslationLanguageDefault');

    return {
      text: that.buildMessageText(variant, translation)
    };
  };

  that.buildMessageText = function(variant, translation) {
    let text = $(translation).find('.jsWhatsappText').val();

    $(translation).find('.jsWhatsappParam').each(function () {
      let key = $(this).data('key');
      let value = $(this).find('input').val();
      text = text.replace('{{'+key+'}}', value);
    })

    return text;
  }

  if (inherit || inherit === undefined) {
    return that.init();
  }

  return that;
};
