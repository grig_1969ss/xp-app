import ContentPushSection from './contentPushSection'
import Dropzone from 'vendor/dropzone/package'
import UploadImage from '@/img/upload-image.png'

export default function (campaignPage) {

    var that = new ContentPushSection(campaignPage, 'onsite', false);
    var $contentSection = null;

    that._init = that.init;
    that.init = function() {
        that._init();

        $contentSection = campaignPage.getLayout().find('#content-onsite_section');
        $contentSection.on('section-tab-click', function (event) {
            event.preventDefault();
            let activeVariant = $(this).find('.jsMessageVariantsTabButton.tabContentActive');
            that.enableVariant('.jsMessageVariant[data-key=' + activeVariant.data('key') + ']')
        });
        $contentSection.trigger('section-tab-click');
    }

    that._initVariant = that.initVariant;
    that.initVariant = function(variant) {
        that._initVariant(variant);
        that.initTopButtons(variant);
        $(variant).data('popup', new contentOnsitePopup(variant))
    };

    that.enableVariant = function (variant) {
        if ($(variant).data('popup')) {
            $(variant).data('popup').updatePreview();
        }
    };

    that.initFormInputs = function(variant) {
      // Intentionally Empty
    };

    that.initTopButtons = function(variant) {
        $(variant).find('.stdPlashesColumn').click(function() {
            $(this).find('input:radio').prop('checked', true);
            $(this).find('input:radio').trigger('change');
        });

        $(variant).find('.onsiteMessageType input:radio').change(function () {
            if ($(this).prop('checked')) {
                $(variant).find('.pushControls > .tab').hide();
                $(variant).find('.pushControls input, .pushControls select').attr("disabled", "disabled");
                $(variant).find('.pushPreview .previewWrapper').hide();

                var activeTabName = $(this).data('tab');
                $(variant).find('.tab.' + activeTabName + ' input, .tab.' + activeTabName + ' select').removeAttr("disabled");
                $(variant).find('.pushControls .tab.' + activeTabName).show();
                $(variant).find('.pushPreview .' + activeTabName + 'Preview').show();
            }
        }).trigger('change');
    };

    return that.init();
};

var contentOnsitePopup = function (variant) {
    var that = this;

    var $controlsContainer,
        $previewContainer,
        sweetElement
            = null;

    var init = true;

    this.init = function(variant) {
        $controlsContainer = $(variant).find('.tab.onsitePopup');
        $previewContainer = $(variant).find('.onsitePopupPreview');
        sweetElement = $(variant).find('.jsOnsitePopupPreview').vue();

        if (!$controlsContainer.length) return false;

        that.initPopupSelector();
        that.initTopControls();
        that.initColorPickers();
        that.initImageLayout();
        that.initTitleLayout();
        that.initTextLayout();
        that.initFooterLayout();
        that.initFormLayout();
        that.initButtonsLayout();
        that.initTranslations();
        that.initDropdowns();
        that.updateControlsLayout();
        that.updatePreview();

        init = false;

        return that;
    };

    this.renderPreview = function (config) {
        sweetElement.configObject = config;
        setTimeout(function () {
            that.enablePreviewInputs();
            that.applyPreviewColors();
        }, 0);
    }

    this.updatePreview = function () {
        let config = this.buildSwalConfig();
        this.renderPreview(config);
    };

    this.reopenPreview = function () {
        setTimeout(function () {
            that.updatePreview();
        }, 0);
    };

    this.enablePreviewInputs = function () {
        setTimeout(function () {
          $(sweetElement.$el).find('input').prop('disabled', false);
        }, 0);
    };

    this.initPopupSelector = function() {
        $controlsContainer.find('.onsitePopupSelectorContainer .button').click(function() {
            $controlsContainer.find('.onsitePopupSelectorContainer .button').removeClass('active');
            $(this).addClass('active');

            $controlsContainer.find('.innerTab').hide();
            $controlsContainer.find('.' + $(this).data('value')).show();
        });
    };

    this.initTopControls = function() {
        that.config.style.width = parseInt($controlsContainer.find('.message-width').val()) || 0;
        $controlsContainer.find('.message-width').on('change', function() {
            that.config.style.width = parseInt($(this).val()) || 0;
            that.updatePreview();
        });
    };

    this.initDropdowns = function() {
        $controlsContainer.on('change', '.actionSelector', function() {
            var value = $(this).val();
            $(this).closest('.jsSelectWrapper').find('.choice').each(function (i, item) {
                $(item).hide();
                if (!$(item).hasClass('choice-' + value)) {
                    $(item).find('input,select,textarea').attr("disabled", "disabled").trigger('disable');
                }
            });
            $(this).closest('.jsSelectWrapper').find('.choice-' + value)
                .css('display', 'inline-block').find('input,select,textarea').removeAttr("disabled").trigger('enable');
        });
        $controlsContainer.find('.actionSelector').trigger('change', ['doNotUpdatePreview']);

        $controlsContainer.on('change', '.subActionSelector', function() {
            var value = $(this).val();
            $(this).closest('.jsSelectWrapper').find('.sub-choice').each(function (i, item) {
                $(item).hide();
                if (!$(item).hasClass('sub-choice-' + value)) {
                    $(item).find('input,select,textarea').attr("disabled", "disabled").trigger('disable');
                }
            });
            $(this).closest('.jsSelectWrapper').find('.sub-choice-' + value)
                .css('display', 'inline-block').find('input,select,textarea').removeAttr("disabled").trigger('enable');
        });
        $controlsContainer.find('.subActionSelector').trigger('change');
    };

    this.initTranslations = function () {
        $(variant).off('init', '.jsTranslation')
          .on('init', '.jsTranslation', function(event){
            if ($(event.target).hasClass('jsTranslation')) {
              that.initTranslation(this);
            }
          });

        $(variant).find('.jsTranslations .jsTranslation').trigger('init');
        $(variant).find('.jsTranslationDefault .jsTranslation').trigger('init');

        $(variant).on('update', '.jsTranslationsContainer', function (event) {
            if ($(event.target).hasClass('jsTranslationsContainer')) {
                $(variant).find('.jsTranslation.jsTranslationLanguageDefault').each(function () {
                    that.updateConfig(this);
                    that.updatePreview();
                });
            }
        });
    }



    this.initImageLayout = function () {
        $(variant).find('.jsOnsiteImageCheckbox').on('change', function () {
            that.updateControlsLayout();
            that.updateImageConfig($(variant).find('.jsTranslationLanguageDefault'));
            that.updatePreview();
        });
    };

    this.initTitleLayout = function () {
        $(variant).find('.jsOnsiteTitleCheckbox').on('change', function () {
            that.updateControlsLayout();
            that.updateTitleConfig($(variant).find('.jsTranslationLanguageDefault'));
            that.updatePreview();
         });
    };

    this.initTextLayout = function () {
        $(variant).find('.jsOnsiteTextCheckbox').on('change', function () {
            that.updateControlsLayout();
            that.updateTextConfig($(variant).find('.jsTranslationLanguageDefault'));
            that.updatePreview();
         });
    };

    this.initFooterLayout = function () {
        $(variant).find('.jsOnsiteFooterCheckbox').on('change', function () {
            that.updateControlsLayout();
            that.updateFooterConfig($(variant).find('.jsTranslationLanguageDefault'));
            that.updatePreview();
         });
    };

    this.initFormLayout = function () {
        if ($(variant).find('.jsOnsiteEmailCheckbox').prop('checked')) {
            $(variant).find('.jsOnsiteFormLayout').val('email');
        } else {
            $(variant).find('.jsOnsiteFormLayout').val('');
        }

        $(variant).find('.jsOnsiteFormLayout').change(function () {
            switch (this.value) {
                case 'email':
                    $(variant).find('.jsOnsiteEmailCheckbox').prop('checked', true);
                    break;
                default:
                    $(variant).find('.jsOnsiteEmailCheckbox').prop('checked', false);
                    break;
            }

            that.updateControlsLayout();
            that.updateFormConfig($(variant).find('.jsTranslationLanguageDefault'));
            that.updatePreview();
        });
    };

    this.initButtonsLayout = function () {
        if ($(variant).find('.jsOnsiteButtonEnabled1').prop('checked') && $(variant).find('.jsOnsiteButtonEnabled2').prop('checked')) {
            $(variant).find('.jsOnsiteButtonLayout').val('2');
        } else if ($(variant).find('.jsOnsiteButtonEnabled1').prop('checked')) {
            $(variant).find('.jsOnsiteButtonLayout').val('1');
        } else {
            $(variant).find('.jsOnsiteButtonLayout').val('0');
        }

        $(variant).find('.jsOnsiteButtonLayout').change(function () {
            switch (this.value) {
                case '0':
                    $(variant).find('.jsOnsiteButtonEnabled1').prop('checked', false);
                    $(variant).find('.jsOnsiteButtonEnabled2').prop('checked', false);
                    break;
                case '1':
                    $(variant).find('.jsOnsiteButtonEnabled1').prop('checked', true);
                    $(variant).find('.jsOnsiteButtonEnabled2').prop('checked', false);
                    break;
                case '2':
                    $(variant).find('.jsOnsiteButtonEnabled1').prop('checked', true);
                    $(variant).find('.jsOnsiteButtonEnabled2').prop('checked', true);
                    break;
            }

            that.updateControlsLayout();
            that.updateButtonsConfig($(variant).find('.jsTranslationLanguageDefault'));
            that.updatePreview();
        });
    };

    this.updateControlsLayout = function () {
        if ($(variant).find('.jsOnsiteImageCheckbox').prop('checked')) {
            $(variant).find('.jsOnsiteImageContainer').show();
        } else {
            $(variant).find('.jsOnsiteImageContainer').hide();
        }

        if ($(variant).find('.jsOnsiteTitleCheckbox').prop('checked')) {
            $(variant).find('.jsOnsiteTitleContainer').show();
        } else {
            $(variant).find('.jsOnsiteTitleContainer').hide();
        }

        if ($(variant).find('.jsOnsiteTextCheckbox').prop('checked')) {
            $(variant).find('.jsOnsiteTextContainer').show();
        } else {
            $(variant).find('.jsOnsiteTextContainer').hide();
        }

        if ($(variant).find('.jsOnsiteFooterCheckbox').prop('checked')) {
            $(variant).find('.jsOnsiteFooterContainer').show();
        } else {
            $(variant).find('.jsOnsiteFooterContainer').hide();
        }

        for (let i = 1; i <= 2; i++) {
            if ($(variant).find('.jsOnsiteButtonEnabled'+i).prop('checked')) {
                $(variant).find('.jsOnsiteButtonContainer'+i).show();
            } else {
                $(variant).find('.jsOnsiteButtonContainer'+i).hide();
            }
        }
    };



    this.initTranslation = function (translation) {
        that.initImage(translation);
        that.initTitle(translation);
        that.initText(translation);
        that.initFooter(translation);
        that.initForm(translation);
        that.initButtons(translation);
        that.initAutocompletes(translation);
    };

    this.updateConfig = function (translation) {
        that.updateImageConfig(translation);
        that.updateTitleConfig(translation);
        that.updateTextConfig(translation);
        that.updateFooterConfig(translation);
        that.updateFormConfig(translation);
        that.updateButtonsConfig(translation);
    };

    this.initImage = function (translation) {
        $(translation).find('.fake-input-file').off('click').dropzone({
            url: "/push/campaign/upload-image?project_id="+window.xp.project.id,
            maxFilesize: 10,
            thumbnailWidth: null,
            acceptedFiles: 'image/*',
            addRemoveLinks: true,
            paramName: "CampaignMessage[image]",
            params: {
                _csrf: $(translation).parents("form").find('input[type=hidden][name=_csrf]').val()
            },
            dragenter: function() {
                this.options.addRemoveLinks = true;
            },
            addedfile: function(file) {
                Dropzone.prototype.defaultOptions.addedfile.call(this, file);
            },
            sending: function(file, xhr, formData) {
                formData.append('type', 'image-upload');
            },
            success: function(event, response) {
                $(translation).find('.onsitePopupImageFile').val(response.filename);
                that.config.image = {
                    url: response.filename
                };
                that.updatePreview();
            }
        });

        that.updateImageConfig(translation);

        let onChangeCallback = (e, customParam) => {
          if (customParam === 'doNotUpdatePreview') {
            return;
          }

          that.updateImageConfig(translation);
          that.updatePreview();
        }

        $(translation).find('.onsitePopupImageSelector,.onsitePopupImageUrl').on('change', onChangeCallback);
        $(translation).find('.onsitePopupImageUploadHeight,.onsitePopupImageUrlHeight').on('change', onChangeCallback);
        $(translation).find('.onsitePopupImageUploadLink,.onsitePopupImageUrlLink').on('change', onChangeCallback);
    };

    this.updateImageConfig = function (translation) {
        if ($(translation).hasClass('jsTranslationLanguageDefault')) {
            if ($(variant).find('.jsOnsiteImageCheckbox').prop('checked')) {
                var imageType = $(translation).find('.onsitePopupImageSelector').val();
                that.config.image = {
                    url: UploadImage,
                    height: 150,
                    type: '',
                    link: '',
                };
                switch (imageType) {
                    case '':
                        break;
                    case 'upload':
                        if ( $(translation).find('.onsitePopupImageFile').val() != '') {
                            that.config.image = {
                                url:  $(translation).find('.onsitePopupImageFile').val(),
                                height: parseInt( $(translation).find('.onsitePopupImageUploadHeight').val()) || 0,
                                link:  $(translation).find('.onsitePopupImageUploadLink').val() || ''
                            };
                        }
                        break;
                    case 'url':
                        if ( $(translation).find('.onsitePopupImageUrl').val() != '') {
                            that.config.image = {
                                url:  $(translation).find('.onsitePopupImageUrl').val(),
                                height: parseInt( $(translation).find('.onsitePopupImageUrlHeight').val()) || 0,
                                link:  $(translation).find('.onsitePopupImageUrlLink').val() || ''
                            };
                        }
                        break;
                    case 'success':
                    case 'error':
                    case 'warning':
                    case 'info':
                    case 'question':
                        that.config.image = {
                            type: imageType
                        };
                }
            } else {
                that.config.image = null;
            }
        }
    };

    this.initForm = function (translation) {
        that.updateFormConfig(translation);
    };

    this.updateFormConfig = function (translation) {
        if ($(translation).hasClass('jsTranslationLanguageDefault')) {
            that.config.email = $controlsContainer.find('.jsOnsiteEmailCheckbox').prop('checked') ? 1 : 0;
        }
    };

    this.initTitle = function (translation) {
        $(translation).find('.jsOnsiteTitle').redactor({
            tabKey: false,
            enterKey: false,
            maxHeight: 70,
            buttons: ['bold', 'italic', 'underline'],
            plugins: ['source', 'fontsize', 'fontfamily', 'cleanformat', 'personalization'],
            callbacks: {
                init: function () {
                    that.updateTitleConfig(translation);
                    that.updatePreview();
                },
                blur: function () {
                    that.updateTitleConfig(translation);
                    that.updatePreview();
                }
            }
        });
    };

    this.updateTitleConfig = function (translation) {
        if ($(translation).hasClass('jsTranslationLanguageDefault')) {
            if ($(variant).find('.jsOnsiteTitleCheckbox').prop('checked')) {
                that.config.title = $(translation).find('.jsOnsiteTitle').val() || 'Title';
            } else {
                that.config.title = null;
            }
        }
    };

    this.initText = function (translation) {
        $(translation).find('.jsOnsiteText').redactor({
            minHeight: 45,
            maxHeight: 700,
            buttons: ['bold', 'italic', 'underline', 'ol', 'ul'],
            plugins: ['source', 'alignment', 'fontsize', 'fontfamily', 'fontcolor', 'cleanformat', 'personalization'],
            allowedTags: ['span', 'br', 'div'],
            removeEmpty: ['span', 'p'],
            linebreaks: true,
            callbacks: {
                init: function () {
                    that.updateTextConfig(translation);
                    that.updatePreview();
                },
                blur: function () {
                    that.updateTextConfig(translation);
                    that.updatePreview();
                }
            }
        });
    };

    this.updateTextConfig = function (translation) {
        if ($(translation).hasClass('jsTranslationLanguageDefault')) {
            if ($(variant).find('.jsOnsiteTextCheckbox').prop('checked')) {
                that.config.html = $(translation).find('.jsOnsiteText').val() || 'Message content';
            } else {
                that.config.html = null;
            }
        }
    };

    this.initFooter = function (translation) {
        $(translation).find('.jsOnsiteFooter').redactor({
            minHeight: 45,
            maxHeight: 700,
            buttons: ['bold', 'italic', 'underline', 'ol', 'ul'],
            plugins: ['source', 'alignment', 'fontsize', 'fontfamily', 'fontcolor', 'cleanformat', 'personalization'],
            allowedTags: ['span', 'br', 'div'],
            removeEmpty: ['span', 'p'],
            linebreaks: true,
            callbacks: {
                init: function () {
                    that.updateFooterConfig(translation);
                    that.updatePreview();
                },
                blur: function () {
                    that.updateFooterConfig(translation);
                    that.updatePreview();
                }
            }
        });
    };

    this.updateFooterConfig = function (translation) {
        if ($(translation).hasClass('jsTranslationLanguageDefault')) {
            if ($(variant).find('.jsOnsiteFooterCheckbox').prop('checked')) {
                that.config.footer = $(translation).find('.jsOnsiteFooter').val() || 'Footer';
            } else {
                that.config.footer = null;
            }
        }
    };

    this.initButtons = function (translation) {
        that.updateButtonsConfig(translation);

        $(translation).find('.jsOnsiteButtonsContainer input[type=text]').on('change', function() {
            that.updateButtonsConfig(translation);
            that.updatePreview();
        });
    };

    this.updateButtonsConfig = function (translation) {
        if ($(translation).hasClass('jsTranslationLanguageDefault')) {
            that.config.buttons = [];
            for (let i = 1; i <= 2; i++) {
                if ($(variant).find('.jsOnsiteButtonEnabled'+i).prop('checked')) {
                    let target = i === 1 ? '.swal2-confirm' : '.swal2-cancel';
                    let $block = $(translation).find('.jsOnsiteButtonContainer'+i);
                    let button = {
                        'text': $block.find('input.button-label').val(),
                        'action': $block.find('.actionSelector').val(),
                        'text_color': $controlsContainer.find(".colorpickerField[data-target='" + target + "'][data-property='color']").val(),
                        'color': $controlsContainer.find(".colorpickerField[data-target='" + target + "'][data-property='background']").val()
                    };
                    if (button.action === 'url') {
                        button.url_mode = $block.find('.urlModeSelector').val();
                        button.url_blank = $block.find('.url-blank').prop('checked');
                        button.url = $block.find('.url').val();
                    }
                    that.config.buttons.push(button);
                }
            }
        }
    };

    this.initAutocompletes = function(translation) {
        $(translation).find('.jsOnsiteSpecialSetAttribute').autocomplete({
            source: '/api/tag/tag/autocomplete?project_id=' + window.xp.project.id + '&attribute=1'
        });

        $(translation).find('.jsOnsiteSpecialAddToList').comboboxAutocomplete({
            source: '/api/lists/user-list/autocomplete?project_id=' + window.xp.project.id
              + '&identifier[0]=profile_id&identifier[1]=device_id'
        });

        $(translation).find('.jsOnsiteSpecialTriggerEvent').autocomplete({
            source: '/api/event/event/autocomplete?project_id=' + window.xp.project.id
        });

        $.get('/api/push/subscription-preference/autocomplete?project_id=' + window.xp.project.id, function (prefs) {
            $(translation).find('.jsOnsiteSpecialSubscriptionPreference').on('enable', function () {
                this.selectize.enable();
            }).selectize({
                plugins: ['remove_button'],
                delimiter: ',',
                persist: false,
                options: prefs,
                labelField: 'value',
                valueField: 'id',
                sortField: 'value',
                searchField: 'value',
                openOnFocus: true
            });
        });
    };



    this.initColorPickers = function() {
        var $caller;
        $controlsContainer.find('span.colorpickerField').click(function() {
            $(this).siblings('input.colorpickerField').click();
        });
        $controlsContainer.find('input.colorpickerField').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                if (this.value != undefined) {
                    $(this).ColorPickerSetColor(this.value);
                }
                $caller = $(this);
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(200);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(200);
                return false;
            },
            onChange: function (hsb, hex) {
                that.updatePopupPreviewElementColor($caller, hex);
            }
        }).bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
            that.updatePopupPreviewElementColor(this);
        }).bind('change', function() {
            if (this.value.search(/^#[0-9a-f]{6}$/i) == -1) {
                $(this).val("");
            }
            $(this).ColorPickerSetColor(this.value);
            that.updatePopupPreviewElementColor(this);
        }).each(function(i, elem) {
            var opacity = $(elem).data('opacity');
            if (opacity) {
                $('[name="'+opacity+'"]').change(function () {
                    that.updatePopupPreviewElementColor(elem)
                });
                $('[name="'+opacity+'"]').keyup(function () {
                    that.updatePopupPreviewElementColor(elem)
                });
            }

            that.updatePopupPreviewElementColor(elem);
        });
    };

    this.updatePopupPreviewElementColor = function($caller, color) {
        $caller = $($caller);
        if (color == undefined) {
            color = $caller.val();
        }
        if (color.length && color.indexOf('#') !== 0) {
            color = '#' + color;
        }
        $caller.val(color);

        var $elem;
        var target = $caller.data('target');
        var property = $caller.data('property');
        var opacity = $caller.data('opacity');

        if (opacity) {
            var opacityValue = $('[name="'+opacity+'"]').val();
            color = this.hexToRgbA(color, opacityValue / 100);
        }

        if (target === '.swal2-modal' && property === 'background') {
            $(variant).find('.redactor-layer').css('background', color.length ? color : '#ffffff');
        }

        if (target === '.swal2-title' && property === 'color') {
            $(variant).find('.jsOnsiteTitleContainer').find('.redactor-layer').css('color', color.length ? color : '#545454');
        }

        if (target === '.swal2-content' && property === 'color') {
            $(variant).find('.jsOnsiteTextContainer').find('.redactor-layer').css('color', color.length ? color : '#545454');
        }

        if (target === '.swal2-footer' && property === 'color') {
            $(variant).find('.jsOnsiteFooterContainer').find('.redactor-layer').css('color', color.length ? color : '#545454');
        }

        $elem = $previewContainer.find(target);

        if (property == 'border-color') {
            $elem.css('border', color.length ? '1px solid' : '0px');
        }

        $elem.css(property, color);
        that.setConfigColor($caller, color);
        $caller.next().css('background', color.length ? color : '#eeeeee');
    };

    this.hexToRgbA = function(hex, opacity) {
        if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
            var c= hex.substring(1).split('');
            if(c.length== 3){
                c= [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c= '0x'+c.join('');
            return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+','+opacity+')';
        } else {
            return hex;
        }
    };

    this.setConfigColor = function($caller, color) {
        if ($caller.hasClass('button0') && that.config.buttons[0] != undefined) {
            if ($caller.data('property') == 'color') {
                that.config.buttons[0].text_color = color;
            } else if ($caller.data('property') == 'background') {
                that.config.buttons[0].color = color;
            }
        } else if ($caller.hasClass('button1') && that.config.buttons[1] != undefined) {
            if ($caller.data('property') == 'color') {
                that.config.buttons[1].text_color = color;
            } else if ($caller.data('property') == 'background') {
                that.config.buttons[1].color = color;
            }
        } else if ($caller.data('target') == 'this' && $caller.data('property') == 'background') {
            that.config.style.background = color;
        }
    };

    this.applyPreviewColors = function() {
        $controlsContainer.find('input.colorpickerField').each(function(i, elem) {
            that.updatePopupPreviewElementColor(elem);
        });
    };



    this.config = {
        "image": {},
        "style": {},
        "buttons": []
    };

    this.buildSwalConfig = function () {
        var rebuildConfig = {}

        if (that.config.image) {
            if (that.config.image.type) {
                rebuildConfig.type = that.config.image.type;
            } else {
                if (that.config.image.url) {
                    rebuildConfig.imageUrl = that.config.image.url;
                }
                if (that.config.image.width) {
                    rebuildConfig.imageWidth = that.config.image.width + 'px';
                }
                if (that.config.image.height) {
                    rebuildConfig.imageHeight = that.config.image.height + 'px';
                }
            }
        }

        if (that.config.title) {
            rebuildConfig.title = that.config.title;
        }

        if (that.config.html) {
            rebuildConfig.html = that.config.html;
        }

        if (that.config.footer) {
            rebuildConfig.footer = that.config.footer;
        }

        if (that.config.buttons) {
            if (that.config.buttons[0]) {
                rebuildConfig.showConfirmButton = true;
                rebuildConfig.confirmButtonText = that.config.buttons[0].text;
                rebuildConfig.confirmButtonColor = that.config.buttons[0].color;
            } else {
                rebuildConfig.showConfirmButton = false;
            }

            if (that.config.buttons[1]) {
                rebuildConfig.showCancelButton = true;
                rebuildConfig.cancelButtonText = that.config.buttons[1].text;
                rebuildConfig.cancelButtonColor = that.config.buttons[1].color;
            } else {
                rebuildConfig.showCancelButton = false;
            }
        } else {
            rebuildConfig.showConfirmButton = false;
            rebuildConfig.showCancelButton = false;
        }

        if (that.config.email === 1) {
            rebuildConfig.input = 'email';
            rebuildConfig.inputPlaceholder = 'info@example.com';
            rebuildConfig.inputAttributes = {'disabled': 'disabled'};
        }

        if (that.config.style) {
            if (that.config.style.background) {
                rebuildConfig.background = that.config.style.background;
            }

            if (that.config.style.width) {
                rebuildConfig.width = that.config.style.width + 'px';
            }
        }

        rebuildConfig.animation = false;
        rebuildConfig.backdrop = false;
        rebuildConfig.focusCancel = false;
        rebuildConfig.focusConfirm = false;
        rebuildConfig.allowEnterKey = false;
        rebuildConfig.allowEscapeKey = false;
        rebuildConfig.allowOutsideClick = false;
        rebuildConfig.showCloseButton = true;
        rebuildConfig.onClose = that.reopenPreview;
        rebuildConfig.position = 'bottom';
        return rebuildConfig
    };

    return that.init(variant);
};
