import ContentEmailColorPicker from './contentEmailColorPicker'
import Dropzone from 'vendor/dropzone/package'

export default function  (contentEmailSection, $editorItemContainer, $previewItemContainer) {
    var that = this;

    var $eItem = null,
        $pItem = null,
        $bgColorPicker = null;

    this.init = function() {
        $eItem = $editorItemContainer;
        $pItem = $previewItemContainer;

        $eItem.find('.fake-input-file').dropzone({
            url: "/push/campaign/upload-image?project_id="+window.xp.project.id,
            maxFilesize: 10,
            thumbnailWidth: null,
            acceptedFiles: 'image/*',
            addRemoveLinks: true,
            paramName: "CampaignMessage[image]",
            params: {
                _csrf: $eItem.parents("form").find('input[type=hidden][name=_csrf]').val(),
                button_id: $eItem.find('.imageFile').attr('static-id')
            },
            dragenter: function() {
                this.options.previewTemplate = $eItem.find('.uploadPreviewTemplate')[0].innerHTML;
                this.options.addRemoveLinks = true;
            },
            addedfile: function(file) {
                this.previewsContainer = $pItem[0];
                Dropzone.prototype.defaultOptions.addedfile.call(this, file);
            },
            sending: function(file, xhr, formData) {
                formData.append('type', 'image-upload');
            },
            success: function(file, response, xhr) {
                $eItem.find('.imageFile').val(response.filename);
                $eItem.find('.imageAlt').val(file.name);
                that.render();
            }
        });
        $eItem.on('click', '.fake-input-file a', function() {
            $(this).parent().click();
        });
        
        $eItem.find('.imageWidth, .imageHeight, .imageHref, .imageFile, .imageAlt, .edgeToEdge, .textAlign').change(function() {
            that.render();
        });

        var $colorpicker = $eItem.find('input.colorpickerField');
        $bgColorPicker = new ContentEmailColorPicker($colorpicker, $pItem.parent(), 'background');

        return this;
    };

    this.render = function() {
        var filename = $eItem.find('.imageFile').val();
        if (!filename) filename = '';

        var href = $eItem.find('.imageHref').val();
        var alt = $eItem.find('.imageAlt').val();
        var textAlign = $eItem.find('.textAlign').val();
        var width = parseInt($eItem.find('.imageWidth').val());
        var height = parseInt($eItem.find('.imageHeight').val());
        var edgeToEdge = parseInt($eItem.find('.edgeToEdge').val());
        var $img = $('<img/>', {src: filename, style: "max-width: 100% !important; height: auto !important; margin: 0;"});

        if (width) {
            $img.css('width', width + 'px').attr('width', width);
        }
        if (height) {
            $img.css('max-height', height + 'px');
        }

        if (alt) {
            $img.attr('alt', alt);
        } else {
            $img.removeAttr('alt');
        }

        var $el;
        if (href) {
            var $a = $('<a/>').attr('href', href).attr('target', '_blank').css('text-decoration', 'none');
            $el = $a.append($img);
        } else {
            $el = $img;
        }

        $pItem.css('font-size', '0').css('padding', edgeToEdge ? '0' : '16px').css('text-align', textAlign);
        $pItem.empty().append($el);
        $bgColorPicker.render();

        $img.each(function () {
            var tmpImg = new Image();
            tmpImg.onload = function () {
                contentEmailSection.updateIframe();
            };
            tmpImg.src = $(this).attr('src');
        });
    };

    return this.init();
};
