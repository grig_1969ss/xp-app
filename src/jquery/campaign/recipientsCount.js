import { addCommas } from '@/jquery/utils'
import RecipientsPreviewModal from '@/jquery/widgets/recipientsPreviewModal';

export default function (campaignPage) {
  let that = this;

  let $recipientsCountContainer;
  let $campaignPageContent;
  let $previewRecipientsModal;
  let $calculateLink;
  let $previewLink;
  let $progressLoader;
  let loading = false;

  this.init = function () {
    $campaignPageContent = campaignPage.getLayout()
    $recipientsCountContainer = $campaignPageContent.find('.jsRecipientsCountContainer')
    $previewRecipientsModal = $('#pushRecipientsModal')

    $calculateLink = $recipientsCountContainer.find('.jsCalculateLink')
    $previewLink = $recipientsCountContainer.find('.jsPreviewLink')
    $progressLoader = $recipientsCountContainer.find('.progressLoader')

    this.initCount()
    this.initChannels()
    setTimeout(function () {
      that.showOrHide()
    })
  };

  this.initCount = function () {
    $calculateLink.on('click', function (event) {
      if (!loading) {
        event.preventDefault()
        that.markStatusInProgress()
        $recipientsCountContainer.find('b').text('?')

        $campaignPageContent.find('.createCampaignForm').ajaxSubmit({
          url: API_URL+'/push/campaign/recipients-count?project_id='+window.xp.project.id,
          success: function (result) {
            if (result.task_id && !result.error) {
              that.checkCountTask(result.task_id)
            } else {
              that.markStatusError(result.error)
            }
          }, error: function () {
            that.markStatusError(t('all', 'PushCampaign__Recipients_count_failed_refresh'))
          }
        })
      }
    })

    $previewLink.on('click', function (event) {
      if (!loading) {
        event.preventDefault()
        that.markStatusInProgress()

        $campaignPageContent.find('.createCampaignForm').ajaxSubmit({
          url: API_URL + '/push/campaign/recipients-preview?project_id=' + window.xp.project.id,
          success: function (result) {
            if (result.task_id && !result.error) {
              $previewRecipientsModal.data('task_id', result.task_id)
              that.checkPreviewTask(result.task_id)
            } else {
              that.markStatusError(result.error)
            }
          }, error: function () {
            that.markStatusError(t('all', 'PushCampaign__Recipients_preview_failed_refresh'))
          }
        })
      }
    })
  };

  this.initChannels = function() {
    $campaignPageContent.find('.jsIos, .jsIosProduction').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsIos').prop('checked') && $campaignPageContent.find('.jsIosProduction').prop('checked')) {
        $campaignPageContent.find('.jsIosProductionCount').show();
      } else {
        $campaignPageContent.find('.jsIosProductionCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsIos, .jsIosSandbox').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsIos').prop('checked') && $campaignPageContent.find('.jsIosSandbox').prop('checked')) {
        $campaignPageContent.find('.jsIosSandboxCount').show();
      } else {
        $campaignPageContent.find('.jsIosSandboxCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsIos, .jsIosLegacy').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsIos').prop('checked') && $campaignPageContent.find('.jsIosLegacy').prop('checked')) {
        $campaignPageContent.find('.jsIosLegacyCount').show();
      } else {
        $campaignPageContent.find('.jsIosLegacyCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsAndroid, .jsAndroidProduction').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsAndroid').prop('checked') && $campaignPageContent.find('.jsAndroidProduction').prop('checked')) {
        $campaignPageContent.find('.jsAndroidProductionCount').show();
      } else {
        $campaignPageContent.find('.jsAndroidProductionCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsAndroid, .jsAndroidLegacy').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsAndroid').prop('checked') && $campaignPageContent.find('.jsAndroidLegacy').prop('checked')) {
        $campaignPageContent.find('.jsAndroidLegacyCount').show();
      } else {
        $campaignPageContent.find('.jsAndroidLegacyCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsWebpush, .jsSafari').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsWebpush').prop('checked') && $campaignPageContent.find('.jsSafari').prop('checked')) {
        $campaignPageContent.find('.jsWebSafariCount').show();
      } else {
        $campaignPageContent.find('.jsWebSafariCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsWebpush, .jsChrome').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsWebpush').prop('checked') && $campaignPageContent.find('.jsChrome').prop('checked')) {
        $campaignPageContent.find('.jsWebChromeCount').show();
      } else {
        $campaignPageContent.find('.jsWebChromeCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsWebpush, .jsFirefox').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsWebpush').prop('checked') && $campaignPageContent.find('.jsFirefox').prop('checked')) {
        $campaignPageContent.find('.jsWebFirefoxCount').show();
      } else {
        $campaignPageContent.find('.jsWebFirefoxCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsInappIos').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsInappIos').prop('checked')) {
        $campaignPageContent.find('.jsIosCount').show();
      } else {
        $campaignPageContent.find('.jsIosCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsInappAndroid').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsInappAndroid').prop('checked')) {
        $campaignPageContent.find('.jsAndroidCount').show();
      } else {
        $campaignPageContent.find('.jsAndroidCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsInbox').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsInbox').prop('checked')) {
        $campaignPageContent.find('.jsInboxCount').show();
      } else {
        $campaignPageContent.find('.jsInboxCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsSms').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsSms').prop('checked')) {
        $campaignPageContent.find('.jsSmsCount').show();
      } else {
        $campaignPageContent.find('.jsSmsCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsFacebook').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsFacebook').prop('checked')) {
        $campaignPageContent.find('.jsFacebookCount').show();
      } else {
        $campaignPageContent.find('.jsFacebookCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsWhatsapp').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsWhatsapp').prop('checked')) {
        $campaignPageContent.find('.jsWhatsappCount').show();
      } else {
        $campaignPageContent.find('.jsWhatsappCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsEmail').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsEmail').prop('checked')) {
        $campaignPageContent.find('.jsEmailCount').show();
      } else {
        $campaignPageContent.find('.jsEmailCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsWebhook').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsWebhook').prop('checked')) {
        $campaignPageContent.find('.jsWebhookCount').show();
      } else {
        $campaignPageContent.find('.jsWebhookCount').hide();
      }
      that.showOrHide();
    }).trigger('init');

    $campaignPageContent.find('.jsOnsite').on('change select deselect init', function(){
      if ($campaignPageContent.find('.jsOnsite').prop('checked')) {
        $campaignPageContent.find('.jsWebCount').show();
      } else {
        $campaignPageContent.find('.jsWebCount').hide();
      }
      that.showOrHide();
    }).trigger('init');
  };

  this.checkCountTask = function (taskId, tryNum = 0) {
    $.ajax(API_URL + '/task/record/result?id=' + taskId, {
      success: function (response) {
        switch (response.status) {
          case 0: // TASK_STATUS_NEW
          case 1: // TASK_STATUS_IN_PROGRESS
            if (tryNum >= 5) {
              that.markStatusProgressInfo(t('all', 'PushCampaign__Recipients_count_wait', {'url': '/task/history/view?project_id=' + window.xp.project.id + '&id=' + taskId}))
            }
            setTimeout(function () {
              that.checkCountTask(taskId, ++tryNum)
            }, 1000)
            break

          case 2: // TASK_STATUS_DONE
            that.countDisplay(response.result.count)
            that.markStatusDone()
            break
          default: // TASK_STATUS_ERROR
            that.markStatusError(t('all', 'PushCampaign__Recipients_count_failed_contact'))
            break
        }
      },
      error: function () {
        that.markStatusError(t('all', 'PushCampaign__Recipients_count_failed_refresh'))
      }
    })
  };

  this.checkPreviewTask = function (taskId, tryNum = 0) {
    $.ajax(API_URL + '/task/record/result?id=' + taskId, {
      success: function (response) {
        switch (response.status) {
          case 0: // TASK_STATUS_NEW
          case 1: // TASK_STATUS_IN_PROGRESS
            if (tryNum >= 5) {
              that.markStatusProgressInfo(t('all', 'PushCampaign__Recipients_preview_wait', {'url': '/task/history/view?project_id=' + window.xp.project.id + '&id=' + taskId}))
            }
            setTimeout(function () {
              that.checkPreviewTask(taskId, ++tryNum)
            }, 1000)
            break

          case 2: // TASK_STATUS_DONE
            that.showPreviewModal(taskId)
            break
          default: // TASK_STATUS_ERROR
            that.markStatusError(t('all', 'PushCampaign__Recipients_preview_failed_contact'))
            break
        }
      },
      error: function () {
        that.markStatusError(t('all', 'PushCampaign__Recipients_preview_failed_refresh'))
      }
    })
  };

  this.showPreviewModal = function (taskId) {
    $.ajax({
      url: API_URL + '/push/campaign-preview/view?project_id=' + window.xp.project.id + '&task_id=' + taskId,
      success: function (result) {
        that.markStatusDone()
        let previewRecipientsModal = new RecipientsPreviewModal()
        previewRecipientsModal.init(result)
      }, error: function () {
        that.markStatusError(t('all', 'PushCampaign__Recipients_preview_failed_refresh'))
      }
    })
  }

  this.countDisplay = function (count) {
    $recipientsCountContainer.find('[data-channel]').text('?');
    $recipientsCountContainer.find('[data-message-type]').text('?');

    for (let key in count.channel) {
      if (count.channel.hasOwnProperty(key)) {
        if (count.channel[key] === null) {
          $recipientsCountContainer.find('[data-channel*="' + key + '"]').text('?');
        } else {
          $recipientsCountContainer.find('[data-channel*="' + key + '"]').each(function () {
            let current = isNaN($(this).text()) ? 0 : parseInt($(this).text());
            $(this).text(current + parseInt(count.channel[key]));
          });
        }
      }
    }

    for (let key in count.message_type) {
      if (count.message_type.hasOwnProperty(key)) {
        if (count.message_type[key] === null) {
          $recipientsCountContainer.find('[data-message-type*="'+key+'"]').text('?');
        } else {
          $recipientsCountContainer.find('[data-message-type*="'+key+'"]').each(function(){
            let current = isNaN($(this).text()) ? 0 : parseInt($(this).text());
            $(this).text(current + parseInt(count.message_type[key]));
          });
        }
      }
    }

    if (count.sampling_level) {
      $recipientsCountContainer.find('b').each(function(){
        if (!isNaN($(this).text())) {
          $(this).text('~'+addCommas(parseInt($(this).text())));
        }
      });
    } else {
      $recipientsCountContainer.find('b').each(function(){
        if (!isNaN($(this).text())) {
          $(this).text(addCommas(parseInt($(this).text())));
        }
      });
    }
  };

  this.showOrHide = function () {
    if ($recipientsCountContainer.find('.jsDeviceCountList div:visible').length) {
      $recipientsCountContainer.find('.jsDeviceCountPlaceholder').hide();
      $recipientsCountContainer.find('.jsHeaderLinks').show();
    } else {
      $recipientsCountContainer.find('.jsDeviceCountPlaceholder').show();
      $recipientsCountContainer.find('.jsDeviceCountInfo').hide();
      $recipientsCountContainer.find('.jsHeaderLinks').hide();
    }

    if ($recipientsCountContainer.find('.jsDeviceCountList div:visible').length > 6) {
      $recipientsCountContainer.addClass('compactList');
    } else {
      $recipientsCountContainer.removeClass('compactList');
    }
  };

  this.markStatusInProgress = function () {
    loading = true
    $progressLoader.show()
    $calculateLink.addClass('disabled')
    $previewLink.addClass('disabled')
    $recipientsCountContainer.find('.jsDeviceCountList').show()
    $recipientsCountContainer.find('.jsDeviceCountInfo').hide()
    $recipientsCountContainer.find('.jsDeviceCountInfo span').html('')
  };

  this.markStatusProgressInfo = function (info) {
    loading = true
    $progressLoader.show()
    $calculateLink.addClass('disabled')
    $previewLink.addClass('disabled')
    $recipientsCountContainer.find('.jsDeviceCountList').hide()
    $recipientsCountContainer.find('.jsDeviceCountInfo').show()
    $recipientsCountContainer.find('.jsDeviceCountInfo span').html(info)
  };

  this.markStatusError = function (error) {
    loading = false
    $progressLoader.hide()
    $calculateLink.removeClass('disabled')
    $previewLink.removeClass('disabled')
    $recipientsCountContainer.find('.jsDeviceCountList').hide()
    $recipientsCountContainer.find('.jsDeviceCountInfo').show()
    $recipientsCountContainer.find('.jsDeviceCountInfo span').html(error)
  };

  this.markStatusDone = function () {
    loading = false
    $progressLoader.hide()
    $calculateLink.removeClass('disabled')
    $previewLink.removeClass('disabled')
    $recipientsCountContainer.find('.jsDeviceCountList').show()
    $recipientsCountContainer.find('.jsDeviceCountInfo').hide()
    $recipientsCountContainer.find('.jsDeviceCountInfo span').html('')
  };

  this.init();
};
