import Vue from 'vue'
import SendTestDialog from "@/jquery/widgets/sendTestDialog";
import { $can } from '@/core/utils/VueAcl'

export default function(campaignPage, contentSection, variant) {
  let that = this

  this.init = function () {
    that.sendTestDialog = new SendTestDialog(variant)

    $(variant).find('.jsEmailEdit').click(function () {
      that.openEditor()
    })

    $(variant).find('.jsEmailCreateTemplate').click(function () {
      that.openTemplateEditor()
    })

    $(variant).find('.jsEmailUploadHtml').click(function () {
      $(variant).find('.jsEmailUploadHtmlFile').trigger('click')
    })

    $(variant).find('.jsEmailUploadHtmlFile').change(function () {
      that.processFileImport(this.files[0])
    })

    $(contentSection).on('section-tab-click', function() {
      that.snapshot()
    })

    that.updateButtons()

    that.snapshot()
  }

  this.openEditor = function () {
    const json = $(variant).find('.jsEmailTemplateInput').val()

    if (json) {
      that.openTemplateEditor()
    } else {
      that.openHtmlEditor()
    }
  }

  this.openTemplateEditor = function () {
    $(variant).find('.jsEmailEditor').show()

    let template = $(variant).find('.jsEmailTemplateInput').val()
    if (template) {
      template = JSON.parse(template)
    }

    let container = $("<div>")
    $(variant).find('.jsEmailEditor').append(container)

    const Connector = Vue.extend(Vue.component('EmailBuilder'))
    this.editor = new Connector({
      el: container[0],
      propsData: {
        template: template,
        onError: that.handleSaveError,
        onSave: that.saveTemplateEditor,
        onClose: that.closeEditor,
        onConfirmClose: that.promptCloseEditor,
        onSendTest: that.showSendTestDialog,
        generateTextPreview: that.generateTextPreview
      }
    })
  }

  this.openHtmlEditor = function () {
    $(variant).find('.jsEmailEditor').show()

    let html = $(variant).find('.jsEmailHtmlInput').val()

    let container = $("<div>")
    $(variant).find('.jsEmailEditor').append(container)

    const Connector = Vue.extend(Vue.component('EmailHtmlEditor'))
    this.editor = new Connector({
      el: container[0],
      propsData: {
        html: html,
        onSave: that.saveHtmlEditor,
        onClose: that.closeEditor,
        onConfirmClose: that.promptCloseEditor,
        onSendTest: that.showSendTestDialog,
        generateTextPreview: that.generateTextPreview
      }
    })
  }

  this.generateTextPreview = function (html, success) {
    $.ajax({
      type: 'post',
      url: '/push/campaign/generate-email-text?project_id=' + window.xp.project.id,
      data: {
        html: html
      },
      success: success
    });
  }

  this.saveTemplateEditor = function (json, html) {
    that.saveTemplate(json, html)
    that.closeEditor()
  }

  this.saveHtmlEditor = function (html) {
    that.saveHtml(html)
    that.closeEditor()
  }

  this.closeEditor = function () {
    that.editor.$destroy()
    that.editor = null

    $(variant).find('.jsEmailEditor').html('').hide()
  }

  this.promptCloseEditor = function () {
    let closeWithSave = t('all', 'PushCampaign__Email_builder_save_and_close')
    let closeWithoutSave = t('all', 'PushCampaign__Email_builder_close_without_save')
    let cancel = t('all', 'PushCampaign__Email_builder_close_cancel')

    const buttons = {};

    if ($can('updateProjectItem')) {
      Object.assign(buttons, {
        [closeWithSave]: function() {
          dialog.dialog('close')
          that.editor.save()
        }
      })
    }

    Object.assign(buttons, {
      [closeWithoutSave]: function () {
        dialog.dialog('close')
        that.editor.close()
      },
      [cancel]: function () {
        dialog.dialog('close')
      }
    })

    let dialog = $('<p>' + t('all', 'PushCampaign__Email_builder_close_confirm') + '</p>').dialog({
      appendTo: '#popupsContainer',
      buttons: buttons,
      draggable: false,
      modal:true,
      resizable: false,
      title: t('all', 'PushCampaign__Email_builder_close_confirm_title'),
      width: 600,
      zIndex: 5000000
    });
  }

  this.saveTemplate = function (json, html) {
    $(variant).find('.jsEmailTemplateInput').val(json)
    $(variant).find('.jsEmailHtmlInput').val(html)
    that.updateButtons()
    that.snapshot()
  }

  this.saveHtml = function (html) {
    $(variant).find('.jsEmailTemplateInput').val('')
    $(variant).find('.jsEmailHtmlInput').val(html)
    that.updateButtons()
    that.snapshot()
  }

  this.handleSaveError = function () {
    alert('Error saving template')
  }

  this.showSendTestDialog = function (html) {
    const emailDetails = {
      subject: $(variant).find('.jsEmailSubjectContainer input').val(),
      from_address: $(variant).find('.pushEmailFromAddress').val(),
      from_name: $(variant).find('.pushEmailFromName').val(),
      content_fetch_sources: campaignPage.startSection.serializeContentFetchSources(),
      html: html
    }

    that.sendTestDialog.show(emailDetails)
  }

  this.processFileImport = function (file) {
    if (!file) {
      alert('Failed to handle file upload.')
      return
    } else if (file.type !== 'text/html') {
      alert(`Wrong File Format '${file.type}'. Please provide a HTML file.`)
      return
    } else if (file.size > 250000) {
      alert('Specified file is too large.')
      return
    }

    file.text().then(html => this.saveHtml(html))
  }

  this.updateButtons = function () {
    const html = $(variant).find('.jsEmailHtmlInput').val()
    const json = $(variant).find('.jsEmailTemplateInput').val()

    if (json || html) {
      $(variant).find('.jsEmailNewButtons').hide()
      $(variant).find('.jsEmailExistingButtons').show()
    } else {
      $(variant).find('.jsEmailExistingButtons').hide()
      $(variant).find('.jsEmailNewButtons').show()
    }
  }

  this.snapshot = function () {
    let html = $(variant).find('.jsEmailHtmlInput').val()
    const json = $(variant).find('.jsEmailTemplateInput').val()
    const iframe = $(variant).find('.jsEmailSnapshot')[0]

    if (json) {
      html = this.transformHtmlForPreview(html, json)
    }

    const container = iframe.contentWindow || iframe.contentDocument.document || iframe.contentDocument
    container.document.open()
    container.document.write(html)
    container.document.close()

    if (html) {
      $(variant).find('.jsEmailSnapshot').show()
    } else {
      $(variant).find('.jsEmailSnapshot').hide()
    }
  }

  this.transformHtmlForPreview = function (html, json) {
    try {
      let data = JSON.parse(json);
      this.callbackForElementsRecursively(data, 'image', (item) => {
        if (item.dynamicSrc) {
          html = html.replace('src="'+item.dynamicSrc+'"', 'src="'+item.src+'"')
        }
      })
    } catch (e) {}
    return html
  }

  this.callbackForElementsRecursively = function (data, key, callback)
  {
    for (let i in data) {
      if (data.hasOwnProperty(i)) {
        if (i === key) {
          callback(data[i])
        } else if (typeof data[i] === 'object') {
          this.callbackForElementsRecursively(data[i], key, callback)
        }
      }
    }
  }

  return this.init()
}
