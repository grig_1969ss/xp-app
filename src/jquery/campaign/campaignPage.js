import RecipientsCount from './recipientsCount'
import StartSection from './startSection'
import LocationsSection from './locationsSection'
import EventsSection from './eventsSection'
import SegmentSection from './segmentSection'
import ScheduleSection from './scheduleSection'
import ContentPushSection from './contentPushSection'
import ContentInboxSection from './contentInboxSection'
import ContentInappSection from './contentInappSection'
import ContentEmailSection from './contentEmailSection'
import ContentSmsSection from './contentSmsSection'
import ContentFacebookSection from './contentFacebookSection'
import ContentWhatsappSection from './contentWhatsappSection'
import ContentWebhookSection from './contentWebhookSection'
import ContentOnsiteSection from './contentOnsiteSection'
import ContentWorkflowSection from './contentWorkflowSection'
import CheckFormChanges from "@/jquery/widgets/checkCampaignFormChanges";
import { twig } from 'vendor/twig/twig'
import RestApi from "@/core/services/RestApi";
import { projectEndpoint } from "@/core/helpers";

export default function () {

    var $campaignPageContent,
        $campaignType,
        $allSectionContainer,
        $currentSection,
        $crumbs,
        $controlBox,
        $campaignForm,
        $previewCampaignModal,
        $previewStatusChangeModal,
        twPreviewModalWindowsContent,
        twStatusChangeModalWindowsContent,
        isFutureSend = false,
        deviceCountRequest
            = null;

    var that = this;

    this.init = function () {
        $crumbs                         = $('#breadcrumb');
        $controlBox                     = $('.controlBox').first();
        $campaignPageContent            = $('#createCampaingContentBlock');
        $previewCampaignModal           = $('#pushPreviewModal');
        $previewStatusChangeModal       = $('#changeStatusModal');
        $allSectionContainer            = $campaignPageContent.find('#allSectionContainer');
        $currentSection                 = $campaignPageContent.find('#currentCampainSection');
        $campaignForm                   = $campaignPageContent.find('> form.createCampaignForm');
        $campaignType                   = $('#createCampaignTypePage');

        twPreviewModalWindowsContent    = twig({
            id: "previewModalContent",
            href: VIEWS_DIR+"/push/views/campaign/templates/preview.twig",
            async: false
        });

        if (!$campaignPageContent.data('manage')) {
            $campaignPageContent.addClass("viewMode");
            $campaignPageContent.find('input, select, textarea').prop('disabled', true).addClass('viewable');
            $campaignPageContent.find('.tabContentTabs input').prop('disabled', false).removeClass('viewable');
            $campaignPageContent.find('.jsApprovePush').prop('disabled', false).removeClass('viewable');
        }

        this.initByHash();
        this.initCrumbs();
        this.initControlButton();
        this.initCreateCampaignType();
        this.recipientsCount        = new RecipientsCount(this);
        this.startSection           = new StartSection(this);
        this.contentIosSection      = new ContentPushSection(this, 'ios');
        this.contentAndroidSection  = new ContentPushSection(this, 'android');
        this.contentWebpushSection  = new ContentPushSection(this, 'webpush');
        this.contentInboxSection    = new ContentInboxSection(this, 'inbox');
        this.contentSmsSection      = new ContentSmsSection(this);
        this.contentFacebookSection = new ContentFacebookSection(this);
        this.contentWhatsappSection = new ContentWhatsappSection(this);
        this.contentEmailSection    = new ContentEmailSection(this);
        this.contentInappSection    = new ContentInappSection(this);
        this.contentOnsiteSection   = new ContentOnsiteSection(this);
        this.contentWebhookSection  = new ContentWebhookSection(this);
        this.contentWorkflowSection = new ContentWorkflowSection(this);
        this.locationsSection       = new LocationsSection(this);
        this.eventsSection          = new EventsSection(this);
        this.segmentSection         = new SegmentSection(this);
        this.scheduleSection        = new ScheduleSection(this);

        for (var i in this) {
            if (i.indexOf('Section') >= 0 && typeof this[i] == 'object' && !Object.keys(this[i]).length) {
                this[i] = null;
            }
        }

        /*Set up a timeout because of redactor callback, i.e runs below after everything is loaded on the page */
        setTimeout(function() {
          this.checkFormChanges = new CheckFormChanges($campaignForm);
        }, 500)
    };

    this.initCreateCampaignType = function() {
      $campaignType.find('.jsCampaignTypeContainer input').on('select', function() {
        if ($(this).val() === 'journey') {
          $campaignType.find('.jsTypeSingle').hide()
          $campaignType.find('.jsTypeJourney').show()
        } else {
          $campaignType.find('.jsTypeJourney').hide()
          $campaignType.find('.jsTypeSingle').show()
        }
      })
    }

    this.initByHash = function() {
        $currentSection.html('');
        if(window.location.hash && this.isEnabledSection(window.location.hash)) {
            this.openSection(window.location.hash);
        } else {
            this.openSection('#start');
        }
    };

    this.initCrumbs = function() {
        $crumbs.find('.crumbs a').click(function(e){
            e.preventDefault();
            var $link=$(this);
            if(!$link.hasClass('inactive')) {
                var href = $link.attr('href');
                that.openSection(href);
            }
            return false;
        });
    };

    this.goNextSection = function() {
        var $currentLi  = $crumbs.find('.crumbs a.active').parent('li');
        while (true) {
            var $nextLi = $currentLi.next('li')
            if (!$nextLi.length) return;
            if ($nextLi.is(':visible')) {
                that.openSection($nextLi.find('> a').attr('href'));
                return
            }
            $currentLi = $nextLi;
        }
    };

    this.goPrevSection = function() {
        var $currentLi  = $crumbs.find('.crumbs a.active').parent('li');
        while (true) {
            var $prevLi = $currentLi.prev('li')
            if (!$prevLi.length) return;
            if ($prevLi.is(':visible')) {
                that.openSection($prevLi.find('> a').attr('href'));
                return
            }
            $currentLi = $prevLi;
        }
    };

    this.enableSection = function(sectionName) {
        $crumbs.find('.crumbs a[href="#'+ sectionName +'"]').parent().show();
    };

    this.disableSection = function(sectionName) {
        $crumbs.find('.crumbs a[href="#'+ sectionName +'"]').parent().hide();
    };

    this.isEnabledSection = function(sectionHash) {
        return $crumbs.find('.crumbs a[href="'+ sectionHash +'"]').parent().is(':visible');
    };

    this.openSection = function (sectionHash) {
        var sectionName = sectionHash + '_section';
        var $newSection = $(sectionName);
        if($newSection.length > 0) {
            $currentSection.find('> section').appendTo($allSectionContainer);
            $currentSection.html('');
            $newSection.appendTo($currentSection);
            window.location.hash = sectionHash;

            $crumbs.find('.crumbs a.active').removeClass('active');
            $crumbs.find('.crumbs .prev').removeClass('prev');
            var $newLink = $crumbs.find('.crumbs a[href="'+ sectionHash +'"]');
            var $parentLi = $newLink.parent('li');
            $newLink.addClass('active');
            var $prevLi = $parentLi.prev();
            while ($prevLi.length && !$prevLi.is(':visible')) $prevLi = $prevLi.prev();
            $prevLi.addClass('prev');

            if($parentLi.next('li').length == 0) {
                $controlBox.find('input[type="button"].jsNext').prop('disabled',true);
            } else {
                $controlBox.find('input[type="button"].jsNext').prop('disabled',false);
            }
            if($parentLi.prev('li').length == 0) {
                $controlBox.find('input[type="button"].jsPrev').prop('disabled',true);
            } else {
                $controlBox.find('input[type="button"].jsPrev').prop('disabled',false);
            }

            $newSection.trigger('section-tab-click');
        }
    };

    this.updateInappSection = function() {
        if (this.contentInappSection && this.contentInappSection.init) {
            var inappIos = false;
            var inappAndroid = false;
            var inappStart = false;
            if (this.contentIosSection && this.contentIosSection.requiresInapp && this.contentIosSection.requiresInapp()) {
                inappIos = true;
            }
            if (this.contentAndroidSection && this.contentAndroidSection.requiresInapp && this.contentAndroidSection.requiresInapp()) {
                inappAndroid = true;
            }
            if (this.contentInboxSection && this.contentInboxSection.requiresInapp && this.contentInboxSection.requiresInapp()) {
                inappIos = true;
                inappAndroid = true;
            }
            if (this.startSection && this.startSection.requiresInapp && this.startSection.requiresInapp()) {
                inappStart = true;
            }

            $campaignForm.find('.jsInappStatus').val((inappIos || inappAndroid || inappStart) ? 1 : 0);
            $campaignForm.find('.jsInappIosStatus').val(inappIos ? 1 : 0);
            $campaignForm.find('.jsInappAndroidStatus').val(inappAndroid ? 1 : 0);

            if (inappIos || inappAndroid || inappStart) {
                this.enableSection('content-inapp');
            } else {
                this.disableSection('content-inapp');
            }
        }
    };


    this.initControlButton = function() {
      const campaignId = Number($campaignForm.find('#PushMessage_campaignId').val()),
        type = Number($campaignForm.find('#PushMessage_type').val()),
        draft = Number($campaignForm.find('#PushMessage_draft').val()),
        status = $campaignForm.find('#PushMessage_status'),
        workflowStatusBtn = $controlBox.find('.jsStatusChange');

        if (type === 3 && !draft) {
          if (Number(status.val()) === 3) {
            workflowStatusBtn.val(t('all', 'Common__Resume'));
          }
          workflowStatusBtn.show();
        }


       $controlBox.find('.jsPrev').click(function(e){
            e.preventDefault();
            that.goPrevSection();
        });

        $controlBox.find('.jsNext').click(function(e){
            e.preventDefault();
            that.goNextSection();
        });

        $controlBox.find('.jsPreviewPush').click(function(e){
            e.preventDefault();
            $campaignForm.find('#PushMessage_draft').val(0);
            $previewCampaignModal.find('#sendPushModal').val($(this).data('submit-text'));
            that.preview();
        });

        $controlBox.find('.jsDraftPush').click(function(e){
            e.preventDefault();
            $campaignForm.find('#PushMessage_draft').val(1);
            $previewCampaignModal.find('#sendPushModal').val($(this).data('submit-text'));
            that.preview(true);
        });

        const statusChangeBtn = $previewStatusChangeModal.find('#changeStatusPushModal');

        workflowStatusBtn.click(function(e) {
            workflowStatusBtn.attr('disabled', true);
            $previewStatusChangeModal.find('#errorMsg').hide();
            e.preventDefault();

            if (Number(status.val()) === 3) {
              $previewStatusChangeModal.find('#pauseMsg').hide();
              $previewStatusChangeModal.find('#resumeMsg').show();
              statusChangeBtn.val(t('all', 'Common__Resume'));
            } else {
              $previewStatusChangeModal.find('#pauseMsg').show();
              $previewStatusChangeModal.find('#resumeMsg').hide();
              statusChangeBtn.val(t('all', 'Common__Pause'));
            }

            that.changeStatusPreview();
            workflowStatusBtn.attr('disabled', false);
        });

        $previewStatusChangeModal.find('#editCampaignModal').click(function(event) {
            event.preventDefault();
            $previewStatusChangeModal.close();
        });

        statusChangeBtn.click(function(event) {
            event.preventDefault();
            that.setDisabledStatusChangeBtns(true);

            const method = Number(status.val()) === 3 ? 'resume' : 'pause';
            RestApi(
                projectEndpoint({
                  method: 'post',
                  url: `campaigns/${campaignId}/${method}`
                })
            ).then(({ data }) => {
                status.val(data.statusCode);
                workflowStatusBtn.val(t('all', 'Common__' + (Number(status.val()) === 3 ? 'Resume' : 'Pause')));
                workflowStatusBtn.attr('disabled', false);

                // update workflow button labels (applies to all variants)
                $('#content-workflow_section .jsWorkflowBtn').each((index, buttonEL) => {
                  buttonEL.innerHTML = '<i class="icon-layout"></i>' + t('all', 'Campaign__Workflow_Edit');
                });

                $previewStatusChangeModal.close();
                that.setDisabledStatusChangeBtns(false);
            }).catch(() => {
                workflowStatusBtn.attr('disabled', false);
                $previewStatusChangeModal.find('#errorMsg').show();
                that.setDisabledStatusChangeBtns(false);
            });
        });

        $controlBox.find('.jsApprovePush').click(function(e){
            e.preventDefault();
            that.approve();
        });

        $previewCampaignModal.find('#editCampaignModal').click(function(event){
            event.preventDefault();
            $previewCampaignModal.bPopup({
              appendTo: '#popupsContainer'
            }).close();
            return false;
        });

        $previewCampaignModal.find('#sendPushModal').click(function(event){
            return that.submitCampaignForm(event, this);
        });
    };

    this.setDisabledStatusChangeBtns = function(isDisabled) {
      $previewStatusChangeModal.find('#changeStatusPushModal').attr('disabled', isDisabled);
      $previewStatusChangeModal.find('#editCampaignModal').attr('disabled', isDisabled);
    };

    this.changeStatusPreview = function() {
      $previewStatusChangeModal.bPopup({
        appendTo: '#popupsContainer',
        opacity: 0.9
      });
    };

    this.submitCampaignForm = function(event, element) {
        event.preventDefault();
        $(element).prop('disabled', true);
        $campaignForm.submit();
        return false;
    };

    this.preview = function(isSave = false) {
        let url
        if ($campaignPageContent.data('id')) {
          url = '/push/campaign/validate?project_id=' + window.xp.project.id + '&id=' + $campaignPageContent.data('id')
        } else {
          url = '/push/campaign/validate?project_id=' + window.xp.project.id
        }

        var options = {
            url:        API_URL + url,
            success:    function(data) {
                const attributes = data.model.attributes,
                    hasErrors = Object.keys(data.model.errors).length > 0,
                    launchBtn = $previewCampaignModal.find('#sendPushModal'),
                    sendType = Number(attributes['send_type']),
                    campaignType = Number(attributes['type']),
                    status = Number(attributes['status']),

                    showSendScheduleMsg =
                        !hasErrors &&
                        !isSave && // don't count recipients on Save Draft click
                        (sendType === 0 || sendType === 1 || sendType === 2) && // 'send now', 'send in the future', 'repeat every' send types
                        Number(attributes['trigger']) === 0; // exclude trigger campaigns

                    let showSendWarningMsg = showSendScheduleMsg &&
                        (Number(attributes['whatsapp']) || Number(attributes['sms']) || window.vue.$store.state.app.project.calculate_recipients);

                    if (campaignType === 3) {
                        showSendWarningMsg = showSendScheduleMsg;
                    }

                // use class variable to append additional text in markAsDone() method
                isFutureSend = sendType === 1 || sendType === 2

                hasErrors ? launchBtn.hide() : launchBtn.show()

                const showPauseWarningMessage = Number($campaignForm.find('#PushMessage_status').val()) === 3;
                const showClosedWorkflowWarningMessage = status === 2;

                // rename the buttons to match the send type, disable the launch button and initiate the user send count
                if (showSendWarningMsg) {
                    launchBtn.attr('disabled', true);
                    $previewCampaignModal.find('#editCampaignModal').val(t('all', 'Dialog__cancel'));
                    $previewCampaignModal.find('#sendPushModal').val(Number(attributes['send_type']) === 0 ?
                        t('all', 'Dialog__confirm_send_now') :
                        t('all', 'Dialog__schedule')
                    );

                    that.initRcptCount();
                }
                let savedInDrafts = Number($campaignForm.find('#PushMessage_savedInDrafts').val())
                const template = twPreviewModalWindowsContent.render({
                  showAdditionalInfo: !isSave && (!attributes['id'] || savedInDrafts !== 0),
                  isSave: isSave,
                  model: data.model,
                  showSendWarningMsg: showSendWarningMsg,
                  showSendScheduleMsg: showSendScheduleMsg,
                  showPauseWarningMessage: showPauseWarningMessage,
                  showClosedWorkflowWarningMessage: showClosedWorkflowWarningMessage,
                });

                $previewCampaignModal.find('.modalContent').html(template);
                $previewCampaignModal.bPopup({
                  appendTo: '#popupsContainer',
                  opacity: 0.9
                });
            }
        };
        $campaignForm.ajaxSubmit(options);
    };

    this.approve = function() {
        if (confirm(t("PushCampaign__approve_confirm"))) {
            window.location = '/push/campaign/launch?project_id=' + window.xp.project.id + '&id=' + $campaignPageContent.data('id');
        }
    };


    this.getLayout = function() {
        return $campaignPageContent;
    };

    this.initRcptCount = function() {
        $campaignPageContent.find('.createCampaignForm').ajaxSubmit({
            url: `${API_URL}/push/campaign/recipients-count?project_id=${window.xp.project.id}`,
            success: (result) => {
                if (result.task_id && !result.error) {
                that.checkCountTaskStatus(result.task_id)
                } else {
                    that.markAsError()
                }
            },
            error: function () {
                that.markAsError()
            }
        })
    };

    this.checkCountTaskStatus = function (taskId, tryNum = 0) {
        $.ajax(`${API_URL}/task/record/result?id=${taskId}`, {
            success: function (response) {
                switch (response.status) {
                    case 0: // TASK_STATUS_NEW
                    case 1: // TASK_STATUS_IN_PROGRESS
                        if (tryNum >= 5) {
                            that.markStatusProgress(taskId, tryNum)
                        }
                        setTimeout(function () {
                            that.checkCountTaskStatus(taskId, ++tryNum)
                        }, 1000)
                        break
                    case 2: // TASK_STATUS_DONE
                        that.displayCount(response.result.count)
                        that.markAsDone()
                    break
                default: // TASK_STATUS_ERROR
                    that.markAsError()
                    break
                }
            },
            error: function () {
                that.markAsError()
            }
        })
    };

    this.markStatusProgress = function (taskId, tryNum) {
        if (tryNum === 5) {
            $previewCampaignModal.find('.channelCountCt').append(
                `<div id="showWaitMsg" style="margin-top:10px">
                    ${t(
                        'all',
                        'PushCampaign__Recipients_count_in_progress',
                        { 'url': `/task/history/view?project_id=${window.xp.project.id}&id=${taskId}` }
                    )}
                </div>`
            )
        }
    };

    this.markAsError = function () {
        $previewCampaignModal.find('.channelCountCt').html(
            t(
                'all',
                'PushCampaign__Recipients_count_failed_support',
                { 'url' : 'https://support.xtremepush.com/hc/en-us/requests/new' }
            )
        )
    };

    this.markAsDone = function () {
        $previewCampaignModal.find('.warningElProgressLoader').hide();
        $previewCampaignModal.find('.channelCountCt').append(
            `<div class="sendWarningConfirmMsg">
                ${ isFutureSend ? '<div>' + t('all', 'PushCampaign__Recipients_future_send_info_msg') + '</div>' : ''}
                <div>${t('all', 'PushCampaign__Recipients_send_warning_message_confirm')}</div>
            </div>`
        )
        $previewCampaignModal.find('#sendPushModal').removeAttr('disabled');
    };

    this.displayCount = function (count) {
        const messageTypes = {
            '1': 'MessageType_PushIos',
            '2': 'MessageType_PushAndroid',
            '3': 'MessageType_PushWeb',
            '4': 'MessageType_InApp',
            '5': 'MessageType_OnSite',
            '6': 'MessageType_Inbox',
            '7': 'MessageType_Email',
            '8': 'MessageType_Webhook',
            '9': 'MessageType_Sms',
            '10': 'MessageType_Facebook',
            '11': 'MessageType_Whatsapp',
            '12': 'MessageType_Workflow'
        },
            countCt = $previewCampaignModal.find('.channelCountCt')

        for (let key in count.message_type) {
            if (messageTypes.hasOwnProperty(key)) {
                countCt.append(
                    `<div>
                        <b>${Number(count.message_type[key])}</b>
                        ${t('all', messageTypes[key])}
                        ${t('all', 'PushCampaign__Recipients_users')}
                    </div>`
                )

                if (key === '12') {
                    countCt.append(`<br><div>${t('all', 'PushCampaign__Recipients_workflow_info')}</div>`);
                }
            }
        }
    };
};
