import ContentSection from './contentSection'
import ContentInappBannerSection from './contentInappBannerSection'
import ContentInappFullsizeSection from './contentInappFullsizeSection'
import ContentInappEditorSection from './contentInappEditorSection'
import ContentInappHtmlSection from './contentInappHtmlSection'
import Dropzone from 'vendor/dropzone/package'
import { stdPlashes } from '@/jquery/widgets/general'

export default function (campaignPage) {

    var TYPE_BANNER = '0';
    var TYPE_FULLSIZE = '1';
    var TYPE_CUSTOM_HTML = '2';
    var TYPE_EDITOR = '3';

    var ORIENTATION_BOTH = '0';
    var ORIENTATION_PORTRAIT = '1';
    var ORIENTATION_LANDSCAPE = '2';

    var that = new ContentSection(campaignPage);

    var $campaignPage,
        $contentSection
            = null;

    var contentInappBanner,
        contentInappFullsize,
        contentInappEditor,
        contentInappHtml
             = null;

    that.init = function() {
        $campaignPage = campaignPage.getLayout();
        $contentSection = $campaignPage.find('#content-inapp_section');
        if (!$contentSection.length) return null;

        contentInappBanner = new ContentInappBannerSection(that);
        contentInappFullsize = new ContentInappFullsizeSection(that);
        contentInappEditor = new ContentInappEditorSection(that);
        contentInappHtml = new ContentInappHtmlSection(that);

        that.initSection($contentSection);
        return that;
    };

    that.initVariant = function(variant) {
        $(variant).data('initiated', false);

        contentInappBanner.initVariant(variant);
        contentInappFullsize.initVariant(variant);
        contentInappEditor.initVariant(variant);
        contentInappHtml.initVariant(variant);

        $contentSection.on('section-tab-click', function() {
            switch ($(variant).find('.inappType input:radio:checked').val()) {
                case TYPE_BANNER:
                    contentInappBanner.updateBannerPreview(variant);
                    break;
                case TYPE_FULLSIZE:
                    contentInappFullsize.updateFullsizePreview(variant);
                    break;
                case TYPE_EDITOR:
                    contentInappEditor.updateEditorPreview(variant);
                    break;
                case TYPE_CUSTOM_HTML:
                    contentInappHtml.updateCustomPreview(variant);
                    break;
            }
        });

        if (!$(variant).find('.inappType').data('stdPlashes')) {
            new stdPlashes($(variant).find('.inappType'));
        }

        $(variant).find('.inappType input:radio').change(function() {
            // if banner-position is hidden and clicked not a CUSTOM HTML button...
            if (!$(variant).find('.inappOrientation:visible').length
                    && ($(this).val() == TYPE_BANNER || $(this).val() == TYPE_FULLSIZE)) {
                $(variant).find('.inappOrientation').slideDown();
            } else if ($(variant).find('.inappOrientation:visible').length
                && ($(this).val() == TYPE_EDITOR || $(this).val() == TYPE_CUSTOM_HTML)) {
                $(variant).find('.inappOrientation').slideUp();
            }

            $(variant).find('.inappControls > .tab').hide();
            $(variant).find('.inappControls .formElement').attr("disabled", "disabled");
            $(variant).find('.inappPreview .portrait .editorContainer').parent()
                .removeClass('container').hide();
            $(variant).find('.inappPreview .portrait .oldContainer')
                .removeClass('oldContainer')
                .addClass('container').show();

            switch ($(this).val()) {
                case TYPE_BANNER:
                    if ($(variant).find('.inappOrientation input:radio:checked').val() == undefined) {
                        if ($(variant).find('.inappStdSectionWrapper:visible').length) {
                            $(variant).find('.inappStdSectionWrapper').slideUp();
                        }
                    } else {
                        $(variant).find('.inappOrientation input:radio:checked').trigger("change");
                    }
                    $(variant).find('.inappControls .tab.banner').show();
                    $(variant).find('.banner.tab .jsBanner .bannerFormElement, ' +
                        '.banner.tab .jsCreatives .bannerFormElement').removeAttr("disabled");

                    $.each(['portrait', 'landscape'], function(i, orientation) {
                        if ($(variant).find('.jsCreatives .' + orientation + ' .jsCreative').length == 0) {
                            $(variant).find('.inappControls .jsAddCreative').trigger('click', [orientation]);
                        }
                    });
                    $(variant).find('.banner.tab .jsBanner .mainSelect').trigger('change');

                    contentInappBanner.updateBannerPreview(variant);
                    break;
                case TYPE_FULLSIZE:
                    if ($(variant).find('.inappOrientation input:radio:checked').val() == undefined) {
                        if ($(variant).find('.inappStdSectionWrapper:visible').length) {
                            $(variant).find('.inappStdSectionWrapper').slideUp();
                        }
                    } else {
                        $(variant).find('.inappOrientation input:radio:checked').trigger("change");
                    }

                    $(variant).find('.inappControls .tab.fullsize').show();
                    $(variant).find('.buttonsContainer .fullsizeFormElement, ' +
                        '.backgroundButton .fullsizeFormElement')
                        .removeAttr("disabled");

                    $(variant).find('.fullsize.tab .mainSelect').trigger('change');

                    contentInappFullsize.updateFullsizePreview(variant);
                    break;
                case TYPE_EDITOR:
                    if ($(variant).find('.inappOrientation:visible').length) {
                        $(variant).find('.inappOrientation').slideUp();
                    }
                    if (!$(variant).find('.inappStdSectionWrapper:visible').length) {
                        $(variant).find('.inappStdSectionWrapper').slideDown();
                    }

                    $(variant).find('.inappControls .tab.editor').show();
                    $(variant).find('.inappControls .editorFormElement').removeAttr("disabled");
                    $(variant).find('.orientationSelectorContainer').hide();
                    $(variant).find('.inappPreview .outerContainer.portrait .container')
                        .addClass('oldContainer')
                        .removeClass('container').hide();
                    $(variant).find('.inappPreview .outerContainer.portrait .editorContainer').parent()
                        .addClass('container').removeClass('oldContainer').show();
                    that.changeOrientation(variant, 'orientationPortrait');

                    $(variant).find('.editor.tab .mainSelect').trigger('change');

                    contentInappEditor.updateEditorPreview(variant);
                    break;
                case TYPE_CUSTOM_HTML:
                    if ($(variant).find('.inappOrientation:visible').length) {
                        $(variant).find('.inappOrientation').slideUp();
                    }
                    if (!$(variant).find('.inappStdSectionWrapper:visible').length) {
                        $(variant).find('.inappStdSectionWrapper').slideDown();
                    }

                    $(variant).find('.inappControls .tab.customHtml').show();
                    $(variant).find('.orientationSelectorContainer')
                        .css('visibility', 'visible').show();
                    $(variant).find('.inappControls .customHtmlFormElement').removeAttr("disabled");

                    contentInappHtml.updateCustomPreview(variant);
                    break;
            }
        });

        if (!$(variant).find('.inappOrientation').data('stdPlashes')) {
            new stdPlashes($(variant).find('.inappOrientation'));
        }

        $(variant).find('.inappOrientation input:radio').change(function() {
            if (!$(variant).find('.inappStdSectionWrapper:visible').length) {
                $(variant).find('.inappStdSectionWrapper').slideDown();
            }
            switch ($(this).val()) {
                case ORIENTATION_PORTRAIT:
                    $(variant).find('.orientationSelectorContainer').css('visibility', 'hidden').show();
                    that.changeOrientation(variant, 'orientationPortrait');
                    break;
                case ORIENTATION_LANDSCAPE:
                    $(variant).find('.orientationSelectorContainer').css('visibility', 'hidden').show();
                    that.changeOrientation(variant, 'orientationLandscape');
                    break;
                case ORIENTATION_BOTH:
                    $(variant).find('.orientationSelectorContainer').css('visibility', 'visible').show();
                    var orientation = $(variant).find(".orientationValue").val();
                    if (orientation == "" && orientation == undefined) {
                        that.changeOrientation(variant, 'orientationPortrait');
                    }
                    break;
            }
        });

        if ($campaignPage.data("manage")) {
            $(variant).find('.stdPlashesColumn').click(function() {
                $(this).find('input:radio').trigger('change');
            });
        } else {
            $(variant).find('.screenSizeSelector').prop('disabled', false).removeClass('viewable');
        }

        that.initOrientationSelector(variant);
        $(variant).find('.orientationSelectorContainer .active.dbutton').trigger('click');

        $(variant).find('.screenSizeSelector').change(function() {
            var original_width = parseInt($(this).val().replace(/x.+$/, ''));
            var original_height = parseInt($(this).val().replace(/^\d+x/, ''));
            var height = 530;
            var width = Math.round(height / original_height * original_width);

            var screen = $(variant).find('.preview .screen');
            screen.css('width', width + "px").css('height', height + "px");
            screen.find('.outerContainer.landscape .container')
                    .css('width', height + "px").css('height', width + "px")
                    .css('top', ((height - width) / 2) + "px").css('left', (-(height - width) / 2) + "px")
                    .attr('original-width', original_width).attr('original-height', original_height);
            if ($(this).find('option:selected').text().indexOf('iPad') > -1) {
                screen.parents('table').removeClass('iphone').addClass('ipad');
            } else {
                screen.parents('table').removeClass('ipad').addClass('iphone');
            }

            switch ($(variant).find('.inappType input:radio:checked').val()) {
                case TYPE_BANNER:
                    contentInappBanner.updateBannerPreview(variant);
                    break;
                case TYPE_FULLSIZE:
                    contentInappFullsize.updateFullsizePreview(variant);
                    break;
                case TYPE_EDITOR:
                    contentInappEditor.updateEditorPreview(variant);
                    break;
                case TYPE_CUSTOM_HTML:
                    contentInappHtml.updateCustomPreview(variant);
                    break;
            }
        }).trigger('change');

        $(variant).on('change', '.inappControls .mainSelect', function() {
            var value = $(this).val();
            $(this).parents('.input-controlBox').find('.choice').each(function (i, item) {
                $(item).hide();
                if (!$(item).hasClass('choice-' + value)) {
                    $(item).find('input,select,textarea').attr("disabled", "disabled");
                }
            });
            $(this).parents('.input-controlBox').find('.choice-' + value)
                .css('display', 'inline-block').find('input,select,textarea').removeAttr("disabled");
        });

        $(variant).find('.inappControls .jsItems .jsItem').each(function(i, item) {
            that.initItem(variant, item);
        });

        $(variant).find('.jsOnsiteSpecialSetAttribute').autocomplete({
            source: '/api/tag/tag/autocomplete?project_id=' + window.xp.project.id + '&attribute=1'
        });

        $(variant).find('.jsOnsiteSpecialAddToList').comboboxAutocomplete({
            source: '/api/lists/user-list/autocomplete?project_id=' + window.xp.project.id
                + '&identifier[0]=profile_id&identifier[1]=device_id'
        });

        $(variant).find('.jsOnsiteSpecialSubscriptionPreference').comboboxAutocomplete({
            source: '/api/push/subscription-preference/autocomplete?project_id=' + window.xp.project.id
        });

        $(variant).find('.jsOnsiteSpecialTriggerEvent').autocomplete({
            source: '/api/event/event/autocomplete?project_id=' + window.xp.project.id
        });

        Dropzone.autoDiscover = false;
        $(variant).find('.inappPreview .container, .backgroundButton .fake-input-file, .editor .sortableImage .fake-input-file').dropzone({
            url: "/push/campaign/upload-image?project_id="+window.xp.project.id,
            maxFilesize: 10,
            thumbnailWidth: null,
            acceptedFiles: 'image/*',
            addRemoveLinks: true,
            paramName: "CampaignMessage[image]",
            params: {
                _csrf: $(variant).parents("form").find('input[type=hidden][name=_csrf]').val()
            },
            dragenter: function() {
                switch ($(variant).find('.inappType input:radio:checked').val()) {
                    case TYPE_BANNER:
                        this.options.previewTemplate = $(variant).find('.previewBannerTemplate')[0].innerHTML;
                        this.options.addRemoveLinks = false;
                        break;
                    case TYPE_FULLSIZE:
                        this.options.previewTemplate = $(variant).find('.previewFullsizeTemplate')[0].innerHTML;
                        this.options.addRemoveLinks = true;
                        break;
                    case TYPE_EDITOR:
                        this.options.previewTemplate = $(variant).find('.previewEditorTemplate')[0].innerHTML;
                        this.options.addRemoveLinks = true;
                        break;
                }
            },
            addedfile: function(file) {
                this.previewsContainer = $(variant).find(".inappPreview .container:visible")[0];
                Dropzone.prototype.defaultOptions.addedfile.call(this, file);
            },
            sending: function(file, xhr, formData) {
                switch ($(variant).find('.inappType input:radio:checked').val()) {
                    case TYPE_BANNER:
                        formData.append('type', 'creative-upload');
                        break;
                    case TYPE_FULLSIZE:
                        formData.append('type', 'button-upload');
                        break;
                    case TYPE_EDITOR:
                        formData.append('type', 'image-upload');
                        break;
                }
                $(variant).find('.inappPreview .container:visible .watermark').hide();
            },
            success: function(event, response, xhr) {
                switch ($(variant).find('.inappType input:radio:checked').val()) {
                    case TYPE_BANNER:
                        var creative = contentInappBanner.getRespectiveCreative(variant);
                        var button_id = $(creative).find('.imageFile').attr("static-id");
                        contentInappBanner.updateBannerPreview(variant, response.filename, button_id);
                        break;
                    case TYPE_FULLSIZE:
                        contentInappFullsize.updateFullsizePreview(variant, response.filename);
                        break;
                    case TYPE_EDITOR:
                        contentInappEditor.updateEditorPreview(variant, response.filename);
                        break;
                }
            }
        });
        $(variant).find('.inappPreview .container').addClass('dropzone');
        $(variant).on('click', '.fake-input-file a', function() {
            $(this).parent().click();
        });

        // initialize InApp form with loaded data
        var formType = $(variant).find('.inappType input:radio:checked').val();

        $(variant).find('.inappType input:radio:checked').trigger('change');
        $(variant).find('.inappOrientation input:radio:checked').trigger('change');
        that.changeOrientation(variant, $(variant).find('.orientationValue').val() || 'orientationPortrait');

        $(variant).data('initiated', true);

        switch (formType) {
            case TYPE_BANNER:
                contentInappBanner.updateBannerPreview(variant);
                break;
            case TYPE_FULLSIZE:
                contentInappFullsize.updateFullsizePreview(variant, undefined, 'portrait', undefined, true);
                contentInappFullsize.updateFullsizePreview(variant, undefined, 'landscape', undefined, true);
                break;
            case TYPE_EDITOR:
                contentInappEditor.updateEditorPreview(variant);
                break;
            case TYPE_CUSTOM_HTML:
                contentInappHtml.updateCustomPreview(variant);
                break;
        }
    };

    that.initItem = function(variant, item, orientation) {
        $(item).find('.fake-input-file').dropzone({
            url: "/push/campaign/upload-image?project_id="+window.xp.project.id,
            maxFilesize: 10,
            thumbnailWidth: null,
            acceptedFiles: 'image/*',
            addRemoveLinks: true,
            paramName: "CampaignMessage[image]",
            params: {
                _csrf: $(variant).parents("form").find('input[type=hidden][name=_csrf]').val()
            },
            addedfile: function(file) {
                this.previewsContainer = $(variant).find(".inappPreview .container:visible")[0];
                switch ($(variant).find('.inappType input:radio:checked').val()) {
                    case TYPE_BANNER:
                        this.options.previewTemplate = $(variant).find('.previewBannerTemplate')[0].innerHTML;
                        this.options.addRemoveLinks = false;
                        break;
                    case TYPE_FULLSIZE:
                        this.options.previewTemplate = $(variant).find('.previewFullsizeTemplate')[0].innerHTML;
                        this.options.addRemoveLinks = true;
                        break;
                }
                Dropzone.prototype.defaultOptions.addedfile.call(this, file);
            },
            sending: function(file, xhr, formData) {
                switch ($(variant).find('.inappType input:radio:checked').val()) {
                    case TYPE_BANNER:
                        formData.append('type', 'creative-upload');
                        break;
                    case TYPE_FULLSIZE:
                        formData.append('type', 'button-upload');
                        break;
                }
            },
            success: function(event, response, xhr) {
                $(variant).find('.inappPreview .container:visible .dz-preview').hide();
                switch ($(variant).find('.inappType input:radio:checked').val()) {
                    case TYPE_BANNER:
                        var button_id = $(item).find('.imageFile').attr('static-id');
                        contentInappBanner.updateBannerPreview(variant, response.filename, button_id);
                        break;
                    case TYPE_FULLSIZE:
                        contentInappFullsize.createButtonImage(variant, $(this.element), response.filename);
                        break;
                }
            }
        });
        // change "orientation" keyword to orientation value
        $(item).find('input,select').each(function() {
            var name_attr = $(this).attr("name");
            name_attr = name_attr.replace("orientation", orientation);
            $(this).attr("name", name_attr).removeAttr("disabled");
        });
        $(item).find('.typeSelector').val('image').trigger('change');
        if ($(item).parents('.' + orientation).find('.jsCreative').length == 1) {
            $(item).find('.height').val(50);
            $(item).find('.minWidth').val(320);
        }
        $(item).find('.mainSelect').trigger('change');
        var orientation = that.getScreenParams(variant).or;
        if (orientation == 'landscape') {
            $(item).find(".portrait").hide();
            $(item).find(".landscape").show();
        }
    };

    that.updateItemsIndexes = function(variant, item) {
        var static_id_elem = $(item).parents(".jsItems");
        var static_id = static_id_elem.attr("static-id");
        var item_type = $(item).find('.itemLabel').text().indexOf('BUTTON') > -1 ?
                'Button' : 'Creative';
        var name = $(variant).find('.inappControls .js' + item_type + 'Template .jsItem .itemLabel').text();

        var counter = 1;
        var container = $(item).parents('.jsButtons').length ?
                $(item).parents('.jsButtons') :
                $(item).parents('.creativeOrientation');

        $(container).find('.jsItem').each(function(){
            var index = counter++;
            $(this).find('.itemLabel').text(name + ' ' + index);
            $(this).find('input,select').each(function() {
                if ($(this).attr('static-id') == undefined) {
                    $(this).attr('static-id', static_id);
                    var fieldName = $(this).attr('name');
                    fieldName = fieldName.replace(/^(.+\[button_)(\].+$)/, "$1" + static_id + "$2")
                            .replace(/^(.+\[creative_)(\].+$)/, "$1" + static_id + "$2");
                    $(this).attr('name', fieldName);
                }
            });
        });
    };

    that.initOrientationSelector = function(variant) {
        var $container = $(variant).find('.orientationSelectorContainer');
        $container.find('.dbutton').click(function () {
            that.changeOrientation(variant, $(this).data('value'));
        });
    };

    that.changeOrientation = function(variant, orientation) {
        var $container = $(variant).find('.orientationSelectorContainer');
        var $button = $container.find('[data-value=' + orientation + ']');
        $container.find('.dbutton').removeClass('active');
        $button.addClass('active');
        $container.find('.orientationValue').val($button.data('value'));
        if (orientation === "orientationPortrait") {
            $(variant).find(".inappPreview table").removeClass('landscape');
            $(variant).find(".inappPreview .outerContainer.landscape").hide();
            $(variant).find(".inappPreview .outerContainer.portrait").show();

            $(variant).find(".backgroundButton .landscape").hide();
            $(variant).find(".backgroundButton .portrait").show();

            $(variant).find(".buttonsContainer .jsButton .choice-image .landscape").hide();
            $(variant).find(".buttonsContainer .jsButton .choice-image .portrait").show();

            $(variant).find(".creativesContainer .landscape").hide();
            $(variant).find(".creativesContainer .portrait").show();
        } else {
            $(variant).find(".inappPreview table").addClass('landscape');
            $(variant).find(".inappPreview .outerContainer.portrait").hide();
            $(variant).find(".inappPreview .outerContainer.landscape").show();

            $(variant).find(".backgroundButton .portrait").hide();
            $(variant).find(".backgroundButton .landscape").show();

            $(variant).find(".buttonsContainer .jsButton .choice-image .portrait").hide();
            $(variant).find(".buttonsContainer .jsButton .choice-image .landscape").show();

            $(variant).find(".creativesContainer .portrait").hide();
            $(variant).find(".creativesContainer .landscape").show();
        }
        switch ($(variant).find('.inappType input:radio:checked').val()) {
            case TYPE_BANNER:
                contentInappBanner.updateBannerPreview(variant);
                break;
            case TYPE_FULLSIZE:
                contentInappFullsize.updateFullsizePreview(variant);
                break;
            case TYPE_EDITOR:
                contentInappEditor.updateEditorPreview(variant);
                break;
            case TYPE_CUSTOM_HTML:
                contentInappHtml.updateCustomPreview(variant);
                break;
        }
    };

    that.getScreenParams = function(variant, orientation) {
        if (orientation == undefined) {
            orientation = $(variant).find(".orientationValue").val().replace("orientation", "").toLowerCase();
        }
        var screen_size = $(variant).find(".screenSizeSelector").val();
        var original_width = parseInt(screen_size.replace(/x.+$/, ''));
        var original_height = parseInt(screen_size.replace(/^\d+x/, ''));
        var height = 530;
        var width = Math.round(height / original_height * original_width);
        if (orientation == 'landscape') {
            var t = height; height = width; width = t;
            t = original_height; original_height = original_width; original_width = t;
        }
        return {
            or: orientation,
            width: width,
            height: height,
            original_width: original_width,
            original_height: original_height
        };
    };

    return that.init();
};
