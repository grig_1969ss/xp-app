import ContentEmailColorPicker from './contentEmailColorPicker'

export default function  (contentEmailSection, $editorItemContainer, $previewItemContainer) {
    var that = this;

    var $eItem = null,
        $pItem = null,
        $targetButton = null,
        $titleField = null,
        $urlField = null,
        $containerBgColorPicker = null,
        $textColorPicker = null,
        $bgColorPicker = null,
        $borderColorPicker = null,
        $textAlignField = null,
        $borderRadiusField = null,
        $borderWidthField = null,
        $borderPaddingField = null,
        $fontFamilyField = null,
        $fontWeightField = null,
        $fontSizeField = null;

    this.init = function() {
        $eItem = $editorItemContainer;
        $pItem = $previewItemContainer;

        $titleField  = $eItem.find('.titleField');
        $urlField  = $eItem.find('.urlField');
        $borderRadiusField  = $eItem.find('.borderRadiusField');
        $borderWidthField  = $eItem.find('.borderWidthField');
        $borderPaddingField  = $eItem.find('.borderPaddingField');
        $fontFamilyField  = $eItem.find('.fontFamilyField');
        $fontSizeField  = $eItem.find('.fontSizeField');
        $fontWeightField = $eItem.find('.fontWeightField');
        $textAlignField  = $eItem.find('.textAlignField');

        var $link = $('<a/>', {
            href: $urlField.val(),
            target: '_blank',
        }).addClass('button');
        $pItem.empty().append($link)
            .css('text-align', $textAlignField.val());

        var $colorpicker = $eItem.find('input.colorpickerField.containerBgColorField');
        $containerBgColorPicker = new ContentEmailColorPicker($colorpicker, $pItem.parent(), 'background');
        $colorpicker = $eItem.find('input.colorpickerField.textColorField');
        $textColorPicker = new ContentEmailColorPicker($colorpicker, $pItem.find('a.button'), 'color');
        $colorpicker = $eItem.find('input.colorpickerField.bgColorField');
        $bgColorPicker = new ContentEmailColorPicker($colorpicker, $pItem.find('a.button'), 'background');
        $colorpicker = $eItem.find('input.colorpickerField.borderColorField');
        $borderColorPicker = new ContentEmailColorPicker($colorpicker, $pItem.find('a.button'), 'border-color');

        var $targetFields = [$titleField, $urlField, $borderRadiusField, $borderWidthField,
            $borderPaddingField, $fontFamilyField, $fontSizeField, $fontWeightField, $textAlignField];
        $.each($targetFields, function(i, $elem) {
            $elem.on('keyup change', function() {
                that.updateButton();
            });
        });

        return this;
    };

    this.updateButton = function() {
        if ($targetButton !== undefined) {
            $targetButton = $pItem.find('.button');
        }
        if ($titleField.val().length) {
            $targetButton.text($titleField.val());
        } else {
            $targetButton.html('&nbsp;');
        }
        var radius = (parseInt($borderRadiusField.val()) || 0) + 'px';
        var padding = (parseInt($borderPaddingField.val()) || 0) + 'px';
        $targetButton.attr('href', $urlField.val())
            .css('border-width', (parseInt($borderWidthField.val()) || 0) + 'px')
            .css('border-style', 'solid')
            .css('border-radius', radius)
            .css('padding', padding)
            .css('font-family', $fontFamilyField.val())
            .css('font-size', $fontSizeField.val())
            .css('font-weight', $fontWeightField.val())
            .css('-webkit-border-radius', radius)
            .css('-moz-border-radius', radius)
            .css('text-decoration', 'none')
            .css('display', 'inline-block');

        $pItem.css('padding', '16px');

        if ($textAlignField.val() == 'full_width') {
            $pItem.css('text-align', 'center');
            $targetButton.css('width', '100%')
                .css('box-sizing', 'border-box');
        } else {
            $pItem.css('text-align', $textAlignField.val());
            $targetButton.css('width', 'auto');
        }
        contentEmailSection.updateIframe();
    };

    this.render = function() {
        that.updateButton();
        $containerBgColorPicker.render();
        $textColorPicker.render();
        $bgColorPicker.render();
        $borderColorPicker.render();
        contentEmailSection.updateIframe();
    };

    return this.init();
};
