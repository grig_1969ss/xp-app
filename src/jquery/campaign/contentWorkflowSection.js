import ContentSection from './contentSection'
import RestApi from "@/core/services/RestApi";
import { projectEndpoint } from "@/core/helpers";
import Vue from 'vue'

export default function (campaignPage, platform, inherit) {

  let that = new ContentSection(campaignPage);

  let $campaignPage,
    $contentSection,
    $editWorkflowDialog
      = null;

  that.init = function() {
    $campaignPage = campaignPage.getLayout();
    $contentSection = $campaignPage.find('#content-workflow_section');
    if (!$contentSection.length) return null;

    $editWorkflowDialog = $('#editWorkflowModal');
    if (!$editWorkflowDialog.length) return null;

    $contentSection.on('section-tab-click', function (event) {
      event.preventDefault();
      let activeVariant = $(this).find('.jsMessageVariantsTabButton.tabContentActive');
      that.enableVariant('.jsMessageVariant[data-key=' + activeVariant.data('key') + ']')
    });

    $contentSection.on('click', '.jsMessageVariantsTabButton', function () {
      const variant = '.jsMessageVariant[data-key=' + $(this).data('key') + ']';
      $(variant).find('.workflow-preview').remove();
    });

    that.initSection($contentSection);
    return that;
  };

  that.initVariant = function(variant) {
    that.initWorkflow(variant);
  };

  that.openWorkflowBuilder = function(variant, data) {
    $(variant).find('.jsWorkflowEditor').show();

    let container = $('<div>');
    $(variant).find('.jsWorkflowEditor').append(container);

    const Connector = Vue.extend(Vue.component('WorkflowBuilder'));
    that.editor = new Connector({
      el: container[0],
      propsData: {
        onClose: function() {
          that.editor.$destroy();
          that.editor = null;
          $(variant).find('.jsWorkflowEditor').html('').hide();
          that.reloadPreview(variant);
        },
        workflowId: Number(data.workflowId),
        projectId: data.projectId,
        mxaDomain: data.mxaDomain
      }
    })
  };

  that.enableVariant = function(variant) {
    const workflowId = $(variant).find('#workflow_id').val(),
      projectId = window.xp.project.id,
      mxaDomain = window.xp.mxa,
      url = `https://${mxaDomain}/project/${projectId}/workflow/${workflowId}/preview`;

    const iframeEl = $(variant).find('.workflow-preview');
    if (iframeEl.length > 0) {
      return;
    }

    if (!workflowId) {
      return;
    }

    const iframe = document.createElement('iframe');
    const frameEl = $(variant).find('.frameEl');

    iframe.style.border = '1px solid #d6d8db';
    iframe.className = 'workflow-preview';
    Object.assign(iframe, {
      width: '100%',
      height: '700px',
      src: url
    });

    frameEl.append(iframe);
    $(variant).find('.frameCt').show();
  };

  that.reloadPreview = function(variant) {
    const frameEl = $(variant).find('.workflow-preview');
    frameEl.attr('src', frameEl.attr('src'));
  };

  that.showWorkflowDialog = function() {
    $editWorkflowDialog.bPopup({
      appendTo: '#popupsContainer',
      opacity: 0.9,
    });
  };

  that.initWorkflowDialogButtons = function(callback) {
    $editWorkflowDialog.find('#cancelEditWorkflowModal').click(function(event) {
      event.preventDefault();
      $editWorkflowDialog.close();
    });

    $editWorkflowDialog.find('#confirmEditWorkflowModal').click(function(event) {
      event.preventDefault();
      $editWorkflowDialog.close();

      $.when($('#changeStatusModal #changeStatusPushModal').click()).done(function() {
        $contentSection.attr('data-is_running', 0);
        if ($.isFunction(callback)) {
          callback();
        }
      });
    });
  };

  that.initWorkflow = function(variant) {
    const workflow = variant.querySelector('#workflow_id'),
      projectId = window.xp.project.id,
      mxaDomain = window.xp.mxa;

    const workflowOpenCallback = function() {
      that.openWorkflowBuilder(variant, {projectId: projectId, mxaDomain: mxaDomain, workflowId: workflow.value});
      that.enableVariant(variant);
    };

    that.initWorkflowDialogButtons(workflowOpenCallback);

    $(variant).find('.jsWorkflowBtn').click(async function() {
      if (!workflow.value) {
        this.style['pointer-events'] = 'none';
        await RestApi(
          projectEndpoint({
            method: 'post',
            url: `campaigns/create-workflow`,
            data: {"campaignId": $campaignPage.find('#PushMessage_campaignId').val()}
          })
        ).then(({data}) => {
          workflow.value = data.id;
          this.style['pointer-events'] = '';
          this.innerHTML = '<i class="icon-layout"></i>' + t('all', 'Campaign__Workflow_Edit');
          that.enableVariant(variant);
        })
      }

      let status = $campaignPage.find('#PushMessage_status');
      if ($.inArray(Number(status.val()), [1, 2]) !== -1) {
        that.showWorkflowDialog();
      } else {
        workflowOpenCallback();
      }
    });
  };

  if (inherit || inherit === undefined) {
    return that.init();
  }

  return that;
};
