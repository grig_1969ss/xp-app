import ConditionsEditor from '@/jquery/widgets/conditionsEditor'
import TabContent from '@/jquery/widgets/tabContent'
import RestApi from '@/core/services/RestApi'
import { projectEndpoint } from '@/core/helpers'

export default function (campaignPage) {
    var that = this;

    var $campaignPage,
        $startSection,
        $mobileEncryption
            = null;

    var $goalContainer,
        $conditionsEditor
            = null;

    this.init = function() {
        $campaignPage = campaignPage.getLayout();
        $startSection = $campaignPage.find('#start_section');
        $mobileEncryption = $startSection.find('.jsMobileEncryption');

        this.initChannels();
        this.initPromotion();
        this.initConversions();
        this.initRedemptions();
        this.initPushCategory();
        this.initAb();
        this.initMultilanguage();
        this.initContentFetch();

        return this;
    };

    this.initChannels = function() {
        if ($campaignPage.data('workflow')) {
            campaignPage.enableSection('content-workflow');
        }

        $startSection.find('.jsIos').on('select', function(){
            campaignPage.enableSection('content-ios');
            $mobileEncryption.addClass('ios');
            that.updateScheduleOptions();
        }).on('deselect', function(){
            campaignPage.disableSection('content-ios');
            $mobileEncryption.removeClass('ios');
            that.updateScheduleOptions();
        });

        $startSection.find('.jsAndroid').on('select', function(){
            campaignPage.enableSection('content-android');
            $mobileEncryption.addClass('android');
            that.updateScheduleOptions();
        }).on('deselect', function(){
            campaignPage.disableSection('content-android');
            $mobileEncryption.removeClass('android');
            that.updateScheduleOptions();
        });

        $startSection.find('.jsWebpush').on('select', function(){
            campaignPage.enableSection('content-webpush');
            if (campaignPage.contentInboxSection) campaignPage.contentInboxSection.enableSameSelect();
            that.updateScheduleOptions();
        }).on('deselect', function(){
            campaignPage.disableSection('content-webpush');
            if (campaignPage.contentInboxSection) campaignPage.contentInboxSection.disableSameSelect();
            that.updateScheduleOptions();
        });

        $startSection.find('.jsInbox').on('select', function(){
            campaignPage.enableSection('content-inbox');
            that.updateScheduleOptions();
        }).on('deselect', function(){
            campaignPage.disableSection('content-inbox');
            that.updateScheduleOptions();
        });

        $startSection.find('.jsSms').on('select', function(){
            campaignPage.enableSection('content-sms');
        }).on('deselect', function(){
            campaignPage.disableSection('content-sms');
         });

        $startSection.find('.jsFacebook').on('select', function(){
            campaignPage.enableSection('content-facebook');
        }).on('deselect', function(){
            campaignPage.disableSection('content-facebook');
        });

        $startSection.find('.jsWhatsapp').on('select', function(){
            campaignPage.enableSection('content-whatsapp');
        }).on('deselect', function(){
            campaignPage.disableSection('content-whatsapp');
        });

        $startSection.find('.jsEmail').on('select', function(){
            campaignPage.enableSection('content-email');
        }).on('deselect', function(){
            campaignPage.disableSection('content-email');
        });

        $startSection.find('.jsWebhook').on('select', function(){
            campaignPage.enableSection('content-webhook');
            that.updateScheduleOptions();
        }).on('deselect', function() {
            campaignPage.disableSection('content-webhook');
            that.updateScheduleOptions();
        });

        $startSection.find('.jsOnsite').on('select', function(){
            campaignPage.enableSection('content-onsite');
            that.updateScheduleOptions();
        }).on('deselect', function(){
            campaignPage.disableSection('content-onsite');
            that.updateScheduleOptions();
        });

        $startSection.find('.jsInappIos').on('select', function(){
            campaignPage.enableSection('content-inapp');
            that.updateScheduleOptions();
            $startSection.find('.jsInappStatus').val(1);
        }).on('deselect', function(){
            if(!$startSection.find('.jsInappAndroid').prop('checked')) {
                campaignPage.disableSection('content-inapp');
                that.updateScheduleOptions();
                $startSection.find('.jsInappStatus').val(0);
            }
        });

        $startSection.find('.jsInappAndroid').on('select', function(){
            campaignPage.enableSection('content-inapp');
            that.updateScheduleOptions();
            $startSection.find('.jsInappStatus').val(1);
        }).on('deselect', function(){
            if(!$startSection.find('.jsInappIos').prop('checked')) {
                campaignPage.disableSection('content-inapp');
                that.updateScheduleOptions();
                $startSection.find('.jsInappStatus').val(0);
            }
        });

        setTimeout(function(){
            that.updateScheduleOptions();
        }, 0);
    };

    this.initPromotion = function() {
        $('.jsPromotionLoader').comboboxAutocomplete({
            source: '/promotion/promotion/autocomplete?project_id=' + window.xp.project.id,
            select: function(event, ui) {
                that.selectPromotion(ui.item);
            },
            change: function(event, ui) {
                if(!ui.item) {
                    that.selectPromotion({id: '', value: ''});
                }
            }
        });

        $startSection.find('.jsPromotion').on('select', function() {
            that.enablePromotion();
        }).on('deselect', function() {
            that.disablePromotion();
        });
    };

    this.enablePromotion = function() {
        $startSection.find('.jsVariantDataContainer').addClass('abVariantColumns2');
        $startSection.find('.jsPromotionField, .jsVariantPromotion').show();
    };

    this.disablePromotion = function() {
        $startSection.find('.jsVariantDataContainer').removeClass('abVariantColumns2');
        $startSection.find('.jsPromotionField, .jsVariantPromotion').hide();
    };

    this.selectPromotion = function(item) {
        $startSection.find('.jsVariantTemplate .jsVariantPromotionLoader').val(item.id);
        $startSection.find('.jsVariants .jsVariantPromotionLoader[data-initial]').each(function() {
            $(this).val(item.value).parent().find('input[type=hidden]').val(item.id);
        });
    };

    this.initConversions = function() {
        $goalContainer  = $startSection.find('#goalContainer');
        if (!$goalContainer.length) return;

        $conditionsEditor = new ConditionsEditor();
        $conditionsEditor.init($goalContainer, '/goal/goal/attribute-list-data');

        $startSection.find('.jsConversions').on('select', function(){
            that.enableConversions();
        }).on('deselect', function(){
            that.disableConversions();
        });
    };

    this.enableConversions = function() {
        $startSection.find('.jsGoal').show();
    };

    this.disableConversions = function() {
        $startSection.find('.jsGoal').hide();
    };

    this.initRedemptions = function() {
        $startSection.find('.jsRedemptions').on('select', function(){
            that.enableRedemptions();
        }).on('deselect', function(){
            that.disableRedemptions();
        });
        $('.pushRedemptionCampaign').comboboxAutocomplete({
            source: '/redemption/redemption-campaign/autocomplete?project_id=' + window.xp.project.id
        });
    };

    this.enableRedemptions = function() {
        $startSection.find('.jsRedemptionsContainer').show();
        campaignPage.updateInappSection();
    };

    this.disableRedemptions = function() {
        $startSection.find('.jsRedemptionsContainer').hide();
        $startSection.find('input[name="Campaign[redemption_campaign_id]"]').val('');
        campaignPage.updateInappSection();
    };

    this.requiresInapp = function() {
        return $startSection.find('.jsRedemptions').prop('checked') ||
            $startSection.find('.jsInappAndroid').prop('checked') ||
            $startSection.find('.jsInappIos').prop('checked');
    };

    this.initPushCategory = function() {
        $('.pushCategory').comboboxAutocomplete({
            source: '/push/category/autocomplete?project_id=' + window.xp.project.id
        });
    };

    this.updateScheduleOptions = function()
    {
        if (campaignPage.scheduleSection) {
            if ($startSection.find('.jsInbox:checked').length) {
                campaignPage.scheduleSection.enablePersistOptions();
            } else {
                campaignPage.scheduleSection.disablePersistOptions();
            }

            if ($startSection.find('.jsIos:checked, .jsAndroid:checked, .jsWebpush:checked').length) {
                campaignPage.scheduleSection.enableRetryOptions();
            } else {
                campaignPage.scheduleSection.disableRetryOptions();
            }
        }
    };


    this.initAb = function() {
        $startSection.find('.jsAb').on('select', function(){
            that.enableAb();
        }).on('deselect', function(){
            that.disableAb();
        });

        $startSection.find('.jsAddVariant').click(function(){
            that.addVariant();
        });
        $startSection.find('.jsVariants .jsVariant').each(function(){
            that.initVariantInfo($(this));
        });

        $startSection.find('.jsCgSize').change(function () {
            var size = parseInt($(this).val());
            if (!size) {
                $(this).val("");
                $startSection.find('.jsCg').prop('checked', false);
            } else {
                $(this).val(size);
                $startSection.find('.jsCg').prop('checked', true);
            }
        });

        $campaignPage.find('.jsMessageVariantsContainer').each(function(){
            new TabContent({
                element: $(this),
                default_tab: 'A'
            });
        });

        that.showHideVariantInfoButtons();
    };

    this.enableAb = function() {
        $startSection.find('.jsVariantsContainer').show();

        $campaignPage.find('.jsMessageVariantsContainer').each(function(){
            $(this).find('.jsMessageVariantsSelectorContainer').show();
        });
    };

    this.disableAb = function() {
        $startSection.find('.jsVariantsContainer').hide();

        $campaignPage.find('.jsMessageVariantsContainer').each(function(){
            $(this).find('jsMessageVariantsSelectorContainer').hide();
            $(this).data('tabContent').selectTab('A');
        });
    };

    this.addVariant = function() {
        var variantsCount = $startSection.find('.jsVariants .jsVariant').length;
        if (variantsCount >= 10) return;

        var lastVariantKey = $startSection.find('.jsVariants .jsVariant:last-child').data('key');
        var newVariantKey = lastVariantKey ? String.fromCharCode(lastVariantKey.charCodeAt(0)+1) : 'A';

        var tmpVariantInfo = $startSection.find('.jsVariantTemplate .jsVariant');
        var newVariantInfo = tmpVariantInfo.clone();
        $startSection.find('.jsVariants').append(newVariantInfo);

        var newVariantMessages = [];
        $campaignPage.find('.jsMessageVariantsContainer').each(function(){
            var tmpVariantMessage = $(this).find('.jsMessageVariantTemplate .jsMessageVariant');
            var newVariantMessage = tmpVariantMessage.clone();
            $(this).find('.jsMessageVariants').append(newVariantMessage);
            newVariantMessages.push(newVariantMessage);

            var $tabsContainer = $(this);
            var tmpVariantTab = $tabsContainer.find('.jsMessageVariantsTabTemplate .jsMessageVariantsTab');
            var newVariantTab = tmpVariantTab.clone();
            $tabsContainer.find('.jsMessageVariantsTabs').append(newVariantTab);
        });

        this.showHideVariantInfoButtons();

        this.updateVariantKey(newVariantInfo, newVariantKey);
        this.initVariantInfo(newVariantInfo);
        for (var i in newVariantMessages) $(newVariantMessages[i]).trigger('init');
    };

    this.removeVariant = function(variantInfo) {
        var variantsCount = $startSection.find('.jsVariants .jsVariant').length;
        if (variantsCount <= 1) return;

        $campaignPage.find('.jsMessageVariantsContainer').each(function(){
            var $messageVariant = $(this).find('.jsMessageVariant[data-key="'+variantInfo.data('key')+'"]');

            // remove workflow orphan
            const workflowId = $messageVariant.find('#workflow_id').val();
            if (workflowId) {
                RestApi(projectEndpoint({
                    method: 'delete',
                    url: `campaigns/delete-workflow/${workflowId}`
                }))
            }
            $messageVariant.remove();

            var $tabsContainer = $(this);
            $tabsContainer.find('.jsMessageVariantsTabButton[data-key="'+variantInfo.data('key')+'"]').parents('.jsMessageVariantsTab').remove();
        });

        variantInfo.remove();

        this.showHideVariantInfoButtons();

        $campaignPage.find('.jsMessageVariantsContainer').each(function(){
            var $tabsContainer = $(this);
            if (!$tabsContainer.find('.jsVariantMessage:visible').length) {
                var tabsContent = $tabsContainer.data('tabContent');
                tabsContent.selectTab('A');
            }
        });
    };

    this.updateVariantKey = function(variantInfo, newKey) {
        var oldKey = variantInfo.data('key');
        if (oldKey == newKey) return;

        variantInfo.attr('data-key', newKey).data('key', newKey);
        variantInfo.find('.jsVariantKey').text(newKey);
        variantInfo.find('input, textarea, select').each(function(){
            if ($(this).attr('name')) {
                $(this).attr('name', $(this).attr('name').replace('['+oldKey+']', '['+newKey+']'));
                $(this).attr('name', $(this).attr('name').replace('['+oldKey+'_', '['+newKey+'_'));
            }
        });
        variantInfo.find('[id^=CampaignVariant_]').each(function(){
            $(this).attr('id', $(this).attr('id').replace('_'+oldKey+'_', '_'+newKey+'_'));
        });
        variantInfo.find('[for^=CampaignVariant_]').each(function(){
            $(this).attr('for', $(this).attr('for').replace('_'+oldKey+'_', '_'+newKey+'_'));
        });

        $campaignPage.find('.jsMessageVariantsContainer').each(function(){
            var $messageVariant = $(this).find('.jsMessageVariants .jsMessageVariant[data-key="'+oldKey+'"]');
            var messageType = $messageVariant.parents('.jsMessage').eq(0).data('type');
            $messageVariant.attr('data-key', newKey).data('key', newKey);
            $messageVariant.find('input, textarea, select').each(function(){
                if ($(this).attr('name')) {
                    $(this).attr('name', $(this).attr('name').replace('['+oldKey+']', '['+newKey+']'));
                    $(this).attr('name', $(this).attr('name').replace('['+oldKey+'_', '['+newKey+'_'));
                }
            });
            $messageVariant.find('input[type="radio"][checked="checked"]').each(function(){
                $(this).prop('checked', true);
            });
            $messageVariant.find('[id^=CampaignMessage_'+messageType+'_]').each(function(){
                $(this).attr('id', $(this).attr('id').replace('_'+oldKey+'_', '_'+newKey+'_'));
            });
            $messageVariant.find('[for^=CampaignMessage_'+messageType+'_]').each(function(){
                $(this).attr('for', $(this).attr('for').replace('_'+oldKey+'_', '_'+newKey+'_'));
            });

            var $tabsContainer = $(this);
            $tabsContainer.find('.jsMessageVariantsTabs .jsMessageVariantsTabButton[data-key="'+oldKey+'"]').attr('data-key', newKey).data('key', newKey).val(newKey);
        });
    };

    this.initVariantInfo = function(variantInfo) {
        variantInfo.find('.jsRemoveVariant').click(function() {
            that.removeVariant($(this).parents('.jsVariant').eq(0));
        });

        variantInfo.find('.jsVariantPromotionLoader').comboboxAutocomplete({
            source: '/promotion/promotion/autocomplete?project_id=' + window.xp.project.id,
            change: function() {
                $(this).removeAttr('data-initial');
            },
        });
    };

    this.normalizeVariantKeys = function() {
        var key = 'A';
        $startSection.find('.jsVariants .jsVariant').each(function(){
            that.updateVariantKey($(this), key);
            key = String.fromCharCode(key.charCodeAt(0)+1);
        });
    };

    this.showHideVariantInfoButtons = function() {
        var variantsCount = $startSection.find('.jsVariants .jsVariant').length;
        if (variantsCount <= 1) {
            $startSection.find('.jsVariants .jsRemoveVariant').hide();
        } else {
            $startSection.find('.jsVariants .jsRemoveVariant').show();
        }
        if (variantsCount >= 10) {
            $startSection.find('.jsAddVariant').hide();
        } else {
            $startSection.find('.jsAddVariant').show();
        }
    };


    this.initMultilanguage = function() {
        $startSection.find('.jsMultilanguage').on('select', function(){
            that.enableMultilanguage();
        }).on('deselect', function(){
            that.disableMultilanguage();
        });

        $startSection.find('.jsAddLanguage').click(function(){
            that.addLanguage();
        });

        $startSection.find('.jsLanguages .jsLanguage').each(function(){
            that.initLanguageInfo($(this));
        });
    };

    this.enableMultilanguage = function() {
        $startSection.find('.jsLanguagesContainer').show();

        $campaignPage.find('.jsTranslationsContainer').each(function(){
            $(this).find('.jsTranslationDefault').hide();
            $(this).find('.jsTranslations').show();
        });

        this.updateTranslationLanguageDefault();
    };

    this.disableMultilanguage = function() {
        $startSection.find('.jsLanguagesContainer').hide();

        $campaignPage.find('.jsTranslationsContainer').each(function(){
            $(this).find('.jsTranslationDefault').show();
            $(this).find('.jsTranslations').hide();
        });

        this.updateTranslationLanguageDefault();
    };

    this.addLanguage = function() {
        var tmpLanguageInfo = $startSection.find('.jsLanguageTemplate .jsLanguage');
        var newLanguageInfo = tmpLanguageInfo.clone();
        $startSection.find('.jsLanguages').append(newLanguageInfo);
        this.initLanguageInfo(newLanguageInfo);
        this.normalizeLanguages();
    };

    this.removeLanguage = function(languageInfo) {
        var code = languageInfo.data('code');
        languageInfo.remove();
        this.normalizeLanguages();
        if (code != '-') this.removeTranslation(code);
    };

    this.updateLanguageCode = function(languageInfo, newCode) {
        var oldCode = languageInfo.data('code');
        if (oldCode == newCode) return;

        languageInfo.attr('data-code', newCode).data('code', newCode);
        languageInfo.find('.jsLanguageCode').text(newCode);

        languageInfo.find('[id^=CampaignLanguage_]').each(function(){
            $(this).attr('id', $(this).attr('id').replace('_'+oldCode+'_', '_'+newCode+'_'));
        });
        languageInfo.find('[for^=CampaignLanguage_]').each(function(){
            $(this).attr('for', $(this).attr('for').replace('_'+oldCode+'_', '_'+newCode+'_'));
        });

        languageInfo.find('.jsLanguageDefault').val(newCode);

        var newTitle = languageInfo.find('.jsLanguageSelect option[value="'+newCode+'"]').text();
        if (oldCode != '-') this.removeTranslation(oldCode);
        if (newCode != '-') this.addTranslation(newCode, newTitle);
    };

    this.initLanguageInfo = function(languageInfo) {
        languageInfo.find('.jsRemoveLanguage').click(function(){
            that.removeLanguage($(this).parents('.jsLanguage').eq(0))
        });

        languageInfo.find('.jsLanguageSelect').change(function(){
            that.updateLanguageCode(languageInfo, $(this).val());
            that.normalizeLanguages();
        });

        languageInfo.find('.jsLanguageDefault').change(function(){
            that.updateTranslationLanguageDefault();
        });
    };

    this.normalizeLanguages = function() {
        var languages = [];
        $startSection.find('.jsLanguages .jsLanguage').each(function(){
            languages.push($(this).data('code'));
        });

        $startSection.find('.jsLanguages .jsLanguage').each(function(){
            var language = $(this).find('.jsLanguageSelect').val();
            $(this).find('.jsLanguageSelect option').each(function(){
                var val = $(this).val();
                if (val != language && val != '-' && $.inArray(val, languages) >= 0) {
                    $(this).prop('disabled', true);
                } else {
                    $(this).prop('disabled', false);
                }
            });

            if (language == '-') {
                $(this).find('.jsLanguageDefault').prop('disabled', true).prop('checked', false);
            } else {
                $(this).find('.jsLanguageDefault').prop('disabled', false);
            }
        });

        if (!$startSection.find('.jsLanguages .jsLanguageDefault:checked').length) {
            $startSection.find('.jsLanguages .jsLanguage[data-code!="-"]:first .jsLanguageDefault').prop('checked', true);
        }

        this.updateTranslationLanguageDefault();
    };

    this.addTranslation = function(language, languageTitle) {
        $campaignPage.find('.jsTranslationsContainer').each(function(){
            var tmpTranslation = $(this).find('.jsTranslationTemplate .jsTranslation');
            var newTranslation = tmpTranslation.clone();
            that.updateTranslationLanguage(newTranslation, language, languageTitle);
            $(this).find('.jsTranslations').append(newTranslation);
            newTranslation.trigger('init');
        });
    };

    this.removeTranslation = function(language) {
        $campaignPage.find('.jsTranslationsContainer').each(function(){
            $(this).find('.jsTranslation[data-language="'+language+'"]').remove();
        });
    };

    this.updateTranslationLanguage = function(translation, newLanguage, newLanguageTitle) {
        var oldLanguage = translation.data('language');
        if (oldLanguage == newLanguage) return;

        translation.attr('data-language', newLanguage).data('language', newLanguage);
        translation.find('.jsLanguageTitle').text(newLanguageTitle);
        translation.find('input, textarea, select').each(function(){
            if ($(this).attr('name')) {
                $(this).attr('name', $(this).attr('name').replace('['+oldLanguage+']', '['+newLanguage+']'));
                $(this).attr('name', $(this).attr('name').replace('['+oldLanguage+'_', '['+newLanguage+'_'));
            }
        });
        translation.find('[id^=CampaignMessage_]').each(function(){
            $(this).attr('id', $(this).attr('id').replace('_'+oldLanguage+'_', '_'+newLanguage+'_'));
        });
        translation.find('[for^=CampaignMessage_]').each(function(){
            $(this).attr('for', $(this).attr('for').replace('_'+oldLanguage+'_', '_'+newLanguage+'_'));
        });
    };

    this.updateTranslationLanguageDefault = function() {
        var multiLanguage =  $startSection.find('.jsMultilanguage').is(':checked');
        var defaultCode = $startSection.find('.jsLanguages .jsLanguageDefault:checked').val();

        $campaignPage.find('.jsTranslationsContainer').each(function(){
            $(this).find('.jsTranslation').removeClass('jsTranslationLanguageDefault');
            $(this).find('.jsTranslationLanguageTitleDefault').hide();

            var translation;
            if (!multiLanguage) {
                translation = $(this).find('.jsTranslationDefault .jsTranslation');
            } else if (defaultCode) {
                translation = $(this).find('.jsTranslations .jsTranslation[data-language="'+defaultCode+'"]');
            }
            if (translation) {
                translation.addClass('jsTranslationLanguageDefault');
                translation.find('.jsTranslationLanguageTitleDefault').show();
            }

            $(this).trigger('update');
        });
    };


    this.initContentFetch = function() {
        $startSection.find('.jsContentFetch').on('select', function(){
            that.enableContentFetch();
        }).on('deselect', function(){
            that.disableContentFetch();
        });

        $startSection.find('.jsAddContentFetchSource').click(function(){
            that.addContentFetchSource();
        });

        $startSection.find('.jsContentFetchSources .jsContentFetchSource').each(function(){
            that.initContentFetchSource($(this));
        });
    };

    this.enableContentFetch = function() {
        $startSection.find('.jsContentFetchContainer').show();

        if (!$startSection.find('.jsContentFetchSources .jsContentFetchSource').length) {
            this.addContentFetchSource();
        }
    };

    this.disableContentFetch = function() {
        $startSection.find('.jsContentFetchContainer').hide();
    };

    this.addContentFetchSource = function() {
        var template = $startSection.find('.jsContentFetchSourceTemplate .jsContentFetchSource');
        var source = template.clone();
        $startSection.find('.jsContentFetchSources').append(source);
        this.initContentFetchSource(source);
        this.normalizeContentFetchSources();
    };

    this.removeContentFetchSource = function(source) {
        source.remove();
        this.normalizeContentFetchSources();
    };

    this.initContentFetchSource = function(source) {
        source.find('.jsRemoveContentFetchSource').click(function(){
            that.removeContentFetchSource($(this).parents('.jsContentFetchSource').eq(0))
        });

        source.find('.jsContentFetchSourceUrl').change(function() {
            that.showHideContentFetchSourcePersonalizationHint(source)
        })

        that.showHideContentFetchSourcePersonalizationHint(source)
    };

    this.showHideContentFetchSourcePersonalizationHint = function(source) {
        var url = source.find('.jsContentFetchSourceUrl').val()
        if (RegExp('{{.+}}').test(url)) {
            source.find('.jsContentFetchSourceUrlPersonalizationHint').show()
        } else {
            source.find('.jsContentFetchSourceUrlPersonalizationHint').hide()
        }
    };

    this.normalizeContentFetchSources = function() {
        var index = 0
        $startSection.find('.jsContentFetchSources .jsContentFetchSource').each(function(){
            that.updateContentFetchSourceIndex($(this), index++);
        });
    };

    this.updateContentFetchSourceIndex = function(source, index) {
        source.find('input, select').each(function() {
            if ($(this).attr('name')) {
                $(this).attr('name', $(this).attr('name').replace(/[[0-9-]+]/, '['+index+']'));
            }
        });
    };

    this.serializeContentFetchSources = function() {
        var sources = []

        if ($startSection.find('.jsContentFetch').prop('checked')) {
            $startSection.find('.jsContentFetchSources .jsContentFetchSource').each(function(){
                sources.push({
                    namespace: $(this).find('.jsContentFetchSourceNamespace').val(),
                    url: $(this).find('.jsContentFetchSourceUrl').val(),
                    format: $(this).find('.jsContentFetchSourceFormat').val()
                })
            });
        }

        return sources;
    }

    return this.init();
};
