import ContentEmailColorPicker from './contentEmailColorPicker'

export default function (contentEmailSection, $editorItemContainer, $previewItemContainer) {
    var that = this;

    var $eItem = null,
        $pItem = null,
        $rows = [ ],
        $bgColorPicker = null,
        $buttonsType = null,
        $buttonsSize = null,
        $textAlign = null;

    this.init = function() {
        $eItem = $editorItemContainer;
        $pItem = $previewItemContainer;

        $rows = $eItem.find('.activeRows .rowContainer');
        $rows.each(function(i, row) {
            that.initRow($(row));
        });

        $buttonsType = $eItem.find('.buttonTypeField');
        $buttonsSize = $eItem.find('.buttonSizeField');
        $textAlign = $eItem.find('.textAlignField');
        var $elements = $buttonsType.add($buttonsSize).add($textAlign);
        $elements.change(function() {
            that.renderButtons();
        });

        that.renderButtons();

        var $colorpicker = $eItem.find('input.colorpickerField');
        $bgColorPicker = new ContentEmailColorPicker($colorpicker, $pItem.parent(), 'background');

        $eItem.find('.rowTemplate input, .rowTemplate select').attr('disabled', 'disabled');

        $eItem.find('.jsSocialAddButton').click(function() {
            var $row = $eItem.find('.rowTemplate .rowContainer').clone();
            $eItem.find('.activeRows').append($row);

            that.updateItemsIndexes();
            that.initRow($row);
            that.renderButtons();
        });

        return this;
    };

    this.updateItemsIndexes = function() {
        var $staticIdElem = $eItem.find(".activeRows");
        $staticIdElem.attr("static-id", $staticIdElem.attr("static-id") == undefined
            ? $eItem.find(".activeRows .rowContainer").length - 1
            : parseInt($staticIdElem.attr("static-id")) + 1);
        var staticId = $staticIdElem.attr("static-id");

        $eItem.find('.activeRows input, .activeRows select').each(function() {
            var fieldName = $(this).attr('name');
            fieldName = fieldName.replace(/^(.+\[icons\]\[)(\].+$)/, "$1" + staticId + "$2");
            $(this).attr('name', fieldName);
        });
    };

    this.initRow = function($row) {
        $row.find('.socialSelector, .socialUrl').removeAttr('disabled').change(function() {
            that.renderButtons();
        });

        $row.find('.jsRemoveSocialRowButton').click(function() {
            var $row = $(this).parents('.rowContainer');
            $row.fadeOut(200, function() {
                $row.remove();
                that.renderButtons();
            });
        });
    };

    this.renderButtons = function() {
        $pItem.empty().css('text-align', $textAlign.val())
            .css('padding', ($buttonsSize.val() == '24px') ? '8px' : '16px');

        var colorType = $buttonsType.val();
        $rows = $eItem.find('.activeRows .rowContainer');
        $rows.each(function(i, v) {
            var $row = $(v);
            var socialType = $row.find('.socialSelector').val();
            var icon = $('<img/>').attr('src', '/img/icon/social-' + socialType + '-' + colorType + '.png')
                .css('width', $buttonsSize.val()).css('height', $buttonsSize.val());
            var socialUrl = $row.find('.socialUrl').val();
            var $link = $('<a/>', {
                href: socialUrl,
                target: '_blank',
                style: 'margin: 5px; display: inline-block',
            }).addClass('socialButton ' + socialType).append(icon);

            $pItem.append($link);
        });

        // if update Iframe before all images are loaded, update fails.
        // So we need some trick to handle this situation.
        var $allImages = $pItem.find('img');
        var imgCount = $allImages.length;
        $allImages.each(function(i, element) {
            $(element).load(function() {
                if (--imgCount <= 0) {
                    contentEmailSection.updateIframe();
                }
            });
            if (element.complete) $(element).load();
        });
    };

    this.render = function() {
        $bgColorPicker.render();
        that.renderButtons();
    };

    return this.init();
};
