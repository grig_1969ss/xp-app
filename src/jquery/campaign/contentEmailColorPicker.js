export default function  ($input, $target, property, defaultBorderWidth) {

    var that = this;

    var $caller,
        $container = null;

    this.init = function() {
        $caller = $input;
        $container = $caller.parent();
        $container.find('span.colorpickerField').click(function() {
            $(this).siblings('input.colorpickerField').click();
        });
        $caller.ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
                $caller = $(this);
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(200);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(200);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                that.updateColor(hex);
            }
        }).bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
            that.updateColor();
        }).bind('change', function() {
            if (this.value.search(/^#[0-9a-f]{6}$/i) == -1) {
                $(this).val("");
            }
            $(this).ColorPickerSetColor(this.value);
            that.updateColor();
        });
        that.updateColor();

        return this;
    };

    this.updateColor = function(color) {
        if (color == undefined) {
            color = $caller.val();
        }
        if (color.length && color.indexOf('#') !== 0) {
            color = '#' + color;
        }
        $caller.val(color);
        if ($target != undefined) {
            var $elem;
            if (typeof $target === 'function') {
                $elem = $target();
            } else {
                $elem = $target;
            }
            if (property == 'border-color' && !color.length) {
                $elem.css('border', '0px');
            } else if (defaultBorderWidth != undefined) {
                $elem.css('border', defaultBorderWidth);
            }
            $elem.css(property, color);
        }
        $caller.next().css('background', color.length ? color : '#eeeeee');
        $caller.trigger('colorUpdated');
    };

    this.render = function() {
        that.updateColor();
    };

    this.getElement = function() {
        return $caller;
    };

    return this.init();
};
