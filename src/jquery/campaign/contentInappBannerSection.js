export default function (contentInapp) {
    var that = this;
    var TYPE_BANNER = '0';

    this.initVariant = function(variant) {
        $(variant).find('.bannerDetails select, .bannerDetails input').each(function() {
            var fieldName = $(this).attr('name');
            fieldName = fieldName.replace(/^(.+\[)buttons\]\[button_(\].+$)/, "$1banner$2");
            $(this).attr('name', fieldName);
        });

        $(variant).find('.banner.tab .mainSelect.bannerPosition').change(function() {
            that.updateBannerPreview(variant);
        });

        $(variant).on('change', '.banner.tab .jsCreative .height', function() {
            that.updateBannerPreview(variant);
        });

        $(variant).on('change', '.banner.tab .jsCreative .minWidth', function() {
            that.updateBannerPreview(variant);
        });

        $(variant).find('.banner.tab .jsAddCreative').click(function(e, orientation) {
            orientation = contentInapp.getScreenParams(variant, orientation).or;
            var static_id_elem = $(variant).find(".banner.tab .jsCreatives");
            static_id_elem.attr("static-id", static_id_elem.attr("static-id") == undefined
                ? $(variant).find(".banner.tab .jsCreative").length
                : parseInt(static_id_elem.attr("static-id")) + 1);

            var baseCreativeElement = $(variant).find('.jsCreativeTemplate .jsCreative');
            var newCreativeElement = baseCreativeElement.clone();

            $(variant).find('.banner.tab .jsCreatives .' + orientation).append(newCreativeElement);
            contentInapp.updateItemsIndexes(variant, newCreativeElement);
            contentInapp.initItem(variant, newCreativeElement, orientation);
            that.updateBannerPreview(variant);
        });

        $(variant).on('click', '.banner.tab .jsCreatives .jsRemoveCreative', function() {
            var creative = $(this).closest('.jsCreative');
            var static_id = $(creative).find('.imageFile').attr('static-id');
            $(variant).find('.container:visible .creative.static-id-' + static_id).fadeOut(200, function() {
                $(this).remove();
            });
            creative.fadeOut(200, function() {
                $(this).removeClass('jsItem');
                contentInapp.updateItemsIndexes(variant, creative);
                $(this).remove();
                that.updateBannerPreview(variant);
            });
        });

        return this;
    };

    this.updateBannerPreview = function(variant, filename, creative_id, orientation) {
        if (!$(variant).data('initiated') || $(variant).find('.inappType input:radio:checked').val() != TYPE_BANNER) {
            return false;
        }
        var params = contentInapp.getScreenParams(variant, orientation);
        orientation = params.or;
        if (orientation == "" || orientation == undefined) {
            return false;
        }
        var screen_width = params.width;
        var screen_height = params.height;
        var container = $(variant).find('.inappPreview .outerContainer.' + orientation + ' .container');
        container.find('.button').hide();
        container.find('.editorContainer').hide();
        container.find('iframe').hide();
        container.find('img.background').show();
        var bg = container.find('img.background');
        if (filename != undefined && filename != "") {
            var onload = function(_bg, ori) {
                return function() {
                    container.css('background', 'white');
                    container.find('img.watermark').hide();
                    container.find('.dz-preview').hide();
                    
                    $(_bg).css("width", "auto").css("height", "auto");
                    
                    // trick to get sizes of image in hidden div
                    _bg = $("<img src=\"" + filename + "\" />");
                    $("body").append(_bg); _bg.css("opacity", "0.01");

                    $(variant).find(".banner.tab .jsCreatives ."
                        + ori + " .jsCreative .imageFile[static-id=" + creative_id + "]")
                        .attr('original-width', _bg.width())
                        .attr('original-height', _bg.height())
                        .val(filename);
                    _bg.hide();

                    that.updateBannerPreview(variant, undefined, undefined, ori);
                }
            };
            bg.off("load").one("load", onload(bg, orientation)).each(function() {
                if (this.complete) $(this).load();
            });
            
            return false;
        }
        // get creative, respective to the current screen width
        var creative = that.getRespectiveCreative(variant);
        var input = $(creative).find(".imageFile");
        var filename = input.val();
        if (!creative || filename == undefined || filename == "") {
            bg.hide();
            container.css('background', 'white');
            container.find('img.watermark').hide();
            if (creative) {
                that.showBannerPlaceholder(variant, orientation);
            }
            return false;
        }
        var creative_height = $(creative).find(".height").val() || 0;
        creative_height = Math.round(creative_height * params.width / params.original_width);

        $(container).height(creative_height).css('min-height', '0px').hide();
        bg.height(creative_height).css("width", "auto").off("load").attr("src", input.val()).one('load', function() {
            bg.fadeIn(200);
            setTimeout(function() {
                $(container).show();
                $(container).width(Math.min(bg.width(), screen_width))
                        .height(Math.min(creative_height, screen_height));
                that.placeBannerContainer(variant, container, orientation);
                
                var xdiff = Math.round(Math.max(0, (bg.width() - $(container).width()) / 2));
                var ydiff = Math.round(Math.max(0, (creative_height - $(container).height()) / 2));
                
                $(bg).css("left", -xdiff + "px").css("top", -ydiff + "px").css("position", "relative");
                input.attr('displayed-width', bg.width())
                    .attr('displayed-height', creative_height);
            }, 200);
        }).each(function() {
            if (this.complete) $(this).load();
        });
        
        $(container).find("span").hide();
        $(container).find(".watermark").hide();
        $(container).css("background", "white");
    };
    
    this.showBannerPlaceholder = function(variant, orientation) {
        var screen_width = contentInapp.getScreenParams(variant, orientation).width;
        var banner_height = 50; // default value
        
        // first of all we look for desired cretive
        var creative = that.getRespectiveCreative(variant);
        if (creative) {
            // height should be equal or more than 15 px and less or equal to 200 px
            banner_height = Math.min(200, Math.max(0, 
                    parseInt($(creative).find('.height').val()) || 0));
        }
        var container = $(variant).find('.inappPreview .outerContainer.' + orientation + ' .container');
        container.css('background', '#1D87C8').css("width", screen_width)
                .css("height", banner_height + "px").css("min-height", "0px");
        container.find('img.background').hide();
        container.find('img.watermark').css('height', Math.round(banner_height / 2) + 'px')
                .css('width', 'auto').show();
        container.find('span').show();
        
        that.placeBannerContainer(variant, undefined, orientation);
    };
    
    this.placeBannerContainer = function(variant, container, orientation) {
        var params = contentInapp.getScreenParams(variant, orientation);
        orientation = params.or;
        var screen_width = params.width;
        var screen_height = params.height;
        
        if (container == undefined) {
            container = $(variant).find('.inappPreview .outerContainer.' + orientation + ' .container');
        }
        var banner_position_selector = $(variant).find(".banner.tab .mainSelect.bannerPosition");
        var banner_position = banner_position_selector.val();
        
        if (orientation == 'portrait') {
            switch (banner_position) {
                case "top" :
                    $(container).css("top", "0px").css("bottom", "auto").css("left", "0px").css("right", "0px")
                            .css("margin", "0 auto");
                    break;
                case "center" :
                    $(container).css("top", "0px").css("bottom", "0px").css("left", "0px").css("right", "0px")
                            .css("margin", "auto");
                    break;
                case "bottom" :
                    $(container).css("top", "auto").css("bottom", "0px").css("left", "0px").css("right", "0px")
                            .css("margin", "0 auto");
                    break;
            }
        } else {
            switch (banner_position) {
                case "top" :
                    var xdiff = 0;
                    var ydiff = Math.min(-screen_height + $(container).height(),
                            -screen_height + $(container).height() - ($(container).width() - screen_height)) / 2;
                    $(container).css("top", "0px").css("bottom", "0px")
                            .css("left", ydiff + "px").css("right", -ydiff + "px")
                            .css("margin", "auto");
                    break;
                case "center" :
                    var ydiff = Math.max(0, ($(container).width() - screen_height) / 2);
                    $(container).css("top", "0px").css("bottom", "0px")
                            .css("left", -ydiff + "px").css("right", ydiff + "0px")
                            .css("margin", "auto");
                    break;
                case "bottom" :
                    var ydiff = (screen_height - $(container).height()) / 2 
                            - Math.max(0, ($(container).width() - screen_height) / 2);
                    $(container).css("top", "0px").css("bottom", "0px")
                            .css("left", ydiff + "px").css("right", -ydiff + "px")
                            .css("margin", "auto");
                    break;
            }
        } 
    }
    
    this.getRespectiveCreative = function(variant) {
        var params = contentInapp.getScreenParams(variant);
        var orientation = params.or;
        var screen_width = parseInt(params.original_width);
        var screen_height = parseInt(params.original_height);
        
        var creatives = $(variant).find(".banner.tab .jsCreatives ." + orientation + " .jsCreative");
        var result = false;
        var min = Number.MAX_VALUE;
        creatives.each(function(i, creative) {
            var filename = $(creative).find('.imageFile').val();
            var width = parseInt($(creative).find('.minWidth').val()) || 0;
            var height = parseInt($(creative).find('.height').val()) || 0;
            // we are looking for creative, which has uploaded image file and
            // with Min Width field value, closest to current screen width, 
            // but less that screen width
            if ( // filename != undefined && filename != "" && 
                    width >= 0 && height >= 0
                    && width <= screen_width && screen_width - width < min) {
                min = screen_width - width;
                result = creative;
            }
        });
        
        return result;
    };

    return this;
};
