import StartSection from './startSection'
import ContentPushSection from './contentPushSection'
import ContentInboxSection from './contentInboxSection'
import ContentInappSection from './contentInappSection'
import ContentSmsSection from './contentSmsSection'
import ContentFacebookSection from './contentFacebookSection'
import ContentWhatsappSection from './contentWhatsappSection'
import ContentEmailSection from './contentEmailSection'
import ContentWebhookSection from './contentWebhookSection'
import ContentOnsiteSection from './contentOnsiteSection'
import CheckFormChanges  from '@/jquery/widgets/checkFormChanges'
import { twig } from 'vendor/twig/twig'

export default function () {

    var $campaignPageContent,
        $campaignForm,
        $templatesPage,
        $previewCampaignModal,
        twPreviewModalWindowsContent
            = null;

    var that = this;

    this.init = function () {
        $campaignPageContent            = $('#createCampaingContentBlock');
        $templatesPage                  = $('#templatesPage');
        $previewCampaignModal           = $('#pushPreviewModal');
        $campaignForm                   = $campaignPageContent.find('> form.createCampaignForm');

        twPreviewModalWindowsContent    = twig({
            id: "previewModalContent",
            href: VIEWS_DIR+"/push/views/campaign/templates/preview-template.twig",
            async: false
        });

        if (!$campaignPageContent.data('manage')) {
            $campaignPageContent.addClass("viewMode");
            $campaignPageContent.find('input, select, textarea').prop('disabled', true).addClass('viewable');
            $campaignPageContent.find('.tabContentTabs input').prop('disabled', false).removeClass('viewable');
        }

        this.initControlButton();
        this.initTemplateImportLoader();

        this.startSection           = new StartSection(this);
        this.contentIosSection      = new ContentPushSection(this, 'ios');
        this.contentAndroidSection  = new ContentPushSection(this, 'android');
        this.contentWebpushSection  = new ContentPushSection(this, 'webpush');
        this.contentInboxSection    = new ContentInboxSection(this, 'inbox');
        this.contentInappSection    = new ContentInappSection(this);
        this.contentSmsSection      = new ContentSmsSection(this);
        this.contentFacebookSection = new ContentFacebookSection(this);
        this.contentWhatsappSection = new ContentWhatsappSection(this);
        this.contentEmailSection    = new ContentEmailSection(this);
        this.contentWebhookSection  = new ContentWebhookSection(this);
        this.contentOnsiteSection   = new ContentOnsiteSection(this);
        this.checkFormChanges       = new CheckFormChanges($campaignForm);

        if ($campaignPageContent.data('copy')) {
            this.checkFormChanges.initialChange = true;
        }

        for (var i in this) {
            if (i.indexOf('Section') >= 0 && typeof this[i] == 'object' && !Object.keys(this[i]).length) {
                this[i] = null;
            }
        }
    };

    this.initControlButton = function() {
        $campaignForm.find('.jsSave').click(function(e){
            that.submit();
        });
    };

    this.submit = function() {
        var options = {
            url:        API_URL + '/push/campaign-template/validate?project_id=' + window.xp.project.id,
            success:    function(data) {
                if (Object.keys(data.model.errors).length > 0) {
                    var template = twPreviewModalWindowsContent.render({
                        model: data.model
                    });
                    $previewCampaignModal.find('.modalContent').html(template);
                    $previewCampaignModal.bPopup({
                      appendTo: '#popupsContainer',
                      opacity: 0.9
                    });
                } else {
                    $campaignForm.submit();
                }
            }
        };
        $campaignForm.ajaxSubmit(options);
    };

    this.getLayout = function() {
        return $campaignPageContent;
    };

    this.initTemplateImportLoader = function() {
        $templatesPage.find('.jsTemplateImport').comboboxAutocomplete({
            source: '/push/campaign-template/autocomplete-for-import?project_id=' + window.xp.project.id,
            select: function(event, ui) {
                $templatesPage.find('.jsTemplateImport').blur();
                if (ui.item.id && ui.item.project_id) {
                    window.location.assign(
                      '/push/campaign-template/import?id=' + ui.item.id
                      + '&project_id=' + window.xp.project.id
                      + '&from_project_id=' + ui.item.project_id,
                    );
                }
            },
            response: function(event, ui) {
                if (!ui.content.length) {
                    ui.content.push({value: '', label: t('all', 'PushCampaign__Not_found')});
                }
            },
        });
    };
};
