export default function (contentInapp) {
    var that = this;

    this.initVariant = function(variant) {
        $(variant).find('.customHtmlContent').change(function() {
            that.updateCustomPreview(variant);
        });

        return this;
    };

    this.updateCustomPreview = function(variant, orientation) {
        console.log('Update custom HTML preview');
        var params = contentInapp.getScreenParams(variant, orientation);
        orientation = params.or;
        var screen_width = params.width;
        var screen_height = params.height;
        if (orientation == "" || orientation == undefined) return;

        var container = $(variant).find('.inappPreview .outerContainer.' + orientation + ' .container');
        var html_text = $(variant).find('.inappControls .customHtmlContent');
        var iframe = container.find('iframe');

        container.css('background', 'white');
        container.find('img.watermark').hide();
        container.find('.editorContainer').hide();
        container.find('span').hide();
        container.find('.background').hide();
        container.find('.button').hide();

        try {
            $(iframe).width(screen_width).height(screen_height).css("border", "none");
            var iFrameDoc = iframe[0].contentDocument || iframe[0].contentWindow.document;
            iFrameDoc.write(html_text.val());
            iFrameDoc.close();
            $(iframe).show();
        } catch(e) {}

        $(container).css("width", screen_width + "px").css("height", screen_height + "px")
            .css("top", "0px").css("left", "0px").css("right", "auto").css("bottom", "auto");

        if (orientation == 'landscape') {
            var xdiff = (screen_width - screen_height) / 2;
            $(container).css("top", "0px").css("bottom", "0px")
                .css("left", -xdiff + "px").css("right", xdiff + "px");
        }
    };

    return this;
};
