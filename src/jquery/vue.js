import Vue from 'vue'

(function ($) {
  $.fn.vue = function () {
    let ref = null

    this.each(function () {
      const Connector = Vue.extend(Vue.component($(this).data('vue')))

      ref = new Connector({
        el: this,
        propsData: $(this).data()
      })
    })

    return ref
  }

  $(window).on('vue-ready', function () {
    $('div[data-vue]').each(function () {
      if ($(this).data('vue-autoinit') === undefined || $(this).data('vue-autoinit')) {
        $(this).vue()
      }
    })
  })
}(jQuery));
