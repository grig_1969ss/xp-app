import ContentEditor from '@/jquery/widgets/contentEditor'

(function ($) {
  $.Redactor.prototype.snippet = function () {
    return {
      init: function () {
        var $button = this.button.add('snippet', "Snippets");
        this.button.setIcon($button, '<i class="icon icon-code"></i>');
        new ContentEditor(
          this.$box, {
            wysiwyg: 'redactor',
            snippets: true,
            snippetButton: $button
          });
      }
    };
  };
})(jQuery);
