(function ($) {
  $.Redactor.prototype.cleanformat = function () {
    return {
      init: function () {
        var $button = this.button.addAfter('html', 'cleanformat', "Clean Format");
        this.button.setIcon($button, '<i class="icon icon-block"></i>');
        this.button.addCallback($button, 'inline.removeFormat');
      }
    };
  };
})(jQuery);