(function ($) {
  $.Redactor.prototype.lineheight = function () {
    return {
      init: function () {
        var values = {
          1: 'Normal',
          1.2: 'Slight',
          1.5: 'Medium',
          2: 'Double'
        };
        var that = this;
        var dropdown = {};

        $.each(values, function (i, s) {
          dropdown['l' + i] = {
            title: s, func: function () {
              that.lineheight.set(i);
            }
          };
        });

        dropdown.remove = { title: 'Remove Line Height', func: that.lineheight.reset };

        var $button = this.button.add('lineheight', 'Line Height');
        this.button.setIcon($button, '<i class="re-icon-fontsize"></i>');
        this.button.addDropdown($button, dropdown);
      },
      set: function (size) {
        this.inline.format('span', 'style', 'line-height: ' + size + ';');
      },
      reset: function () {
        this.inline.removeStyleRule('line-height');
      }
    };
  };
})(jQuery);