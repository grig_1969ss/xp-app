(function ($) {
  $.Redactor.prototype.fontcolor = function () {
    return {
      init: function () {
        var self = this;

        var $button1 = this.button.add(name, 'Text Color');
        this.button.setIcon($button1, '<i class="re-icon-fontcolor"></i>');
        this.fontcolor.buildPicker($button1, 'color');

        var $button2 = this.button.add(name, 'Highlight Color');
        this.button.setIcon($button2, '<i class="re-icon-highlightcolor"></i>');
        this.fontcolor.buildPicker($button2, 'background-color');

        this.button.addCallback($button1, function () {
          self.selection.save();
        });

        this.button.addCallback($button2, function () {
          self.selection.save();
        });
      },
      set: function (rule, type) {
        this.inline.format('span', 'style', rule + ': ' + type + ';');
      },
      remove: function (rule) {
        this.inline.removeStyleRule(rule);
      },
      buildPicker: function (button, rule) {
        var self = this;
        var $colorpicker = null;
        button.ColorPicker({
          color: (rule == 'background-color') ? '#FFFFFF' : '#000000',
          onShow: function (colpkr) {
            $colorpicker = $(colpkr);
            $(colpkr).fadeIn(200);
            setTimeout(function () {
              self.selection.restore();
              self.selection.save();
            }, 0);
            return false;
          },
          onHide: function (colpkr) {
            $(colpkr).fadeOut(200);
            setTimeout(function () {
              self.selection.restore();
              self.code.sync();
            }, 0);
            return false;
          },
          onChangeConfirm: function () {
            setTimeout(function () {
              self.selection.restore();
              self.fontcolor.set(rule, $colorpicker.find('.colorpicker_new_color').css('background-color'));
              self.selection.save();
              self.$editor.blur();
            }, 0);
          }
        });
      }
    };
  };
})(jQuery);