import ContentEditor from '@/jquery/widgets/contentEditor'

(function ($) {
  $.Redactor.prototype.personalization = function () {
    return {
      init: function () {
        var $button = this.button.add('personalization', "Personalization");
        this.button.setIcon($button, '<i class="icon iconp-user-1"></i>');
        new ContentEditor(
          this.$box, {
            wysiwyg: 'redactor',
            personalization: true,
            personalizationButton: $button
          });
      }
    };
  };
})(jQuery);
