import ContentEditor from '@/jquery/widgets/contentEditor'

(function ($) {
    $.Redactor.prototype.optimove = function () {
        return {
            init: function () {
                var $button = this.button.addAfter('optimove', "Optimove");
                this.button.setIcon($button, '<i class="icon iconp-Optimove-Head"></i>');
                new ContentEditor(
                    this.$box, {
                        wysiwyg: 'redactor',
                        optimove: true,
                        optimoveButton: $button
                    });
            }
        };
    };
})(jQuery);
