(function ($) {
  $.Redactor.prototype.alignment = function () {
    return {
      langs: {
        en: {
          "align": "Align",
          "align-left": "Align Left",
          "align-center": "Align Center",
          "align-right": "Align Right",
          "align-justify": "Align Justify",
          "align-remove": "Remove Alignment",
        }
      },
      init: function () {
        var that = this;
        var dropdown = {};

        dropdown.left = { title: '<i class="re-icon-alignleft"></i>' + that.lang.get('align-left'), func: that.alignment.setLeft };
        dropdown.center = { title: '<i class="re-icon-aligncenter"></i>' + that.lang.get('align-center'), func: that.alignment.setCenter };
        dropdown.right = { title: '<i class="re-icon-alignright"></i>' + that.lang.get('align-right'), func: that.alignment.setRight };
        dropdown.justify = { title: '<i class="re-icon-alignjustify"></i>' + that.lang.get('align-justify'), func: that.alignment.setJustify };
        dropdown.remove = { title: '<i class="icon icon-block"></i>' + that.lang.get('align-remove'), func: that.alignment.removeAlign };

        var $button = this.button.addAfter('link', 'alignment', this.lang.get('align'));
        this.button.setIcon($button, '<i class="re-icon-alignment"></i>');
        this.button.addDropdown($button, dropdown);
      },
      removeAlign: function () {
        this.inline.removeStyle('text-align: none;', this.selection.block());
      },
      setLeft: function () {
        this.buffer.set();
        this.inline.addStyle('text-align: left;', this.selection.block());
      },
      setCenter: function () {
        this.buffer.set();
        this.inline.addStyle('text-align: center;', this.selection.block());
      },
      setRight: function () {
        this.buffer.set();
        this.inline.addStyle('text-align: right;', this.selection.block());
      },
      setJustify: function () {
        this.buffer.set();
        this.inline.addStyle('text-align: justify;', this.selection.block());
      }
    };
  };
})(jQuery);