import ContentEditor from '@/jquery/widgets/contentEditor'

(function ($) {
  $.Redactor.prototype.emojis = function () {
    return {
      init: function () {
        var $button = this.button.add('emojis', "Emoticons");
        this.button.setIcon($button, '<span style="font-size: 16px; line-height: 13px">☺</span>');
        new ContentEditor(
          this.$box, {
            wysiwyg: 'redactor',
            emojis: true,
            emojisButton: $button
          });
      }
    };
  };
})(jQuery);
