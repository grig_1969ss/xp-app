(function ($) {
  $.Redactor.prototype.fontfamily = function () {
    return {
      init: function () {
        var fonts = {
          'Arial': "Arial, Helvetica, sans-serif",
          'Comic Sans': "Comic Sans MS, cursive, sans-serif",
          'Courier New': "Courier New, Courier, monospace",
          'Georgia': "Georgia, serif",
          'Helvetica': "Helvetica Neue, Helvetica, Arial, sans-serif",
          'Lucida': "Lucida Sans Unicode, Lucida Grande, sans-serif",
          'Tahoma': "Tahoma, Geneva, sans-serif",
          'Times New Roman': "Times New Roman, Times, serif",
          'Trebuchet MS': "Trebuchet MS, Helvetica, sans-serif",
          'Verdana': "Verdana, Geneva, sans-serif"
        };

        var that = this;
        var dropdown = {};

        $.each(fonts, function (i, s) {
          dropdown['Font ' + i] = {
            title: s, func: function () {
              that.fontfamily.set(s);
            }
          };
        });

        dropdown.remove = { title: 'Remove Font Family', func: that.fontfamily.reset };

        var $button = this.button.add('fontfamily', 'Font');
        this.button.setIcon($button, '<i class="re-icon-fontfamily"></i>');
        this.button.addDropdown($button, dropdown);

      },
      set: function (value) {
        this.inline.format('span', 'style', 'font-family:' + value + ';');
      },
      reset: function () {
        this.inline.removeStyleRule('font-family');
      }
    };
  };
})(jQuery);