import GoogleMapsApiLoader from 'google-maps-api-loader'

var google
var googleLoader

export default function load (callback, settings) {
  if (!google) {
    if (googleLoader) {
      googleLoader.then(function() {
        callback(google)
      })
    } else {
      settings = settings || {}
      settings.apiKey = 'AIzaSyB6QBqyYAcRdpTR89iEI3ObaxOuQXmxB80'

      googleLoader = GoogleMapsApiLoader(settings || {}).then(function (googleApi) {
        google = googleApi
        googleLoader = undefined
        callback(google)
      })
    }
  } else {
    callback(google)
  }
}
