import ContentEditor from '@/jquery/widgets/contentEditor'

export default {
  init: function( editor ) {
    editor.ui.addButton( 'Snippets', {
      label: 'Snippet',
      toolbar: 'insert',
      icon: '/img/code-icon.png'
    });

     editor.on('uiSpace',function(uiSpace){
       if (uiSpace.data.space == 'top') {
         new ContentEditor(
           $(editor.element.$), {
             wysiwyg: 'ckeditor',
             ckeditor: editor,
             snippets: true,
             snippetButton: $(editor.element.$).find('.cke_button__snippets'),
           });
       }
     });
  },
}
