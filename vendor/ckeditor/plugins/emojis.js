import ContentEditor from '@/jquery/widgets/contentEditor'

export default {
  init: function( editor ) {
    editor.ui.addButton( 'Emojis', {
      label: 'Emoji',
      toolbar: 'insert',
      icon: '/img/emoji-icon.png'
    });

    editor.on('uiSpace',function(uiSpace){
      if (uiSpace.data.space == 'top') {
        new ContentEditor(
          $(editor.element.$), {
            wysiwyg: 'ckeditor',
            ckeditor: editor,
            emojis: true,
            emojisButton: $(editor.element.$).find('.cke_button__emojis'),
          });
      }
    });
  },
}
