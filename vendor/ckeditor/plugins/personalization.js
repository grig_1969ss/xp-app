import ContentEditor from '@/jquery/widgets/contentEditor'

export default {
  init: function( editor ) {
    editor.ui.addButton( 'Personalization', {
      label: 'Personalization',
      toolbar: 'insert',
      icon: '/img/person-icon.png'
    });

    editor.on('uiSpace',function(uiSpace){
      if (uiSpace.data.space == 'top') {
        new ContentEditor(
          $(editor.element.$), {
            wysiwyg: 'ckeditor',
            ckeditor: editor,
            personalization: true,
            personalizationButton: $(editor.element.$).find('.cke_button__personalization'),
          });
      }
    });
  },
}
