import loadScript from 'load-script'

var loader

export default {
  load: function () {
    if (!loader) {
      loader = new Promise(function (resolve, reject) {
        loadScript('https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js', function (err) {
          if (err) reject(err)
          resolve(window.CKEDITOR)
        })
      })
    }

    return loader
  }
}
