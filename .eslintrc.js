// https://eslint.org/docs/user-guide/configuring
module.exports = {
  root: true,
  parser: "vue-eslint-parser",
  parserOptions: {
    parser: "babel-eslint",
    sourceType: "module"
  },
  env: {
    browser: true,
    jquery: true
  },
  extends: [
    "plugin:prettier/recommended",
    "plugin:vue/recommended",
    "prettier/vue",
    "standard",
  ],
  plugins: ["vue"],
  rules: {
    "no-debugger": process.env.NODE_ENV === "production" ? 2 : 0,
    "space-before-function-paren": "off",

    "vue/require-default-prop": "off",
    "vue/require-prop-types": "off",
    "vue/attributes-order": "off",
    "vue/order-in-components": "off",

    // TODO: ELIMINATE FOLLOWING WARNINGS
    "vue/no-v-html": "off",
    "vue/require-v-for-key": "off",
    "vue/no-use-v-if-with-v-for": "off",
    "vue/return-in-computed-property": "off",
    "vue/require-valid-default-prop": "off",
    "vue/no-side-effects-in-computed-properties": "off",
    "vue/no-duplicate-attributes": "off",
    "vue/no-async-in-computed-properties": "off",
  }
};
